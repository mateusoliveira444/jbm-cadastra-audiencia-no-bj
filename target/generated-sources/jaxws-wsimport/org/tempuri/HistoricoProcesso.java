
package org.tempuri;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java de HistoricoProcesso complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="HistoricoProcesso">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ProcessoUnico" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="StatusProcessoAtual" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Historicos" type="{http://tempuri.org/}Historicos" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "HistoricoProcesso", propOrder = {
    "processoUnico",
    "statusProcessoAtual",
    "historicos"
})
public class HistoricoProcesso {

    @XmlElement(name = "ProcessoUnico")
    protected String processoUnico;
    @XmlElement(name = "StatusProcessoAtual")
    protected String statusProcessoAtual;
    @XmlElement(name = "Historicos")
    protected Historicos historicos;

    /**
     * Obtém o valor da propriedade processoUnico.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProcessoUnico() {
        return processoUnico;
    }

    /**
     * Define o valor da propriedade processoUnico.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProcessoUnico(String value) {
        this.processoUnico = value;
    }

    /**
     * Obtém o valor da propriedade statusProcessoAtual.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStatusProcessoAtual() {
        return statusProcessoAtual;
    }

    /**
     * Define o valor da propriedade statusProcessoAtual.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStatusProcessoAtual(String value) {
        this.statusProcessoAtual = value;
    }

    /**
     * Obtém o valor da propriedade historicos.
     * 
     * @return
     *     possible object is
     *     {@link Historicos }
     *     
     */
    public Historicos getHistoricos() {
        return historicos;
    }

    /**
     * Define o valor da propriedade historicos.
     * 
     * @param value
     *     allowed object is
     *     {@link Historicos }
     *     
     */
    public void setHistoricos(Historicos value) {
        this.historicos = value;
    }

}
