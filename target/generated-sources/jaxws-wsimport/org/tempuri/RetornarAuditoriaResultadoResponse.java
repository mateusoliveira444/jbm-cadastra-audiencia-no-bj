
package org.tempuri;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java de anonymous complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="RetornarAuditoriaResultadoResult" type="{http://tempuri.org/}ArrayOfResultadoRetornaAuditoriaResultado" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "retornarAuditoriaResultadoResult"
})
@XmlRootElement(name = "RetornarAuditoriaResultadoResponse")
public class RetornarAuditoriaResultadoResponse {

    @XmlElement(name = "RetornarAuditoriaResultadoResult")
    protected ArrayOfResultadoRetornaAuditoriaResultado retornarAuditoriaResultadoResult;

    /**
     * Obtém o valor da propriedade retornarAuditoriaResultadoResult.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfResultadoRetornaAuditoriaResultado }
     *     
     */
    public ArrayOfResultadoRetornaAuditoriaResultado getRetornarAuditoriaResultadoResult() {
        return retornarAuditoriaResultadoResult;
    }

    /**
     * Define o valor da propriedade retornarAuditoriaResultadoResult.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfResultadoRetornaAuditoriaResultado }
     *     
     */
    public void setRetornarAuditoriaResultadoResult(ArrayOfResultadoRetornaAuditoriaResultado value) {
        this.retornarAuditoriaResultadoResult = value;
    }

}
