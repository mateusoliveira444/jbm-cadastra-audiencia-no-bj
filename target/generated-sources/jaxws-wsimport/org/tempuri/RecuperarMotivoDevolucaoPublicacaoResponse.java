
package org.tempuri;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java de anonymous complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="RecuperarMotivoDevolucaoPublicacaoResult" type="{http://tempuri.org/}ArrayOfMotivoPublicacao" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "recuperarMotivoDevolucaoPublicacaoResult"
})
@XmlRootElement(name = "RecuperarMotivoDevolucaoPublicacaoResponse")
public class RecuperarMotivoDevolucaoPublicacaoResponse {

    @XmlElement(name = "RecuperarMotivoDevolucaoPublicacaoResult")
    protected ArrayOfMotivoPublicacao recuperarMotivoDevolucaoPublicacaoResult;

    /**
     * Obtém o valor da propriedade recuperarMotivoDevolucaoPublicacaoResult.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfMotivoPublicacao }
     *     
     */
    public ArrayOfMotivoPublicacao getRecuperarMotivoDevolucaoPublicacaoResult() {
        return recuperarMotivoDevolucaoPublicacaoResult;
    }

    /**
     * Define o valor da propriedade recuperarMotivoDevolucaoPublicacaoResult.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfMotivoPublicacao }
     *     
     */
    public void setRecuperarMotivoDevolucaoPublicacaoResult(ArrayOfMotivoPublicacao value) {
        this.recuperarMotivoDevolucaoPublicacaoResult = value;
    }

}
