
package org.tempuri;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java de anonymous complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="RecuperarAcaoResult" type="{http://tempuri.org/}ArrayOfAcao" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "recuperarAcaoResult"
})
@XmlRootElement(name = "RecuperarAcaoResponse")
public class RecuperarAcaoResponse {

    @XmlElement(name = "RecuperarAcaoResult")
    protected ArrayOfAcao recuperarAcaoResult;

    /**
     * Obtém o valor da propriedade recuperarAcaoResult.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfAcao }
     *     
     */
    public ArrayOfAcao getRecuperarAcaoResult() {
        return recuperarAcaoResult;
    }

    /**
     * Define o valor da propriedade recuperarAcaoResult.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfAcao }
     *     
     */
    public void setRecuperarAcaoResult(ArrayOfAcao value) {
        this.recuperarAcaoResult = value;
    }

}
