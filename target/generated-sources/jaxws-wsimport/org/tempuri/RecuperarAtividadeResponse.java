
package org.tempuri;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java de anonymous complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="RecuperarAtividadeResult" type="{http://tempuri.org/}ArrayOfAtividade" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "recuperarAtividadeResult"
})
@XmlRootElement(name = "RecuperarAtividadeResponse")
public class RecuperarAtividadeResponse {

    @XmlElement(name = "RecuperarAtividadeResult")
    protected ArrayOfAtividade recuperarAtividadeResult;

    /**
     * Obtém o valor da propriedade recuperarAtividadeResult.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfAtividade }
     *     
     */
    public ArrayOfAtividade getRecuperarAtividadeResult() {
        return recuperarAtividadeResult;
    }

    /**
     * Define o valor da propriedade recuperarAtividadeResult.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfAtividade }
     *     
     */
    public void setRecuperarAtividadeResult(ArrayOfAtividade value) {
        this.recuperarAtividadeResult = value;
    }

}
