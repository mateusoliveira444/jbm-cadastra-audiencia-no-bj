
package org.tempuri;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java de anonymous complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="lstrIDProcessoUnico" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="lintIdTipoParte" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="lstrNomeDaParte" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="lstrUsuario" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="lstrSenha" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "lstrIDProcessoUnico",
    "lintIdTipoParte",
    "lstrNomeDaParte",
    "lstrUsuario",
    "lstrSenha"
})
@XmlRootElement(name = "InserirParteProcesso")
public class InserirParteProcesso {

    protected String lstrIDProcessoUnico;
    protected int lintIdTipoParte;
    protected String lstrNomeDaParte;
    protected String lstrUsuario;
    protected String lstrSenha;

    /**
     * Obtém o valor da propriedade lstrIDProcessoUnico.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLstrIDProcessoUnico() {
        return lstrIDProcessoUnico;
    }

    /**
     * Define o valor da propriedade lstrIDProcessoUnico.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLstrIDProcessoUnico(String value) {
        this.lstrIDProcessoUnico = value;
    }

    /**
     * Obtém o valor da propriedade lintIdTipoParte.
     * 
     */
    public int getLintIdTipoParte() {
        return lintIdTipoParte;
    }

    /**
     * Define o valor da propriedade lintIdTipoParte.
     * 
     */
    public void setLintIdTipoParte(int value) {
        this.lintIdTipoParte = value;
    }

    /**
     * Obtém o valor da propriedade lstrNomeDaParte.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLstrNomeDaParte() {
        return lstrNomeDaParte;
    }

    /**
     * Define o valor da propriedade lstrNomeDaParte.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLstrNomeDaParte(String value) {
        this.lstrNomeDaParte = value;
    }

    /**
     * Obtém o valor da propriedade lstrUsuario.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLstrUsuario() {
        return lstrUsuario;
    }

    /**
     * Define o valor da propriedade lstrUsuario.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLstrUsuario(String value) {
        this.lstrUsuario = value;
    }

    /**
     * Obtém o valor da propriedade lstrSenha.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLstrSenha() {
        return lstrSenha;
    }

    /**
     * Define o valor da propriedade lstrSenha.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLstrSenha(String value) {
        this.lstrSenha = value;
    }

}
