
package org.tempuri;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java de anonymous complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ConsultaCNPJCustaResult" type="{http://tempuri.org/}ArrayOfCelulaCnpj" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "consultaCNPJCustaResult"
})
@XmlRootElement(name = "ConsultaCNPJCustaResponse")
public class ConsultaCNPJCustaResponse {

    @XmlElement(name = "ConsultaCNPJCustaResult")
    protected ArrayOfCelulaCnpj consultaCNPJCustaResult;

    /**
     * Obtém o valor da propriedade consultaCNPJCustaResult.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfCelulaCnpj }
     *     
     */
    public ArrayOfCelulaCnpj getConsultaCNPJCustaResult() {
        return consultaCNPJCustaResult;
    }

    /**
     * Define o valor da propriedade consultaCNPJCustaResult.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfCelulaCnpj }
     *     
     */
    public void setConsultaCNPJCustaResult(ArrayOfCelulaCnpj value) {
        this.consultaCNPJCustaResult = value;
    }

}
