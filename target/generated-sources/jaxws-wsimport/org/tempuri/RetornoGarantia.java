
package org.tempuri;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java de RetornoGarantia complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="RetornoGarantia">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Processo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="TipoDescricao" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Valor" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DTGarantia" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Memo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Levantado" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DTLevantado" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="IdGarantia" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Marca" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Modelo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Chassi" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="AnoFabricacao" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="AnoModelo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Cor" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Placa" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Renavam" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="TipoVeiculo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="TipoCombustivel" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="EstadoConservacao" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CodMolicar" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="VlrMolicar" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Serie" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DescricaoMotor" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DtCadastro" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "RetornoGarantia", propOrder = {
    "processo",
    "tipoDescricao",
    "valor",
    "dtGarantia",
    "memo",
    "levantado",
    "dtLevantado",
    "idGarantia",
    "marca",
    "modelo",
    "chassi",
    "anoFabricacao",
    "anoModelo",
    "cor",
    "placa",
    "renavam",
    "tipoVeiculo",
    "tipoCombustivel",
    "estadoConservacao",
    "codMolicar",
    "vlrMolicar",
    "serie",
    "descricaoMotor",
    "dtCadastro"
})
public class RetornoGarantia {

    @XmlElement(name = "Processo")
    protected String processo;
    @XmlElement(name = "TipoDescricao")
    protected String tipoDescricao;
    @XmlElement(name = "Valor")
    protected String valor;
    @XmlElement(name = "DTGarantia")
    protected String dtGarantia;
    @XmlElement(name = "Memo")
    protected String memo;
    @XmlElement(name = "Levantado")
    protected String levantado;
    @XmlElement(name = "DTLevantado")
    protected String dtLevantado;
    @XmlElement(name = "IdGarantia")
    protected String idGarantia;
    @XmlElement(name = "Marca")
    protected String marca;
    @XmlElement(name = "Modelo")
    protected String modelo;
    @XmlElement(name = "Chassi")
    protected String chassi;
    @XmlElement(name = "AnoFabricacao")
    protected String anoFabricacao;
    @XmlElement(name = "AnoModelo")
    protected String anoModelo;
    @XmlElement(name = "Cor")
    protected String cor;
    @XmlElement(name = "Placa")
    protected String placa;
    @XmlElement(name = "Renavam")
    protected String renavam;
    @XmlElement(name = "TipoVeiculo")
    protected String tipoVeiculo;
    @XmlElement(name = "TipoCombustivel")
    protected String tipoCombustivel;
    @XmlElement(name = "EstadoConservacao")
    protected String estadoConservacao;
    @XmlElement(name = "CodMolicar")
    protected String codMolicar;
    @XmlElement(name = "VlrMolicar")
    protected String vlrMolicar;
    @XmlElement(name = "Serie")
    protected String serie;
    @XmlElement(name = "DescricaoMotor")
    protected String descricaoMotor;
    @XmlElement(name = "DtCadastro")
    protected String dtCadastro;

    /**
     * Obtém o valor da propriedade processo.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProcesso() {
        return processo;
    }

    /**
     * Define o valor da propriedade processo.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProcesso(String value) {
        this.processo = value;
    }

    /**
     * Obtém o valor da propriedade tipoDescricao.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTipoDescricao() {
        return tipoDescricao;
    }

    /**
     * Define o valor da propriedade tipoDescricao.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTipoDescricao(String value) {
        this.tipoDescricao = value;
    }

    /**
     * Obtém o valor da propriedade valor.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getValor() {
        return valor;
    }

    /**
     * Define o valor da propriedade valor.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setValor(String value) {
        this.valor = value;
    }

    /**
     * Obtém o valor da propriedade dtGarantia.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDTGarantia() {
        return dtGarantia;
    }

    /**
     * Define o valor da propriedade dtGarantia.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDTGarantia(String value) {
        this.dtGarantia = value;
    }

    /**
     * Obtém o valor da propriedade memo.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMemo() {
        return memo;
    }

    /**
     * Define o valor da propriedade memo.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMemo(String value) {
        this.memo = value;
    }

    /**
     * Obtém o valor da propriedade levantado.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLevantado() {
        return levantado;
    }

    /**
     * Define o valor da propriedade levantado.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLevantado(String value) {
        this.levantado = value;
    }

    /**
     * Obtém o valor da propriedade dtLevantado.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDTLevantado() {
        return dtLevantado;
    }

    /**
     * Define o valor da propriedade dtLevantado.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDTLevantado(String value) {
        this.dtLevantado = value;
    }

    /**
     * Obtém o valor da propriedade idGarantia.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdGarantia() {
        return idGarantia;
    }

    /**
     * Define o valor da propriedade idGarantia.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdGarantia(String value) {
        this.idGarantia = value;
    }

    /**
     * Obtém o valor da propriedade marca.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMarca() {
        return marca;
    }

    /**
     * Define o valor da propriedade marca.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMarca(String value) {
        this.marca = value;
    }

    /**
     * Obtém o valor da propriedade modelo.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getModelo() {
        return modelo;
    }

    /**
     * Define o valor da propriedade modelo.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setModelo(String value) {
        this.modelo = value;
    }

    /**
     * Obtém o valor da propriedade chassi.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getChassi() {
        return chassi;
    }

    /**
     * Define o valor da propriedade chassi.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setChassi(String value) {
        this.chassi = value;
    }

    /**
     * Obtém o valor da propriedade anoFabricacao.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAnoFabricacao() {
        return anoFabricacao;
    }

    /**
     * Define o valor da propriedade anoFabricacao.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAnoFabricacao(String value) {
        this.anoFabricacao = value;
    }

    /**
     * Obtém o valor da propriedade anoModelo.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAnoModelo() {
        return anoModelo;
    }

    /**
     * Define o valor da propriedade anoModelo.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAnoModelo(String value) {
        this.anoModelo = value;
    }

    /**
     * Obtém o valor da propriedade cor.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCor() {
        return cor;
    }

    /**
     * Define o valor da propriedade cor.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCor(String value) {
        this.cor = value;
    }

    /**
     * Obtém o valor da propriedade placa.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPlaca() {
        return placa;
    }

    /**
     * Define o valor da propriedade placa.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPlaca(String value) {
        this.placa = value;
    }

    /**
     * Obtém o valor da propriedade renavam.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRenavam() {
        return renavam;
    }

    /**
     * Define o valor da propriedade renavam.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRenavam(String value) {
        this.renavam = value;
    }

    /**
     * Obtém o valor da propriedade tipoVeiculo.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTipoVeiculo() {
        return tipoVeiculo;
    }

    /**
     * Define o valor da propriedade tipoVeiculo.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTipoVeiculo(String value) {
        this.tipoVeiculo = value;
    }

    /**
     * Obtém o valor da propriedade tipoCombustivel.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTipoCombustivel() {
        return tipoCombustivel;
    }

    /**
     * Define o valor da propriedade tipoCombustivel.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTipoCombustivel(String value) {
        this.tipoCombustivel = value;
    }

    /**
     * Obtém o valor da propriedade estadoConservacao.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEstadoConservacao() {
        return estadoConservacao;
    }

    /**
     * Define o valor da propriedade estadoConservacao.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEstadoConservacao(String value) {
        this.estadoConservacao = value;
    }

    /**
     * Obtém o valor da propriedade codMolicar.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodMolicar() {
        return codMolicar;
    }

    /**
     * Define o valor da propriedade codMolicar.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodMolicar(String value) {
        this.codMolicar = value;
    }

    /**
     * Obtém o valor da propriedade vlrMolicar.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVlrMolicar() {
        return vlrMolicar;
    }

    /**
     * Define o valor da propriedade vlrMolicar.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVlrMolicar(String value) {
        this.vlrMolicar = value;
    }

    /**
     * Obtém o valor da propriedade serie.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSerie() {
        return serie;
    }

    /**
     * Define o valor da propriedade serie.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSerie(String value) {
        this.serie = value;
    }

    /**
     * Obtém o valor da propriedade descricaoMotor.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescricaoMotor() {
        return descricaoMotor;
    }

    /**
     * Define o valor da propriedade descricaoMotor.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescricaoMotor(String value) {
        this.descricaoMotor = value;
    }

    /**
     * Obtém o valor da propriedade dtCadastro.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDtCadastro() {
        return dtCadastro;
    }

    /**
     * Define o valor da propriedade dtCadastro.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDtCadastro(String value) {
        this.dtCadastro = value;
    }

}
