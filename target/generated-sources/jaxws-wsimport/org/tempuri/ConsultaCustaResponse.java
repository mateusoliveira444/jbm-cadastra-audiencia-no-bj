
package org.tempuri;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java de anonymous complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ConsultaCustaResult" type="{http://tempuri.org/}ArrayOfCusta" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "consultaCustaResult"
})
@XmlRootElement(name = "ConsultaCustaResponse")
public class ConsultaCustaResponse {

    @XmlElement(name = "ConsultaCustaResult")
    protected ArrayOfCusta consultaCustaResult;

    /**
     * Obtém o valor da propriedade consultaCustaResult.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfCusta }
     *     
     */
    public ArrayOfCusta getConsultaCustaResult() {
        return consultaCustaResult;
    }

    /**
     * Define o valor da propriedade consultaCustaResult.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfCusta }
     *     
     */
    public void setConsultaCustaResult(ArrayOfCusta value) {
        this.consultaCustaResult = value;
    }

}
