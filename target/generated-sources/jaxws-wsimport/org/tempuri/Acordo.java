
package org.tempuri;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java de Acordo complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="Acordo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Responsavel" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Memo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ValorAcordo" type="{http://www.w3.org/2001/XMLSchema}float"/>
 *         &lt;element name="DataAcordo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="InsereLembrete" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="Parcelas" type="{http://tempuri.org/}Parcela" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Acordo", propOrder = {
    "responsavel",
    "memo",
    "valorAcordo",
    "dataAcordo",
    "insereLembrete",
    "parcelas"
})
public class Acordo {

    @XmlElement(name = "Responsavel")
    protected String responsavel;
    @XmlElement(name = "Memo")
    protected String memo;
    @XmlElement(name = "ValorAcordo")
    protected float valorAcordo;
    @XmlElement(name = "DataAcordo")
    protected String dataAcordo;
    @XmlElement(name = "InsereLembrete")
    protected boolean insereLembrete;
    @XmlElement(name = "Parcelas")
    protected List<Parcela> parcelas;

    /**
     * Obtém o valor da propriedade responsavel.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getResponsavel() {
        return responsavel;
    }

    /**
     * Define o valor da propriedade responsavel.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setResponsavel(String value) {
        this.responsavel = value;
    }

    /**
     * Obtém o valor da propriedade memo.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMemo() {
        return memo;
    }

    /**
     * Define o valor da propriedade memo.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMemo(String value) {
        this.memo = value;
    }

    /**
     * Obtém o valor da propriedade valorAcordo.
     * 
     */
    public float getValorAcordo() {
        return valorAcordo;
    }

    /**
     * Define o valor da propriedade valorAcordo.
     * 
     */
    public void setValorAcordo(float value) {
        this.valorAcordo = value;
    }

    /**
     * Obtém o valor da propriedade dataAcordo.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDataAcordo() {
        return dataAcordo;
    }

    /**
     * Define o valor da propriedade dataAcordo.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDataAcordo(String value) {
        this.dataAcordo = value;
    }

    /**
     * Obtém o valor da propriedade insereLembrete.
     * 
     */
    public boolean isInsereLembrete() {
        return insereLembrete;
    }

    /**
     * Define o valor da propriedade insereLembrete.
     * 
     */
    public void setInsereLembrete(boolean value) {
        this.insereLembrete = value;
    }

    /**
     * Gets the value of the parcelas property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the parcelas property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getParcelas().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Parcela }
     * 
     * 
     */
    public List<Parcela> getParcelas() {
        if (parcelas == null) {
            parcelas = new ArrayList<Parcela>();
        }
        return this.parcelas;
    }

}
