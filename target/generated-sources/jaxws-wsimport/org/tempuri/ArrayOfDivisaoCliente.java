
package org.tempuri;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java de ArrayOfDivisaoCliente complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="ArrayOfDivisaoCliente">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="DivisaoCliente" type="{http://tempuri.org/}DivisaoCliente" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ArrayOfDivisaoCliente", propOrder = {
    "divisaoCliente"
})
public class ArrayOfDivisaoCliente {

    @XmlElement(name = "DivisaoCliente", nillable = true)
    protected List<DivisaoCliente> divisaoCliente;

    /**
     * Gets the value of the divisaoCliente property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the divisaoCliente property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getDivisaoCliente().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link DivisaoCliente }
     * 
     * 
     */
    public List<DivisaoCliente> getDivisaoCliente() {
        if (divisaoCliente == null) {
            divisaoCliente = new ArrayList<DivisaoCliente>();
        }
        return this.divisaoCliente;
    }

}
