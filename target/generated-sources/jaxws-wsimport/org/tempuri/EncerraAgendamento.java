
package org.tempuri;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java de anonymous complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="lstrIDProcessoUnico" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="lintIDAgenda" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="lstrResponsavel" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="lstrStatusFinal" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="lstrMotivoStatusFinal" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="lstrUsuario" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="lstrSenha" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "lstrIDProcessoUnico",
    "lintIDAgenda",
    "lstrResponsavel",
    "lstrStatusFinal",
    "lstrMotivoStatusFinal",
    "lstrUsuario",
    "lstrSenha"
})
@XmlRootElement(name = "EncerraAgendamento")
public class EncerraAgendamento {

    protected String lstrIDProcessoUnico;
    protected int lintIDAgenda;
    protected String lstrResponsavel;
    protected String lstrStatusFinal;
    protected String lstrMotivoStatusFinal;
    protected String lstrUsuario;
    protected String lstrSenha;

    /**
     * Obtém o valor da propriedade lstrIDProcessoUnico.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLstrIDProcessoUnico() {
        return lstrIDProcessoUnico;
    }

    /**
     * Define o valor da propriedade lstrIDProcessoUnico.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLstrIDProcessoUnico(String value) {
        this.lstrIDProcessoUnico = value;
    }

    /**
     * Obtém o valor da propriedade lintIDAgenda.
     * 
     */
    public int getLintIDAgenda() {
        return lintIDAgenda;
    }

    /**
     * Define o valor da propriedade lintIDAgenda.
     * 
     */
    public void setLintIDAgenda(int value) {
        this.lintIDAgenda = value;
    }

    /**
     * Obtém o valor da propriedade lstrResponsavel.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLstrResponsavel() {
        return lstrResponsavel;
    }

    /**
     * Define o valor da propriedade lstrResponsavel.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLstrResponsavel(String value) {
        this.lstrResponsavel = value;
    }

    /**
     * Obtém o valor da propriedade lstrStatusFinal.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLstrStatusFinal() {
        return lstrStatusFinal;
    }

    /**
     * Define o valor da propriedade lstrStatusFinal.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLstrStatusFinal(String value) {
        this.lstrStatusFinal = value;
    }

    /**
     * Obtém o valor da propriedade lstrMotivoStatusFinal.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLstrMotivoStatusFinal() {
        return lstrMotivoStatusFinal;
    }

    /**
     * Define o valor da propriedade lstrMotivoStatusFinal.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLstrMotivoStatusFinal(String value) {
        this.lstrMotivoStatusFinal = value;
    }

    /**
     * Obtém o valor da propriedade lstrUsuario.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLstrUsuario() {
        return lstrUsuario;
    }

    /**
     * Define o valor da propriedade lstrUsuario.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLstrUsuario(String value) {
        this.lstrUsuario = value;
    }

    /**
     * Obtém o valor da propriedade lstrSenha.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLstrSenha() {
        return lstrSenha;
    }

    /**
     * Define o valor da propriedade lstrSenha.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLstrSenha(String value) {
        this.lstrSenha = value;
    }

}
