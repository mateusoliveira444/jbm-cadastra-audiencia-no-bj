
package org.tempuri;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java de OpcoesCampoLista complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="OpcoesCampoLista">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="IdCampoLista" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="Descricao" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "OpcoesCampoLista", propOrder = {
    "idCampoLista",
    "descricao"
})
public class OpcoesCampoLista {

    @XmlElement(name = "IdCampoLista")
    protected int idCampoLista;
    @XmlElement(name = "Descricao")
    protected String descricao;

    /**
     * Obtém o valor da propriedade idCampoLista.
     * 
     */
    public int getIdCampoLista() {
        return idCampoLista;
    }

    /**
     * Define o valor da propriedade idCampoLista.
     * 
     */
    public void setIdCampoLista(int value) {
        this.idCampoLista = value;
    }

    /**
     * Obtém o valor da propriedade descricao.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescricao() {
        return descricao;
    }

    /**
     * Define o valor da propriedade descricao.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescricao(String value) {
        this.descricao = value;
    }

}
