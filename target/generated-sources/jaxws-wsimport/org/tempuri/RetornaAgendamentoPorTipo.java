
package org.tempuri;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java de RetornaAgendamentoPorTipo complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="RetornaAgendamentoPorTipo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Celula" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="NumeroControleCliente" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="TipoAgendamento" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="IdProcessoUnico" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DataAgendamento" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="TextoAgendamento" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ResponsavelAgendamento" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DataConclusaoAgendamento" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="HoraConclusaoAgendamento" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="StatusAgendamento" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="MotivoAgendamento" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="IdAgendamento" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="RealizadoPor" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="HoraAgendamento" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DataCriacao" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "RetornaAgendamentoPorTipo", propOrder = {
    "celula",
    "numeroControleCliente",
    "tipoAgendamento",
    "idProcessoUnico",
    "dataAgendamento",
    "textoAgendamento",
    "responsavelAgendamento",
    "dataConclusaoAgendamento",
    "horaConclusaoAgendamento",
    "statusAgendamento",
    "motivoAgendamento",
    "idAgendamento",
    "realizadoPor",
    "horaAgendamento",
    "dataCriacao"
})
public class RetornaAgendamentoPorTipo {

    @XmlElement(name = "Celula")
    protected String celula;
    @XmlElement(name = "NumeroControleCliente")
    protected String numeroControleCliente;
    @XmlElement(name = "TipoAgendamento")
    protected String tipoAgendamento;
    @XmlElement(name = "IdProcessoUnico")
    protected String idProcessoUnico;
    @XmlElement(name = "DataAgendamento")
    protected String dataAgendamento;
    @XmlElement(name = "TextoAgendamento")
    protected String textoAgendamento;
    @XmlElement(name = "ResponsavelAgendamento")
    protected String responsavelAgendamento;
    @XmlElement(name = "DataConclusaoAgendamento")
    protected String dataConclusaoAgendamento;
    @XmlElement(name = "HoraConclusaoAgendamento")
    protected String horaConclusaoAgendamento;
    @XmlElement(name = "StatusAgendamento")
    protected String statusAgendamento;
    @XmlElement(name = "MotivoAgendamento")
    protected String motivoAgendamento;
    @XmlElement(name = "IdAgendamento")
    protected String idAgendamento;
    @XmlElement(name = "RealizadoPor")
    protected String realizadoPor;
    @XmlElement(name = "HoraAgendamento")
    protected String horaAgendamento;
    @XmlElement(name = "DataCriacao")
    protected String dataCriacao;

    /**
     * Obtém o valor da propriedade celula.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCelula() {
        return celula;
    }

    /**
     * Define o valor da propriedade celula.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCelula(String value) {
        this.celula = value;
    }

    /**
     * Obtém o valor da propriedade numeroControleCliente.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNumeroControleCliente() {
        return numeroControleCliente;
    }

    /**
     * Define o valor da propriedade numeroControleCliente.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNumeroControleCliente(String value) {
        this.numeroControleCliente = value;
    }

    /**
     * Obtém o valor da propriedade tipoAgendamento.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTipoAgendamento() {
        return tipoAgendamento;
    }

    /**
     * Define o valor da propriedade tipoAgendamento.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTipoAgendamento(String value) {
        this.tipoAgendamento = value;
    }

    /**
     * Obtém o valor da propriedade idProcessoUnico.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdProcessoUnico() {
        return idProcessoUnico;
    }

    /**
     * Define o valor da propriedade idProcessoUnico.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdProcessoUnico(String value) {
        this.idProcessoUnico = value;
    }

    /**
     * Obtém o valor da propriedade dataAgendamento.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDataAgendamento() {
        return dataAgendamento;
    }

    /**
     * Define o valor da propriedade dataAgendamento.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDataAgendamento(String value) {
        this.dataAgendamento = value;
    }

    /**
     * Obtém o valor da propriedade textoAgendamento.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTextoAgendamento() {
        return textoAgendamento;
    }

    /**
     * Define o valor da propriedade textoAgendamento.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTextoAgendamento(String value) {
        this.textoAgendamento = value;
    }

    /**
     * Obtém o valor da propriedade responsavelAgendamento.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getResponsavelAgendamento() {
        return responsavelAgendamento;
    }

    /**
     * Define o valor da propriedade responsavelAgendamento.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setResponsavelAgendamento(String value) {
        this.responsavelAgendamento = value;
    }

    /**
     * Obtém o valor da propriedade dataConclusaoAgendamento.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDataConclusaoAgendamento() {
        return dataConclusaoAgendamento;
    }

    /**
     * Define o valor da propriedade dataConclusaoAgendamento.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDataConclusaoAgendamento(String value) {
        this.dataConclusaoAgendamento = value;
    }

    /**
     * Obtém o valor da propriedade horaConclusaoAgendamento.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getHoraConclusaoAgendamento() {
        return horaConclusaoAgendamento;
    }

    /**
     * Define o valor da propriedade horaConclusaoAgendamento.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setHoraConclusaoAgendamento(String value) {
        this.horaConclusaoAgendamento = value;
    }

    /**
     * Obtém o valor da propriedade statusAgendamento.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStatusAgendamento() {
        return statusAgendamento;
    }

    /**
     * Define o valor da propriedade statusAgendamento.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStatusAgendamento(String value) {
        this.statusAgendamento = value;
    }

    /**
     * Obtém o valor da propriedade motivoAgendamento.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMotivoAgendamento() {
        return motivoAgendamento;
    }

    /**
     * Define o valor da propriedade motivoAgendamento.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMotivoAgendamento(String value) {
        this.motivoAgendamento = value;
    }

    /**
     * Obtém o valor da propriedade idAgendamento.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdAgendamento() {
        return idAgendamento;
    }

    /**
     * Define o valor da propriedade idAgendamento.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdAgendamento(String value) {
        this.idAgendamento = value;
    }

    /**
     * Obtém o valor da propriedade realizadoPor.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRealizadoPor() {
        return realizadoPor;
    }

    /**
     * Define o valor da propriedade realizadoPor.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRealizadoPor(String value) {
        this.realizadoPor = value;
    }

    /**
     * Obtém o valor da propriedade horaAgendamento.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getHoraAgendamento() {
        return horaAgendamento;
    }

    /**
     * Define o valor da propriedade horaAgendamento.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setHoraAgendamento(String value) {
        this.horaAgendamento = value;
    }

    /**
     * Obtém o valor da propriedade dataCriacao.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDataCriacao() {
        return dataCriacao;
    }

    /**
     * Define o valor da propriedade dataCriacao.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDataCriacao(String value) {
        this.dataCriacao = value;
    }

}
