
package org.tempuri;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java de DadoComplementar complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="DadoComplementar">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Grupo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Campo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="TipoCampo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Tamanho" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="Conteudo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ItemLista" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="IdProcessoUnico" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Valor" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="IdIncoDado" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="IdIncoCampo" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="IdTabela" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="IdIncoCampoLista" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DadoComplementar", propOrder = {
    "grupo",
    "campo",
    "tipoCampo",
    "tamanho",
    "conteudo",
    "itemLista",
    "idProcessoUnico",
    "valor",
    "idIncoDado",
    "idIncoCampo",
    "idTabela",
    "idIncoCampoLista"
})
public class DadoComplementar {

    @XmlElement(name = "Grupo")
    protected String grupo;
    @XmlElement(name = "Campo")
    protected String campo;
    @XmlElement(name = "TipoCampo")
    protected String tipoCampo;
    @XmlElement(name = "Tamanho")
    protected int tamanho;
    @XmlElement(name = "Conteudo")
    protected String conteudo;
    @XmlElement(name = "ItemLista")
    protected String itemLista;
    @XmlElement(name = "IdProcessoUnico")
    protected String idProcessoUnico;
    @XmlElement(name = "Valor")
    protected String valor;
    @XmlElement(name = "IdIncoDado")
    protected int idIncoDado;
    @XmlElement(name = "IdIncoCampo")
    protected int idIncoCampo;
    @XmlElement(name = "IdTabela")
    protected int idTabela;
    @XmlElement(name = "IdIncoCampoLista")
    protected int idIncoCampoLista;

    /**
     * Obtém o valor da propriedade grupo.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGrupo() {
        return grupo;
    }

    /**
     * Define o valor da propriedade grupo.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGrupo(String value) {
        this.grupo = value;
    }

    /**
     * Obtém o valor da propriedade campo.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCampo() {
        return campo;
    }

    /**
     * Define o valor da propriedade campo.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCampo(String value) {
        this.campo = value;
    }

    /**
     * Obtém o valor da propriedade tipoCampo.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTipoCampo() {
        return tipoCampo;
    }

    /**
     * Define o valor da propriedade tipoCampo.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTipoCampo(String value) {
        this.tipoCampo = value;
    }

    /**
     * Obtém o valor da propriedade tamanho.
     * 
     */
    public int getTamanho() {
        return tamanho;
    }

    /**
     * Define o valor da propriedade tamanho.
     * 
     */
    public void setTamanho(int value) {
        this.tamanho = value;
    }

    /**
     * Obtém o valor da propriedade conteudo.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getConteudo() {
        return conteudo;
    }

    /**
     * Define o valor da propriedade conteudo.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setConteudo(String value) {
        this.conteudo = value;
    }

    /**
     * Obtém o valor da propriedade itemLista.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getItemLista() {
        return itemLista;
    }

    /**
     * Define o valor da propriedade itemLista.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setItemLista(String value) {
        this.itemLista = value;
    }

    /**
     * Obtém o valor da propriedade idProcessoUnico.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdProcessoUnico() {
        return idProcessoUnico;
    }

    /**
     * Define o valor da propriedade idProcessoUnico.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdProcessoUnico(String value) {
        this.idProcessoUnico = value;
    }

    /**
     * Obtém o valor da propriedade valor.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getValor() {
        return valor;
    }

    /**
     * Define o valor da propriedade valor.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setValor(String value) {
        this.valor = value;
    }

    /**
     * Obtém o valor da propriedade idIncoDado.
     * 
     */
    public int getIdIncoDado() {
        return idIncoDado;
    }

    /**
     * Define o valor da propriedade idIncoDado.
     * 
     */
    public void setIdIncoDado(int value) {
        this.idIncoDado = value;
    }

    /**
     * Obtém o valor da propriedade idIncoCampo.
     * 
     */
    public int getIdIncoCampo() {
        return idIncoCampo;
    }

    /**
     * Define o valor da propriedade idIncoCampo.
     * 
     */
    public void setIdIncoCampo(int value) {
        this.idIncoCampo = value;
    }

    /**
     * Obtém o valor da propriedade idTabela.
     * 
     */
    public int getIdTabela() {
        return idTabela;
    }

    /**
     * Define o valor da propriedade idTabela.
     * 
     */
    public void setIdTabela(int value) {
        this.idTabela = value;
    }

    /**
     * Obtém o valor da propriedade idIncoCampoLista.
     * 
     */
    public int getIdIncoCampoLista() {
        return idIncoCampoLista;
    }

    /**
     * Define o valor da propriedade idIncoCampoLista.
     * 
     */
    public void setIdIncoCampoLista(int value) {
        this.idIncoCampoLista = value;
    }

}
