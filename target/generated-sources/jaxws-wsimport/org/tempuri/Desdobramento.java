
package org.tempuri;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Classe Java de Desdobramento complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="Desdobramento">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="IDDesdobramento" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="Numero" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DataDistribuicao" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="ExecucaoProvisoria" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="IDProcesso" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="IDInstancia" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="IDJustica" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="IDForo" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="IDVara" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="Acao" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="IDDecisao" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="IDProcessoStatus" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="IDReparticao" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="IDEstados" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="Pasta" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DataEncerramento" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="Memo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Ordinal" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="NumeroAntigo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ParteInteressada" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ParteContraria" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="PosicaoParteInteressada" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Desdobramento", propOrder = {
    "idDesdobramento",
    "numero",
    "dataDistribuicao",
    "execucaoProvisoria",
    "idProcesso",
    "idInstancia",
    "idJustica",
    "idForo",
    "idVara",
    "acao",
    "idDecisao",
    "idProcessoStatus",
    "idReparticao",
    "idEstados",
    "pasta",
    "dataEncerramento",
    "memo",
    "ordinal",
    "numeroAntigo",
    "parteInteressada",
    "parteContraria",
    "posicaoParteInteressada"
})
public class Desdobramento {

    @XmlElement(name = "IDDesdobramento")
    protected int idDesdobramento;
    @XmlElement(name = "Numero")
    protected String numero;
    @XmlElement(name = "DataDistribuicao", required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar dataDistribuicao;
    @XmlElement(name = "ExecucaoProvisoria")
    protected boolean execucaoProvisoria;
    @XmlElement(name = "IDProcesso")
    protected int idProcesso;
    @XmlElement(name = "IDInstancia")
    protected int idInstancia;
    @XmlElement(name = "IDJustica")
    protected int idJustica;
    @XmlElement(name = "IDForo")
    protected int idForo;
    @XmlElement(name = "IDVara")
    protected int idVara;
    @XmlElement(name = "Acao")
    protected String acao;
    @XmlElement(name = "IDDecisao")
    protected int idDecisao;
    @XmlElement(name = "IDProcessoStatus")
    protected int idProcessoStatus;
    @XmlElement(name = "IDReparticao")
    protected int idReparticao;
    @XmlElement(name = "IDEstados")
    protected int idEstados;
    @XmlElement(name = "Pasta")
    protected String pasta;
    @XmlElement(name = "DataEncerramento", required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar dataEncerramento;
    @XmlElement(name = "Memo")
    protected String memo;
    @XmlElement(name = "Ordinal")
    protected int ordinal;
    @XmlElement(name = "NumeroAntigo")
    protected String numeroAntigo;
    @XmlElement(name = "ParteInteressada")
    protected String parteInteressada;
    @XmlElement(name = "ParteContraria")
    protected String parteContraria;
    @XmlElement(name = "PosicaoParteInteressada")
    protected String posicaoParteInteressada;

    /**
     * Obtém o valor da propriedade idDesdobramento.
     * 
     */
    public int getIDDesdobramento() {
        return idDesdobramento;
    }

    /**
     * Define o valor da propriedade idDesdobramento.
     * 
     */
    public void setIDDesdobramento(int value) {
        this.idDesdobramento = value;
    }

    /**
     * Obtém o valor da propriedade numero.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNumero() {
        return numero;
    }

    /**
     * Define o valor da propriedade numero.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNumero(String value) {
        this.numero = value;
    }

    /**
     * Obtém o valor da propriedade dataDistribuicao.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDataDistribuicao() {
        return dataDistribuicao;
    }

    /**
     * Define o valor da propriedade dataDistribuicao.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDataDistribuicao(XMLGregorianCalendar value) {
        this.dataDistribuicao = value;
    }

    /**
     * Obtém o valor da propriedade execucaoProvisoria.
     * 
     */
    public boolean isExecucaoProvisoria() {
        return execucaoProvisoria;
    }

    /**
     * Define o valor da propriedade execucaoProvisoria.
     * 
     */
    public void setExecucaoProvisoria(boolean value) {
        this.execucaoProvisoria = value;
    }

    /**
     * Obtém o valor da propriedade idProcesso.
     * 
     */
    public int getIDProcesso() {
        return idProcesso;
    }

    /**
     * Define o valor da propriedade idProcesso.
     * 
     */
    public void setIDProcesso(int value) {
        this.idProcesso = value;
    }

    /**
     * Obtém o valor da propriedade idInstancia.
     * 
     */
    public int getIDInstancia() {
        return idInstancia;
    }

    /**
     * Define o valor da propriedade idInstancia.
     * 
     */
    public void setIDInstancia(int value) {
        this.idInstancia = value;
    }

    /**
     * Obtém o valor da propriedade idJustica.
     * 
     */
    public int getIDJustica() {
        return idJustica;
    }

    /**
     * Define o valor da propriedade idJustica.
     * 
     */
    public void setIDJustica(int value) {
        this.idJustica = value;
    }

    /**
     * Obtém o valor da propriedade idForo.
     * 
     */
    public int getIDForo() {
        return idForo;
    }

    /**
     * Define o valor da propriedade idForo.
     * 
     */
    public void setIDForo(int value) {
        this.idForo = value;
    }

    /**
     * Obtém o valor da propriedade idVara.
     * 
     */
    public int getIDVara() {
        return idVara;
    }

    /**
     * Define o valor da propriedade idVara.
     * 
     */
    public void setIDVara(int value) {
        this.idVara = value;
    }

    /**
     * Obtém o valor da propriedade acao.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAcao() {
        return acao;
    }

    /**
     * Define o valor da propriedade acao.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAcao(String value) {
        this.acao = value;
    }

    /**
     * Obtém o valor da propriedade idDecisao.
     * 
     */
    public int getIDDecisao() {
        return idDecisao;
    }

    /**
     * Define o valor da propriedade idDecisao.
     * 
     */
    public void setIDDecisao(int value) {
        this.idDecisao = value;
    }

    /**
     * Obtém o valor da propriedade idProcessoStatus.
     * 
     */
    public int getIDProcessoStatus() {
        return idProcessoStatus;
    }

    /**
     * Define o valor da propriedade idProcessoStatus.
     * 
     */
    public void setIDProcessoStatus(int value) {
        this.idProcessoStatus = value;
    }

    /**
     * Obtém o valor da propriedade idReparticao.
     * 
     */
    public int getIDReparticao() {
        return idReparticao;
    }

    /**
     * Define o valor da propriedade idReparticao.
     * 
     */
    public void setIDReparticao(int value) {
        this.idReparticao = value;
    }

    /**
     * Obtém o valor da propriedade idEstados.
     * 
     */
    public int getIDEstados() {
        return idEstados;
    }

    /**
     * Define o valor da propriedade idEstados.
     * 
     */
    public void setIDEstados(int value) {
        this.idEstados = value;
    }

    /**
     * Obtém o valor da propriedade pasta.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPasta() {
        return pasta;
    }

    /**
     * Define o valor da propriedade pasta.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPasta(String value) {
        this.pasta = value;
    }

    /**
     * Obtém o valor da propriedade dataEncerramento.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDataEncerramento() {
        return dataEncerramento;
    }

    /**
     * Define o valor da propriedade dataEncerramento.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDataEncerramento(XMLGregorianCalendar value) {
        this.dataEncerramento = value;
    }

    /**
     * Obtém o valor da propriedade memo.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMemo() {
        return memo;
    }

    /**
     * Define o valor da propriedade memo.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMemo(String value) {
        this.memo = value;
    }

    /**
     * Obtém o valor da propriedade ordinal.
     * 
     */
    public int getOrdinal() {
        return ordinal;
    }

    /**
     * Define o valor da propriedade ordinal.
     * 
     */
    public void setOrdinal(int value) {
        this.ordinal = value;
    }

    /**
     * Obtém o valor da propriedade numeroAntigo.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNumeroAntigo() {
        return numeroAntigo;
    }

    /**
     * Define o valor da propriedade numeroAntigo.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNumeroAntigo(String value) {
        this.numeroAntigo = value;
    }

    /**
     * Obtém o valor da propriedade parteInteressada.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getParteInteressada() {
        return parteInteressada;
    }

    /**
     * Define o valor da propriedade parteInteressada.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setParteInteressada(String value) {
        this.parteInteressada = value;
    }

    /**
     * Obtém o valor da propriedade parteContraria.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getParteContraria() {
        return parteContraria;
    }

    /**
     * Define o valor da propriedade parteContraria.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setParteContraria(String value) {
        this.parteContraria = value;
    }

    /**
     * Obtém o valor da propriedade posicaoParteInteressada.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPosicaoParteInteressada() {
        return posicaoParteInteressada;
    }

    /**
     * Define o valor da propriedade posicaoParteInteressada.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPosicaoParteInteressada(String value) {
        this.posicaoParteInteressada = value;
    }

}
