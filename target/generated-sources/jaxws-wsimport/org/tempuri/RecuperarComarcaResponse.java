
package org.tempuri;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java de anonymous complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="RecuperarComarcaResult" type="{http://tempuri.org/}ArrayOfComarca" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "recuperarComarcaResult"
})
@XmlRootElement(name = "RecuperarComarcaResponse")
public class RecuperarComarcaResponse {

    @XmlElement(name = "RecuperarComarcaResult")
    protected ArrayOfComarca recuperarComarcaResult;

    /**
     * Obtém o valor da propriedade recuperarComarcaResult.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfComarca }
     *     
     */
    public ArrayOfComarca getRecuperarComarcaResult() {
        return recuperarComarcaResult;
    }

    /**
     * Define o valor da propriedade recuperarComarcaResult.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfComarca }
     *     
     */
    public void setRecuperarComarcaResult(ArrayOfComarca value) {
        this.recuperarComarcaResult = value;
    }

}
