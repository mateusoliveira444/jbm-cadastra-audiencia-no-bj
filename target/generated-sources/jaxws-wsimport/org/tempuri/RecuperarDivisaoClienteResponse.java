
package org.tempuri;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java de anonymous complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="RecuperarDivisaoClienteResult" type="{http://tempuri.org/}ArrayOfDivisaoCliente" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "recuperarDivisaoClienteResult"
})
@XmlRootElement(name = "RecuperarDivisaoClienteResponse")
public class RecuperarDivisaoClienteResponse {

    @XmlElement(name = "RecuperarDivisaoClienteResult")
    protected ArrayOfDivisaoCliente recuperarDivisaoClienteResult;

    /**
     * Obtém o valor da propriedade recuperarDivisaoClienteResult.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfDivisaoCliente }
     *     
     */
    public ArrayOfDivisaoCliente getRecuperarDivisaoClienteResult() {
        return recuperarDivisaoClienteResult;
    }

    /**
     * Define o valor da propriedade recuperarDivisaoClienteResult.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfDivisaoCliente }
     *     
     */
    public void setRecuperarDivisaoClienteResult(ArrayOfDivisaoCliente value) {
        this.recuperarDivisaoClienteResult = value;
    }

}
