
package org.tempuri;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java de anonymous complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="RecuperarVaraResult" type="{http://tempuri.org/}ArrayOfVara" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "recuperarVaraResult"
})
@XmlRootElement(name = "RecuperarVaraResponse")
public class RecuperarVaraResponse {

    @XmlElement(name = "RecuperarVaraResult")
    protected ArrayOfVara recuperarVaraResult;

    /**
     * Obtém o valor da propriedade recuperarVaraResult.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfVara }
     *     
     */
    public ArrayOfVara getRecuperarVaraResult() {
        return recuperarVaraResult;
    }

    /**
     * Define o valor da propriedade recuperarVaraResult.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfVara }
     *     
     */
    public void setRecuperarVaraResult(ArrayOfVara value) {
        this.recuperarVaraResult = value;
    }

}
