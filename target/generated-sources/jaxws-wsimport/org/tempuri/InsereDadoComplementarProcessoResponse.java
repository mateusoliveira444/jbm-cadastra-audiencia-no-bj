
package org.tempuri;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java de anonymous complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="InsereDadoComplementarProcessoResult" type="{http://tempuri.org/}RetornoMetodos" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "insereDadoComplementarProcessoResult"
})
@XmlRootElement(name = "InsereDadoComplementarProcessoResponse")
public class InsereDadoComplementarProcessoResponse {

    @XmlElement(name = "InsereDadoComplementarProcessoResult")
    protected RetornoMetodos insereDadoComplementarProcessoResult;

    /**
     * Obtém o valor da propriedade insereDadoComplementarProcessoResult.
     * 
     * @return
     *     possible object is
     *     {@link RetornoMetodos }
     *     
     */
    public RetornoMetodos getInsereDadoComplementarProcessoResult() {
        return insereDadoComplementarProcessoResult;
    }

    /**
     * Define o valor da propriedade insereDadoComplementarProcessoResult.
     * 
     * @param value
     *     allowed object is
     *     {@link RetornoMetodos }
     *     
     */
    public void setInsereDadoComplementarProcessoResult(RetornoMetodos value) {
        this.insereDadoComplementarProcessoResult = value;
    }

}
