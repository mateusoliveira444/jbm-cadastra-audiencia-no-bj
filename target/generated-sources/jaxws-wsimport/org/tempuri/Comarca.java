
package org.tempuri;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java de Comarca complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="Comarca">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="IDComarca" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="ComarcaDesc" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="UF" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Prazo" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="Metropolitana" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Comarca", propOrder = {
    "idComarca",
    "comarcaDesc",
    "uf",
    "prazo",
    "metropolitana"
})
public class Comarca {

    @XmlElement(name = "IDComarca")
    protected int idComarca;
    @XmlElement(name = "ComarcaDesc")
    protected String comarcaDesc;
    @XmlElement(name = "UF")
    protected String uf;
    @XmlElement(name = "Prazo")
    protected int prazo;
    @XmlElement(name = "Metropolitana")
    protected boolean metropolitana;

    /**
     * Obtém o valor da propriedade idComarca.
     * 
     */
    public int getIDComarca() {
        return idComarca;
    }

    /**
     * Define o valor da propriedade idComarca.
     * 
     */
    public void setIDComarca(int value) {
        this.idComarca = value;
    }

    /**
     * Obtém o valor da propriedade comarcaDesc.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getComarcaDesc() {
        return comarcaDesc;
    }

    /**
     * Define o valor da propriedade comarcaDesc.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setComarcaDesc(String value) {
        this.comarcaDesc = value;
    }

    /**
     * Obtém o valor da propriedade uf.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUF() {
        return uf;
    }

    /**
     * Define o valor da propriedade uf.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUF(String value) {
        this.uf = value;
    }

    /**
     * Obtém o valor da propriedade prazo.
     * 
     */
    public int getPrazo() {
        return prazo;
    }

    /**
     * Define o valor da propriedade prazo.
     * 
     */
    public void setPrazo(int value) {
        this.prazo = value;
    }

    /**
     * Obtém o valor da propriedade metropolitana.
     * 
     */
    public boolean isMetropolitana() {
        return metropolitana;
    }

    /**
     * Define o valor da propriedade metropolitana.
     * 
     */
    public void setMetropolitana(boolean value) {
        this.metropolitana = value;
    }

}
