
package org.tempuri;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java de anonymous complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="lstrIDProcessoUnico" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="lstrGrupo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="lstrCampo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="lstrValor" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="lstrUsuario" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="lstrSenha" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "lstrIDProcessoUnico",
    "lstrGrupo",
    "lstrCampo",
    "lstrValor",
    "lstrUsuario",
    "lstrSenha"
})
@XmlRootElement(name = "InsereDadoComplementarProcesso")
public class InsereDadoComplementarProcesso {

    protected String lstrIDProcessoUnico;
    protected String lstrGrupo;
    protected String lstrCampo;
    protected String lstrValor;
    protected String lstrUsuario;
    protected String lstrSenha;

    /**
     * Obtém o valor da propriedade lstrIDProcessoUnico.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLstrIDProcessoUnico() {
        return lstrIDProcessoUnico;
    }

    /**
     * Define o valor da propriedade lstrIDProcessoUnico.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLstrIDProcessoUnico(String value) {
        this.lstrIDProcessoUnico = value;
    }

    /**
     * Obtém o valor da propriedade lstrGrupo.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLstrGrupo() {
        return lstrGrupo;
    }

    /**
     * Define o valor da propriedade lstrGrupo.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLstrGrupo(String value) {
        this.lstrGrupo = value;
    }

    /**
     * Obtém o valor da propriedade lstrCampo.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLstrCampo() {
        return lstrCampo;
    }

    /**
     * Define o valor da propriedade lstrCampo.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLstrCampo(String value) {
        this.lstrCampo = value;
    }

    /**
     * Obtém o valor da propriedade lstrValor.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLstrValor() {
        return lstrValor;
    }

    /**
     * Define o valor da propriedade lstrValor.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLstrValor(String value) {
        this.lstrValor = value;
    }

    /**
     * Obtém o valor da propriedade lstrUsuario.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLstrUsuario() {
        return lstrUsuario;
    }

    /**
     * Define o valor da propriedade lstrUsuario.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLstrUsuario(String value) {
        this.lstrUsuario = value;
    }

    /**
     * Obtém o valor da propriedade lstrSenha.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLstrSenha() {
        return lstrSenha;
    }

    /**
     * Define o valor da propriedade lstrSenha.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLstrSenha(String value) {
        this.lstrSenha = value;
    }

}
