
package org.tempuri;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java de Movimento complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="Movimento">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Data" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ProcessosCadastrados" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="DocumentoAnexados" type="{http://tempuri.org/}DocumentoAnexados" minOccurs="0"/>
 *         &lt;element name="Andamentos" type="{http://tempuri.org/}Andamentos" minOccurs="0"/>
 *         &lt;element name="Agendamentos" type="{http://tempuri.org/}Agendamentos" minOccurs="0"/>
 *         &lt;element name="Publicacoes" type="{http://tempuri.org/}Publicacoes" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Movimento", propOrder = {
    "data",
    "processosCadastrados",
    "documentoAnexados",
    "andamentos",
    "agendamentos",
    "publicacoes"
})
public class Movimento {

    @XmlElement(name = "Data")
    protected String data;
    @XmlElement(name = "ProcessosCadastrados")
    protected int processosCadastrados;
    @XmlElement(name = "DocumentoAnexados")
    protected DocumentoAnexados documentoAnexados;
    @XmlElement(name = "Andamentos")
    protected Andamentos andamentos;
    @XmlElement(name = "Agendamentos")
    protected Agendamentos agendamentos;
    @XmlElement(name = "Publicacoes")
    protected Publicacoes publicacoes;

    /**
     * Obtém o valor da propriedade data.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getData() {
        return data;
    }

    /**
     * Define o valor da propriedade data.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setData(String value) {
        this.data = value;
    }

    /**
     * Obtém o valor da propriedade processosCadastrados.
     * 
     */
    public int getProcessosCadastrados() {
        return processosCadastrados;
    }

    /**
     * Define o valor da propriedade processosCadastrados.
     * 
     */
    public void setProcessosCadastrados(int value) {
        this.processosCadastrados = value;
    }

    /**
     * Obtém o valor da propriedade documentoAnexados.
     * 
     * @return
     *     possible object is
     *     {@link DocumentoAnexados }
     *     
     */
    public DocumentoAnexados getDocumentoAnexados() {
        return documentoAnexados;
    }

    /**
     * Define o valor da propriedade documentoAnexados.
     * 
     * @param value
     *     allowed object is
     *     {@link DocumentoAnexados }
     *     
     */
    public void setDocumentoAnexados(DocumentoAnexados value) {
        this.documentoAnexados = value;
    }

    /**
     * Obtém o valor da propriedade andamentos.
     * 
     * @return
     *     possible object is
     *     {@link Andamentos }
     *     
     */
    public Andamentos getAndamentos() {
        return andamentos;
    }

    /**
     * Define o valor da propriedade andamentos.
     * 
     * @param value
     *     allowed object is
     *     {@link Andamentos }
     *     
     */
    public void setAndamentos(Andamentos value) {
        this.andamentos = value;
    }

    /**
     * Obtém o valor da propriedade agendamentos.
     * 
     * @return
     *     possible object is
     *     {@link Agendamentos }
     *     
     */
    public Agendamentos getAgendamentos() {
        return agendamentos;
    }

    /**
     * Define o valor da propriedade agendamentos.
     * 
     * @param value
     *     allowed object is
     *     {@link Agendamentos }
     *     
     */
    public void setAgendamentos(Agendamentos value) {
        this.agendamentos = value;
    }

    /**
     * Obtém o valor da propriedade publicacoes.
     * 
     * @return
     *     possible object is
     *     {@link Publicacoes }
     *     
     */
    public Publicacoes getPublicacoes() {
        return publicacoes;
    }

    /**
     * Define o valor da propriedade publicacoes.
     * 
     * @param value
     *     allowed object is
     *     {@link Publicacoes }
     *     
     */
    public void setPublicacoes(Publicacoes value) {
        this.publicacoes = value;
    }

}
