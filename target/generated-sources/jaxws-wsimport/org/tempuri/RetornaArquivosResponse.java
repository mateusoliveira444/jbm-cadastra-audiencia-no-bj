
package org.tempuri;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java de anonymous complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="RetornaArquivosResult" type="{http://tempuri.org/}ArrayOfArquivo" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "retornaArquivosResult"
})
@XmlRootElement(name = "RetornaArquivosResponse")
public class RetornaArquivosResponse {

    @XmlElement(name = "RetornaArquivosResult")
    protected ArrayOfArquivo retornaArquivosResult;

    /**
     * Obtém o valor da propriedade retornaArquivosResult.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfArquivo }
     *     
     */
    public ArrayOfArquivo getRetornaArquivosResult() {
        return retornaArquivosResult;
    }

    /**
     * Define o valor da propriedade retornaArquivosResult.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfArquivo }
     *     
     */
    public void setRetornaArquivosResult(ArrayOfArquivo value) {
        this.retornaArquivosResult = value;
    }

}
