
package org.tempuri;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java de Acao complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="Acao">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="IDAcao" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="AcaoDesc" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="IDPratica" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="PraticaDesc" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="IDInstancia" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="InstanciaDesc" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Acao", propOrder = {
    "idAcao",
    "acaoDesc",
    "idPratica",
    "praticaDesc",
    "idInstancia",
    "instanciaDesc"
})
public class Acao {

    @XmlElement(name = "IDAcao")
    protected int idAcao;
    @XmlElement(name = "AcaoDesc")
    protected String acaoDesc;
    @XmlElement(name = "IDPratica")
    protected int idPratica;
    @XmlElement(name = "PraticaDesc")
    protected String praticaDesc;
    @XmlElement(name = "IDInstancia")
    protected int idInstancia;
    @XmlElement(name = "InstanciaDesc")
    protected String instanciaDesc;

    /**
     * Obtém o valor da propriedade idAcao.
     * 
     */
    public int getIDAcao() {
        return idAcao;
    }

    /**
     * Define o valor da propriedade idAcao.
     * 
     */
    public void setIDAcao(int value) {
        this.idAcao = value;
    }

    /**
     * Obtém o valor da propriedade acaoDesc.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAcaoDesc() {
        return acaoDesc;
    }

    /**
     * Define o valor da propriedade acaoDesc.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAcaoDesc(String value) {
        this.acaoDesc = value;
    }

    /**
     * Obtém o valor da propriedade idPratica.
     * 
     */
    public int getIDPratica() {
        return idPratica;
    }

    /**
     * Define o valor da propriedade idPratica.
     * 
     */
    public void setIDPratica(int value) {
        this.idPratica = value;
    }

    /**
     * Obtém o valor da propriedade praticaDesc.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPraticaDesc() {
        return praticaDesc;
    }

    /**
     * Define o valor da propriedade praticaDesc.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPraticaDesc(String value) {
        this.praticaDesc = value;
    }

    /**
     * Obtém o valor da propriedade idInstancia.
     * 
     */
    public int getIDInstancia() {
        return idInstancia;
    }

    /**
     * Define o valor da propriedade idInstancia.
     * 
     */
    public void setIDInstancia(int value) {
        this.idInstancia = value;
    }

    /**
     * Obtém o valor da propriedade instanciaDesc.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInstanciaDesc() {
        return instanciaDesc;
    }

    /**
     * Define o valor da propriedade instanciaDesc.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInstanciaDesc(String value) {
        this.instanciaDesc = value;
    }

}
