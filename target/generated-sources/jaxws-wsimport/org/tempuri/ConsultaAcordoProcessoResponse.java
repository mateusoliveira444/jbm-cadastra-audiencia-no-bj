
package org.tempuri;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java de anonymous complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ResultadoAcordo" type="{http://tempuri.org/}ResultadoAcordo"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "resultadoAcordo"
})
@XmlRootElement(name = "ConsultaAcordoProcessoResponse")
public class ConsultaAcordoProcessoResponse {

    @XmlElement(name = "ResultadoAcordo", required = true, nillable = true)
    protected ResultadoAcordo resultadoAcordo;

    /**
     * Obtém o valor da propriedade resultadoAcordo.
     * 
     * @return
     *     possible object is
     *     {@link ResultadoAcordo }
     *     
     */
    public ResultadoAcordo getResultadoAcordo() {
        return resultadoAcordo;
    }

    /**
     * Define o valor da propriedade resultadoAcordo.
     * 
     * @param value
     *     allowed object is
     *     {@link ResultadoAcordo }
     *     
     */
    public void setResultadoAcordo(ResultadoAcordo value) {
        this.resultadoAcordo = value;
    }

}
