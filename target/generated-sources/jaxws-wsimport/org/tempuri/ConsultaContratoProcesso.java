
package org.tempuri;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java de anonymous complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="lstrUsuario" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="lstrSenha" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="lstrNome" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="lstrNumero" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "lstrUsuario",
    "lstrSenha",
    "lstrNome",
    "lstrNumero"
})
@XmlRootElement(name = "ConsultaContratoProcesso")
public class ConsultaContratoProcesso {

    protected String lstrUsuario;
    protected String lstrSenha;
    protected String lstrNome;
    protected String lstrNumero;

    /**
     * Obtém o valor da propriedade lstrUsuario.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLstrUsuario() {
        return lstrUsuario;
    }

    /**
     * Define o valor da propriedade lstrUsuario.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLstrUsuario(String value) {
        this.lstrUsuario = value;
    }

    /**
     * Obtém o valor da propriedade lstrSenha.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLstrSenha() {
        return lstrSenha;
    }

    /**
     * Define o valor da propriedade lstrSenha.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLstrSenha(String value) {
        this.lstrSenha = value;
    }

    /**
     * Obtém o valor da propriedade lstrNome.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLstrNome() {
        return lstrNome;
    }

    /**
     * Define o valor da propriedade lstrNome.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLstrNome(String value) {
        this.lstrNome = value;
    }

    /**
     * Obtém o valor da propriedade lstrNumero.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLstrNumero() {
        return lstrNumero;
    }

    /**
     * Define o valor da propriedade lstrNumero.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLstrNumero(String value) {
        this.lstrNumero = value;
    }

}
