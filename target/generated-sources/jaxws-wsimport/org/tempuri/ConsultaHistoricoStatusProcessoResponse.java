
package org.tempuri;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java de anonymous complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ConsultaHistoricoStatusProcessoResult" type="{http://tempuri.org/}ArrayOfHistoricoStatusProcesso" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "consultaHistoricoStatusProcessoResult"
})
@XmlRootElement(name = "ConsultaHistoricoStatusProcessoResponse")
public class ConsultaHistoricoStatusProcessoResponse {

    @XmlElement(name = "ConsultaHistoricoStatusProcessoResult")
    protected ArrayOfHistoricoStatusProcesso consultaHistoricoStatusProcessoResult;

    /**
     * Obtém o valor da propriedade consultaHistoricoStatusProcessoResult.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfHistoricoStatusProcesso }
     *     
     */
    public ArrayOfHistoricoStatusProcesso getConsultaHistoricoStatusProcessoResult() {
        return consultaHistoricoStatusProcessoResult;
    }

    /**
     * Define o valor da propriedade consultaHistoricoStatusProcessoResult.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfHistoricoStatusProcesso }
     *     
     */
    public void setConsultaHistoricoStatusProcessoResult(ArrayOfHistoricoStatusProcesso value) {
        this.consultaHistoricoStatusProcessoResult = value;
    }

}
