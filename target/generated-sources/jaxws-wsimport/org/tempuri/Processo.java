
package org.tempuri;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Classe Java de Processo complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="Processo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="IDProcessoUnico" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="NumeroProcesso" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ControleCliente" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Vara" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Ordinal" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="IDComarca" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="ParteInteressada" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ParteContraria" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Projudi" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="SuperEspecial" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="ProcessoEletronico" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="Pasta" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="NumeroAntigo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DataDistribuicao" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="ValorCausa" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Contrato" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="AdvogadoInteressado" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="AdvogadoInteressadoSec" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Instancia" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="Justica" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="PosicaoParteInteressada" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="AdvCoordenador" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="AdvOperacional" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Area" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Acao" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Cliente" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="IDStatusProcesso" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Pratica" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Materia" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DivisaoCliente" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="IDDecisao" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Foro" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="PossibilidadePerda" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="EnviaFornecedorPublicacao" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="RecebePublicacoes" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="IDSistema" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DataRecebimento" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="IDTipoVara" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ControleInterno" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="JusticaGratuita" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Processo", propOrder = {
    "idProcessoUnico",
    "numeroProcesso",
    "controleCliente",
    "vara",
    "ordinal",
    "idComarca",
    "parteInteressada",
    "parteContraria",
    "projudi",
    "superEspecial",
    "processoEletronico",
    "pasta",
    "numeroAntigo",
    "dataDistribuicao",
    "valorCausa",
    "contrato",
    "advogadoInteressado",
    "advogadoInteressadoSec",
    "instancia",
    "justica",
    "posicaoParteInteressada",
    "advCoordenador",
    "advOperacional",
    "area",
    "acao",
    "cliente",
    "idStatusProcesso",
    "pratica",
    "materia",
    "divisaoCliente",
    "idDecisao",
    "foro",
    "possibilidadePerda",
    "enviaFornecedorPublicacao",
    "recebePublicacoes",
    "idSistema",
    "dataRecebimento",
    "idTipoVara",
    "controleInterno",
    "justicaGratuita"
})
public class Processo {

    @XmlElement(name = "IDProcessoUnico")
    protected String idProcessoUnico;
    @XmlElement(name = "NumeroProcesso")
    protected String numeroProcesso;
    @XmlElement(name = "ControleCliente")
    protected String controleCliente;
    @XmlElement(name = "Vara")
    protected String vara;
    @XmlElement(name = "Ordinal")
    protected int ordinal;
    @XmlElement(name = "IDComarca")
    protected int idComarca;
    @XmlElement(name = "ParteInteressada")
    protected String parteInteressada;
    @XmlElement(name = "ParteContraria")
    protected String parteContraria;
    @XmlElement(name = "Projudi", required = true, type = Boolean.class, nillable = true)
    protected Boolean projudi;
    @XmlElement(name = "SuperEspecial", required = true, type = Boolean.class, nillable = true)
    protected Boolean superEspecial;
    @XmlElement(name = "ProcessoEletronico", required = true, type = Boolean.class, nillable = true)
    protected Boolean processoEletronico;
    @XmlElement(name = "Pasta")
    protected String pasta;
    @XmlElement(name = "NumeroAntigo")
    protected String numeroAntigo;
    @XmlElement(name = "DataDistribuicao", required = true, nillable = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar dataDistribuicao;
    @XmlElement(name = "ValorCausa")
    protected String valorCausa;
    @XmlElement(name = "Contrato")
    protected String contrato;
    @XmlElement(name = "AdvogadoInteressado")
    protected String advogadoInteressado;
    @XmlElement(name = "AdvogadoInteressadoSec")
    protected String advogadoInteressadoSec;
    @XmlElement(name = "Instancia")
    protected int instancia;
    @XmlElement(name = "Justica")
    protected String justica;
    @XmlElement(name = "PosicaoParteInteressada")
    protected String posicaoParteInteressada;
    @XmlElement(name = "AdvCoordenador")
    protected String advCoordenador;
    @XmlElement(name = "AdvOperacional")
    protected String advOperacional;
    @XmlElement(name = "Area")
    protected String area;
    @XmlElement(name = "Acao")
    protected String acao;
    @XmlElement(name = "Cliente")
    protected String cliente;
    @XmlElement(name = "IDStatusProcesso")
    protected String idStatusProcesso;
    @XmlElement(name = "Pratica")
    protected String pratica;
    @XmlElement(name = "Materia")
    protected String materia;
    @XmlElement(name = "DivisaoCliente")
    protected String divisaoCliente;
    @XmlElement(name = "IDDecisao")
    protected String idDecisao;
    @XmlElement(name = "Foro")
    protected String foro;
    @XmlElement(name = "PossibilidadePerda")
    protected String possibilidadePerda;
    @XmlElement(name = "EnviaFornecedorPublicacao")
    protected String enviaFornecedorPublicacao;
    @XmlElement(name = "RecebePublicacoes")
    protected String recebePublicacoes;
    @XmlElement(name = "IDSistema")
    protected String idSistema;
    @XmlElement(name = "DataRecebimento", required = true, nillable = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar dataRecebimento;
    @XmlElement(name = "IDTipoVara")
    protected String idTipoVara;
    @XmlElement(name = "ControleInterno")
    protected String controleInterno;
    @XmlElement(name = "JusticaGratuita")
    protected String justicaGratuita;

    /**
     * Obtém o valor da propriedade idProcessoUnico.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIDProcessoUnico() {
        return idProcessoUnico;
    }

    /**
     * Define o valor da propriedade idProcessoUnico.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIDProcessoUnico(String value) {
        this.idProcessoUnico = value;
    }

    /**
     * Obtém o valor da propriedade numeroProcesso.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNumeroProcesso() {
        return numeroProcesso;
    }

    /**
     * Define o valor da propriedade numeroProcesso.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNumeroProcesso(String value) {
        this.numeroProcesso = value;
    }

    /**
     * Obtém o valor da propriedade controleCliente.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getControleCliente() {
        return controleCliente;
    }

    /**
     * Define o valor da propriedade controleCliente.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setControleCliente(String value) {
        this.controleCliente = value;
    }

    /**
     * Obtém o valor da propriedade vara.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVara() {
        return vara;
    }

    /**
     * Define o valor da propriedade vara.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVara(String value) {
        this.vara = value;
    }

    /**
     * Obtém o valor da propriedade ordinal.
     * 
     */
    public int getOrdinal() {
        return ordinal;
    }

    /**
     * Define o valor da propriedade ordinal.
     * 
     */
    public void setOrdinal(int value) {
        this.ordinal = value;
    }

    /**
     * Obtém o valor da propriedade idComarca.
     * 
     */
    public int getIDComarca() {
        return idComarca;
    }

    /**
     * Define o valor da propriedade idComarca.
     * 
     */
    public void setIDComarca(int value) {
        this.idComarca = value;
    }

    /**
     * Obtém o valor da propriedade parteInteressada.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getParteInteressada() {
        return parteInteressada;
    }

    /**
     * Define o valor da propriedade parteInteressada.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setParteInteressada(String value) {
        this.parteInteressada = value;
    }

    /**
     * Obtém o valor da propriedade parteContraria.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getParteContraria() {
        return parteContraria;
    }

    /**
     * Define o valor da propriedade parteContraria.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setParteContraria(String value) {
        this.parteContraria = value;
    }

    /**
     * Obtém o valor da propriedade projudi.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isProjudi() {
        return projudi;
    }

    /**
     * Define o valor da propriedade projudi.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setProjudi(Boolean value) {
        this.projudi = value;
    }

    /**
     * Obtém o valor da propriedade superEspecial.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isSuperEspecial() {
        return superEspecial;
    }

    /**
     * Define o valor da propriedade superEspecial.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setSuperEspecial(Boolean value) {
        this.superEspecial = value;
    }

    /**
     * Obtém o valor da propriedade processoEletronico.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isProcessoEletronico() {
        return processoEletronico;
    }

    /**
     * Define o valor da propriedade processoEletronico.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setProcessoEletronico(Boolean value) {
        this.processoEletronico = value;
    }

    /**
     * Obtém o valor da propriedade pasta.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPasta() {
        return pasta;
    }

    /**
     * Define o valor da propriedade pasta.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPasta(String value) {
        this.pasta = value;
    }

    /**
     * Obtém o valor da propriedade numeroAntigo.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNumeroAntigo() {
        return numeroAntigo;
    }

    /**
     * Define o valor da propriedade numeroAntigo.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNumeroAntigo(String value) {
        this.numeroAntigo = value;
    }

    /**
     * Obtém o valor da propriedade dataDistribuicao.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDataDistribuicao() {
        return dataDistribuicao;
    }

    /**
     * Define o valor da propriedade dataDistribuicao.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDataDistribuicao(XMLGregorianCalendar value) {
        this.dataDistribuicao = value;
    }

    /**
     * Obtém o valor da propriedade valorCausa.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getValorCausa() {
        return valorCausa;
    }

    /**
     * Define o valor da propriedade valorCausa.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setValorCausa(String value) {
        this.valorCausa = value;
    }

    /**
     * Obtém o valor da propriedade contrato.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getContrato() {
        return contrato;
    }

    /**
     * Define o valor da propriedade contrato.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setContrato(String value) {
        this.contrato = value;
    }

    /**
     * Obtém o valor da propriedade advogadoInteressado.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAdvogadoInteressado() {
        return advogadoInteressado;
    }

    /**
     * Define o valor da propriedade advogadoInteressado.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAdvogadoInteressado(String value) {
        this.advogadoInteressado = value;
    }

    /**
     * Obtém o valor da propriedade advogadoInteressadoSec.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAdvogadoInteressadoSec() {
        return advogadoInteressadoSec;
    }

    /**
     * Define o valor da propriedade advogadoInteressadoSec.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAdvogadoInteressadoSec(String value) {
        this.advogadoInteressadoSec = value;
    }

    /**
     * Obtém o valor da propriedade instancia.
     * 
     */
    public int getInstancia() {
        return instancia;
    }

    /**
     * Define o valor da propriedade instancia.
     * 
     */
    public void setInstancia(int value) {
        this.instancia = value;
    }

    /**
     * Obtém o valor da propriedade justica.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getJustica() {
        return justica;
    }

    /**
     * Define o valor da propriedade justica.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setJustica(String value) {
        this.justica = value;
    }

    /**
     * Obtém o valor da propriedade posicaoParteInteressada.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPosicaoParteInteressada() {
        return posicaoParteInteressada;
    }

    /**
     * Define o valor da propriedade posicaoParteInteressada.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPosicaoParteInteressada(String value) {
        this.posicaoParteInteressada = value;
    }

    /**
     * Obtém o valor da propriedade advCoordenador.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAdvCoordenador() {
        return advCoordenador;
    }

    /**
     * Define o valor da propriedade advCoordenador.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAdvCoordenador(String value) {
        this.advCoordenador = value;
    }

    /**
     * Obtém o valor da propriedade advOperacional.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAdvOperacional() {
        return advOperacional;
    }

    /**
     * Define o valor da propriedade advOperacional.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAdvOperacional(String value) {
        this.advOperacional = value;
    }

    /**
     * Obtém o valor da propriedade area.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getArea() {
        return area;
    }

    /**
     * Define o valor da propriedade area.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setArea(String value) {
        this.area = value;
    }

    /**
     * Obtém o valor da propriedade acao.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAcao() {
        return acao;
    }

    /**
     * Define o valor da propriedade acao.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAcao(String value) {
        this.acao = value;
    }

    /**
     * Obtém o valor da propriedade cliente.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCliente() {
        return cliente;
    }

    /**
     * Define o valor da propriedade cliente.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCliente(String value) {
        this.cliente = value;
    }

    /**
     * Obtém o valor da propriedade idStatusProcesso.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIDStatusProcesso() {
        return idStatusProcesso;
    }

    /**
     * Define o valor da propriedade idStatusProcesso.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIDStatusProcesso(String value) {
        this.idStatusProcesso = value;
    }

    /**
     * Obtém o valor da propriedade pratica.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPratica() {
        return pratica;
    }

    /**
     * Define o valor da propriedade pratica.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPratica(String value) {
        this.pratica = value;
    }

    /**
     * Obtém o valor da propriedade materia.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMateria() {
        return materia;
    }

    /**
     * Define o valor da propriedade materia.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMateria(String value) {
        this.materia = value;
    }

    /**
     * Obtém o valor da propriedade divisaoCliente.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDivisaoCliente() {
        return divisaoCliente;
    }

    /**
     * Define o valor da propriedade divisaoCliente.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDivisaoCliente(String value) {
        this.divisaoCliente = value;
    }

    /**
     * Obtém o valor da propriedade idDecisao.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIDDecisao() {
        return idDecisao;
    }

    /**
     * Define o valor da propriedade idDecisao.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIDDecisao(String value) {
        this.idDecisao = value;
    }

    /**
     * Obtém o valor da propriedade foro.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getForo() {
        return foro;
    }

    /**
     * Define o valor da propriedade foro.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setForo(String value) {
        this.foro = value;
    }

    /**
     * Obtém o valor da propriedade possibilidadePerda.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPossibilidadePerda() {
        return possibilidadePerda;
    }

    /**
     * Define o valor da propriedade possibilidadePerda.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPossibilidadePerda(String value) {
        this.possibilidadePerda = value;
    }

    /**
     * Obtém o valor da propriedade enviaFornecedorPublicacao.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEnviaFornecedorPublicacao() {
        return enviaFornecedorPublicacao;
    }

    /**
     * Define o valor da propriedade enviaFornecedorPublicacao.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEnviaFornecedorPublicacao(String value) {
        this.enviaFornecedorPublicacao = value;
    }

    /**
     * Obtém o valor da propriedade recebePublicacoes.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRecebePublicacoes() {
        return recebePublicacoes;
    }

    /**
     * Define o valor da propriedade recebePublicacoes.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRecebePublicacoes(String value) {
        this.recebePublicacoes = value;
    }

    /**
     * Obtém o valor da propriedade idSistema.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIDSistema() {
        return idSistema;
    }

    /**
     * Define o valor da propriedade idSistema.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIDSistema(String value) {
        this.idSistema = value;
    }

    /**
     * Obtém o valor da propriedade dataRecebimento.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDataRecebimento() {
        return dataRecebimento;
    }

    /**
     * Define o valor da propriedade dataRecebimento.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDataRecebimento(XMLGregorianCalendar value) {
        this.dataRecebimento = value;
    }

    /**
     * Obtém o valor da propriedade idTipoVara.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIDTipoVara() {
        return idTipoVara;
    }

    /**
     * Define o valor da propriedade idTipoVara.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIDTipoVara(String value) {
        this.idTipoVara = value;
    }

    /**
     * Obtém o valor da propriedade controleInterno.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getControleInterno() {
        return controleInterno;
    }

    /**
     * Define o valor da propriedade controleInterno.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setControleInterno(String value) {
        this.controleInterno = value;
    }

    /**
     * Obtém o valor da propriedade justicaGratuita.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getJusticaGratuita() {
        return justicaGratuita;
    }

    /**
     * Define o valor da propriedade justicaGratuita.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setJusticaGratuita(String value) {
        this.justicaGratuita = value;
    }

}
