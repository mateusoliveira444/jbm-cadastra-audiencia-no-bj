
package org.tempuri;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Classe Java de RetornoProcesso complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="RetornoProcesso">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="RetornoIDSistema" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="RetornoSistema" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="RetornoSuperEspecial" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="RetornoIDProcessoUnico" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="RetornoNumeroProcesso" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="RetornoControleCliente" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="RetornoVara" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="RetornoOrdinal" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="RetornoComarca" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="RetornoUF" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="RetornoParteInteressada" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="RetornoParteContraria" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="RetornoProjudi" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="RetornoProcessoEletronico" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="RetornoPasta" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="RetornoNumeroAntigo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="RetornoDataDistribuicao" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="RetornoDataEncerramento" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="RetornoDataCadastro" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="RetornoValorCausa" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="RetornoControleInterno" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="RetornoAdvogadoInteressado" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="RetornoAdvogadoInteressadoSec" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="RetornoInstancia" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="RetornoJustica" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="RetornoPosicaoParteInteressada" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="RetornoAdvCoordenador" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="RetornoAdvOperacional" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="RetornoArea" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="RetornoAcao" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="RetornoCliente" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="RetornoStatusProcesso" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="RetornoPratica" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="RetornoMateria" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="RetornoDivisaoCliente" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="GrupoComplementar" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CampoComplementar" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DadoComplementar" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="RetornoEnviaFornecedorPublicacao" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="RetornoRecebePublicacoes" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="RetornoJusticaGratuita" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "RetornoProcesso", propOrder = {
    "retornoIDSistema",
    "retornoSistema",
    "retornoSuperEspecial",
    "retornoIDProcessoUnico",
    "retornoNumeroProcesso",
    "retornoControleCliente",
    "retornoVara",
    "retornoOrdinal",
    "retornoComarca",
    "retornoUF",
    "retornoParteInteressada",
    "retornoParteContraria",
    "retornoProjudi",
    "retornoProcessoEletronico",
    "retornoPasta",
    "retornoNumeroAntigo",
    "retornoDataDistribuicao",
    "retornoDataEncerramento",
    "retornoDataCadastro",
    "retornoValorCausa",
    "retornoControleInterno",
    "retornoAdvogadoInteressado",
    "retornoAdvogadoInteressadoSec",
    "retornoInstancia",
    "retornoJustica",
    "retornoPosicaoParteInteressada",
    "retornoAdvCoordenador",
    "retornoAdvOperacional",
    "retornoArea",
    "retornoAcao",
    "retornoCliente",
    "retornoStatusProcesso",
    "retornoPratica",
    "retornoMateria",
    "retornoDivisaoCliente",
    "grupoComplementar",
    "campoComplementar",
    "dadoComplementar",
    "retornoEnviaFornecedorPublicacao",
    "retornoRecebePublicacoes",
    "retornoJusticaGratuita"
})
public class RetornoProcesso {

    @XmlElement(name = "RetornoIDSistema")
    protected String retornoIDSistema;
    @XmlElement(name = "RetornoSistema")
    protected String retornoSistema;
    @XmlElement(name = "RetornoSuperEspecial")
    protected String retornoSuperEspecial;
    @XmlElement(name = "RetornoIDProcessoUnico")
    protected String retornoIDProcessoUnico;
    @XmlElement(name = "RetornoNumeroProcesso")
    protected String retornoNumeroProcesso;
    @XmlElement(name = "RetornoControleCliente")
    protected String retornoControleCliente;
    @XmlElement(name = "RetornoVara")
    protected String retornoVara;
    @XmlElement(name = "RetornoOrdinal")
    protected String retornoOrdinal;
    @XmlElement(name = "RetornoComarca")
    protected String retornoComarca;
    @XmlElement(name = "RetornoUF")
    protected String retornoUF;
    @XmlElement(name = "RetornoParteInteressada")
    protected String retornoParteInteressada;
    @XmlElement(name = "RetornoParteContraria")
    protected String retornoParteContraria;
    @XmlElement(name = "RetornoProjudi", required = true, type = Boolean.class, nillable = true)
    protected Boolean retornoProjudi;
    @XmlElement(name = "RetornoProcessoEletronico", required = true, type = Boolean.class, nillable = true)
    protected Boolean retornoProcessoEletronico;
    @XmlElement(name = "RetornoPasta")
    protected String retornoPasta;
    @XmlElement(name = "RetornoNumeroAntigo")
    protected String retornoNumeroAntigo;
    @XmlElement(name = "RetornoDataDistribuicao", required = true, nillable = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar retornoDataDistribuicao;
    @XmlElement(name = "RetornoDataEncerramento", required = true, nillable = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar retornoDataEncerramento;
    @XmlElement(name = "RetornoDataCadastro", required = true, nillable = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar retornoDataCadastro;
    @XmlElement(name = "RetornoValorCausa")
    protected String retornoValorCausa;
    @XmlElement(name = "RetornoControleInterno")
    protected String retornoControleInterno;
    @XmlElement(name = "RetornoAdvogadoInteressado")
    protected String retornoAdvogadoInteressado;
    @XmlElement(name = "RetornoAdvogadoInteressadoSec")
    protected String retornoAdvogadoInteressadoSec;
    @XmlElement(name = "RetornoInstancia")
    protected String retornoInstancia;
    @XmlElement(name = "RetornoJustica")
    protected String retornoJustica;
    @XmlElement(name = "RetornoPosicaoParteInteressada")
    protected String retornoPosicaoParteInteressada;
    @XmlElement(name = "RetornoAdvCoordenador")
    protected String retornoAdvCoordenador;
    @XmlElement(name = "RetornoAdvOperacional")
    protected String retornoAdvOperacional;
    @XmlElement(name = "RetornoArea")
    protected String retornoArea;
    @XmlElement(name = "RetornoAcao")
    protected String retornoAcao;
    @XmlElement(name = "RetornoCliente")
    protected String retornoCliente;
    @XmlElement(name = "RetornoStatusProcesso")
    protected String retornoStatusProcesso;
    @XmlElement(name = "RetornoPratica")
    protected String retornoPratica;
    @XmlElement(name = "RetornoMateria")
    protected String retornoMateria;
    @XmlElement(name = "RetornoDivisaoCliente")
    protected String retornoDivisaoCliente;
    @XmlElement(name = "GrupoComplementar")
    protected String grupoComplementar;
    @XmlElement(name = "CampoComplementar")
    protected String campoComplementar;
    @XmlElement(name = "DadoComplementar")
    protected String dadoComplementar;
    @XmlElement(name = "RetornoEnviaFornecedorPublicacao")
    protected String retornoEnviaFornecedorPublicacao;
    @XmlElement(name = "RetornoRecebePublicacoes")
    protected String retornoRecebePublicacoes;
    @XmlElement(name = "RetornoJusticaGratuita")
    protected String retornoJusticaGratuita;

    /**
     * Obtém o valor da propriedade retornoIDSistema.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRetornoIDSistema() {
        return retornoIDSistema;
    }

    /**
     * Define o valor da propriedade retornoIDSistema.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRetornoIDSistema(String value) {
        this.retornoIDSistema = value;
    }

    /**
     * Obtém o valor da propriedade retornoSistema.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRetornoSistema() {
        return retornoSistema;
    }

    /**
     * Define o valor da propriedade retornoSistema.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRetornoSistema(String value) {
        this.retornoSistema = value;
    }

    /**
     * Obtém o valor da propriedade retornoSuperEspecial.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRetornoSuperEspecial() {
        return retornoSuperEspecial;
    }

    /**
     * Define o valor da propriedade retornoSuperEspecial.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRetornoSuperEspecial(String value) {
        this.retornoSuperEspecial = value;
    }

    /**
     * Obtém o valor da propriedade retornoIDProcessoUnico.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRetornoIDProcessoUnico() {
        return retornoIDProcessoUnico;
    }

    /**
     * Define o valor da propriedade retornoIDProcessoUnico.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRetornoIDProcessoUnico(String value) {
        this.retornoIDProcessoUnico = value;
    }

    /**
     * Obtém o valor da propriedade retornoNumeroProcesso.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRetornoNumeroProcesso() {
        return retornoNumeroProcesso;
    }

    /**
     * Define o valor da propriedade retornoNumeroProcesso.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRetornoNumeroProcesso(String value) {
        this.retornoNumeroProcesso = value;
    }

    /**
     * Obtém o valor da propriedade retornoControleCliente.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRetornoControleCliente() {
        return retornoControleCliente;
    }

    /**
     * Define o valor da propriedade retornoControleCliente.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRetornoControleCliente(String value) {
        this.retornoControleCliente = value;
    }

    /**
     * Obtém o valor da propriedade retornoVara.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRetornoVara() {
        return retornoVara;
    }

    /**
     * Define o valor da propriedade retornoVara.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRetornoVara(String value) {
        this.retornoVara = value;
    }

    /**
     * Obtém o valor da propriedade retornoOrdinal.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRetornoOrdinal() {
        return retornoOrdinal;
    }

    /**
     * Define o valor da propriedade retornoOrdinal.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRetornoOrdinal(String value) {
        this.retornoOrdinal = value;
    }

    /**
     * Obtém o valor da propriedade retornoComarca.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRetornoComarca() {
        return retornoComarca;
    }

    /**
     * Define o valor da propriedade retornoComarca.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRetornoComarca(String value) {
        this.retornoComarca = value;
    }

    /**
     * Obtém o valor da propriedade retornoUF.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRetornoUF() {
        return retornoUF;
    }

    /**
     * Define o valor da propriedade retornoUF.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRetornoUF(String value) {
        this.retornoUF = value;
    }

    /**
     * Obtém o valor da propriedade retornoParteInteressada.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRetornoParteInteressada() {
        return retornoParteInteressada;
    }

    /**
     * Define o valor da propriedade retornoParteInteressada.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRetornoParteInteressada(String value) {
        this.retornoParteInteressada = value;
    }

    /**
     * Obtém o valor da propriedade retornoParteContraria.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRetornoParteContraria() {
        return retornoParteContraria;
    }

    /**
     * Define o valor da propriedade retornoParteContraria.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRetornoParteContraria(String value) {
        this.retornoParteContraria = value;
    }

    /**
     * Obtém o valor da propriedade retornoProjudi.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isRetornoProjudi() {
        return retornoProjudi;
    }

    /**
     * Define o valor da propriedade retornoProjudi.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setRetornoProjudi(Boolean value) {
        this.retornoProjudi = value;
    }

    /**
     * Obtém o valor da propriedade retornoProcessoEletronico.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isRetornoProcessoEletronico() {
        return retornoProcessoEletronico;
    }

    /**
     * Define o valor da propriedade retornoProcessoEletronico.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setRetornoProcessoEletronico(Boolean value) {
        this.retornoProcessoEletronico = value;
    }

    /**
     * Obtém o valor da propriedade retornoPasta.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRetornoPasta() {
        return retornoPasta;
    }

    /**
     * Define o valor da propriedade retornoPasta.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRetornoPasta(String value) {
        this.retornoPasta = value;
    }

    /**
     * Obtém o valor da propriedade retornoNumeroAntigo.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRetornoNumeroAntigo() {
        return retornoNumeroAntigo;
    }

    /**
     * Define o valor da propriedade retornoNumeroAntigo.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRetornoNumeroAntigo(String value) {
        this.retornoNumeroAntigo = value;
    }

    /**
     * Obtém o valor da propriedade retornoDataDistribuicao.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getRetornoDataDistribuicao() {
        return retornoDataDistribuicao;
    }

    /**
     * Define o valor da propriedade retornoDataDistribuicao.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setRetornoDataDistribuicao(XMLGregorianCalendar value) {
        this.retornoDataDistribuicao = value;
    }

    /**
     * Obtém o valor da propriedade retornoDataEncerramento.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getRetornoDataEncerramento() {
        return retornoDataEncerramento;
    }

    /**
     * Define o valor da propriedade retornoDataEncerramento.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setRetornoDataEncerramento(XMLGregorianCalendar value) {
        this.retornoDataEncerramento = value;
    }

    /**
     * Obtém o valor da propriedade retornoDataCadastro.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getRetornoDataCadastro() {
        return retornoDataCadastro;
    }

    /**
     * Define o valor da propriedade retornoDataCadastro.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setRetornoDataCadastro(XMLGregorianCalendar value) {
        this.retornoDataCadastro = value;
    }

    /**
     * Obtém o valor da propriedade retornoValorCausa.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRetornoValorCausa() {
        return retornoValorCausa;
    }

    /**
     * Define o valor da propriedade retornoValorCausa.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRetornoValorCausa(String value) {
        this.retornoValorCausa = value;
    }

    /**
     * Obtém o valor da propriedade retornoControleInterno.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRetornoControleInterno() {
        return retornoControleInterno;
    }

    /**
     * Define o valor da propriedade retornoControleInterno.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRetornoControleInterno(String value) {
        this.retornoControleInterno = value;
    }

    /**
     * Obtém o valor da propriedade retornoAdvogadoInteressado.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRetornoAdvogadoInteressado() {
        return retornoAdvogadoInteressado;
    }

    /**
     * Define o valor da propriedade retornoAdvogadoInteressado.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRetornoAdvogadoInteressado(String value) {
        this.retornoAdvogadoInteressado = value;
    }

    /**
     * Obtém o valor da propriedade retornoAdvogadoInteressadoSec.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRetornoAdvogadoInteressadoSec() {
        return retornoAdvogadoInteressadoSec;
    }

    /**
     * Define o valor da propriedade retornoAdvogadoInteressadoSec.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRetornoAdvogadoInteressadoSec(String value) {
        this.retornoAdvogadoInteressadoSec = value;
    }

    /**
     * Obtém o valor da propriedade retornoInstancia.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRetornoInstancia() {
        return retornoInstancia;
    }

    /**
     * Define o valor da propriedade retornoInstancia.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRetornoInstancia(String value) {
        this.retornoInstancia = value;
    }

    /**
     * Obtém o valor da propriedade retornoJustica.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRetornoJustica() {
        return retornoJustica;
    }

    /**
     * Define o valor da propriedade retornoJustica.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRetornoJustica(String value) {
        this.retornoJustica = value;
    }

    /**
     * Obtém o valor da propriedade retornoPosicaoParteInteressada.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRetornoPosicaoParteInteressada() {
        return retornoPosicaoParteInteressada;
    }

    /**
     * Define o valor da propriedade retornoPosicaoParteInteressada.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRetornoPosicaoParteInteressada(String value) {
        this.retornoPosicaoParteInteressada = value;
    }

    /**
     * Obtém o valor da propriedade retornoAdvCoordenador.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRetornoAdvCoordenador() {
        return retornoAdvCoordenador;
    }

    /**
     * Define o valor da propriedade retornoAdvCoordenador.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRetornoAdvCoordenador(String value) {
        this.retornoAdvCoordenador = value;
    }

    /**
     * Obtém o valor da propriedade retornoAdvOperacional.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRetornoAdvOperacional() {
        return retornoAdvOperacional;
    }

    /**
     * Define o valor da propriedade retornoAdvOperacional.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRetornoAdvOperacional(String value) {
        this.retornoAdvOperacional = value;
    }

    /**
     * Obtém o valor da propriedade retornoArea.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRetornoArea() {
        return retornoArea;
    }

    /**
     * Define o valor da propriedade retornoArea.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRetornoArea(String value) {
        this.retornoArea = value;
    }

    /**
     * Obtém o valor da propriedade retornoAcao.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRetornoAcao() {
        return retornoAcao;
    }

    /**
     * Define o valor da propriedade retornoAcao.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRetornoAcao(String value) {
        this.retornoAcao = value;
    }

    /**
     * Obtém o valor da propriedade retornoCliente.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRetornoCliente() {
        return retornoCliente;
    }

    /**
     * Define o valor da propriedade retornoCliente.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRetornoCliente(String value) {
        this.retornoCliente = value;
    }

    /**
     * Obtém o valor da propriedade retornoStatusProcesso.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRetornoStatusProcesso() {
        return retornoStatusProcesso;
    }

    /**
     * Define o valor da propriedade retornoStatusProcesso.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRetornoStatusProcesso(String value) {
        this.retornoStatusProcesso = value;
    }

    /**
     * Obtém o valor da propriedade retornoPratica.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRetornoPratica() {
        return retornoPratica;
    }

    /**
     * Define o valor da propriedade retornoPratica.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRetornoPratica(String value) {
        this.retornoPratica = value;
    }

    /**
     * Obtém o valor da propriedade retornoMateria.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRetornoMateria() {
        return retornoMateria;
    }

    /**
     * Define o valor da propriedade retornoMateria.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRetornoMateria(String value) {
        this.retornoMateria = value;
    }

    /**
     * Obtém o valor da propriedade retornoDivisaoCliente.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRetornoDivisaoCliente() {
        return retornoDivisaoCliente;
    }

    /**
     * Define o valor da propriedade retornoDivisaoCliente.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRetornoDivisaoCliente(String value) {
        this.retornoDivisaoCliente = value;
    }

    /**
     * Obtém o valor da propriedade grupoComplementar.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGrupoComplementar() {
        return grupoComplementar;
    }

    /**
     * Define o valor da propriedade grupoComplementar.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGrupoComplementar(String value) {
        this.grupoComplementar = value;
    }

    /**
     * Obtém o valor da propriedade campoComplementar.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCampoComplementar() {
        return campoComplementar;
    }

    /**
     * Define o valor da propriedade campoComplementar.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCampoComplementar(String value) {
        this.campoComplementar = value;
    }

    /**
     * Obtém o valor da propriedade dadoComplementar.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDadoComplementar() {
        return dadoComplementar;
    }

    /**
     * Define o valor da propriedade dadoComplementar.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDadoComplementar(String value) {
        this.dadoComplementar = value;
    }

    /**
     * Obtém o valor da propriedade retornoEnviaFornecedorPublicacao.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRetornoEnviaFornecedorPublicacao() {
        return retornoEnviaFornecedorPublicacao;
    }

    /**
     * Define o valor da propriedade retornoEnviaFornecedorPublicacao.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRetornoEnviaFornecedorPublicacao(String value) {
        this.retornoEnviaFornecedorPublicacao = value;
    }

    /**
     * Obtém o valor da propriedade retornoRecebePublicacoes.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRetornoRecebePublicacoes() {
        return retornoRecebePublicacoes;
    }

    /**
     * Define o valor da propriedade retornoRecebePublicacoes.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRetornoRecebePublicacoes(String value) {
        this.retornoRecebePublicacoes = value;
    }

    /**
     * Obtém o valor da propriedade retornoJusticaGratuita.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRetornoJusticaGratuita() {
        return retornoJusticaGratuita;
    }

    /**
     * Define o valor da propriedade retornoJusticaGratuita.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRetornoJusticaGratuita(String value) {
        this.retornoJusticaGratuita = value;
    }

}
