
package org.tempuri;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the org.tempuri package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _ArrayOfStepSolicitacao_QNAME = new QName("http://tempuri.org/", "ArrayOfStepSolicitacao");
    private final static QName _ArrayOfCelulaCnpj_QNAME = new QName("http://tempuri.org/", "ArrayOfCelulaCnpj");
    private final static QName _ArrayOfResultadoRetornaDiligencias_QNAME = new QName("http://tempuri.org/", "ArrayOfResultadoRetornaDiligencias");
    private final static QName _ArrayOfSolicitacaoRetorno_QNAME = new QName("http://tempuri.org/", "ArrayOfSolicitacaoRetorno");
    private final static QName _ArrayOfComarca_QNAME = new QName("http://tempuri.org/", "ArrayOfComarca");
    private final static QName _ArrayOfLog_QNAME = new QName("http://tempuri.org/", "ArrayOfLog");
    private final static QName _ArrayOfTipoParte_QNAME = new QName("http://tempuri.org/", "ArrayOfTipoParte");
    private final static QName _ArrayOfOperacaoFinanceira_QNAME = new QName("http://tempuri.org/", "ArrayOfOperacaoFinanceira");
    private final static QName _ArrayOfRetornoProcesso_QNAME = new QName("http://tempuri.org/", "ArrayOfRetornoProcesso");
    private final static QName _ArrayOfResultadoRetornoAgendamento_QNAME = new QName("http://tempuri.org/", "ArrayOfResultadoRetornoAgendamento");
    private final static QName _ArrayOfStatusCusta_QNAME = new QName("http://tempuri.org/", "ArrayOfStatusCusta");
    private final static QName _ResultadoAcordo_QNAME = new QName("http://tempuri.org/", "ResultadoAcordo");
    private final static QName _ArrayOfMateria_QNAME = new QName("http://tempuri.org/", "ArrayOfMateria");
    private final static QName _SolicitacaoRetorno_QNAME = new QName("http://tempuri.org/", "SolicitacaoRetorno");
    private final static QName _ArrayOfMotivoPublicacao_QNAME = new QName("http://tempuri.org/", "ArrayOfMotivoPublicacao");
    private final static QName _ArrayOfAcao_QNAME = new QName("http://tempuri.org/", "ArrayOfAcao");
    private final static QName _ResultadoRetornoApenso_QNAME = new QName("http://tempuri.org/", "ResultadoRetornoApenso");
    private final static QName _ArrayOfRetornoTipoFollowUP_QNAME = new QName("http://tempuri.org/", "ArrayOfRetornoTipoFollowUP");
    private final static QName _ArrayOfJustica_QNAME = new QName("http://tempuri.org/", "ArrayOfJustica");
    private final static QName _ArrayOfLote_QNAME = new QName("http://tempuri.org/", "ArrayOfLote");
    private final static QName _ArrayOfTipoSolicitacao_QNAME = new QName("http://tempuri.org/", "ArrayOfTipoSolicitacao");
    private final static QName _ArrayOfStatusProcesso_QNAME = new QName("http://tempuri.org/", "ArrayOfStatusProcesso");
    private final static QName _ArrayOfTipoVara_QNAME = new QName("http://tempuri.org/", "ArrayOfTipoVara");
    private final static QName _ArrayOfPratica_QNAME = new QName("http://tempuri.org/", "ArrayOfPratica");
    private final static QName _ArrayOfHistoricoStatusProcesso_QNAME = new QName("http://tempuri.org/", "ArrayOfHistoricoStatusProcesso");
    private final static QName _ArrayOfForo_QNAME = new QName("http://tempuri.org/", "ArrayOfForo");
    private final static QName _ArrayOfMovimento_QNAME = new QName("http://tempuri.org/", "ArrayOfMovimento");
    private final static QName _ArrayOfContatos_QNAME = new QName("http://tempuri.org/", "ArrayOfContatos");
    private final static QName _ArrayOfCustaArquivo_QNAME = new QName("http://tempuri.org/", "ArrayOfCustaArquivo");
    private final static QName _ArrayOfAndamento_QNAME = new QName("http://tempuri.org/", "ArrayOfAndamento");
    private final static QName _ArrayOfResultadoRetornaAgendamentoPorTipo_QNAME = new QName("http://tempuri.org/", "ArrayOfResultadoRetornaAgendamentoPorTipo");
    private final static QName _ArrayOfRetornoPublicacaoDescartada_QNAME = new QName("http://tempuri.org/", "ArrayOfRetornoPublicacaoDescartada");
    private final static QName _ArrayOfDivisaoCliente_QNAME = new QName("http://tempuri.org/", "ArrayOfDivisaoCliente");
    private final static QName _ResultadoProcesso_QNAME = new QName("http://tempuri.org/", "ResultadoProcesso");
    private final static QName _ArrayOfNewDataSet_QNAME = new QName("http://tempuri.org/", "ArrayOfNewDataSet");
    private final static QName _ArrayOfDecisao_QNAME = new QName("http://tempuri.org/", "ArrayOfDecisao");
    private final static QName _ArrayOfTipoCusta_QNAME = new QName("http://tempuri.org/", "ArrayOfTipoCusta");
    private final static QName _ArrayOfCusta_QNAME = new QName("http://tempuri.org/", "ArrayOfCusta");
    private final static QName _ArrayOfContratoProcesso_QNAME = new QName("http://tempuri.org/", "ArrayOfContratoProcesso");
    private final static QName _ArrayOfVara_QNAME = new QName("http://tempuri.org/", "ArrayOfVara");
    private final static QName _ArrayOfRetornoDadoComplementar_QNAME = new QName("http://tempuri.org/", "ArrayOfRetornoDadoComplementar");
    private final static QName _ResultadoTamanhoArquivo_QNAME = new QName("http://tempuri.org/", "ResultadoTamanhoArquivo");
    private final static QName _ArrayOfSistema_QNAME = new QName("http://tempuri.org/", "ArrayOfSistema");
    private final static QName _ArrayOfTrocaSubTipoSolicitacao_QNAME = new QName("http://tempuri.org/", "ArrayOfTrocaSubTipoSolicitacao");
    private final static QName _ArrayOfOpcoesCampoLista_QNAME = new QName("http://tempuri.org/", "ArrayOfOpcoesCampoLista");
    private final static QName _String_QNAME = new QName("http://tempuri.org/", "string");
    private final static QName _ArrayOfRetornoDesdobramento_QNAME = new QName("http://tempuri.org/", "ArrayOfRetornoDesdobramento");
    private final static QName _ArrayOfRetornoGarantia_QNAME = new QName("http://tempuri.org/", "ArrayOfRetornoGarantia");
    private final static QName _ArrayOfRespostaSolicitacao_QNAME = new QName("http://tempuri.org/", "ArrayOfRespostaSolicitacao");
    private final static QName _ArrayOfInstancia_QNAME = new QName("http://tempuri.org/", "ArrayOfInstancia");
    private final static QName _ArrayOfArquivo_QNAME = new QName("http://tempuri.org/", "ArrayOfArquivo");
    private final static QName _ArrayOfRetornoAgendamento_QNAME = new QName("http://tempuri.org/", "ArrayOfRetornoAgendamento");
    private final static QName _RetornoMetodos_QNAME = new QName("http://tempuri.org/", "RetornoMetodos");
    private final static QName _ArrayOfResultadoRetornaAgendamento_QNAME = new QName("http://tempuri.org/", "ArrayOfResultadoRetornaAgendamento");
    private final static QName _ArrayOfTipoAgendamento_QNAME = new QName("http://tempuri.org/", "ArrayOfTipoAgendamento");
    private final static QName _ArrayOfRetornoPublicacao_QNAME = new QName("http://tempuri.org/", "ArrayOfRetornoPublicacao");
    private final static QName _ArrayOfParteProcesso_QNAME = new QName("http://tempuri.org/", "ArrayOfParteProcesso");
    private final static QName _ArrayOfRetornoCorrespondente_QNAME = new QName("http://tempuri.org/", "ArrayOfRetornoCorrespondente");
    private final static QName _ArrayOfRetornoFollowUP_QNAME = new QName("http://tempuri.org/", "ArrayOfRetornoFollowUP");
    private final static QName _ArrayOfAtividade_QNAME = new QName("http://tempuri.org/", "ArrayOfAtividade");
    private final static QName _ArrayOfResultadoRetornaAuditoriaResultado_QNAME = new QName("http://tempuri.org/", "ArrayOfResultadoRetornaAuditoriaResultado");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: org.tempuri
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link RecuperarStatusProcessoResponse }
     * 
     */
    public RecuperarStatusProcessoResponse createRecuperarStatusProcessoResponse() {
        return new RecuperarStatusProcessoResponse();
    }

    /**
     * Create an instance of {@link ArrayOfStatusProcesso }
     * 
     */
    public ArrayOfStatusProcesso createArrayOfStatusProcesso() {
        return new ArrayOfStatusProcesso();
    }

    /**
     * Create an instance of {@link DevolverPublicacao }
     * 
     */
    public DevolverPublicacao createDevolverPublicacao() {
        return new DevolverPublicacao();
    }

    /**
     * Create an instance of {@link ExcluirAcordoProcesso }
     * 
     */
    public ExcluirAcordoProcesso createExcluirAcordoProcesso() {
        return new ExcluirAcordoProcesso();
    }

    /**
     * Create an instance of {@link SolicitacaoRetorno }
     * 
     */
    public SolicitacaoRetorno createSolicitacaoRetorno() {
        return new SolicitacaoRetorno();
    }

    /**
     * Create an instance of {@link ArrayOfRetornoTipoFollowUP }
     * 
     */
    public ArrayOfRetornoTipoFollowUP createArrayOfRetornoTipoFollowUP() {
        return new ArrayOfRetornoTipoFollowUP();
    }

    /**
     * Create an instance of {@link ConsultaApenso }
     * 
     */
    public ConsultaApenso createConsultaApenso() {
        return new ConsultaApenso();
    }

    /**
     * Create an instance of {@link AtualizarCamposDiligencia }
     * 
     */
    public AtualizarCamposDiligencia createAtualizarCamposDiligencia() {
        return new AtualizarCamposDiligencia();
    }

    /**
     * Create an instance of {@link InsereDadoComplementarProcessoLoteResponse }
     * 
     */
    public InsereDadoComplementarProcessoLoteResponse createInsereDadoComplementarProcessoLoteResponse() {
        return new InsereDadoComplementarProcessoLoteResponse();
    }

    /**
     * Create an instance of {@link RetornoMetodos }
     * 
     */
    public RetornoMetodos createRetornoMetodos() {
        return new RetornoMetodos();
    }

    /**
     * Create an instance of {@link ArrayOfCelulaCnpj }
     * 
     */
    public ArrayOfCelulaCnpj createArrayOfCelulaCnpj() {
        return new ArrayOfCelulaCnpj();
    }

    /**
     * Create an instance of {@link ArrayOfResultadoRetornaDiligencias }
     * 
     */
    public ArrayOfResultadoRetornaDiligencias createArrayOfResultadoRetornaDiligencias() {
        return new ArrayOfResultadoRetornaDiligencias();
    }

    /**
     * Create an instance of {@link CancelarCusta }
     * 
     */
    public CancelarCusta createCancelarCusta() {
        return new CancelarCusta();
    }

    /**
     * Create an instance of {@link ArrayOfStepSolicitacao }
     * 
     */
    public ArrayOfStepSolicitacao createArrayOfStepSolicitacao() {
        return new ArrayOfStepSolicitacao();
    }

    /**
     * Create an instance of {@link RetornaAgendamentoDataResponse }
     * 
     */
    public RetornaAgendamentoDataResponse createRetornaAgendamentoDataResponse() {
        return new RetornaAgendamentoDataResponse();
    }

    /**
     * Create an instance of {@link ArrayOfResultadoRetornoAgendamento }
     * 
     */
    public ArrayOfResultadoRetornoAgendamento createArrayOfResultadoRetornoAgendamento() {
        return new ArrayOfResultadoRetornoAgendamento();
    }

    /**
     * Create an instance of {@link ConsultaSolicitacaoDataConclusaoResponse }
     * 
     */
    public ConsultaSolicitacaoDataConclusaoResponse createConsultaSolicitacaoDataConclusaoResponse() {
        return new ConsultaSolicitacaoDataConclusaoResponse();
    }

    /**
     * Create an instance of {@link ArrayOfSolicitacaoRetorno }
     * 
     */
    public ArrayOfSolicitacaoRetorno createArrayOfSolicitacaoRetorno() {
        return new ArrayOfSolicitacaoRetorno();
    }

    /**
     * Create an instance of {@link RecuperarRespostaSolicitacaoResponse }
     * 
     */
    public RecuperarRespostaSolicitacaoResponse createRecuperarRespostaSolicitacaoResponse() {
        return new RecuperarRespostaSolicitacaoResponse();
    }

    /**
     * Create an instance of {@link ArrayOfRespostaSolicitacao }
     * 
     */
    public ArrayOfRespostaSolicitacao createArrayOfRespostaSolicitacao() {
        return new ArrayOfRespostaSolicitacao();
    }

    /**
     * Create an instance of {@link ArrayOfStatusCusta }
     * 
     */
    public ArrayOfStatusCusta createArrayOfStatusCusta() {
        return new ArrayOfStatusCusta();
    }

    /**
     * Create an instance of {@link RetornaAgendamentoProcessoResponse }
     * 
     */
    public RetornaAgendamentoProcessoResponse createRetornaAgendamentoProcessoResponse() {
        return new RetornaAgendamentoProcessoResponse();
    }

    /**
     * Create an instance of {@link ArrayOfRetornoAgendamento }
     * 
     */
    public ArrayOfRetornoAgendamento createArrayOfRetornoAgendamento() {
        return new ArrayOfRetornoAgendamento();
    }

    /**
     * Create an instance of {@link ConsultaSolicitacaoDataConclusao }
     * 
     */
    public ConsultaSolicitacaoDataConclusao createConsultaSolicitacaoDataConclusao() {
        return new ConsultaSolicitacaoDataConclusao();
    }

    /**
     * Create an instance of {@link ArrayOfForo }
     * 
     */
    public ArrayOfForo createArrayOfForo() {
        return new ArrayOfForo();
    }

    /**
     * Create an instance of {@link ArrayOfHistoricoStatusProcesso }
     * 
     */
    public ArrayOfHistoricoStatusProcesso createArrayOfHistoricoStatusProcesso() {
        return new ArrayOfHistoricoStatusProcesso();
    }

    /**
     * Create an instance of {@link AlterarCelulaProcesso }
     * 
     */
    public AlterarCelulaProcesso createAlterarCelulaProcesso() {
        return new AlterarCelulaProcesso();
    }

    /**
     * Create an instance of {@link ConsultaProcessoID }
     * 
     */
    public ConsultaProcessoID createConsultaProcessoID() {
        return new ConsultaProcessoID();
    }

    /**
     * Create an instance of {@link ConsultaContatoResponse }
     * 
     */
    public ConsultaContatoResponse createConsultaContatoResponse() {
        return new ConsultaContatoResponse();
    }

    /**
     * Create an instance of {@link ArrayOfContatos }
     * 
     */
    public ArrayOfContatos createArrayOfContatos() {
        return new ArrayOfContatos();
    }

    /**
     * Create an instance of {@link AlterarProcessoResponse }
     * 
     */
    public AlterarProcessoResponse createAlterarProcessoResponse() {
        return new AlterarProcessoResponse();
    }

    /**
     * Create an instance of {@link AtualizarPublicacaoEnviadaConfirmacaoCliente }
     * 
     */
    public AtualizarPublicacaoEnviadaConfirmacaoCliente createAtualizarPublicacaoEnviadaConfirmacaoCliente() {
        return new AtualizarPublicacaoEnviadaConfirmacaoCliente();
    }

    /**
     * Create an instance of {@link RecuperarMateriaResponse }
     * 
     */
    public RecuperarMateriaResponse createRecuperarMateriaResponse() {
        return new RecuperarMateriaResponse();
    }

    /**
     * Create an instance of {@link ArrayOfMateria }
     * 
     */
    public ArrayOfMateria createArrayOfMateria() {
        return new ArrayOfMateria();
    }

    /**
     * Create an instance of {@link ConsultaParteProcessoResponse }
     * 
     */
    public ConsultaParteProcessoResponse createConsultaParteProcessoResponse() {
        return new ConsultaParteProcessoResponse();
    }

    /**
     * Create an instance of {@link ArrayOfParteProcesso }
     * 
     */
    public ArrayOfParteProcesso createArrayOfParteProcesso() {
        return new ArrayOfParteProcesso();
    }

    /**
     * Create an instance of {@link DividirArquivoPorTamanho }
     * 
     */
    public DividirArquivoPorTamanho createDividirArquivoPorTamanho() {
        return new DividirArquivoPorTamanho();
    }

    /**
     * Create an instance of {@link ConsultaSolicitacaoDataSolicitacaoResponse }
     * 
     */
    public ConsultaSolicitacaoDataSolicitacaoResponse createConsultaSolicitacaoDataSolicitacaoResponse() {
        return new ConsultaSolicitacaoDataSolicitacaoResponse();
    }

    /**
     * Create an instance of {@link RecuperarJustica }
     * 
     */
    public RecuperarJustica createRecuperarJustica() {
        return new RecuperarJustica();
    }

    /**
     * Create an instance of {@link AtualizaCustaRobo }
     * 
     */
    public AtualizaCustaRobo createAtualizaCustaRobo() {
        return new AtualizaCustaRobo();
    }

    /**
     * Create an instance of {@link ConsultaTiposFollowUP }
     * 
     */
    public ConsultaTiposFollowUP createConsultaTiposFollowUP() {
        return new ConsultaTiposFollowUP();
    }

    /**
     * Create an instance of {@link RecuperarComarca }
     * 
     */
    public RecuperarComarca createRecuperarComarca() {
        return new RecuperarComarca();
    }

    /**
     * Create an instance of {@link InserirDesdobramentoResponse }
     * 
     */
    public InserirDesdobramentoResponse createInserirDesdobramentoResponse() {
        return new InserirDesdobramentoResponse();
    }

    /**
     * Create an instance of {@link ArrayOfRetornoDesdobramento }
     * 
     */
    public ArrayOfRetornoDesdobramento createArrayOfRetornoDesdobramento() {
        return new ArrayOfRetornoDesdobramento();
    }

    /**
     * Create an instance of {@link ProcessarPublicacao }
     * 
     */
    public ProcessarPublicacao createProcessarPublicacao() {
        return new ProcessarPublicacao();
    }

    /**
     * Create an instance of {@link RecuperarRespostaSolicitacao }
     * 
     */
    public RecuperarRespostaSolicitacao createRecuperarRespostaSolicitacao() {
        return new RecuperarRespostaSolicitacao();
    }

    /**
     * Create an instance of {@link ArrayOfRetornoGarantia }
     * 
     */
    public ArrayOfRetornoGarantia createArrayOfRetornoGarantia() {
        return new ArrayOfRetornoGarantia();
    }

    /**
     * Create an instance of {@link InserirAgendamentoSincronizadoResponse }
     * 
     */
    public InserirAgendamentoSincronizadoResponse createInserirAgendamentoSincronizadoResponse() {
        return new InserirAgendamentoSincronizadoResponse();
    }

    /**
     * Create an instance of {@link ArrayOfResultadoRetornaAgendamentoPorTipo }
     * 
     */
    public ArrayOfResultadoRetornaAgendamentoPorTipo createArrayOfResultadoRetornaAgendamentoPorTipo() {
        return new ArrayOfResultadoRetornaAgendamentoPorTipo();
    }

    /**
     * Create an instance of {@link RetornarContagemDiaria }
     * 
     */
    public RetornarContagemDiaria createRetornarContagemDiaria() {
        return new RetornarContagemDiaria();
    }

    /**
     * Create an instance of {@link InserirProcesso }
     * 
     */
    public InserirProcesso createInserirProcesso() {
        return new InserirProcesso();
    }

    /**
     * Create an instance of {@link Processo }
     * 
     */
    public Processo createProcesso() {
        return new Processo();
    }

    /**
     * Create an instance of {@link InserirCustaResponse }
     * 
     */
    public InserirCustaResponse createInserirCustaResponse() {
        return new InserirCustaResponse();
    }

    /**
     * Create an instance of {@link CancelarSolicitacaoResponse }
     * 
     */
    public CancelarSolicitacaoResponse createCancelarSolicitacaoResponse() {
        return new CancelarSolicitacaoResponse();
    }

    /**
     * Create an instance of {@link InserirGarantiaResponse }
     * 
     */
    public InserirGarantiaResponse createInserirGarantiaResponse() {
        return new InserirGarantiaResponse();
    }

    /**
     * Create an instance of {@link ConsultaDadoComplementarDisponivelResponse }
     * 
     */
    public ConsultaDadoComplementarDisponivelResponse createConsultaDadoComplementarDisponivelResponse() {
        return new ConsultaDadoComplementarDisponivelResponse();
    }

    /**
     * Create an instance of {@link ArrayOfRetornoDadoComplementar }
     * 
     */
    public ArrayOfRetornoDadoComplementar createArrayOfRetornoDadoComplementar() {
        return new ArrayOfRetornoDadoComplementar();
    }

    /**
     * Create an instance of {@link ConsultaParteProcesso }
     * 
     */
    public ConsultaParteProcesso createConsultaParteProcesso() {
        return new ConsultaParteProcesso();
    }

    /**
     * Create an instance of {@link ConsultaProcesso }
     * 
     */
    public ConsultaProcesso createConsultaProcesso() {
        return new ConsultaProcesso();
    }

    /**
     * Create an instance of {@link ArrayOfContratoProcesso }
     * 
     */
    public ArrayOfContratoProcesso createArrayOfContratoProcesso() {
        return new ArrayOfContratoProcesso();
    }

    /**
     * Create an instance of {@link ConsultaOperacaoFinanceira }
     * 
     */
    public ConsultaOperacaoFinanceira createConsultaOperacaoFinanceira() {
        return new ConsultaOperacaoFinanceira();
    }

    /**
     * Create an instance of {@link ArrayOfRetornoPublicacao }
     * 
     */
    public ArrayOfRetornoPublicacao createArrayOfRetornoPublicacao() {
        return new ArrayOfRetornoPublicacao();
    }

    /**
     * Create an instance of {@link RecuperarMotivoDevolucaoPublicacaoResponse }
     * 
     */
    public RecuperarMotivoDevolucaoPublicacaoResponse createRecuperarMotivoDevolucaoPublicacaoResponse() {
        return new RecuperarMotivoDevolucaoPublicacaoResponse();
    }

    /**
     * Create an instance of {@link ArrayOfMotivoPublicacao }
     * 
     */
    public ArrayOfMotivoPublicacao createArrayOfMotivoPublicacao() {
        return new ArrayOfMotivoPublicacao();
    }

    /**
     * Create an instance of {@link InserirCusta }
     * 
     */
    public InserirCusta createInserirCusta() {
        return new InserirCusta();
    }

    /**
     * Create an instance of {@link ConsultaLoteCusta }
     * 
     */
    public ConsultaLoteCusta createConsultaLoteCusta() {
        return new ConsultaLoteCusta();
    }

    /**
     * Create an instance of {@link ArrayOfRetornoFollowUP }
     * 
     */
    public ArrayOfRetornoFollowUP createArrayOfRetornoFollowUP() {
        return new ArrayOfRetornoFollowUP();
    }

    /**
     * Create an instance of {@link RetornaAgendamentoData }
     * 
     */
    public RetornaAgendamentoData createRetornaAgendamentoData() {
        return new RetornaAgendamentoData();
    }

    /**
     * Create an instance of {@link AtualizarCamposDiligenciaResponse }
     * 
     */
    public AtualizarCamposDiligenciaResponse createAtualizarCamposDiligenciaResponse() {
        return new AtualizarCamposDiligenciaResponse();
    }

    /**
     * Create an instance of {@link ConsultaFollowUP }
     * 
     */
    public ConsultaFollowUP createConsultaFollowUP() {
        return new ConsultaFollowUP();
    }

    /**
     * Create an instance of {@link InsereDadoComplementarProcessoLote }
     * 
     */
    public InsereDadoComplementarProcessoLote createInsereDadoComplementarProcessoLote() {
        return new InsereDadoComplementarProcessoLote();
    }

    /**
     * Create an instance of {@link ArrayOfDadoComplementar }
     * 
     */
    public ArrayOfDadoComplementar createArrayOfDadoComplementar() {
        return new ArrayOfDadoComplementar();
    }

    /**
     * Create an instance of {@link InserirDesdobramento }
     * 
     */
    public InserirDesdobramento createInserirDesdobramento() {
        return new InserirDesdobramento();
    }

    /**
     * Create an instance of {@link Desdobramento }
     * 
     */
    public Desdobramento createDesdobramento() {
        return new Desdobramento();
    }

    /**
     * Create an instance of {@link InserirProcessoResponse }
     * 
     */
    public InserirProcessoResponse createInserirProcessoResponse() {
        return new InserirProcessoResponse();
    }

    /**
     * Create an instance of {@link DevolverPublicacaoResponse }
     * 
     */
    public DevolverPublicacaoResponse createDevolverPublicacaoResponse() {
        return new DevolverPublicacaoResponse();
    }

    /**
     * Create an instance of {@link RetornaPublicacaoDescartada }
     * 
     */
    public RetornaPublicacaoDescartada createRetornaPublicacaoDescartada() {
        return new RetornaPublicacaoDescartada();
    }

    /**
     * Create an instance of {@link ConsultaApensoResponse }
     * 
     */
    public ConsultaApensoResponse createConsultaApensoResponse() {
        return new ConsultaApensoResponse();
    }

    /**
     * Create an instance of {@link ResultadoRetornoApenso }
     * 
     */
    public ResultadoRetornoApenso createResultadoRetornoApenso() {
        return new ResultadoRetornoApenso();
    }

    /**
     * Create an instance of {@link RecuperarDecisao }
     * 
     */
    public RecuperarDecisao createRecuperarDecisao() {
        return new RecuperarDecisao();
    }

    /**
     * Create an instance of {@link RetornarAuditoriaResultadoResponse }
     * 
     */
    public RetornarAuditoriaResultadoResponse createRetornarAuditoriaResultadoResponse() {
        return new RetornarAuditoriaResultadoResponse();
    }

    /**
     * Create an instance of {@link ArrayOfResultadoRetornaAuditoriaResultado }
     * 
     */
    public ArrayOfResultadoRetornaAuditoriaResultado createArrayOfResultadoRetornaAuditoriaResultado() {
        return new ArrayOfResultadoRetornaAuditoriaResultado();
    }

    /**
     * Create an instance of {@link RecuperarPraticaResponse }
     * 
     */
    public RecuperarPraticaResponse createRecuperarPraticaResponse() {
        return new RecuperarPraticaResponse();
    }

    /**
     * Create an instance of {@link ArrayOfPratica }
     * 
     */
    public ArrayOfPratica createArrayOfPratica() {
        return new ArrayOfPratica();
    }

    /**
     * Create an instance of {@link RetornarDiligencias }
     * 
     */
    public RetornarDiligencias createRetornarDiligencias() {
        return new RetornarDiligencias();
    }

    /**
     * Create an instance of {@link AtualizaCustaRoboResponse }
     * 
     */
    public AtualizaCustaRoboResponse createAtualizaCustaRoboResponse() {
        return new AtualizaCustaRoboResponse();
    }

    /**
     * Create an instance of {@link InserirFollowUP }
     * 
     */
    public InserirFollowUP createInserirFollowUP() {
        return new InserirFollowUP();
    }

    /**
     * Create an instance of {@link FollowUP }
     * 
     */
    public FollowUP createFollowUP() {
        return new FollowUP();
    }

    /**
     * Create an instance of {@link RecuperarSistema }
     * 
     */
    public RecuperarSistema createRecuperarSistema() {
        return new RecuperarSistema();
    }

    /**
     * Create an instance of {@link ArrayOfLog }
     * 
     */
    public ArrayOfLog createArrayOfLog() {
        return new ArrayOfLog();
    }

    /**
     * Create an instance of {@link ArrayOfTipoParte }
     * 
     */
    public ArrayOfTipoParte createArrayOfTipoParte() {
        return new ArrayOfTipoParte();
    }

    /**
     * Create an instance of {@link FechamentoLoteCustas }
     * 
     */
    public FechamentoLoteCustas createFechamentoLoteCustas() {
        return new FechamentoLoteCustas();
    }

    /**
     * Create an instance of {@link RetornaArquivosPorDataResponse }
     * 
     */
    public RetornaArquivosPorDataResponse createRetornaArquivosPorDataResponse() {
        return new RetornaArquivosPorDataResponse();
    }

    /**
     * Create an instance of {@link ArrayOfArquivo }
     * 
     */
    public ArrayOfArquivo createArrayOfArquivo() {
        return new ArrayOfArquivo();
    }

    /**
     * Create an instance of {@link InsereAgendamentoProcessoResponse }
     * 
     */
    public InsereAgendamentoProcessoResponse createInsereAgendamentoProcessoResponse() {
        return new InsereAgendamentoProcessoResponse();
    }

    /**
     * Create an instance of {@link RecuperarMotivoDevolucaoPublicacao }
     * 
     */
    public RecuperarMotivoDevolucaoPublicacao createRecuperarMotivoDevolucaoPublicacao() {
        return new RecuperarMotivoDevolucaoPublicacao();
    }

    /**
     * Create an instance of {@link InserirAlterarContatoResponse }
     * 
     */
    public InserirAlterarContatoResponse createInserirAlterarContatoResponse() {
        return new InserirAlterarContatoResponse();
    }

    /**
     * Create an instance of {@link RetornarContagemDiariaResponse }
     * 
     */
    public RetornarContagemDiariaResponse createRetornarContagemDiariaResponse() {
        return new RetornarContagemDiariaResponse();
    }

    /**
     * Create an instance of {@link ArrayOfMovimento }
     * 
     */
    public ArrayOfMovimento createArrayOfMovimento() {
        return new ArrayOfMovimento();
    }

    /**
     * Create an instance of {@link InsereAndamento }
     * 
     */
    public InsereAndamento createInsereAndamento() {
        return new InsereAndamento();
    }

    /**
     * Create an instance of {@link InserirAndamento }
     * 
     */
    public InserirAndamento createInserirAndamento() {
        return new InserirAndamento();
    }

    /**
     * Create an instance of {@link RecuperarPratica }
     * 
     */
    public RecuperarPratica createRecuperarPratica() {
        return new RecuperarPratica();
    }

    /**
     * Create an instance of {@link InserirServico }
     * 
     */
    public InserirServico createInserirServico() {
        return new InserirServico();
    }

    /**
     * Create an instance of {@link Servico }
     * 
     */
    public Servico createServico() {
        return new Servico();
    }

    /**
     * Create an instance of {@link ConsultaAndamentoIDProcessoUnico }
     * 
     */
    public ConsultaAndamentoIDProcessoUnico createConsultaAndamentoIDProcessoUnico() {
        return new ConsultaAndamentoIDProcessoUnico();
    }

    /**
     * Create an instance of {@link ArrayOfTipoVara }
     * 
     */
    public ArrayOfTipoVara createArrayOfTipoVara() {
        return new ArrayOfTipoVara();
    }

    /**
     * Create an instance of {@link ExcluirProcesso }
     * 
     */
    public ExcluirProcesso createExcluirProcesso() {
        return new ExcluirProcesso();
    }

    /**
     * Create an instance of {@link AlterarStatusProcessoResponse }
     * 
     */
    public AlterarStatusProcessoResponse createAlterarStatusProcessoResponse() {
        return new AlterarStatusProcessoResponse();
    }

    /**
     * Create an instance of {@link ValidarAlteracaoSubTipoSolicitacaoResponse }
     * 
     */
    public ValidarAlteracaoSubTipoSolicitacaoResponse createValidarAlteracaoSubTipoSolicitacaoResponse() {
        return new ValidarAlteracaoSubTipoSolicitacaoResponse();
    }

    /**
     * Create an instance of {@link ArrayOfTrocaSubTipoSolicitacao }
     * 
     */
    public ArrayOfTrocaSubTipoSolicitacao createArrayOfTrocaSubTipoSolicitacao() {
        return new ArrayOfTrocaSubTipoSolicitacao();
    }

    /**
     * Create an instance of {@link ConsultaSolicitacao }
     * 
     */
    public ConsultaSolicitacao createConsultaSolicitacao() {
        return new ConsultaSolicitacao();
    }

    /**
     * Create an instance of {@link ConsultaHistoricoStatusProcesso }
     * 
     */
    public ConsultaHistoricoStatusProcesso createConsultaHistoricoStatusProcesso() {
        return new ConsultaHistoricoStatusProcesso();
    }

    /**
     * Create an instance of {@link EncerraAgendamento }
     * 
     */
    public EncerraAgendamento createEncerraAgendamento() {
        return new EncerraAgendamento();
    }

    /**
     * Create an instance of {@link RetornaOpcoesCampoListaResponse }
     * 
     */
    public RetornaOpcoesCampoListaResponse createRetornaOpcoesCampoListaResponse() {
        return new RetornaOpcoesCampoListaResponse();
    }

    /**
     * Create an instance of {@link ArrayOfOpcoesCampoLista }
     * 
     */
    public ArrayOfOpcoesCampoLista createArrayOfOpcoesCampoLista() {
        return new ArrayOfOpcoesCampoLista();
    }

    /**
     * Create an instance of {@link AlterarCelulaProcessoResponse }
     * 
     */
    public AlterarCelulaProcessoResponse createAlterarCelulaProcessoResponse() {
        return new AlterarCelulaProcessoResponse();
    }

    /**
     * Create an instance of {@link RecuperarDivisaoCliente }
     * 
     */
    public RecuperarDivisaoCliente createRecuperarDivisaoCliente() {
        return new RecuperarDivisaoCliente();
    }

    /**
     * Create an instance of {@link RetornaArquivosResponse }
     * 
     */
    public RetornaArquivosResponse createRetornaArquivosResponse() {
        return new RetornaArquivosResponse();
    }

    /**
     * Create an instance of {@link ConsultaCusta }
     * 
     */
    public ConsultaCusta createConsultaCusta() {
        return new ConsultaCusta();
    }

    /**
     * Create an instance of {@link ConsultaTipoParteResponse }
     * 
     */
    public ConsultaTipoParteResponse createConsultaTipoParteResponse() {
        return new ConsultaTipoParteResponse();
    }

    /**
     * Create an instance of {@link ConsultaOperacaoFinanceiraResponse }
     * 
     */
    public ConsultaOperacaoFinanceiraResponse createConsultaOperacaoFinanceiraResponse() {
        return new ConsultaOperacaoFinanceiraResponse();
    }

    /**
     * Create an instance of {@link ArrayOfOperacaoFinanceira }
     * 
     */
    public ArrayOfOperacaoFinanceira createArrayOfOperacaoFinanceira() {
        return new ArrayOfOperacaoFinanceira();
    }

    /**
     * Create an instance of {@link RecuperarJusticaResponse }
     * 
     */
    public RecuperarJusticaResponse createRecuperarJusticaResponse() {
        return new RecuperarJusticaResponse();
    }

    /**
     * Create an instance of {@link ArrayOfJustica }
     * 
     */
    public ArrayOfJustica createArrayOfJustica() {
        return new ArrayOfJustica();
    }

    /**
     * Create an instance of {@link RecuperarForo }
     * 
     */
    public RecuperarForo createRecuperarForo() {
        return new RecuperarForo();
    }

    /**
     * Create an instance of {@link ConsultaTipoCusta }
     * 
     */
    public ConsultaTipoCusta createConsultaTipoCusta() {
        return new ConsultaTipoCusta();
    }

    /**
     * Create an instance of {@link RecuperarTipoSolicitacaoResponse }
     * 
     */
    public RecuperarTipoSolicitacaoResponse createRecuperarTipoSolicitacaoResponse() {
        return new RecuperarTipoSolicitacaoResponse();
    }

    /**
     * Create an instance of {@link ArrayOfTipoSolicitacao }
     * 
     */
    public ArrayOfTipoSolicitacao createArrayOfTipoSolicitacao() {
        return new ArrayOfTipoSolicitacao();
    }

    /**
     * Create an instance of {@link RetornaStepSolicitacao }
     * 
     */
    public RetornaStepSolicitacao createRetornaStepSolicitacao() {
        return new RetornaStepSolicitacao();
    }

    /**
     * Create an instance of {@link RetornarAgendamentoResponse }
     * 
     */
    public RetornarAgendamentoResponse createRetornarAgendamentoResponse() {
        return new RetornarAgendamentoResponse();
    }

    /**
     * Create an instance of {@link ArrayOfResultadoRetornaAgendamento }
     * 
     */
    public ArrayOfResultadoRetornaAgendamento createArrayOfResultadoRetornaAgendamento() {
        return new ArrayOfResultadoRetornaAgendamento();
    }

    /**
     * Create an instance of {@link RetornaPublicacao }
     * 
     */
    public RetornaPublicacao createRetornaPublicacao() {
        return new RetornaPublicacao();
    }

    /**
     * Create an instance of {@link ConsultaDadoComplementarProcessoResponse }
     * 
     */
    public ConsultaDadoComplementarProcessoResponse createConsultaDadoComplementarProcessoResponse() {
        return new ConsultaDadoComplementarProcessoResponse();
    }

    /**
     * Create an instance of {@link ConsultaSolicitacaoDataSolicitacao }
     * 
     */
    public ConsultaSolicitacaoDataSolicitacao createConsultaSolicitacaoDataSolicitacao() {
        return new ConsultaSolicitacaoDataSolicitacao();
    }

    /**
     * Create an instance of {@link ArrayOfDivisaoCliente }
     * 
     */
    public ArrayOfDivisaoCliente createArrayOfDivisaoCliente() {
        return new ArrayOfDivisaoCliente();
    }

    /**
     * Create an instance of {@link ConsultaContratoProcessoResponse }
     * 
     */
    public ConsultaContratoProcessoResponse createConsultaContratoProcessoResponse() {
        return new ConsultaContratoProcessoResponse();
    }

    /**
     * Create an instance of {@link RecuperarPublicacaoResponse }
     * 
     */
    public RecuperarPublicacaoResponse createRecuperarPublicacaoResponse() {
        return new RecuperarPublicacaoResponse();
    }

    /**
     * Create an instance of {@link ArrayOfNewDataSet }
     * 
     */
    public ArrayOfNewDataSet createArrayOfNewDataSet() {
        return new ArrayOfNewDataSet();
    }

    /**
     * Create an instance of {@link RecuperarTipoVara }
     * 
     */
    public RecuperarTipoVara createRecuperarTipoVara() {
        return new RecuperarTipoVara();
    }

    /**
     * Create an instance of {@link AlterarParcelaAcordoResponse }
     * 
     */
    public AlterarParcelaAcordoResponse createAlterarParcelaAcordoResponse() {
        return new AlterarParcelaAcordoResponse();
    }

    /**
     * Create an instance of {@link RetornaArquivosPorData }
     * 
     */
    public RetornaArquivosPorData createRetornaArquivosPorData() {
        return new RetornaArquivosPorData();
    }

    /**
     * Create an instance of {@link InserirAgendamentoPortalServicosCetelemResponse }
     * 
     */
    public InserirAgendamentoPortalServicosCetelemResponse createInserirAgendamentoPortalServicosCetelemResponse() {
        return new InserirAgendamentoPortalServicosCetelemResponse();
    }

    /**
     * Create an instance of {@link ArrayOfVara }
     * 
     */
    public ArrayOfVara createArrayOfVara() {
        return new ArrayOfVara();
    }

    /**
     * Create an instance of {@link InsereDadoComplementarProcessoResponse }
     * 
     */
    public InsereDadoComplementarProcessoResponse createInsereDadoComplementarProcessoResponse() {
        return new InsereDadoComplementarProcessoResponse();
    }

    /**
     * Create an instance of {@link RecuperarAtividadeResponse }
     * 
     */
    public RecuperarAtividadeResponse createRecuperarAtividadeResponse() {
        return new RecuperarAtividadeResponse();
    }

    /**
     * Create an instance of {@link ArrayOfAtividade }
     * 
     */
    public ArrayOfAtividade createArrayOfAtividade() {
        return new ArrayOfAtividade();
    }

    /**
     * Create an instance of {@link AtualizarPublicacaoEnviadaConfirmacaoClienteResponse }
     * 
     */
    public AtualizarPublicacaoEnviadaConfirmacaoClienteResponse createAtualizarPublicacaoEnviadaConfirmacaoClienteResponse() {
        return new AtualizarPublicacaoEnviadaConfirmacaoClienteResponse();
    }

    /**
     * Create an instance of {@link ConsultaProcessoResponse }
     * 
     */
    public ConsultaProcessoResponse createConsultaProcessoResponse() {
        return new ConsultaProcessoResponse();
    }

    /**
     * Create an instance of {@link ArrayOfRetornoProcesso }
     * 
     */
    public ArrayOfRetornoProcesso createArrayOfRetornoProcesso() {
        return new ArrayOfRetornoProcesso();
    }

    /**
     * Create an instance of {@link RetornaOpcoesCampoLista }
     * 
     */
    public RetornaOpcoesCampoLista createRetornaOpcoesCampoLista() {
        return new RetornaOpcoesCampoLista();
    }

    /**
     * Create an instance of {@link InserirGarantia }
     * 
     */
    public InserirGarantia createInserirGarantia() {
        return new InserirGarantia();
    }

    /**
     * Create an instance of {@link Garantia }
     * 
     */
    public Garantia createGarantia() {
        return new Garantia();
    }

    /**
     * Create an instance of {@link RetornarAgendamento }
     * 
     */
    public RetornarAgendamento createRetornarAgendamento() {
        return new RetornarAgendamento();
    }

    /**
     * Create an instance of {@link PegarPublicacao }
     * 
     */
    public PegarPublicacao createPegarPublicacao() {
        return new PegarPublicacao();
    }

    /**
     * Create an instance of {@link ConsultaDadoComplementarProcesso }
     * 
     */
    public ConsultaDadoComplementarProcesso createConsultaDadoComplementarProcesso() {
        return new ConsultaDadoComplementarProcesso();
    }

    /**
     * Create an instance of {@link RetornaPublicacaoDescartadaResponse }
     * 
     */
    public RetornaPublicacaoDescartadaResponse createRetornaPublicacaoDescartadaResponse() {
        return new RetornaPublicacaoDescartadaResponse();
    }

    /**
     * Create an instance of {@link ArrayOfRetornoPublicacaoDescartada }
     * 
     */
    public ArrayOfRetornoPublicacaoDescartada createArrayOfRetornoPublicacaoDescartada() {
        return new ArrayOfRetornoPublicacaoDescartada();
    }

    /**
     * Create an instance of {@link RetornaArquivos }
     * 
     */
    public RetornaArquivos createRetornaArquivos() {
        return new RetornaArquivos();
    }

    /**
     * Create an instance of {@link ConsultaProcessoIDResponse }
     * 
     */
    public ConsultaProcessoIDResponse createConsultaProcessoIDResponse() {
        return new ConsultaProcessoIDResponse();
    }

    /**
     * Create an instance of {@link ResultadoProcesso }
     * 
     */
    public ResultadoProcesso createResultadoProcesso() {
        return new ResultadoProcesso();
    }

    /**
     * Create an instance of {@link ConsultaAndamentoIDProcessoUnicoResponse }
     * 
     */
    public ConsultaAndamentoIDProcessoUnicoResponse createConsultaAndamentoIDProcessoUnicoResponse() {
        return new ConsultaAndamentoIDProcessoUnicoResponse();
    }

    /**
     * Create an instance of {@link ArrayOfAndamento }
     * 
     */
    public ArrayOfAndamento createArrayOfAndamento() {
        return new ArrayOfAndamento();
    }

    /**
     * Create an instance of {@link ArrayOfInstancia }
     * 
     */
    public ArrayOfInstancia createArrayOfInstancia() {
        return new ArrayOfInstancia();
    }

    /**
     * Create an instance of {@link InserirApensoResponse }
     * 
     */
    public InserirApensoResponse createInserirApensoResponse() {
        return new InserirApensoResponse();
    }

    /**
     * Create an instance of {@link InserirFollowUPResponse }
     * 
     */
    public InserirFollowUPResponse createInserirFollowUPResponse() {
        return new InserirFollowUPResponse();
    }

    /**
     * Create an instance of {@link InserirApenso }
     * 
     */
    public InserirApenso createInserirApenso() {
        return new InserirApenso();
    }

    /**
     * Create an instance of {@link RetornaCorrespondenteResponse }
     * 
     */
    public RetornaCorrespondenteResponse createRetornaCorrespondenteResponse() {
        return new RetornaCorrespondenteResponse();
    }

    /**
     * Create an instance of {@link ArrayOfRetornoCorrespondente }
     * 
     */
    public ArrayOfRetornoCorrespondente createArrayOfRetornoCorrespondente() {
        return new ArrayOfRetornoCorrespondente();
    }

    /**
     * Create an instance of {@link InserirAcordo }
     * 
     */
    public InserirAcordo createInserirAcordo() {
        return new InserirAcordo();
    }

    /**
     * Create an instance of {@link Acordo }
     * 
     */
    public Acordo createAcordo() {
        return new Acordo();
    }

    /**
     * Create an instance of {@link ConsultaDesdobramentoResponse }
     * 
     */
    public ConsultaDesdobramentoResponse createConsultaDesdobramentoResponse() {
        return new ConsultaDesdobramentoResponse();
    }

    /**
     * Create an instance of {@link RecuperarAcaoResponse }
     * 
     */
    public RecuperarAcaoResponse createRecuperarAcaoResponse() {
        return new RecuperarAcaoResponse();
    }

    /**
     * Create an instance of {@link ArrayOfAcao }
     * 
     */
    public ArrayOfAcao createArrayOfAcao() {
        return new ArrayOfAcao();
    }

    /**
     * Create an instance of {@link ConsultaSolicitacaoProcessoResponse }
     * 
     */
    public ConsultaSolicitacaoProcessoResponse createConsultaSolicitacaoProcessoResponse() {
        return new ConsultaSolicitacaoProcessoResponse();
    }

    /**
     * Create an instance of {@link EnviaEmail }
     * 
     */
    public EnviaEmail createEnviaEmail() {
        return new EnviaEmail();
    }

    /**
     * Create an instance of {@link RetornaAgendamentoProcesso }
     * 
     */
    public RetornaAgendamentoProcesso createRetornaAgendamentoProcesso() {
        return new RetornaAgendamentoProcesso();
    }

    /**
     * Create an instance of {@link ConsultaTipoParte }
     * 
     */
    public ConsultaTipoParte createConsultaTipoParte() {
        return new ConsultaTipoParte();
    }

    /**
     * Create an instance of {@link PegarPublicacaoResponse }
     * 
     */
    public PegarPublicacaoResponse createPegarPublicacaoResponse() {
        return new PegarPublicacaoResponse();
    }

    /**
     * Create an instance of {@link RetornarGarantia }
     * 
     */
    public RetornarGarantia createRetornarGarantia() {
        return new RetornarGarantia();
    }

    /**
     * Create an instance of {@link ConsultaLogCusta }
     * 
     */
    public ConsultaLogCusta createConsultaLogCusta() {
        return new ConsultaLogCusta();
    }

    /**
     * Create an instance of {@link RecuperarAtividade }
     * 
     */
    public RecuperarAtividade createRecuperarAtividade() {
        return new RecuperarAtividade();
    }

    /**
     * Create an instance of {@link RecuperarPublicacao }
     * 
     */
    public RecuperarPublicacao createRecuperarPublicacao() {
        return new RecuperarPublicacao();
    }

    /**
     * Create an instance of {@link ConsultaAndamentoDataResponse }
     * 
     */
    public ConsultaAndamentoDataResponse createConsultaAndamentoDataResponse() {
        return new ConsultaAndamentoDataResponse();
    }

    /**
     * Create an instance of {@link ResultadoAcordo }
     * 
     */
    public ResultadoAcordo createResultadoAcordo() {
        return new ResultadoAcordo();
    }

    /**
     * Create an instance of {@link ConsultaTiposFollowUPResponse }
     * 
     */
    public ConsultaTiposFollowUPResponse createConsultaTiposFollowUPResponse() {
        return new ConsultaTiposFollowUPResponse();
    }

    /**
     * Create an instance of {@link ConsultaContato }
     * 
     */
    public ConsultaContato createConsultaContato() {
        return new ConsultaContato();
    }

    /**
     * Create an instance of {@link RecuperarTipoSolicitacao }
     * 
     */
    public RecuperarTipoSolicitacao createRecuperarTipoSolicitacao() {
        return new RecuperarTipoSolicitacao();
    }

    /**
     * Create an instance of {@link RecuperarComarcaResponse }
     * 
     */
    public RecuperarComarcaResponse createRecuperarComarcaResponse() {
        return new RecuperarComarcaResponse();
    }

    /**
     * Create an instance of {@link ArrayOfComarca }
     * 
     */
    public ArrayOfComarca createArrayOfComarca() {
        return new ArrayOfComarca();
    }

    /**
     * Create an instance of {@link ConsultaCustaResponse }
     * 
     */
    public ConsultaCustaResponse createConsultaCustaResponse() {
        return new ConsultaCustaResponse();
    }

    /**
     * Create an instance of {@link ArrayOfCusta }
     * 
     */
    public ArrayOfCusta createArrayOfCusta() {
        return new ArrayOfCusta();
    }

    /**
     * Create an instance of {@link CancelarCustaResponse }
     * 
     */
    public CancelarCustaResponse createCancelarCustaResponse() {
        return new CancelarCustaResponse();
    }

    /**
     * Create an instance of {@link ConsultaSolicitacaoResponse }
     * 
     */
    public ConsultaSolicitacaoResponse createConsultaSolicitacaoResponse() {
        return new ConsultaSolicitacaoResponse();
    }

    /**
     * Create an instance of {@link InserirParteProcessoResponse }
     * 
     */
    public InserirParteProcessoResponse createInserirParteProcessoResponse() {
        return new InserirParteProcessoResponse();
    }

    /**
     * Create an instance of {@link ArrayOfCustaArquivo }
     * 
     */
    public ArrayOfCustaArquivo createArrayOfCustaArquivo() {
        return new ArrayOfCustaArquivo();
    }

    /**
     * Create an instance of {@link ConsultaLogCustaResponse }
     * 
     */
    public ConsultaLogCustaResponse createConsultaLogCustaResponse() {
        return new ConsultaLogCustaResponse();
    }

    /**
     * Create an instance of {@link TrocarSubTipoSolicitacaoResponse }
     * 
     */
    public TrocarSubTipoSolicitacaoResponse createTrocarSubTipoSolicitacaoResponse() {
        return new TrocarSubTipoSolicitacaoResponse();
    }

    /**
     * Create an instance of {@link RecuperarTipoVaraResponse }
     * 
     */
    public RecuperarTipoVaraResponse createRecuperarTipoVaraResponse() {
        return new RecuperarTipoVaraResponse();
    }

    /**
     * Create an instance of {@link InserirParteProcesso }
     * 
     */
    public InserirParteProcesso createInserirParteProcesso() {
        return new InserirParteProcesso();
    }

    /**
     * Create an instance of {@link AlterarParcelaAcordo }
     * 
     */
    public AlterarParcelaAcordo createAlterarParcelaAcordo() {
        return new AlterarParcelaAcordo();
    }

    /**
     * Create an instance of {@link Parcela }
     * 
     */
    public Parcela createParcela() {
        return new Parcela();
    }

    /**
     * Create an instance of {@link ResultadoTamanhoArquivo }
     * 
     */
    public ResultadoTamanhoArquivo createResultadoTamanhoArquivo() {
        return new ResultadoTamanhoArquivo();
    }

    /**
     * Create an instance of {@link ArrayOfSistema }
     * 
     */
    public ArrayOfSistema createArrayOfSistema() {
        return new ArrayOfSistema();
    }

    /**
     * Create an instance of {@link RetornarAgendamentoPorTipo }
     * 
     */
    public RetornarAgendamentoPorTipo createRetornarAgendamentoPorTipo() {
        return new RetornarAgendamentoPorTipo();
    }

    /**
     * Create an instance of {@link ConsultaFollowUPResponse }
     * 
     */
    public ConsultaFollowUPResponse createConsultaFollowUPResponse() {
        return new ConsultaFollowUPResponse();
    }

    /**
     * Create an instance of {@link RecuperarMateria }
     * 
     */
    public RecuperarMateria createRecuperarMateria() {
        return new RecuperarMateria();
    }

    /**
     * Create an instance of {@link ConsultaContratoProcesso }
     * 
     */
    public ConsultaContratoProcesso createConsultaContratoProcesso() {
        return new ConsultaContratoProcesso();
    }

    /**
     * Create an instance of {@link ConsultaCNPJCustaResponse }
     * 
     */
    public ConsultaCNPJCustaResponse createConsultaCNPJCustaResponse() {
        return new ConsultaCNPJCustaResponse();
    }

    /**
     * Create an instance of {@link ConsultaAcordoProcesso }
     * 
     */
    public ConsultaAcordoProcesso createConsultaAcordoProcesso() {
        return new ConsultaAcordoProcesso();
    }

    /**
     * Create an instance of {@link ArrayOfTipoCusta }
     * 
     */
    public ArrayOfTipoCusta createArrayOfTipoCusta() {
        return new ArrayOfTipoCusta();
    }

    /**
     * Create an instance of {@link InserirSolicitacaoResponse }
     * 
     */
    public InserirSolicitacaoResponse createInserirSolicitacaoResponse() {
        return new InserirSolicitacaoResponse();
    }

    /**
     * Create an instance of {@link RetornarAuditoriaResultado }
     * 
     */
    public RetornarAuditoriaResultado createRetornarAuditoriaResultado() {
        return new RetornarAuditoriaResultado();
    }

    /**
     * Create an instance of {@link ExcluirAcordoProcessoResponse }
     * 
     */
    public ExcluirAcordoProcessoResponse createExcluirAcordoProcessoResponse() {
        return new ExcluirAcordoProcessoResponse();
    }

    /**
     * Create an instance of {@link ConsultaDadoComplementarDisponivel }
     * 
     */
    public ConsultaDadoComplementarDisponivel createConsultaDadoComplementarDisponivel() {
        return new ConsultaDadoComplementarDisponivel();
    }

    /**
     * Create an instance of {@link RetornaCorrespondente }
     * 
     */
    public RetornaCorrespondente createRetornaCorrespondente() {
        return new RetornaCorrespondente();
    }

    /**
     * Create an instance of {@link RetornaStepSolicitacaoResponse }
     * 
     */
    public RetornaStepSolicitacaoResponse createRetornaStepSolicitacaoResponse() {
        return new RetornaStepSolicitacaoResponse();
    }

    /**
     * Create an instance of {@link ConsultaDesdobramento }
     * 
     */
    public ConsultaDesdobramento createConsultaDesdobramento() {
        return new ConsultaDesdobramento();
    }

    /**
     * Create an instance of {@link RetornarDiligenciasResponse }
     * 
     */
    public RetornarDiligenciasResponse createRetornarDiligenciasResponse() {
        return new RetornarDiligenciasResponse();
    }

    /**
     * Create an instance of {@link RecuperarSistemaResponse }
     * 
     */
    public RecuperarSistemaResponse createRecuperarSistemaResponse() {
        return new RecuperarSistemaResponse();
    }

    /**
     * Create an instance of {@link RecuperarInstancia }
     * 
     */
    public RecuperarInstancia createRecuperarInstancia() {
        return new RecuperarInstancia();
    }

    /**
     * Create an instance of {@link InserirAlterarContato }
     * 
     */
    public InserirAlterarContato createInserirAlterarContato() {
        return new InserirAlterarContato();
    }

    /**
     * Create an instance of {@link Contato }
     * 
     */
    public Contato createContato() {
        return new Contato();
    }

    /**
     * Create an instance of {@link ConsultaTipoCustaResponse }
     * 
     */
    public ConsultaTipoCustaResponse createConsultaTipoCustaResponse() {
        return new ConsultaTipoCustaResponse();
    }

    /**
     * Create an instance of {@link ConcluirSolicitacaoResponse }
     * 
     */
    public ConcluirSolicitacaoResponse createConcluirSolicitacaoResponse() {
        return new ConcluirSolicitacaoResponse();
    }

    /**
     * Create an instance of {@link AlterarProcesso }
     * 
     */
    public AlterarProcesso createAlterarProcesso() {
        return new AlterarProcesso();
    }

    /**
     * Create an instance of {@link TrocarSubTipoSolicitacao }
     * 
     */
    public TrocarSubTipoSolicitacao createTrocarSubTipoSolicitacao() {
        return new TrocarSubTipoSolicitacao();
    }

    /**
     * Create an instance of {@link ConcluirSolicitacao }
     * 
     */
    public ConcluirSolicitacao createConcluirSolicitacao() {
        return new ConcluirSolicitacao();
    }

    /**
     * Create an instance of {@link FechamentoLoteCustasResponse }
     * 
     */
    public FechamentoLoteCustasResponse createFechamentoLoteCustasResponse() {
        return new FechamentoLoteCustasResponse();
    }

    /**
     * Create an instance of {@link RecebeArquivo }
     * 
     */
    public RecebeArquivo createRecebeArquivo() {
        return new RecebeArquivo();
    }

    /**
     * Create an instance of {@link ConsultaTipoAgendamentoResponse }
     * 
     */
    public ConsultaTipoAgendamentoResponse createConsultaTipoAgendamentoResponse() {
        return new ConsultaTipoAgendamentoResponse();
    }

    /**
     * Create an instance of {@link ArrayOfTipoAgendamento }
     * 
     */
    public ArrayOfTipoAgendamento createArrayOfTipoAgendamento() {
        return new ArrayOfTipoAgendamento();
    }

    /**
     * Create an instance of {@link CancelarSolicitacao }
     * 
     */
    public CancelarSolicitacao createCancelarSolicitacao() {
        return new CancelarSolicitacao();
    }

    /**
     * Create an instance of {@link InserirAcordoResponse }
     * 
     */
    public InserirAcordoResponse createInserirAcordoResponse() {
        return new InserirAcordoResponse();
    }

    /**
     * Create an instance of {@link ConsultaProcessoLog }
     * 
     */
    public ConsultaProcessoLog createConsultaProcessoLog() {
        return new ConsultaProcessoLog();
    }

    /**
     * Create an instance of {@link RecuperarStatusProcesso }
     * 
     */
    public RecuperarStatusProcesso createRecuperarStatusProcesso() {
        return new RecuperarStatusProcesso();
    }

    /**
     * Create an instance of {@link RetornaPublicacaoResponse }
     * 
     */
    public RetornaPublicacaoResponse createRetornaPublicacaoResponse() {
        return new RetornaPublicacaoResponse();
    }

    /**
     * Create an instance of {@link AlterarStatusProcesso }
     * 
     */
    public AlterarStatusProcesso createAlterarStatusProcesso() {
        return new AlterarStatusProcesso();
    }

    /**
     * Create an instance of {@link InsereDadoComplementarProcesso }
     * 
     */
    public InsereDadoComplementarProcesso createInsereDadoComplementarProcesso() {
        return new InsereDadoComplementarProcesso();
    }

    /**
     * Create an instance of {@link InsereAndamentoResponse }
     * 
     */
    public InsereAndamentoResponse createInsereAndamentoResponse() {
        return new InsereAndamentoResponse();
    }

    /**
     * Create an instance of {@link ConsultaLoteCustaResponse }
     * 
     */
    public ConsultaLoteCustaResponse createConsultaLoteCustaResponse() {
        return new ConsultaLoteCustaResponse();
    }

    /**
     * Create an instance of {@link ArrayOfLote }
     * 
     */
    public ArrayOfLote createArrayOfLote() {
        return new ArrayOfLote();
    }

    /**
     * Create an instance of {@link DividirArquivoPorTamanhoResponse }
     * 
     */
    public DividirArquivoPorTamanhoResponse createDividirArquivoPorTamanhoResponse() {
        return new DividirArquivoPorTamanhoResponse();
    }

    /**
     * Create an instance of {@link ConsultaCustaArquivosResponse }
     * 
     */
    public ConsultaCustaArquivosResponse createConsultaCustaArquivosResponse() {
        return new ConsultaCustaArquivosResponse();
    }

    /**
     * Create an instance of {@link ConsultaAcordoProcessoResponse }
     * 
     */
    public ConsultaAcordoProcessoResponse createConsultaAcordoProcessoResponse() {
        return new ConsultaAcordoProcessoResponse();
    }

    /**
     * Create an instance of {@link InserirAgendamentoSincronizado }
     * 
     */
    public InserirAgendamentoSincronizado createInserirAgendamentoSincronizado() {
        return new InserirAgendamentoSincronizado();
    }

    /**
     * Create an instance of {@link ConsultaProcessoLogResponse }
     * 
     */
    public ConsultaProcessoLogResponse createConsultaProcessoLogResponse() {
        return new ConsultaProcessoLogResponse();
    }

    /**
     * Create an instance of {@link ConsultaHistoricoStatusProcessoResponse }
     * 
     */
    public ConsultaHistoricoStatusProcessoResponse createConsultaHistoricoStatusProcessoResponse() {
        return new ConsultaHistoricoStatusProcessoResponse();
    }

    /**
     * Create an instance of {@link RecuperarVaraResponse }
     * 
     */
    public RecuperarVaraResponse createRecuperarVaraResponse() {
        return new RecuperarVaraResponse();
    }

    /**
     * Create an instance of {@link RetornarGarantiaResponse }
     * 
     */
    public RetornarGarantiaResponse createRetornarGarantiaResponse() {
        return new RetornarGarantiaResponse();
    }

    /**
     * Create an instance of {@link ConsultaTamanhoArquivoResponse }
     * 
     */
    public ConsultaTamanhoArquivoResponse createConsultaTamanhoArquivoResponse() {
        return new ConsultaTamanhoArquivoResponse();
    }

    /**
     * Create an instance of {@link ValidarAlteracaoSubTipoSolicitacao }
     * 
     */
    public ValidarAlteracaoSubTipoSolicitacao createValidarAlteracaoSubTipoSolicitacao() {
        return new ValidarAlteracaoSubTipoSolicitacao();
    }

    /**
     * Create an instance of {@link ConsultaSolicitacaoProcesso }
     * 
     */
    public ConsultaSolicitacaoProcesso createConsultaSolicitacaoProcesso() {
        return new ConsultaSolicitacaoProcesso();
    }

    /**
     * Create an instance of {@link ProcessarPublicacaoResponse }
     * 
     */
    public ProcessarPublicacaoResponse createProcessarPublicacaoResponse() {
        return new ProcessarPublicacaoResponse();
    }

    /**
     * Create an instance of {@link ConsultaAndamentoData }
     * 
     */
    public ConsultaAndamentoData createConsultaAndamentoData() {
        return new ConsultaAndamentoData();
    }

    /**
     * Create an instance of {@link RecuperarVara }
     * 
     */
    public RecuperarVara createRecuperarVara() {
        return new RecuperarVara();
    }

    /**
     * Create an instance of {@link InserirServicoResponse }
     * 
     */
    public InserirServicoResponse createInserirServicoResponse() {
        return new InserirServicoResponse();
    }

    /**
     * Create an instance of {@link RetornarAgendamentoPorTipoResponse }
     * 
     */
    public RetornarAgendamentoPorTipoResponse createRetornarAgendamentoPorTipoResponse() {
        return new RetornarAgendamentoPorTipoResponse();
    }

    /**
     * Create an instance of {@link RecebeArquivoResponse }
     * 
     */
    public RecebeArquivoResponse createRecebeArquivoResponse() {
        return new RecebeArquivoResponse();
    }

    /**
     * Create an instance of {@link RecuperarInstanciaResponse }
     * 
     */
    public RecuperarInstanciaResponse createRecuperarInstanciaResponse() {
        return new RecuperarInstanciaResponse();
    }

    /**
     * Create an instance of {@link ExcluirProcessoResponse }
     * 
     */
    public ExcluirProcessoResponse createExcluirProcessoResponse() {
        return new ExcluirProcessoResponse();
    }

    /**
     * Create an instance of {@link ConsultaTipoAgendamento }
     * 
     */
    public ConsultaTipoAgendamento createConsultaTipoAgendamento() {
        return new ConsultaTipoAgendamento();
    }

    /**
     * Create an instance of {@link RecuperarDecisaoResponse }
     * 
     */
    public RecuperarDecisaoResponse createRecuperarDecisaoResponse() {
        return new RecuperarDecisaoResponse();
    }

    /**
     * Create an instance of {@link ArrayOfDecisao }
     * 
     */
    public ArrayOfDecisao createArrayOfDecisao() {
        return new ArrayOfDecisao();
    }

    /**
     * Create an instance of {@link RecuperarAcao }
     * 
     */
    public RecuperarAcao createRecuperarAcao() {
        return new RecuperarAcao();
    }

    /**
     * Create an instance of {@link ConsultaCNPJCusta }
     * 
     */
    public ConsultaCNPJCusta createConsultaCNPJCusta() {
        return new ConsultaCNPJCusta();
    }

    /**
     * Create an instance of {@link ConsultaStatusCusta }
     * 
     */
    public ConsultaStatusCusta createConsultaStatusCusta() {
        return new ConsultaStatusCusta();
    }

    /**
     * Create an instance of {@link EnviaEmailResponse }
     * 
     */
    public EnviaEmailResponse createEnviaEmailResponse() {
        return new EnviaEmailResponse();
    }

    /**
     * Create an instance of {@link InserirSolicitacao }
     * 
     */
    public InserirSolicitacao createInserirSolicitacao() {
        return new InserirSolicitacao();
    }

    /**
     * Create an instance of {@link Solicitacao }
     * 
     */
    public Solicitacao createSolicitacao() {
        return new Solicitacao();
    }

    /**
     * Create an instance of {@link RecuperarDivisaoClienteResponse }
     * 
     */
    public RecuperarDivisaoClienteResponse createRecuperarDivisaoClienteResponse() {
        return new RecuperarDivisaoClienteResponse();
    }

    /**
     * Create an instance of {@link ConsultaCustaArquivos }
     * 
     */
    public ConsultaCustaArquivos createConsultaCustaArquivos() {
        return new ConsultaCustaArquivos();
    }

    /**
     * Create an instance of {@link ConsultaTamanhoArquivo }
     * 
     */
    public ConsultaTamanhoArquivo createConsultaTamanhoArquivo() {
        return new ConsultaTamanhoArquivo();
    }

    /**
     * Create an instance of {@link InserirAgendamentoPortalServicosCetelem }
     * 
     */
    public InserirAgendamentoPortalServicosCetelem createInserirAgendamentoPortalServicosCetelem() {
        return new InserirAgendamentoPortalServicosCetelem();
    }

    /**
     * Create an instance of {@link ContatoCetelem }
     * 
     */
    public ContatoCetelem createContatoCetelem() {
        return new ContatoCetelem();
    }

    /**
     * Create an instance of {@link InsereAgendamentoProcesso }
     * 
     */
    public InsereAgendamentoProcesso createInsereAgendamentoProcesso() {
        return new InsereAgendamentoProcesso();
    }

    /**
     * Create an instance of {@link ConsultaStatusCustaResponse }
     * 
     */
    public ConsultaStatusCustaResponse createConsultaStatusCustaResponse() {
        return new ConsultaStatusCustaResponse();
    }

    /**
     * Create an instance of {@link EncerraAgendamentoResponse }
     * 
     */
    public EncerraAgendamentoResponse createEncerraAgendamentoResponse() {
        return new EncerraAgendamentoResponse();
    }

    /**
     * Create an instance of {@link RecuperarForoResponse }
     * 
     */
    public RecuperarForoResponse createRecuperarForoResponse() {
        return new RecuperarForoResponse();
    }

    /**
     * Create an instance of {@link TipoAgendamento }
     * 
     */
    public TipoAgendamento createTipoAgendamento() {
        return new TipoAgendamento();
    }

    /**
     * Create an instance of {@link RetornoTipoFollowUP }
     * 
     */
    public RetornoTipoFollowUP createRetornoTipoFollowUP() {
        return new RetornoTipoFollowUP();
    }

    /**
     * Create an instance of {@link RetornoFollowUP }
     * 
     */
    public RetornoFollowUP createRetornoFollowUP() {
        return new RetornoFollowUP();
    }

    /**
     * Create an instance of {@link Publicacoes }
     * 
     */
    public Publicacoes createPublicacoes() {
        return new Publicacoes();
    }

    /**
     * Create an instance of {@link Item }
     * 
     */
    public Item createItem() {
        return new Item();
    }

    /**
     * Create an instance of {@link Vara }
     * 
     */
    public Vara createVara() {
        return new Vara();
    }

    /**
     * Create an instance of {@link StepSolicitacao }
     * 
     */
    public StepSolicitacao createStepSolicitacao() {
        return new StepSolicitacao();
    }

    /**
     * Create an instance of {@link DadoComplementar }
     * 
     */
    public DadoComplementar createDadoComplementar() {
        return new DadoComplementar();
    }

    /**
     * Create an instance of {@link RespostaSolicitacao }
     * 
     */
    public RespostaSolicitacao createRespostaSolicitacao() {
        return new RespostaSolicitacao();
    }

    /**
     * Create an instance of {@link Log }
     * 
     */
    public Log createLog() {
        return new Log();
    }

    /**
     * Create an instance of {@link MotivoPublicacao }
     * 
     */
    public MotivoPublicacao createMotivoPublicacao() {
        return new MotivoPublicacao();
    }

    /**
     * Create an instance of {@link Contrato }
     * 
     */
    public Contrato createContrato() {
        return new Contrato();
    }

    /**
     * Create an instance of {@link RetornoAgendamento }
     * 
     */
    public RetornoAgendamento createRetornoAgendamento() {
        return new RetornoAgendamento();
    }

    /**
     * Create an instance of {@link CelulaCnpj }
     * 
     */
    public CelulaCnpj createCelulaCnpj() {
        return new CelulaCnpj();
    }

    /**
     * Create an instance of {@link TipoVara }
     * 
     */
    public TipoVara createTipoVara() {
        return new TipoVara();
    }

    /**
     * Create an instance of {@link RetornoDesdobramento }
     * 
     */
    public RetornoDesdobramento createRetornoDesdobramento() {
        return new RetornoDesdobramento();
    }

    /**
     * Create an instance of {@link Documento }
     * 
     */
    public Documento createDocumento() {
        return new Documento();
    }

    /**
     * Create an instance of {@link Andamento }
     * 
     */
    public Andamento createAndamento() {
        return new Andamento();
    }

    /**
     * Create an instance of {@link Justica }
     * 
     */
    public Justica createJustica() {
        return new Justica();
    }

    /**
     * Create an instance of {@link RetornaAgendamentoPorTipo }
     * 
     */
    public RetornaAgendamentoPorTipo createRetornaAgendamentoPorTipo() {
        return new RetornaAgendamentoPorTipo();
    }

    /**
     * Create an instance of {@link Movimento }
     * 
     */
    public Movimento createMovimento() {
        return new Movimento();
    }

    /**
     * Create an instance of {@link PessoaJuridica }
     * 
     */
    public PessoaJuridica createPessoaJuridica() {
        return new PessoaJuridica();
    }

    /**
     * Create an instance of {@link TipoParte }
     * 
     */
    public TipoParte createTipoParte() {
        return new TipoParte();
    }

    /**
     * Create an instance of {@link Grupo }
     * 
     */
    public Grupo createGrupo() {
        return new Grupo();
    }

    /**
     * Create an instance of {@link OperacaoFinanceira }
     * 
     */
    public OperacaoFinanceira createOperacaoFinanceira() {
        return new OperacaoFinanceira();
    }

    /**
     * Create an instance of {@link Sistema }
     * 
     */
    public Sistema createSistema() {
        return new Sistema();
    }

    /**
     * Create an instance of {@link Table }
     * 
     */
    public Table createTable() {
        return new Table();
    }

    /**
     * Create an instance of {@link RetornoApenso }
     * 
     */
    public RetornoApenso createRetornoApenso() {
        return new RetornoApenso();
    }

    /**
     * Create an instance of {@link StatusProcesso }
     * 
     */
    public StatusProcesso createStatusProcesso() {
        return new StatusProcesso();
    }

    /**
     * Create an instance of {@link ResultadoRetornoAgendamento }
     * 
     */
    public ResultadoRetornoAgendamento createResultadoRetornoAgendamento() {
        return new ResultadoRetornoAgendamento();
    }

    /**
     * Create an instance of {@link RetornoGarantia }
     * 
     */
    public RetornoGarantia createRetornoGarantia() {
        return new RetornoGarantia();
    }

    /**
     * Create an instance of {@link Lote }
     * 
     */
    public Lote createLote() {
        return new Lote();
    }

    /**
     * Create an instance of {@link DocumentoAnexados }
     * 
     */
    public DocumentoAnexados createDocumentoAnexados() {
        return new DocumentoAnexados();
    }

    /**
     * Create an instance of {@link ResultadoRetornaAgendamento }
     * 
     */
    public ResultadoRetornaAgendamento createResultadoRetornaAgendamento() {
        return new ResultadoRetornaAgendamento();
    }

    /**
     * Create an instance of {@link ResultadoRetornaAgendamentoPorTipo }
     * 
     */
    public ResultadoRetornaAgendamentoPorTipo createResultadoRetornaAgendamentoPorTipo() {
        return new ResultadoRetornaAgendamentoPorTipo();
    }

    /**
     * Create an instance of {@link DocumentoArquivo }
     * 
     */
    public DocumentoArquivo createDocumentoArquivo() {
        return new DocumentoArquivo();
    }

    /**
     * Create an instance of {@link TipoAgendamentoMov }
     * 
     */
    public TipoAgendamentoMov createTipoAgendamentoMov() {
        return new TipoAgendamentoMov();
    }

    /**
     * Create an instance of {@link Atividades }
     * 
     */
    public Atividades createAtividades() {
        return new Atividades();
    }

    /**
     * Create an instance of {@link DivisaoCliente }
     * 
     */
    public DivisaoCliente createDivisaoCliente() {
        return new DivisaoCliente();
    }

    /**
     * Create an instance of {@link Pratica }
     * 
     */
    public Pratica createPratica() {
        return new Pratica();
    }

    /**
     * Create an instance of {@link Historico }
     * 
     */
    public Historico createHistorico() {
        return new Historico();
    }

    /**
     * Create an instance of {@link Agendamentos }
     * 
     */
    public Agendamentos createAgendamentos() {
        return new Agendamentos();
    }

    /**
     * Create an instance of {@link Campos }
     * 
     */
    public Campos createCampos() {
        return new Campos();
    }

    /**
     * Create an instance of {@link Materia }
     * 
     */
    public Materia createMateria() {
        return new Materia();
    }

    /**
     * Create an instance of {@link ContratoProcesso }
     * 
     */
    public ContratoProcesso createContratoProcesso() {
        return new ContratoProcesso();
    }

    /**
     * Create an instance of {@link Decisao }
     * 
     */
    public Decisao createDecisao() {
        return new Decisao();
    }

    /**
     * Create an instance of {@link TipoCusta }
     * 
     */
    public TipoCusta createTipoCusta() {
        return new TipoCusta();
    }

    /**
     * Create an instance of {@link Contatos }
     * 
     */
    public Contatos createContatos() {
        return new Contatos();
    }

    /**
     * Create an instance of {@link ArrayOfTipoAgendamentoMov }
     * 
     */
    public ArrayOfTipoAgendamentoMov createArrayOfTipoAgendamentoMov() {
        return new ArrayOfTipoAgendamentoMov();
    }

    /**
     * Create an instance of {@link RetornaDiligencias }
     * 
     */
    public RetornaDiligencias createRetornaDiligencias() {
        return new RetornaDiligencias();
    }

    /**
     * Create an instance of {@link Custa }
     * 
     */
    public Custa createCusta() {
        return new Custa();
    }

    /**
     * Create an instance of {@link Arquivos }
     * 
     */
    public Arquivos createArquivos() {
        return new Arquivos();
    }

    /**
     * Create an instance of {@link RetornaAgendamento }
     * 
     */
    public RetornaAgendamento createRetornaAgendamento() {
        return new RetornaAgendamento();
    }

    /**
     * Create an instance of {@link ResultadoRetornaAuditoriaResultado }
     * 
     */
    public ResultadoRetornaAuditoriaResultado createResultadoRetornaAuditoriaResultado() {
        return new ResultadoRetornaAuditoriaResultado();
    }

    /**
     * Create an instance of {@link HistoricoProcesso }
     * 
     */
    public HistoricoProcesso createHistoricoProcesso() {
        return new HistoricoProcesso();
    }

    /**
     * Create an instance of {@link Historicos }
     * 
     */
    public Historicos createHistoricos() {
        return new Historicos();
    }

    /**
     * Create an instance of {@link TamanhoArquivo }
     * 
     */
    public TamanhoArquivo createTamanhoArquivo() {
        return new TamanhoArquivo();
    }

    /**
     * Create an instance of {@link HistoricoStatusProcesso }
     * 
     */
    public HistoricoStatusProcesso createHistoricoStatusProcesso() {
        return new HistoricoStatusProcesso();
    }

    /**
     * Create an instance of {@link StatusCusta }
     * 
     */
    public StatusCusta createStatusCusta() {
        return new StatusCusta();
    }

    /**
     * Create an instance of {@link CustaArquivo }
     * 
     */
    public CustaArquivo createCustaArquivo() {
        return new CustaArquivo();
    }

    /**
     * Create an instance of {@link RetornoPublicacao }
     * 
     */
    public RetornoPublicacao createRetornoPublicacao() {
        return new RetornoPublicacao();
    }

    /**
     * Create an instance of {@link RetornaAuditoriaResultado }
     * 
     */
    public RetornaAuditoriaResultado createRetornaAuditoriaResultado() {
        return new RetornaAuditoriaResultado();
    }

    /**
     * Create an instance of {@link ResultadoRetornaDiligencias }
     * 
     */
    public ResultadoRetornaDiligencias createResultadoRetornaDiligencias() {
        return new ResultadoRetornaDiligencias();
    }

    /**
     * Create an instance of {@link Foro }
     * 
     */
    public Foro createForo() {
        return new Foro();
    }

    /**
     * Create an instance of {@link Atividade }
     * 
     */
    public Atividade createAtividade() {
        return new Atividade();
    }

    /**
     * Create an instance of {@link Andamentos }
     * 
     */
    public Andamentos createAndamentos() {
        return new Andamentos();
    }

    /**
     * Create an instance of {@link ArrayOfTipoAndamentoMov }
     * 
     */
    public ArrayOfTipoAndamentoMov createArrayOfTipoAndamentoMov() {
        return new ArrayOfTipoAndamentoMov();
    }

    /**
     * Create an instance of {@link TipoAndamentoMov }
     * 
     */
    public TipoAndamentoMov createTipoAndamentoMov() {
        return new TipoAndamentoMov();
    }

    /**
     * Create an instance of {@link RetornoProcesso }
     * 
     */
    public RetornoProcesso createRetornoProcesso() {
        return new RetornoProcesso();
    }

    /**
     * Create an instance of {@link PessoaFisica }
     * 
     */
    public PessoaFisica createPessoaFisica() {
        return new PessoaFisica();
    }

    /**
     * Create an instance of {@link TrocaSubTipoSolicitacao }
     * 
     */
    public TrocaSubTipoSolicitacao createTrocaSubTipoSolicitacao() {
        return new TrocaSubTipoSolicitacao();
    }

    /**
     * Create an instance of {@link Arquivo }
     * 
     */
    public Arquivo createArquivo() {
        return new Arquivo();
    }

    /**
     * Create an instance of {@link RetornoPublicacaoDescartada }
     * 
     */
    public RetornoPublicacaoDescartada createRetornoPublicacaoDescartada() {
        return new RetornoPublicacaoDescartada();
    }

    /**
     * Create an instance of {@link ParteProcesso }
     * 
     */
    public ParteProcesso createParteProcesso() {
        return new ParteProcesso();
    }

    /**
     * Create an instance of {@link RetornoDadoComplementar }
     * 
     */
    public RetornoDadoComplementar createRetornoDadoComplementar() {
        return new RetornoDadoComplementar();
    }

    /**
     * Create an instance of {@link OpcoesCampoLista }
     * 
     */
    public OpcoesCampoLista createOpcoesCampoLista() {
        return new OpcoesCampoLista();
    }

    /**
     * Create an instance of {@link Instancia }
     * 
     */
    public Instancia createInstancia() {
        return new Instancia();
    }

    /**
     * Create an instance of {@link RetornoCorrespondente }
     * 
     */
    public RetornoCorrespondente createRetornoCorrespondente() {
        return new RetornoCorrespondente();
    }

    /**
     * Create an instance of {@link Comarca }
     * 
     */
    public Comarca createComarca() {
        return new Comarca();
    }

    /**
     * Create an instance of {@link Acao }
     * 
     */
    public Acao createAcao() {
        return new Acao();
    }

    /**
     * Create an instance of {@link NewDataSet }
     * 
     */
    public NewDataSet createNewDataSet() {
        return new NewDataSet();
    }

    /**
     * Create an instance of {@link TipoSolicitacao }
     * 
     */
    public TipoSolicitacao createTipoSolicitacao() {
        return new TipoSolicitacao();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfStepSolicitacao }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "ArrayOfStepSolicitacao")
    public JAXBElement<ArrayOfStepSolicitacao> createArrayOfStepSolicitacao(ArrayOfStepSolicitacao value) {
        return new JAXBElement<ArrayOfStepSolicitacao>(_ArrayOfStepSolicitacao_QNAME, ArrayOfStepSolicitacao.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfCelulaCnpj }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "ArrayOfCelulaCnpj")
    public JAXBElement<ArrayOfCelulaCnpj> createArrayOfCelulaCnpj(ArrayOfCelulaCnpj value) {
        return new JAXBElement<ArrayOfCelulaCnpj>(_ArrayOfCelulaCnpj_QNAME, ArrayOfCelulaCnpj.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfResultadoRetornaDiligencias }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "ArrayOfResultadoRetornaDiligencias")
    public JAXBElement<ArrayOfResultadoRetornaDiligencias> createArrayOfResultadoRetornaDiligencias(ArrayOfResultadoRetornaDiligencias value) {
        return new JAXBElement<ArrayOfResultadoRetornaDiligencias>(_ArrayOfResultadoRetornaDiligencias_QNAME, ArrayOfResultadoRetornaDiligencias.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfSolicitacaoRetorno }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "ArrayOfSolicitacaoRetorno")
    public JAXBElement<ArrayOfSolicitacaoRetorno> createArrayOfSolicitacaoRetorno(ArrayOfSolicitacaoRetorno value) {
        return new JAXBElement<ArrayOfSolicitacaoRetorno>(_ArrayOfSolicitacaoRetorno_QNAME, ArrayOfSolicitacaoRetorno.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfComarca }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "ArrayOfComarca")
    public JAXBElement<ArrayOfComarca> createArrayOfComarca(ArrayOfComarca value) {
        return new JAXBElement<ArrayOfComarca>(_ArrayOfComarca_QNAME, ArrayOfComarca.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfLog }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "ArrayOfLog")
    public JAXBElement<ArrayOfLog> createArrayOfLog(ArrayOfLog value) {
        return new JAXBElement<ArrayOfLog>(_ArrayOfLog_QNAME, ArrayOfLog.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfTipoParte }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "ArrayOfTipoParte")
    public JAXBElement<ArrayOfTipoParte> createArrayOfTipoParte(ArrayOfTipoParte value) {
        return new JAXBElement<ArrayOfTipoParte>(_ArrayOfTipoParte_QNAME, ArrayOfTipoParte.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfOperacaoFinanceira }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "ArrayOfOperacaoFinanceira")
    public JAXBElement<ArrayOfOperacaoFinanceira> createArrayOfOperacaoFinanceira(ArrayOfOperacaoFinanceira value) {
        return new JAXBElement<ArrayOfOperacaoFinanceira>(_ArrayOfOperacaoFinanceira_QNAME, ArrayOfOperacaoFinanceira.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfRetornoProcesso }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "ArrayOfRetornoProcesso")
    public JAXBElement<ArrayOfRetornoProcesso> createArrayOfRetornoProcesso(ArrayOfRetornoProcesso value) {
        return new JAXBElement<ArrayOfRetornoProcesso>(_ArrayOfRetornoProcesso_QNAME, ArrayOfRetornoProcesso.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfResultadoRetornoAgendamento }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "ArrayOfResultadoRetornoAgendamento")
    public JAXBElement<ArrayOfResultadoRetornoAgendamento> createArrayOfResultadoRetornoAgendamento(ArrayOfResultadoRetornoAgendamento value) {
        return new JAXBElement<ArrayOfResultadoRetornoAgendamento>(_ArrayOfResultadoRetornoAgendamento_QNAME, ArrayOfResultadoRetornoAgendamento.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfStatusCusta }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "ArrayOfStatusCusta")
    public JAXBElement<ArrayOfStatusCusta> createArrayOfStatusCusta(ArrayOfStatusCusta value) {
        return new JAXBElement<ArrayOfStatusCusta>(_ArrayOfStatusCusta_QNAME, ArrayOfStatusCusta.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ResultadoAcordo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "ResultadoAcordo")
    public JAXBElement<ResultadoAcordo> createResultadoAcordo(ResultadoAcordo value) {
        return new JAXBElement<ResultadoAcordo>(_ResultadoAcordo_QNAME, ResultadoAcordo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfMateria }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "ArrayOfMateria")
    public JAXBElement<ArrayOfMateria> createArrayOfMateria(ArrayOfMateria value) {
        return new JAXBElement<ArrayOfMateria>(_ArrayOfMateria_QNAME, ArrayOfMateria.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SolicitacaoRetorno }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "SolicitacaoRetorno")
    public JAXBElement<SolicitacaoRetorno> createSolicitacaoRetorno(SolicitacaoRetorno value) {
        return new JAXBElement<SolicitacaoRetorno>(_SolicitacaoRetorno_QNAME, SolicitacaoRetorno.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfMotivoPublicacao }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "ArrayOfMotivoPublicacao")
    public JAXBElement<ArrayOfMotivoPublicacao> createArrayOfMotivoPublicacao(ArrayOfMotivoPublicacao value) {
        return new JAXBElement<ArrayOfMotivoPublicacao>(_ArrayOfMotivoPublicacao_QNAME, ArrayOfMotivoPublicacao.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfAcao }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "ArrayOfAcao")
    public JAXBElement<ArrayOfAcao> createArrayOfAcao(ArrayOfAcao value) {
        return new JAXBElement<ArrayOfAcao>(_ArrayOfAcao_QNAME, ArrayOfAcao.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ResultadoRetornoApenso }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "ResultadoRetornoApenso")
    public JAXBElement<ResultadoRetornoApenso> createResultadoRetornoApenso(ResultadoRetornoApenso value) {
        return new JAXBElement<ResultadoRetornoApenso>(_ResultadoRetornoApenso_QNAME, ResultadoRetornoApenso.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfRetornoTipoFollowUP }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "ArrayOfRetornoTipoFollowUP")
    public JAXBElement<ArrayOfRetornoTipoFollowUP> createArrayOfRetornoTipoFollowUP(ArrayOfRetornoTipoFollowUP value) {
        return new JAXBElement<ArrayOfRetornoTipoFollowUP>(_ArrayOfRetornoTipoFollowUP_QNAME, ArrayOfRetornoTipoFollowUP.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfJustica }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "ArrayOfJustica")
    public JAXBElement<ArrayOfJustica> createArrayOfJustica(ArrayOfJustica value) {
        return new JAXBElement<ArrayOfJustica>(_ArrayOfJustica_QNAME, ArrayOfJustica.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfLote }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "ArrayOfLote")
    public JAXBElement<ArrayOfLote> createArrayOfLote(ArrayOfLote value) {
        return new JAXBElement<ArrayOfLote>(_ArrayOfLote_QNAME, ArrayOfLote.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfTipoSolicitacao }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "ArrayOfTipoSolicitacao")
    public JAXBElement<ArrayOfTipoSolicitacao> createArrayOfTipoSolicitacao(ArrayOfTipoSolicitacao value) {
        return new JAXBElement<ArrayOfTipoSolicitacao>(_ArrayOfTipoSolicitacao_QNAME, ArrayOfTipoSolicitacao.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfStatusProcesso }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "ArrayOfStatusProcesso")
    public JAXBElement<ArrayOfStatusProcesso> createArrayOfStatusProcesso(ArrayOfStatusProcesso value) {
        return new JAXBElement<ArrayOfStatusProcesso>(_ArrayOfStatusProcesso_QNAME, ArrayOfStatusProcesso.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfTipoVara }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "ArrayOfTipoVara")
    public JAXBElement<ArrayOfTipoVara> createArrayOfTipoVara(ArrayOfTipoVara value) {
        return new JAXBElement<ArrayOfTipoVara>(_ArrayOfTipoVara_QNAME, ArrayOfTipoVara.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfPratica }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "ArrayOfPratica")
    public JAXBElement<ArrayOfPratica> createArrayOfPratica(ArrayOfPratica value) {
        return new JAXBElement<ArrayOfPratica>(_ArrayOfPratica_QNAME, ArrayOfPratica.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfHistoricoStatusProcesso }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "ArrayOfHistoricoStatusProcesso")
    public JAXBElement<ArrayOfHistoricoStatusProcesso> createArrayOfHistoricoStatusProcesso(ArrayOfHistoricoStatusProcesso value) {
        return new JAXBElement<ArrayOfHistoricoStatusProcesso>(_ArrayOfHistoricoStatusProcesso_QNAME, ArrayOfHistoricoStatusProcesso.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfForo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "ArrayOfForo")
    public JAXBElement<ArrayOfForo> createArrayOfForo(ArrayOfForo value) {
        return new JAXBElement<ArrayOfForo>(_ArrayOfForo_QNAME, ArrayOfForo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfMovimento }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "ArrayOfMovimento")
    public JAXBElement<ArrayOfMovimento> createArrayOfMovimento(ArrayOfMovimento value) {
        return new JAXBElement<ArrayOfMovimento>(_ArrayOfMovimento_QNAME, ArrayOfMovimento.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfContatos }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "ArrayOfContatos")
    public JAXBElement<ArrayOfContatos> createArrayOfContatos(ArrayOfContatos value) {
        return new JAXBElement<ArrayOfContatos>(_ArrayOfContatos_QNAME, ArrayOfContatos.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfCustaArquivo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "ArrayOfCustaArquivo")
    public JAXBElement<ArrayOfCustaArquivo> createArrayOfCustaArquivo(ArrayOfCustaArquivo value) {
        return new JAXBElement<ArrayOfCustaArquivo>(_ArrayOfCustaArquivo_QNAME, ArrayOfCustaArquivo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfAndamento }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "ArrayOfAndamento")
    public JAXBElement<ArrayOfAndamento> createArrayOfAndamento(ArrayOfAndamento value) {
        return new JAXBElement<ArrayOfAndamento>(_ArrayOfAndamento_QNAME, ArrayOfAndamento.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfResultadoRetornaAgendamentoPorTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "ArrayOfResultadoRetornaAgendamentoPorTipo")
    public JAXBElement<ArrayOfResultadoRetornaAgendamentoPorTipo> createArrayOfResultadoRetornaAgendamentoPorTipo(ArrayOfResultadoRetornaAgendamentoPorTipo value) {
        return new JAXBElement<ArrayOfResultadoRetornaAgendamentoPorTipo>(_ArrayOfResultadoRetornaAgendamentoPorTipo_QNAME, ArrayOfResultadoRetornaAgendamentoPorTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfRetornoPublicacaoDescartada }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "ArrayOfRetornoPublicacaoDescartada")
    public JAXBElement<ArrayOfRetornoPublicacaoDescartada> createArrayOfRetornoPublicacaoDescartada(ArrayOfRetornoPublicacaoDescartada value) {
        return new JAXBElement<ArrayOfRetornoPublicacaoDescartada>(_ArrayOfRetornoPublicacaoDescartada_QNAME, ArrayOfRetornoPublicacaoDescartada.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfDivisaoCliente }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "ArrayOfDivisaoCliente")
    public JAXBElement<ArrayOfDivisaoCliente> createArrayOfDivisaoCliente(ArrayOfDivisaoCliente value) {
        return new JAXBElement<ArrayOfDivisaoCliente>(_ArrayOfDivisaoCliente_QNAME, ArrayOfDivisaoCliente.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ResultadoProcesso }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "ResultadoProcesso")
    public JAXBElement<ResultadoProcesso> createResultadoProcesso(ResultadoProcesso value) {
        return new JAXBElement<ResultadoProcesso>(_ResultadoProcesso_QNAME, ResultadoProcesso.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfNewDataSet }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "ArrayOfNewDataSet")
    public JAXBElement<ArrayOfNewDataSet> createArrayOfNewDataSet(ArrayOfNewDataSet value) {
        return new JAXBElement<ArrayOfNewDataSet>(_ArrayOfNewDataSet_QNAME, ArrayOfNewDataSet.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfDecisao }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "ArrayOfDecisao")
    public JAXBElement<ArrayOfDecisao> createArrayOfDecisao(ArrayOfDecisao value) {
        return new JAXBElement<ArrayOfDecisao>(_ArrayOfDecisao_QNAME, ArrayOfDecisao.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfTipoCusta }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "ArrayOfTipoCusta")
    public JAXBElement<ArrayOfTipoCusta> createArrayOfTipoCusta(ArrayOfTipoCusta value) {
        return new JAXBElement<ArrayOfTipoCusta>(_ArrayOfTipoCusta_QNAME, ArrayOfTipoCusta.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfCusta }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "ArrayOfCusta")
    public JAXBElement<ArrayOfCusta> createArrayOfCusta(ArrayOfCusta value) {
        return new JAXBElement<ArrayOfCusta>(_ArrayOfCusta_QNAME, ArrayOfCusta.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfContratoProcesso }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "ArrayOfContratoProcesso")
    public JAXBElement<ArrayOfContratoProcesso> createArrayOfContratoProcesso(ArrayOfContratoProcesso value) {
        return new JAXBElement<ArrayOfContratoProcesso>(_ArrayOfContratoProcesso_QNAME, ArrayOfContratoProcesso.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfVara }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "ArrayOfVara")
    public JAXBElement<ArrayOfVara> createArrayOfVara(ArrayOfVara value) {
        return new JAXBElement<ArrayOfVara>(_ArrayOfVara_QNAME, ArrayOfVara.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfRetornoDadoComplementar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "ArrayOfRetornoDadoComplementar")
    public JAXBElement<ArrayOfRetornoDadoComplementar> createArrayOfRetornoDadoComplementar(ArrayOfRetornoDadoComplementar value) {
        return new JAXBElement<ArrayOfRetornoDadoComplementar>(_ArrayOfRetornoDadoComplementar_QNAME, ArrayOfRetornoDadoComplementar.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ResultadoTamanhoArquivo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "ResultadoTamanhoArquivo")
    public JAXBElement<ResultadoTamanhoArquivo> createResultadoTamanhoArquivo(ResultadoTamanhoArquivo value) {
        return new JAXBElement<ResultadoTamanhoArquivo>(_ResultadoTamanhoArquivo_QNAME, ResultadoTamanhoArquivo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfSistema }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "ArrayOfSistema")
    public JAXBElement<ArrayOfSistema> createArrayOfSistema(ArrayOfSistema value) {
        return new JAXBElement<ArrayOfSistema>(_ArrayOfSistema_QNAME, ArrayOfSistema.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfTrocaSubTipoSolicitacao }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "ArrayOfTrocaSubTipoSolicitacao")
    public JAXBElement<ArrayOfTrocaSubTipoSolicitacao> createArrayOfTrocaSubTipoSolicitacao(ArrayOfTrocaSubTipoSolicitacao value) {
        return new JAXBElement<ArrayOfTrocaSubTipoSolicitacao>(_ArrayOfTrocaSubTipoSolicitacao_QNAME, ArrayOfTrocaSubTipoSolicitacao.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfOpcoesCampoLista }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "ArrayOfOpcoesCampoLista")
    public JAXBElement<ArrayOfOpcoesCampoLista> createArrayOfOpcoesCampoLista(ArrayOfOpcoesCampoLista value) {
        return new JAXBElement<ArrayOfOpcoesCampoLista>(_ArrayOfOpcoesCampoLista_QNAME, ArrayOfOpcoesCampoLista.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "string")
    public JAXBElement<String> createString(String value) {
        return new JAXBElement<String>(_String_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfRetornoDesdobramento }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "ArrayOfRetornoDesdobramento")
    public JAXBElement<ArrayOfRetornoDesdobramento> createArrayOfRetornoDesdobramento(ArrayOfRetornoDesdobramento value) {
        return new JAXBElement<ArrayOfRetornoDesdobramento>(_ArrayOfRetornoDesdobramento_QNAME, ArrayOfRetornoDesdobramento.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfRetornoGarantia }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "ArrayOfRetornoGarantia")
    public JAXBElement<ArrayOfRetornoGarantia> createArrayOfRetornoGarantia(ArrayOfRetornoGarantia value) {
        return new JAXBElement<ArrayOfRetornoGarantia>(_ArrayOfRetornoGarantia_QNAME, ArrayOfRetornoGarantia.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfRespostaSolicitacao }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "ArrayOfRespostaSolicitacao")
    public JAXBElement<ArrayOfRespostaSolicitacao> createArrayOfRespostaSolicitacao(ArrayOfRespostaSolicitacao value) {
        return new JAXBElement<ArrayOfRespostaSolicitacao>(_ArrayOfRespostaSolicitacao_QNAME, ArrayOfRespostaSolicitacao.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfInstancia }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "ArrayOfInstancia")
    public JAXBElement<ArrayOfInstancia> createArrayOfInstancia(ArrayOfInstancia value) {
        return new JAXBElement<ArrayOfInstancia>(_ArrayOfInstancia_QNAME, ArrayOfInstancia.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfArquivo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "ArrayOfArquivo")
    public JAXBElement<ArrayOfArquivo> createArrayOfArquivo(ArrayOfArquivo value) {
        return new JAXBElement<ArrayOfArquivo>(_ArrayOfArquivo_QNAME, ArrayOfArquivo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfRetornoAgendamento }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "ArrayOfRetornoAgendamento")
    public JAXBElement<ArrayOfRetornoAgendamento> createArrayOfRetornoAgendamento(ArrayOfRetornoAgendamento value) {
        return new JAXBElement<ArrayOfRetornoAgendamento>(_ArrayOfRetornoAgendamento_QNAME, ArrayOfRetornoAgendamento.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RetornoMetodos }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "RetornoMetodos")
    public JAXBElement<RetornoMetodos> createRetornoMetodos(RetornoMetodos value) {
        return new JAXBElement<RetornoMetodos>(_RetornoMetodos_QNAME, RetornoMetodos.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfResultadoRetornaAgendamento }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "ArrayOfResultadoRetornaAgendamento")
    public JAXBElement<ArrayOfResultadoRetornaAgendamento> createArrayOfResultadoRetornaAgendamento(ArrayOfResultadoRetornaAgendamento value) {
        return new JAXBElement<ArrayOfResultadoRetornaAgendamento>(_ArrayOfResultadoRetornaAgendamento_QNAME, ArrayOfResultadoRetornaAgendamento.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfTipoAgendamento }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "ArrayOfTipoAgendamento")
    public JAXBElement<ArrayOfTipoAgendamento> createArrayOfTipoAgendamento(ArrayOfTipoAgendamento value) {
        return new JAXBElement<ArrayOfTipoAgendamento>(_ArrayOfTipoAgendamento_QNAME, ArrayOfTipoAgendamento.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfRetornoPublicacao }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "ArrayOfRetornoPublicacao")
    public JAXBElement<ArrayOfRetornoPublicacao> createArrayOfRetornoPublicacao(ArrayOfRetornoPublicacao value) {
        return new JAXBElement<ArrayOfRetornoPublicacao>(_ArrayOfRetornoPublicacao_QNAME, ArrayOfRetornoPublicacao.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfParteProcesso }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "ArrayOfParteProcesso")
    public JAXBElement<ArrayOfParteProcesso> createArrayOfParteProcesso(ArrayOfParteProcesso value) {
        return new JAXBElement<ArrayOfParteProcesso>(_ArrayOfParteProcesso_QNAME, ArrayOfParteProcesso.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfRetornoCorrespondente }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "ArrayOfRetornoCorrespondente")
    public JAXBElement<ArrayOfRetornoCorrespondente> createArrayOfRetornoCorrespondente(ArrayOfRetornoCorrespondente value) {
        return new JAXBElement<ArrayOfRetornoCorrespondente>(_ArrayOfRetornoCorrespondente_QNAME, ArrayOfRetornoCorrespondente.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfRetornoFollowUP }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "ArrayOfRetornoFollowUP")
    public JAXBElement<ArrayOfRetornoFollowUP> createArrayOfRetornoFollowUP(ArrayOfRetornoFollowUP value) {
        return new JAXBElement<ArrayOfRetornoFollowUP>(_ArrayOfRetornoFollowUP_QNAME, ArrayOfRetornoFollowUP.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfAtividade }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "ArrayOfAtividade")
    public JAXBElement<ArrayOfAtividade> createArrayOfAtividade(ArrayOfAtividade value) {
        return new JAXBElement<ArrayOfAtividade>(_ArrayOfAtividade_QNAME, ArrayOfAtividade.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfResultadoRetornaAuditoriaResultado }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "ArrayOfResultadoRetornaAuditoriaResultado")
    public JAXBElement<ArrayOfResultadoRetornaAuditoriaResultado> createArrayOfResultadoRetornaAuditoriaResultado(ArrayOfResultadoRetornaAuditoriaResultado value) {
        return new JAXBElement<ArrayOfResultadoRetornaAuditoriaResultado>(_ArrayOfResultadoRetornaAuditoriaResultado_QNAME, ArrayOfResultadoRetornaAuditoriaResultado.class, null, value);
    }

}
