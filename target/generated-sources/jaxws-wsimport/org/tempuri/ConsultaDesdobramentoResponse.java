
package org.tempuri;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java de anonymous complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ConsultaDesdobramentoResult" type="{http://tempuri.org/}ArrayOfRetornoDesdobramento" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "consultaDesdobramentoResult"
})
@XmlRootElement(name = "ConsultaDesdobramentoResponse")
public class ConsultaDesdobramentoResponse {

    @XmlElement(name = "ConsultaDesdobramentoResult")
    protected ArrayOfRetornoDesdobramento consultaDesdobramentoResult;

    /**
     * Obtém o valor da propriedade consultaDesdobramentoResult.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfRetornoDesdobramento }
     *     
     */
    public ArrayOfRetornoDesdobramento getConsultaDesdobramentoResult() {
        return consultaDesdobramentoResult;
    }

    /**
     * Define o valor da propriedade consultaDesdobramentoResult.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfRetornoDesdobramento }
     *     
     */
    public void setConsultaDesdobramentoResult(ArrayOfRetornoDesdobramento value) {
        this.consultaDesdobramentoResult = value;
    }

}
