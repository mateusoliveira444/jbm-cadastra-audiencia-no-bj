
package org.tempuri;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java de Garantia complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="Garantia">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="idProcessoUnico" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="dtGarantia" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="levantado" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="dtLevantado" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="memo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="dtCadastro" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="dtExclusao" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="motivo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="dtAlteracao" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="idGarantia" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="tipoGarantia" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="valor" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="marca" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="modelo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="chassi" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="anoFabricacao" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="anoModelo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="cor" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="placa" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="renavam" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="tipoVeiculo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="tipoCombustivel" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="estadoConservacao" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="codMolicar" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="vlrMolicar" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="serie" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="descricaoMotor" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Garantia", propOrder = {
    "idProcessoUnico",
    "dtGarantia",
    "levantado",
    "dtLevantado",
    "memo",
    "dtCadastro",
    "dtExclusao",
    "motivo",
    "dtAlteracao",
    "idGarantia",
    "tipoGarantia",
    "valor",
    "marca",
    "modelo",
    "chassi",
    "anoFabricacao",
    "anoModelo",
    "cor",
    "placa",
    "renavam",
    "tipoVeiculo",
    "tipoCombustivel",
    "estadoConservacao",
    "codMolicar",
    "vlrMolicar",
    "serie",
    "descricaoMotor"
})
public class Garantia {

    protected String idProcessoUnico;
    protected String dtGarantia;
    protected String levantado;
    protected String dtLevantado;
    protected String memo;
    protected String dtCadastro;
    protected String dtExclusao;
    protected String motivo;
    protected String dtAlteracao;
    protected String idGarantia;
    protected String tipoGarantia;
    protected String valor;
    protected String marca;
    protected String modelo;
    protected String chassi;
    protected String anoFabricacao;
    protected String anoModelo;
    protected String cor;
    protected String placa;
    protected String renavam;
    protected String tipoVeiculo;
    protected String tipoCombustivel;
    protected String estadoConservacao;
    protected String codMolicar;
    protected String vlrMolicar;
    protected String serie;
    protected String descricaoMotor;

    /**
     * Obtém o valor da propriedade idProcessoUnico.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdProcessoUnico() {
        return idProcessoUnico;
    }

    /**
     * Define o valor da propriedade idProcessoUnico.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdProcessoUnico(String value) {
        this.idProcessoUnico = value;
    }

    /**
     * Obtém o valor da propriedade dtGarantia.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDtGarantia() {
        return dtGarantia;
    }

    /**
     * Define o valor da propriedade dtGarantia.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDtGarantia(String value) {
        this.dtGarantia = value;
    }

    /**
     * Obtém o valor da propriedade levantado.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLevantado() {
        return levantado;
    }

    /**
     * Define o valor da propriedade levantado.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLevantado(String value) {
        this.levantado = value;
    }

    /**
     * Obtém o valor da propriedade dtLevantado.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDtLevantado() {
        return dtLevantado;
    }

    /**
     * Define o valor da propriedade dtLevantado.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDtLevantado(String value) {
        this.dtLevantado = value;
    }

    /**
     * Obtém o valor da propriedade memo.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMemo() {
        return memo;
    }

    /**
     * Define o valor da propriedade memo.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMemo(String value) {
        this.memo = value;
    }

    /**
     * Obtém o valor da propriedade dtCadastro.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDtCadastro() {
        return dtCadastro;
    }

    /**
     * Define o valor da propriedade dtCadastro.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDtCadastro(String value) {
        this.dtCadastro = value;
    }

    /**
     * Obtém o valor da propriedade dtExclusao.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDtExclusao() {
        return dtExclusao;
    }

    /**
     * Define o valor da propriedade dtExclusao.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDtExclusao(String value) {
        this.dtExclusao = value;
    }

    /**
     * Obtém o valor da propriedade motivo.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMotivo() {
        return motivo;
    }

    /**
     * Define o valor da propriedade motivo.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMotivo(String value) {
        this.motivo = value;
    }

    /**
     * Obtém o valor da propriedade dtAlteracao.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDtAlteracao() {
        return dtAlteracao;
    }

    /**
     * Define o valor da propriedade dtAlteracao.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDtAlteracao(String value) {
        this.dtAlteracao = value;
    }

    /**
     * Obtém o valor da propriedade idGarantia.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdGarantia() {
        return idGarantia;
    }

    /**
     * Define o valor da propriedade idGarantia.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdGarantia(String value) {
        this.idGarantia = value;
    }

    /**
     * Obtém o valor da propriedade tipoGarantia.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTipoGarantia() {
        return tipoGarantia;
    }

    /**
     * Define o valor da propriedade tipoGarantia.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTipoGarantia(String value) {
        this.tipoGarantia = value;
    }

    /**
     * Obtém o valor da propriedade valor.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getValor() {
        return valor;
    }

    /**
     * Define o valor da propriedade valor.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setValor(String value) {
        this.valor = value;
    }

    /**
     * Obtém o valor da propriedade marca.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMarca() {
        return marca;
    }

    /**
     * Define o valor da propriedade marca.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMarca(String value) {
        this.marca = value;
    }

    /**
     * Obtém o valor da propriedade modelo.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getModelo() {
        return modelo;
    }

    /**
     * Define o valor da propriedade modelo.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setModelo(String value) {
        this.modelo = value;
    }

    /**
     * Obtém o valor da propriedade chassi.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getChassi() {
        return chassi;
    }

    /**
     * Define o valor da propriedade chassi.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setChassi(String value) {
        this.chassi = value;
    }

    /**
     * Obtém o valor da propriedade anoFabricacao.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAnoFabricacao() {
        return anoFabricacao;
    }

    /**
     * Define o valor da propriedade anoFabricacao.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAnoFabricacao(String value) {
        this.anoFabricacao = value;
    }

    /**
     * Obtém o valor da propriedade anoModelo.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAnoModelo() {
        return anoModelo;
    }

    /**
     * Define o valor da propriedade anoModelo.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAnoModelo(String value) {
        this.anoModelo = value;
    }

    /**
     * Obtém o valor da propriedade cor.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCor() {
        return cor;
    }

    /**
     * Define o valor da propriedade cor.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCor(String value) {
        this.cor = value;
    }

    /**
     * Obtém o valor da propriedade placa.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPlaca() {
        return placa;
    }

    /**
     * Define o valor da propriedade placa.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPlaca(String value) {
        this.placa = value;
    }

    /**
     * Obtém o valor da propriedade renavam.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRenavam() {
        return renavam;
    }

    /**
     * Define o valor da propriedade renavam.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRenavam(String value) {
        this.renavam = value;
    }

    /**
     * Obtém o valor da propriedade tipoVeiculo.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTipoVeiculo() {
        return tipoVeiculo;
    }

    /**
     * Define o valor da propriedade tipoVeiculo.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTipoVeiculo(String value) {
        this.tipoVeiculo = value;
    }

    /**
     * Obtém o valor da propriedade tipoCombustivel.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTipoCombustivel() {
        return tipoCombustivel;
    }

    /**
     * Define o valor da propriedade tipoCombustivel.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTipoCombustivel(String value) {
        this.tipoCombustivel = value;
    }

    /**
     * Obtém o valor da propriedade estadoConservacao.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEstadoConservacao() {
        return estadoConservacao;
    }

    /**
     * Define o valor da propriedade estadoConservacao.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEstadoConservacao(String value) {
        this.estadoConservacao = value;
    }

    /**
     * Obtém o valor da propriedade codMolicar.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodMolicar() {
        return codMolicar;
    }

    /**
     * Define o valor da propriedade codMolicar.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodMolicar(String value) {
        this.codMolicar = value;
    }

    /**
     * Obtém o valor da propriedade vlrMolicar.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVlrMolicar() {
        return vlrMolicar;
    }

    /**
     * Define o valor da propriedade vlrMolicar.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVlrMolicar(String value) {
        this.vlrMolicar = value;
    }

    /**
     * Obtém o valor da propriedade serie.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSerie() {
        return serie;
    }

    /**
     * Define o valor da propriedade serie.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSerie(String value) {
        this.serie = value;
    }

    /**
     * Obtém o valor da propriedade descricaoMotor.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescricaoMotor() {
        return descricaoMotor;
    }

    /**
     * Define o valor da propriedade descricaoMotor.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescricaoMotor(String value) {
        this.descricaoMotor = value;
    }

}
