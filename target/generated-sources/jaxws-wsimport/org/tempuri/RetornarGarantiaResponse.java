
package org.tempuri;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java de anonymous complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="RetornarGarantiaResult" type="{http://tempuri.org/}ArrayOfRetornoGarantia" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "retornarGarantiaResult"
})
@XmlRootElement(name = "RetornarGarantiaResponse")
public class RetornarGarantiaResponse {

    @XmlElement(name = "RetornarGarantiaResult")
    protected ArrayOfRetornoGarantia retornarGarantiaResult;

    /**
     * Obtém o valor da propriedade retornarGarantiaResult.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfRetornoGarantia }
     *     
     */
    public ArrayOfRetornoGarantia getRetornarGarantiaResult() {
        return retornarGarantiaResult;
    }

    /**
     * Define o valor da propriedade retornarGarantiaResult.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfRetornoGarantia }
     *     
     */
    public void setRetornarGarantiaResult(ArrayOfRetornoGarantia value) {
        this.retornarGarantiaResult = value;
    }

}
