
package org.tempuri;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Classe Java de SolicitacaoRetorno complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="SolicitacaoRetorno">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="IDProcessoUnico" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="NumeroProcesso" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="IDSolicitacao" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="Status" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="TipoSolicitacao" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Observacao" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DataSolicitacao" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="DataLimite" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="ParteContraria" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ControleCliente" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Comarca" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="UF" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DataConclusao" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="ArquivoDisponivel" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="UltinaPagina" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="ObsFinal" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="StatusOriginal" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="PercentualRealizado" type="{http://www.w3.org/2001/XMLSchema}double"/>
 *         &lt;element name="StatusFinal" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="StatusFinalMotivo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="EmCumprimento" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Correspondente" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SolicitacaoRetorno", propOrder = {
    "idProcessoUnico",
    "numeroProcesso",
    "idSolicitacao",
    "status",
    "tipoSolicitacao",
    "observacao",
    "dataSolicitacao",
    "dataLimite",
    "parteContraria",
    "controleCliente",
    "comarca",
    "uf",
    "dataConclusao",
    "arquivoDisponivel",
    "ultinaPagina",
    "obsFinal",
    "statusOriginal",
    "percentualRealizado",
    "statusFinal",
    "statusFinalMotivo",
    "emCumprimento",
    "correspondente"
})
public class SolicitacaoRetorno {

    @XmlElement(name = "IDProcessoUnico")
    protected String idProcessoUnico;
    @XmlElement(name = "NumeroProcesso")
    protected String numeroProcesso;
    @XmlElement(name = "IDSolicitacao")
    protected int idSolicitacao;
    @XmlElement(name = "Status")
    protected String status;
    @XmlElement(name = "TipoSolicitacao")
    protected String tipoSolicitacao;
    @XmlElement(name = "Observacao")
    protected String observacao;
    @XmlElement(name = "DataSolicitacao", required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar dataSolicitacao;
    @XmlElement(name = "DataLimite", required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar dataLimite;
    @XmlElement(name = "ParteContraria")
    protected String parteContraria;
    @XmlElement(name = "ControleCliente")
    protected String controleCliente;
    @XmlElement(name = "Comarca")
    protected String comarca;
    @XmlElement(name = "UF")
    protected String uf;
    @XmlElement(name = "DataConclusao", required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar dataConclusao;
    @XmlElement(name = "ArquivoDisponivel")
    protected boolean arquivoDisponivel;
    @XmlElement(name = "UltinaPagina")
    protected int ultinaPagina;
    @XmlElement(name = "ObsFinal")
    protected String obsFinal;
    @XmlElement(name = "StatusOriginal")
    protected String statusOriginal;
    @XmlElement(name = "PercentualRealizado")
    protected double percentualRealizado;
    @XmlElement(name = "StatusFinal")
    protected String statusFinal;
    @XmlElement(name = "StatusFinalMotivo")
    protected String statusFinalMotivo;
    @XmlElement(name = "EmCumprimento")
    protected String emCumprimento;
    @XmlElement(name = "Correspondente")
    protected String correspondente;

    /**
     * Obtém o valor da propriedade idProcessoUnico.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIDProcessoUnico() {
        return idProcessoUnico;
    }

    /**
     * Define o valor da propriedade idProcessoUnico.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIDProcessoUnico(String value) {
        this.idProcessoUnico = value;
    }

    /**
     * Obtém o valor da propriedade numeroProcesso.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNumeroProcesso() {
        return numeroProcesso;
    }

    /**
     * Define o valor da propriedade numeroProcesso.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNumeroProcesso(String value) {
        this.numeroProcesso = value;
    }

    /**
     * Obtém o valor da propriedade idSolicitacao.
     * 
     */
    public int getIDSolicitacao() {
        return idSolicitacao;
    }

    /**
     * Define o valor da propriedade idSolicitacao.
     * 
     */
    public void setIDSolicitacao(int value) {
        this.idSolicitacao = value;
    }

    /**
     * Obtém o valor da propriedade status.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStatus() {
        return status;
    }

    /**
     * Define o valor da propriedade status.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStatus(String value) {
        this.status = value;
    }

    /**
     * Obtém o valor da propriedade tipoSolicitacao.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTipoSolicitacao() {
        return tipoSolicitacao;
    }

    /**
     * Define o valor da propriedade tipoSolicitacao.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTipoSolicitacao(String value) {
        this.tipoSolicitacao = value;
    }

    /**
     * Obtém o valor da propriedade observacao.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getObservacao() {
        return observacao;
    }

    /**
     * Define o valor da propriedade observacao.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setObservacao(String value) {
        this.observacao = value;
    }

    /**
     * Obtém o valor da propriedade dataSolicitacao.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDataSolicitacao() {
        return dataSolicitacao;
    }

    /**
     * Define o valor da propriedade dataSolicitacao.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDataSolicitacao(XMLGregorianCalendar value) {
        this.dataSolicitacao = value;
    }

    /**
     * Obtém o valor da propriedade dataLimite.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDataLimite() {
        return dataLimite;
    }

    /**
     * Define o valor da propriedade dataLimite.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDataLimite(XMLGregorianCalendar value) {
        this.dataLimite = value;
    }

    /**
     * Obtém o valor da propriedade parteContraria.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getParteContraria() {
        return parteContraria;
    }

    /**
     * Define o valor da propriedade parteContraria.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setParteContraria(String value) {
        this.parteContraria = value;
    }

    /**
     * Obtém o valor da propriedade controleCliente.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getControleCliente() {
        return controleCliente;
    }

    /**
     * Define o valor da propriedade controleCliente.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setControleCliente(String value) {
        this.controleCliente = value;
    }

    /**
     * Obtém o valor da propriedade comarca.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getComarca() {
        return comarca;
    }

    /**
     * Define o valor da propriedade comarca.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setComarca(String value) {
        this.comarca = value;
    }

    /**
     * Obtém o valor da propriedade uf.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUF() {
        return uf;
    }

    /**
     * Define o valor da propriedade uf.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUF(String value) {
        this.uf = value;
    }

    /**
     * Obtém o valor da propriedade dataConclusao.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDataConclusao() {
        return dataConclusao;
    }

    /**
     * Define o valor da propriedade dataConclusao.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDataConclusao(XMLGregorianCalendar value) {
        this.dataConclusao = value;
    }

    /**
     * Obtém o valor da propriedade arquivoDisponivel.
     * 
     */
    public boolean isArquivoDisponivel() {
        return arquivoDisponivel;
    }

    /**
     * Define o valor da propriedade arquivoDisponivel.
     * 
     */
    public void setArquivoDisponivel(boolean value) {
        this.arquivoDisponivel = value;
    }

    /**
     * Obtém o valor da propriedade ultinaPagina.
     * 
     */
    public int getUltinaPagina() {
        return ultinaPagina;
    }

    /**
     * Define o valor da propriedade ultinaPagina.
     * 
     */
    public void setUltinaPagina(int value) {
        this.ultinaPagina = value;
    }

    /**
     * Obtém o valor da propriedade obsFinal.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getObsFinal() {
        return obsFinal;
    }

    /**
     * Define o valor da propriedade obsFinal.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setObsFinal(String value) {
        this.obsFinal = value;
    }

    /**
     * Obtém o valor da propriedade statusOriginal.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStatusOriginal() {
        return statusOriginal;
    }

    /**
     * Define o valor da propriedade statusOriginal.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStatusOriginal(String value) {
        this.statusOriginal = value;
    }

    /**
     * Obtém o valor da propriedade percentualRealizado.
     * 
     */
    public double getPercentualRealizado() {
        return percentualRealizado;
    }

    /**
     * Define o valor da propriedade percentualRealizado.
     * 
     */
    public void setPercentualRealizado(double value) {
        this.percentualRealizado = value;
    }

    /**
     * Obtém o valor da propriedade statusFinal.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStatusFinal() {
        return statusFinal;
    }

    /**
     * Define o valor da propriedade statusFinal.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStatusFinal(String value) {
        this.statusFinal = value;
    }

    /**
     * Obtém o valor da propriedade statusFinalMotivo.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStatusFinalMotivo() {
        return statusFinalMotivo;
    }

    /**
     * Define o valor da propriedade statusFinalMotivo.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStatusFinalMotivo(String value) {
        this.statusFinalMotivo = value;
    }

    /**
     * Obtém o valor da propriedade emCumprimento.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEmCumprimento() {
        return emCumprimento;
    }

    /**
     * Define o valor da propriedade emCumprimento.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEmCumprimento(String value) {
        this.emCumprimento = value;
    }

    /**
     * Obtém o valor da propriedade correspondente.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCorrespondente() {
        return correspondente;
    }

    /**
     * Define o valor da propriedade correspondente.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCorrespondente(String value) {
        this.correspondente = value;
    }

}
