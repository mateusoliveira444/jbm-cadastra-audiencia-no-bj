
package org.tempuri;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Classe Java de Lote complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="Lote">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="IdLote" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="IdCusta" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="DataHora" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="Observacao" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="StatusOrigemLote" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="StatusDestinoLote" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="QtdItensLote" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="Valor" type="{http://www.w3.org/2001/XMLSchema}double"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Lote", propOrder = {
    "idLote",
    "idCusta",
    "dataHora",
    "observacao",
    "statusOrigemLote",
    "statusDestinoLote",
    "qtdItensLote",
    "valor"
})
public class Lote {

    @XmlElement(name = "IdLote")
    protected int idLote;
    @XmlElement(name = "IdCusta")
    protected int idCusta;
    @XmlElement(name = "DataHora", required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar dataHora;
    @XmlElement(name = "Observacao")
    protected String observacao;
    @XmlElement(name = "StatusOrigemLote")
    protected String statusOrigemLote;
    @XmlElement(name = "StatusDestinoLote")
    protected String statusDestinoLote;
    @XmlElement(name = "QtdItensLote")
    protected int qtdItensLote;
    @XmlElement(name = "Valor")
    protected double valor;

    /**
     * Obtém o valor da propriedade idLote.
     * 
     */
    public int getIdLote() {
        return idLote;
    }

    /**
     * Define o valor da propriedade idLote.
     * 
     */
    public void setIdLote(int value) {
        this.idLote = value;
    }

    /**
     * Obtém o valor da propriedade idCusta.
     * 
     */
    public int getIdCusta() {
        return idCusta;
    }

    /**
     * Define o valor da propriedade idCusta.
     * 
     */
    public void setIdCusta(int value) {
        this.idCusta = value;
    }

    /**
     * Obtém o valor da propriedade dataHora.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDataHora() {
        return dataHora;
    }

    /**
     * Define o valor da propriedade dataHora.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDataHora(XMLGregorianCalendar value) {
        this.dataHora = value;
    }

    /**
     * Obtém o valor da propriedade observacao.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getObservacao() {
        return observacao;
    }

    /**
     * Define o valor da propriedade observacao.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setObservacao(String value) {
        this.observacao = value;
    }

    /**
     * Obtém o valor da propriedade statusOrigemLote.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStatusOrigemLote() {
        return statusOrigemLote;
    }

    /**
     * Define o valor da propriedade statusOrigemLote.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStatusOrigemLote(String value) {
        this.statusOrigemLote = value;
    }

    /**
     * Obtém o valor da propriedade statusDestinoLote.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStatusDestinoLote() {
        return statusDestinoLote;
    }

    /**
     * Define o valor da propriedade statusDestinoLote.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStatusDestinoLote(String value) {
        this.statusDestinoLote = value;
    }

    /**
     * Obtém o valor da propriedade qtdItensLote.
     * 
     */
    public int getQtdItensLote() {
        return qtdItensLote;
    }

    /**
     * Define o valor da propriedade qtdItensLote.
     * 
     */
    public void setQtdItensLote(int value) {
        this.qtdItensLote = value;
    }

    /**
     * Obtém o valor da propriedade valor.
     * 
     */
    public double getValor() {
        return valor;
    }

    /**
     * Define o valor da propriedade valor.
     * 
     */
    public void setValor(double value) {
        this.valor = value;
    }

}
