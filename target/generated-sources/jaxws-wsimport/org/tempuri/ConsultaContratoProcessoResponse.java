
package org.tempuri;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java de anonymous complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ConsultaContratoProcessoResult" type="{http://tempuri.org/}ArrayOfContratoProcesso" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "consultaContratoProcessoResult"
})
@XmlRootElement(name = "ConsultaContratoProcessoResponse")
public class ConsultaContratoProcessoResponse {

    @XmlElement(name = "ConsultaContratoProcessoResult")
    protected ArrayOfContratoProcesso consultaContratoProcessoResult;

    /**
     * Obtém o valor da propriedade consultaContratoProcessoResult.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfContratoProcesso }
     *     
     */
    public ArrayOfContratoProcesso getConsultaContratoProcessoResult() {
        return consultaContratoProcessoResult;
    }

    /**
     * Define o valor da propriedade consultaContratoProcessoResult.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfContratoProcesso }
     *     
     */
    public void setConsultaContratoProcessoResult(ArrayOfContratoProcesso value) {
        this.consultaContratoProcessoResult = value;
    }

}
