
package org.tempuri;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java de anonymous complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ResultadoProcesso" type="{http://tempuri.org/}ResultadoProcesso"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "resultadoProcesso"
})
@XmlRootElement(name = "ConsultaProcessoIDResponse")
public class ConsultaProcessoIDResponse {

    @XmlElement(name = "ResultadoProcesso", required = true, nillable = true)
    protected ResultadoProcesso resultadoProcesso;

    /**
     * Obtém o valor da propriedade resultadoProcesso.
     * 
     * @return
     *     possible object is
     *     {@link ResultadoProcesso }
     *     
     */
    public ResultadoProcesso getResultadoProcesso() {
        return resultadoProcesso;
    }

    /**
     * Define o valor da propriedade resultadoProcesso.
     * 
     * @param value
     *     allowed object is
     *     {@link ResultadoProcesso }
     *     
     */
    public void setResultadoProcesso(ResultadoProcesso value) {
        this.resultadoProcesso = value;
    }

}
