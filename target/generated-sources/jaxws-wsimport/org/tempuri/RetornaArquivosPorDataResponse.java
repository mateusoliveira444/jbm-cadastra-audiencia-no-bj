
package org.tempuri;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java de anonymous complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="RetornaArquivosPorDataResult" type="{http://tempuri.org/}ArrayOfArquivo" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "retornaArquivosPorDataResult"
})
@XmlRootElement(name = "RetornaArquivosPorDataResponse")
public class RetornaArquivosPorDataResponse {

    @XmlElement(name = "RetornaArquivosPorDataResult")
    protected ArrayOfArquivo retornaArquivosPorDataResult;

    /**
     * Obtém o valor da propriedade retornaArquivosPorDataResult.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfArquivo }
     *     
     */
    public ArrayOfArquivo getRetornaArquivosPorDataResult() {
        return retornaArquivosPorDataResult;
    }

    /**
     * Define o valor da propriedade retornaArquivosPorDataResult.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfArquivo }
     *     
     */
    public void setRetornaArquivosPorDataResult(ArrayOfArquivo value) {
        this.retornaArquivosPorDataResult = value;
    }

}
