
package org.tempuri;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java de anonymous complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ResultadoRetornoApenso" type="{http://tempuri.org/}ResultadoRetornoApenso"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "resultadoRetornoApenso"
})
@XmlRootElement(name = "ConsultaApensoResponse")
public class ConsultaApensoResponse {

    @XmlElement(name = "ResultadoRetornoApenso", required = true, nillable = true)
    protected ResultadoRetornoApenso resultadoRetornoApenso;

    /**
     * Obtém o valor da propriedade resultadoRetornoApenso.
     * 
     * @return
     *     possible object is
     *     {@link ResultadoRetornoApenso }
     *     
     */
    public ResultadoRetornoApenso getResultadoRetornoApenso() {
        return resultadoRetornoApenso;
    }

    /**
     * Define o valor da propriedade resultadoRetornoApenso.
     * 
     * @param value
     *     allowed object is
     *     {@link ResultadoRetornoApenso }
     *     
     */
    public void setResultadoRetornoApenso(ResultadoRetornoApenso value) {
        this.resultadoRetornoApenso = value;
    }

}
