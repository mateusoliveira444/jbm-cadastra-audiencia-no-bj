
package org.tempuri;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java de ArrayOfHistoricoStatusProcesso complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="ArrayOfHistoricoStatusProcesso">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="HistoricoStatusProcesso" type="{http://tempuri.org/}HistoricoStatusProcesso" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ArrayOfHistoricoStatusProcesso", propOrder = {
    "historicoStatusProcesso"
})
public class ArrayOfHistoricoStatusProcesso {

    @XmlElement(name = "HistoricoStatusProcesso", nillable = true)
    protected List<HistoricoStatusProcesso> historicoStatusProcesso;

    /**
     * Gets the value of the historicoStatusProcesso property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the historicoStatusProcesso property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getHistoricoStatusProcesso().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link HistoricoStatusProcesso }
     * 
     * 
     */
    public List<HistoricoStatusProcesso> getHistoricoStatusProcesso() {
        if (historicoStatusProcesso == null) {
            historicoStatusProcesso = new ArrayList<HistoricoStatusProcesso>();
        }
        return this.historicoStatusProcesso;
    }

}
