
package org.tempuri;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java de anonymous complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="lstrProcessoUnico" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="lintIdProcessoStatus" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="lintIdDecisao" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="lstrDataEncerramento" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="lstrUsuarioTerceiro" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="lstrUsuario" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="lstrSenha" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "lstrProcessoUnico",
    "lintIdProcessoStatus",
    "lintIdDecisao",
    "lstrDataEncerramento",
    "lstrUsuarioTerceiro",
    "lstrUsuario",
    "lstrSenha"
})
@XmlRootElement(name = "AlterarStatusProcesso")
public class AlterarStatusProcesso {

    protected String lstrProcessoUnico;
    protected int lintIdProcessoStatus;
    protected int lintIdDecisao;
    protected String lstrDataEncerramento;
    protected String lstrUsuarioTerceiro;
    protected String lstrUsuario;
    protected String lstrSenha;

    /**
     * Obtém o valor da propriedade lstrProcessoUnico.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLstrProcessoUnico() {
        return lstrProcessoUnico;
    }

    /**
     * Define o valor da propriedade lstrProcessoUnico.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLstrProcessoUnico(String value) {
        this.lstrProcessoUnico = value;
    }

    /**
     * Obtém o valor da propriedade lintIdProcessoStatus.
     * 
     */
    public int getLintIdProcessoStatus() {
        return lintIdProcessoStatus;
    }

    /**
     * Define o valor da propriedade lintIdProcessoStatus.
     * 
     */
    public void setLintIdProcessoStatus(int value) {
        this.lintIdProcessoStatus = value;
    }

    /**
     * Obtém o valor da propriedade lintIdDecisao.
     * 
     */
    public int getLintIdDecisao() {
        return lintIdDecisao;
    }

    /**
     * Define o valor da propriedade lintIdDecisao.
     * 
     */
    public void setLintIdDecisao(int value) {
        this.lintIdDecisao = value;
    }

    /**
     * Obtém o valor da propriedade lstrDataEncerramento.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLstrDataEncerramento() {
        return lstrDataEncerramento;
    }

    /**
     * Define o valor da propriedade lstrDataEncerramento.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLstrDataEncerramento(String value) {
        this.lstrDataEncerramento = value;
    }

    /**
     * Obtém o valor da propriedade lstrUsuarioTerceiro.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLstrUsuarioTerceiro() {
        return lstrUsuarioTerceiro;
    }

    /**
     * Define o valor da propriedade lstrUsuarioTerceiro.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLstrUsuarioTerceiro(String value) {
        this.lstrUsuarioTerceiro = value;
    }

    /**
     * Obtém o valor da propriedade lstrUsuario.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLstrUsuario() {
        return lstrUsuario;
    }

    /**
     * Define o valor da propriedade lstrUsuario.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLstrUsuario(String value) {
        this.lstrUsuario = value;
    }

    /**
     * Obtém o valor da propriedade lstrSenha.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLstrSenha() {
        return lstrSenha;
    }

    /**
     * Define o valor da propriedade lstrSenha.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLstrSenha(String value) {
        this.lstrSenha = value;
    }

}
