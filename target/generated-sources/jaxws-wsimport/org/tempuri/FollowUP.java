
package org.tempuri;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java de FollowUP complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="FollowUP">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="IDSolicitacao" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="TextoFollowUP" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="IDTipoFollowUP" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="UsuarioSolicitante" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "FollowUP", propOrder = {
    "idSolicitacao",
    "textoFollowUP",
    "idTipoFollowUP",
    "usuarioSolicitante"
})
public class FollowUP {

    @XmlElement(name = "IDSolicitacao")
    protected int idSolicitacao;
    @XmlElement(name = "TextoFollowUP")
    protected String textoFollowUP;
    @XmlElement(name = "IDTipoFollowUP")
    protected String idTipoFollowUP;
    @XmlElement(name = "UsuarioSolicitante")
    protected String usuarioSolicitante;

    /**
     * Obtém o valor da propriedade idSolicitacao.
     * 
     */
    public int getIDSolicitacao() {
        return idSolicitacao;
    }

    /**
     * Define o valor da propriedade idSolicitacao.
     * 
     */
    public void setIDSolicitacao(int value) {
        this.idSolicitacao = value;
    }

    /**
     * Obtém o valor da propriedade textoFollowUP.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTextoFollowUP() {
        return textoFollowUP;
    }

    /**
     * Define o valor da propriedade textoFollowUP.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTextoFollowUP(String value) {
        this.textoFollowUP = value;
    }

    /**
     * Obtém o valor da propriedade idTipoFollowUP.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIDTipoFollowUP() {
        return idTipoFollowUP;
    }

    /**
     * Define o valor da propriedade idTipoFollowUP.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIDTipoFollowUP(String value) {
        this.idTipoFollowUP = value;
    }

    /**
     * Obtém o valor da propriedade usuarioSolicitante.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUsuarioSolicitante() {
        return usuarioSolicitante;
    }

    /**
     * Define o valor da propriedade usuarioSolicitante.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUsuarioSolicitante(String value) {
        this.usuarioSolicitante = value;
    }

}
