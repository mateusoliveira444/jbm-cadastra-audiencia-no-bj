
package org.tempuri;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Classe Java de RetornoPublicacaoDescartada complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="RetornoPublicacaoDescartada">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="IDPublicacao" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="DataPublicacao" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="DataColagem" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="Fonte" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Texto" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ProcessoUnico" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DataExclusao" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="MotivoExclusao" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "RetornoPublicacaoDescartada", propOrder = {
    "idPublicacao",
    "dataPublicacao",
    "dataColagem",
    "fonte",
    "texto",
    "processoUnico",
    "dataExclusao",
    "motivoExclusao"
})
public class RetornoPublicacaoDescartada {

    @XmlElement(name = "IDPublicacao")
    protected int idPublicacao;
    @XmlElement(name = "DataPublicacao", required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar dataPublicacao;
    @XmlElement(name = "DataColagem", required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar dataColagem;
    @XmlElement(name = "Fonte")
    protected String fonte;
    @XmlElement(name = "Texto")
    protected String texto;
    @XmlElement(name = "ProcessoUnico")
    protected String processoUnico;
    @XmlElement(name = "DataExclusao", required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar dataExclusao;
    @XmlElement(name = "MotivoExclusao")
    protected String motivoExclusao;

    /**
     * Obtém o valor da propriedade idPublicacao.
     * 
     */
    public int getIDPublicacao() {
        return idPublicacao;
    }

    /**
     * Define o valor da propriedade idPublicacao.
     * 
     */
    public void setIDPublicacao(int value) {
        this.idPublicacao = value;
    }

    /**
     * Obtém o valor da propriedade dataPublicacao.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDataPublicacao() {
        return dataPublicacao;
    }

    /**
     * Define o valor da propriedade dataPublicacao.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDataPublicacao(XMLGregorianCalendar value) {
        this.dataPublicacao = value;
    }

    /**
     * Obtém o valor da propriedade dataColagem.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDataColagem() {
        return dataColagem;
    }

    /**
     * Define o valor da propriedade dataColagem.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDataColagem(XMLGregorianCalendar value) {
        this.dataColagem = value;
    }

    /**
     * Obtém o valor da propriedade fonte.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFonte() {
        return fonte;
    }

    /**
     * Define o valor da propriedade fonte.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFonte(String value) {
        this.fonte = value;
    }

    /**
     * Obtém o valor da propriedade texto.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTexto() {
        return texto;
    }

    /**
     * Define o valor da propriedade texto.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTexto(String value) {
        this.texto = value;
    }

    /**
     * Obtém o valor da propriedade processoUnico.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProcessoUnico() {
        return processoUnico;
    }

    /**
     * Define o valor da propriedade processoUnico.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProcessoUnico(String value) {
        this.processoUnico = value;
    }

    /**
     * Obtém o valor da propriedade dataExclusao.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDataExclusao() {
        return dataExclusao;
    }

    /**
     * Define o valor da propriedade dataExclusao.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDataExclusao(XMLGregorianCalendar value) {
        this.dataExclusao = value;
    }

    /**
     * Obtém o valor da propriedade motivoExclusao.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMotivoExclusao() {
        return motivoExclusao;
    }

    /**
     * Define o valor da propriedade motivoExclusao.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMotivoExclusao(String value) {
        this.motivoExclusao = value;
    }

}
