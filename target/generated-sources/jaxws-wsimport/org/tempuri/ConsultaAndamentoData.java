
package org.tempuri;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Classe Java de anonymous complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ldatDataAndamentoIni" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="ldatDataAndamentoFim" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="lstrHoraAndamentoIni" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="lstrHoraAndamentoFim" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="lstrUsuario" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="lstrSenha" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="lstrIgnorarTipoAndamento" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="lstrIgnorarAdvogado" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "ldatDataAndamentoIni",
    "ldatDataAndamentoFim",
    "lstrHoraAndamentoIni",
    "lstrHoraAndamentoFim",
    "lstrUsuario",
    "lstrSenha",
    "lstrIgnorarTipoAndamento",
    "lstrIgnorarAdvogado"
})
@XmlRootElement(name = "ConsultaAndamentoData")
public class ConsultaAndamentoData {

    @XmlElement(required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar ldatDataAndamentoIni;
    @XmlElement(required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar ldatDataAndamentoFim;
    protected String lstrHoraAndamentoIni;
    protected String lstrHoraAndamentoFim;
    protected String lstrUsuario;
    protected String lstrSenha;
    protected String lstrIgnorarTipoAndamento;
    protected String lstrIgnorarAdvogado;

    /**
     * Obtém o valor da propriedade ldatDataAndamentoIni.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getLdatDataAndamentoIni() {
        return ldatDataAndamentoIni;
    }

    /**
     * Define o valor da propriedade ldatDataAndamentoIni.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setLdatDataAndamentoIni(XMLGregorianCalendar value) {
        this.ldatDataAndamentoIni = value;
    }

    /**
     * Obtém o valor da propriedade ldatDataAndamentoFim.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getLdatDataAndamentoFim() {
        return ldatDataAndamentoFim;
    }

    /**
     * Define o valor da propriedade ldatDataAndamentoFim.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setLdatDataAndamentoFim(XMLGregorianCalendar value) {
        this.ldatDataAndamentoFim = value;
    }

    /**
     * Obtém o valor da propriedade lstrHoraAndamentoIni.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLstrHoraAndamentoIni() {
        return lstrHoraAndamentoIni;
    }

    /**
     * Define o valor da propriedade lstrHoraAndamentoIni.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLstrHoraAndamentoIni(String value) {
        this.lstrHoraAndamentoIni = value;
    }

    /**
     * Obtém o valor da propriedade lstrHoraAndamentoFim.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLstrHoraAndamentoFim() {
        return lstrHoraAndamentoFim;
    }

    /**
     * Define o valor da propriedade lstrHoraAndamentoFim.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLstrHoraAndamentoFim(String value) {
        this.lstrHoraAndamentoFim = value;
    }

    /**
     * Obtém o valor da propriedade lstrUsuario.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLstrUsuario() {
        return lstrUsuario;
    }

    /**
     * Define o valor da propriedade lstrUsuario.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLstrUsuario(String value) {
        this.lstrUsuario = value;
    }

    /**
     * Obtém o valor da propriedade lstrSenha.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLstrSenha() {
        return lstrSenha;
    }

    /**
     * Define o valor da propriedade lstrSenha.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLstrSenha(String value) {
        this.lstrSenha = value;
    }

    /**
     * Obtém o valor da propriedade lstrIgnorarTipoAndamento.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLstrIgnorarTipoAndamento() {
        return lstrIgnorarTipoAndamento;
    }

    /**
     * Define o valor da propriedade lstrIgnorarTipoAndamento.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLstrIgnorarTipoAndamento(String value) {
        this.lstrIgnorarTipoAndamento = value;
    }

    /**
     * Obtém o valor da propriedade lstrIgnorarAdvogado.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLstrIgnorarAdvogado() {
        return lstrIgnorarAdvogado;
    }

    /**
     * Define o valor da propriedade lstrIgnorarAdvogado.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLstrIgnorarAdvogado(String value) {
        this.lstrIgnorarAdvogado = value;
    }

}
