
package org.tempuri;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java de anonymous complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="lintIDSolicitacao" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="lstrIdProcessoUnico" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="lstrNomeArquivo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="lstrUsuario" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="lstrSenha" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="lstrAutor" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="lstrTipoDocumento" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="lstrObservacao" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="lstrTipoArquivo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "lintIDSolicitacao",
    "lstrIdProcessoUnico",
    "lstrNomeArquivo",
    "lstrUsuario",
    "lstrSenha",
    "lstrAutor",
    "lstrTipoDocumento",
    "lstrObservacao",
    "lstrTipoArquivo"
})
@XmlRootElement(name = "RecebeArquivo")
public class RecebeArquivo {

    protected int lintIDSolicitacao;
    protected String lstrIdProcessoUnico;
    protected String lstrNomeArquivo;
    protected String lstrUsuario;
    protected String lstrSenha;
    protected String lstrAutor;
    protected String lstrTipoDocumento;
    protected String lstrObservacao;
    protected String lstrTipoArquivo;

    /**
     * Obtém o valor da propriedade lintIDSolicitacao.
     * 
     */
    public int getLintIDSolicitacao() {
        return lintIDSolicitacao;
    }

    /**
     * Define o valor da propriedade lintIDSolicitacao.
     * 
     */
    public void setLintIDSolicitacao(int value) {
        this.lintIDSolicitacao = value;
    }

    /**
     * Obtém o valor da propriedade lstrIdProcessoUnico.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLstrIdProcessoUnico() {
        return lstrIdProcessoUnico;
    }

    /**
     * Define o valor da propriedade lstrIdProcessoUnico.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLstrIdProcessoUnico(String value) {
        this.lstrIdProcessoUnico = value;
    }

    /**
     * Obtém o valor da propriedade lstrNomeArquivo.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLstrNomeArquivo() {
        return lstrNomeArquivo;
    }

    /**
     * Define o valor da propriedade lstrNomeArquivo.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLstrNomeArquivo(String value) {
        this.lstrNomeArquivo = value;
    }

    /**
     * Obtém o valor da propriedade lstrUsuario.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLstrUsuario() {
        return lstrUsuario;
    }

    /**
     * Define o valor da propriedade lstrUsuario.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLstrUsuario(String value) {
        this.lstrUsuario = value;
    }

    /**
     * Obtém o valor da propriedade lstrSenha.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLstrSenha() {
        return lstrSenha;
    }

    /**
     * Define o valor da propriedade lstrSenha.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLstrSenha(String value) {
        this.lstrSenha = value;
    }

    /**
     * Obtém o valor da propriedade lstrAutor.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLstrAutor() {
        return lstrAutor;
    }

    /**
     * Define o valor da propriedade lstrAutor.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLstrAutor(String value) {
        this.lstrAutor = value;
    }

    /**
     * Obtém o valor da propriedade lstrTipoDocumento.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLstrTipoDocumento() {
        return lstrTipoDocumento;
    }

    /**
     * Define o valor da propriedade lstrTipoDocumento.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLstrTipoDocumento(String value) {
        this.lstrTipoDocumento = value;
    }

    /**
     * Obtém o valor da propriedade lstrObservacao.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLstrObservacao() {
        return lstrObservacao;
    }

    /**
     * Define o valor da propriedade lstrObservacao.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLstrObservacao(String value) {
        this.lstrObservacao = value;
    }

    /**
     * Obtém o valor da propriedade lstrTipoArquivo.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLstrTipoArquivo() {
        return lstrTipoArquivo;
    }

    /**
     * Define o valor da propriedade lstrTipoArquivo.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLstrTipoArquivo(String value) {
        this.lstrTipoArquivo = value;
    }

}
