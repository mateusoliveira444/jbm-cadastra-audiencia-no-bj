
package org.tempuri;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java de ParteProcesso complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="ParteProcesso">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="IdProcessoUnico" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="IdTipoParte" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="Descricao" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="NomeDaParte" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Principal" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ParteProcesso", propOrder = {
    "idProcessoUnico",
    "idTipoParte",
    "descricao",
    "nomeDaParte",
    "principal"
})
public class ParteProcesso {

    @XmlElement(name = "IdProcessoUnico")
    protected String idProcessoUnico;
    @XmlElement(name = "IdTipoParte")
    protected int idTipoParte;
    @XmlElement(name = "Descricao")
    protected String descricao;
    @XmlElement(name = "NomeDaParte")
    protected String nomeDaParte;
    @XmlElement(name = "Principal")
    protected boolean principal;

    /**
     * Obtém o valor da propriedade idProcessoUnico.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdProcessoUnico() {
        return idProcessoUnico;
    }

    /**
     * Define o valor da propriedade idProcessoUnico.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdProcessoUnico(String value) {
        this.idProcessoUnico = value;
    }

    /**
     * Obtém o valor da propriedade idTipoParte.
     * 
     */
    public int getIdTipoParte() {
        return idTipoParte;
    }

    /**
     * Define o valor da propriedade idTipoParte.
     * 
     */
    public void setIdTipoParte(int value) {
        this.idTipoParte = value;
    }

    /**
     * Obtém o valor da propriedade descricao.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescricao() {
        return descricao;
    }

    /**
     * Define o valor da propriedade descricao.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescricao(String value) {
        this.descricao = value;
    }

    /**
     * Obtém o valor da propriedade nomeDaParte.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNomeDaParte() {
        return nomeDaParte;
    }

    /**
     * Define o valor da propriedade nomeDaParte.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNomeDaParte(String value) {
        this.nomeDaParte = value;
    }

    /**
     * Obtém o valor da propriedade principal.
     * 
     */
    public boolean isPrincipal() {
        return principal;
    }

    /**
     * Define o valor da propriedade principal.
     * 
     */
    public void setPrincipal(boolean value) {
        this.principal = value;
    }

}
