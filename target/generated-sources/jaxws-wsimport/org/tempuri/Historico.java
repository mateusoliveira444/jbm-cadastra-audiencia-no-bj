
package org.tempuri;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java de Historico complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="Historico">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Usuario" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DataAlteracao" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="StatusProcessoAnterior" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DataEncerramentoAnterior" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Decisao" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Historico", propOrder = {
    "usuario",
    "dataAlteracao",
    "statusProcessoAnterior",
    "dataEncerramentoAnterior",
    "decisao"
})
public class Historico {

    @XmlElement(name = "Usuario")
    protected String usuario;
    @XmlElement(name = "DataAlteracao")
    protected String dataAlteracao;
    @XmlElement(name = "StatusProcessoAnterior")
    protected String statusProcessoAnterior;
    @XmlElement(name = "DataEncerramentoAnterior")
    protected String dataEncerramentoAnterior;
    @XmlElement(name = "Decisao")
    protected String decisao;

    /**
     * Obtém o valor da propriedade usuario.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUsuario() {
        return usuario;
    }

    /**
     * Define o valor da propriedade usuario.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUsuario(String value) {
        this.usuario = value;
    }

    /**
     * Obtém o valor da propriedade dataAlteracao.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDataAlteracao() {
        return dataAlteracao;
    }

    /**
     * Define o valor da propriedade dataAlteracao.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDataAlteracao(String value) {
        this.dataAlteracao = value;
    }

    /**
     * Obtém o valor da propriedade statusProcessoAnterior.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStatusProcessoAnterior() {
        return statusProcessoAnterior;
    }

    /**
     * Define o valor da propriedade statusProcessoAnterior.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStatusProcessoAnterior(String value) {
        this.statusProcessoAnterior = value;
    }

    /**
     * Obtém o valor da propriedade dataEncerramentoAnterior.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDataEncerramentoAnterior() {
        return dataEncerramentoAnterior;
    }

    /**
     * Define o valor da propriedade dataEncerramentoAnterior.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDataEncerramentoAnterior(String value) {
        this.dataEncerramentoAnterior = value;
    }

    /**
     * Obtém o valor da propriedade decisao.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDecisao() {
        return decisao;
    }

    /**
     * Define o valor da propriedade decisao.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDecisao(String value) {
        this.decisao = value;
    }

}
