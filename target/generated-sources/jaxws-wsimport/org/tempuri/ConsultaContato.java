
package org.tempuri;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java de anonymous complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="lstrUsuario" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="lstrSenha" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="lstrNomeCompletoRazaoSocial" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="lstrCpfCnpj" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="lstrTipoPessoa" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "lstrUsuario",
    "lstrSenha",
    "lstrNomeCompletoRazaoSocial",
    "lstrCpfCnpj",
    "lstrTipoPessoa"
})
@XmlRootElement(name = "ConsultaContato")
public class ConsultaContato {

    protected String lstrUsuario;
    protected String lstrSenha;
    protected String lstrNomeCompletoRazaoSocial;
    protected String lstrCpfCnpj;
    protected String lstrTipoPessoa;

    /**
     * Obtém o valor da propriedade lstrUsuario.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLstrUsuario() {
        return lstrUsuario;
    }

    /**
     * Define o valor da propriedade lstrUsuario.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLstrUsuario(String value) {
        this.lstrUsuario = value;
    }

    /**
     * Obtém o valor da propriedade lstrSenha.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLstrSenha() {
        return lstrSenha;
    }

    /**
     * Define o valor da propriedade lstrSenha.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLstrSenha(String value) {
        this.lstrSenha = value;
    }

    /**
     * Obtém o valor da propriedade lstrNomeCompletoRazaoSocial.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLstrNomeCompletoRazaoSocial() {
        return lstrNomeCompletoRazaoSocial;
    }

    /**
     * Define o valor da propriedade lstrNomeCompletoRazaoSocial.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLstrNomeCompletoRazaoSocial(String value) {
        this.lstrNomeCompletoRazaoSocial = value;
    }

    /**
     * Obtém o valor da propriedade lstrCpfCnpj.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLstrCpfCnpj() {
        return lstrCpfCnpj;
    }

    /**
     * Define o valor da propriedade lstrCpfCnpj.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLstrCpfCnpj(String value) {
        this.lstrCpfCnpj = value;
    }

    /**
     * Obtém o valor da propriedade lstrTipoPessoa.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLstrTipoPessoa() {
        return lstrTipoPessoa;
    }

    /**
     * Define o valor da propriedade lstrTipoPessoa.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLstrTipoPessoa(String value) {
        this.lstrTipoPessoa = value;
    }

}
