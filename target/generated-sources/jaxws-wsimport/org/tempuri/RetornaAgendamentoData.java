
package org.tempuri;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java de anonymous complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="lstrUsuario" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="lstrSenha" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="lstrTipoAgendamento" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="lstrStatusAgendamento" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="lstrDataInicioAgendamento" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="lstrDataFimAgendamento" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="lstrDataInicioConclusao" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="lstrDataFimConclusao" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="lstrStatusSincronizado" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="lstrStatusConclusaoAgendamento" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "lstrUsuario",
    "lstrSenha",
    "lstrTipoAgendamento",
    "lstrStatusAgendamento",
    "lstrDataInicioAgendamento",
    "lstrDataFimAgendamento",
    "lstrDataInicioConclusao",
    "lstrDataFimConclusao",
    "lstrStatusSincronizado",
    "lstrStatusConclusaoAgendamento"
})
@XmlRootElement(name = "RetornaAgendamentoData")
public class RetornaAgendamentoData {

    protected String lstrUsuario;
    protected String lstrSenha;
    protected String lstrTipoAgendamento;
    protected String lstrStatusAgendamento;
    protected String lstrDataInicioAgendamento;
    protected String lstrDataFimAgendamento;
    protected String lstrDataInicioConclusao;
    protected String lstrDataFimConclusao;
    protected String lstrStatusSincronizado;
    protected String lstrStatusConclusaoAgendamento;

    /**
     * Obtém o valor da propriedade lstrUsuario.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLstrUsuario() {
        return lstrUsuario;
    }

    /**
     * Define o valor da propriedade lstrUsuario.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLstrUsuario(String value) {
        this.lstrUsuario = value;
    }

    /**
     * Obtém o valor da propriedade lstrSenha.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLstrSenha() {
        return lstrSenha;
    }

    /**
     * Define o valor da propriedade lstrSenha.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLstrSenha(String value) {
        this.lstrSenha = value;
    }

    /**
     * Obtém o valor da propriedade lstrTipoAgendamento.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLstrTipoAgendamento() {
        return lstrTipoAgendamento;
    }

    /**
     * Define o valor da propriedade lstrTipoAgendamento.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLstrTipoAgendamento(String value) {
        this.lstrTipoAgendamento = value;
    }

    /**
     * Obtém o valor da propriedade lstrStatusAgendamento.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLstrStatusAgendamento() {
        return lstrStatusAgendamento;
    }

    /**
     * Define o valor da propriedade lstrStatusAgendamento.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLstrStatusAgendamento(String value) {
        this.lstrStatusAgendamento = value;
    }

    /**
     * Obtém o valor da propriedade lstrDataInicioAgendamento.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLstrDataInicioAgendamento() {
        return lstrDataInicioAgendamento;
    }

    /**
     * Define o valor da propriedade lstrDataInicioAgendamento.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLstrDataInicioAgendamento(String value) {
        this.lstrDataInicioAgendamento = value;
    }

    /**
     * Obtém o valor da propriedade lstrDataFimAgendamento.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLstrDataFimAgendamento() {
        return lstrDataFimAgendamento;
    }

    /**
     * Define o valor da propriedade lstrDataFimAgendamento.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLstrDataFimAgendamento(String value) {
        this.lstrDataFimAgendamento = value;
    }

    /**
     * Obtém o valor da propriedade lstrDataInicioConclusao.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLstrDataInicioConclusao() {
        return lstrDataInicioConclusao;
    }

    /**
     * Define o valor da propriedade lstrDataInicioConclusao.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLstrDataInicioConclusao(String value) {
        this.lstrDataInicioConclusao = value;
    }

    /**
     * Obtém o valor da propriedade lstrDataFimConclusao.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLstrDataFimConclusao() {
        return lstrDataFimConclusao;
    }

    /**
     * Define o valor da propriedade lstrDataFimConclusao.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLstrDataFimConclusao(String value) {
        this.lstrDataFimConclusao = value;
    }

    /**
     * Obtém o valor da propriedade lstrStatusSincronizado.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLstrStatusSincronizado() {
        return lstrStatusSincronizado;
    }

    /**
     * Define o valor da propriedade lstrStatusSincronizado.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLstrStatusSincronizado(String value) {
        this.lstrStatusSincronizado = value;
    }

    /**
     * Obtém o valor da propriedade lstrStatusConclusaoAgendamento.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLstrStatusConclusaoAgendamento() {
        return lstrStatusConclusaoAgendamento;
    }

    /**
     * Define o valor da propriedade lstrStatusConclusaoAgendamento.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLstrStatusConclusaoAgendamento(String value) {
        this.lstrStatusConclusaoAgendamento = value;
    }

}
