
package org.tempuri;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java de anonymous complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ConsultaSolicitacaoDataConclusaoResult" type="{http://tempuri.org/}ArrayOfSolicitacaoRetorno" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "consultaSolicitacaoDataConclusaoResult"
})
@XmlRootElement(name = "ConsultaSolicitacaoDataConclusaoResponse")
public class ConsultaSolicitacaoDataConclusaoResponse {

    @XmlElement(name = "ConsultaSolicitacaoDataConclusaoResult")
    protected ArrayOfSolicitacaoRetorno consultaSolicitacaoDataConclusaoResult;

    /**
     * Obtém o valor da propriedade consultaSolicitacaoDataConclusaoResult.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfSolicitacaoRetorno }
     *     
     */
    public ArrayOfSolicitacaoRetorno getConsultaSolicitacaoDataConclusaoResult() {
        return consultaSolicitacaoDataConclusaoResult;
    }

    /**
     * Define o valor da propriedade consultaSolicitacaoDataConclusaoResult.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfSolicitacaoRetorno }
     *     
     */
    public void setConsultaSolicitacaoDataConclusaoResult(ArrayOfSolicitacaoRetorno value) {
        this.consultaSolicitacaoDataConclusaoResult = value;
    }

}
