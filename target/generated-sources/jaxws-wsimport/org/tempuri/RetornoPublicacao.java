
package org.tempuri;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Classe Java de RetornoPublicacao complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="RetornoPublicacao">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="IDPublicacao" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="DataPublicacao" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="DataColagem" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="Fonte" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Texto" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ProcessoUnico" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Pegou" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Situacao" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="UF" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="NumeroProcesso" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Observacao" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="lstrDataInicioPrazo" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "RetornoPublicacao", propOrder = {
    "idPublicacao",
    "dataPublicacao",
    "dataColagem",
    "fonte",
    "texto",
    "processoUnico",
    "pegou",
    "situacao",
    "uf",
    "numeroProcesso",
    "observacao",
    "lstrDataInicioPrazo"
})
public class RetornoPublicacao {

    @XmlElement(name = "IDPublicacao")
    protected int idPublicacao;
    @XmlElement(name = "DataPublicacao", required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar dataPublicacao;
    @XmlElement(name = "DataColagem", required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar dataColagem;
    @XmlElement(name = "Fonte")
    protected String fonte;
    @XmlElement(name = "Texto")
    protected String texto;
    @XmlElement(name = "ProcessoUnico")
    protected String processoUnico;
    @XmlElement(name = "Pegou")
    protected String pegou;
    @XmlElement(name = "Situacao")
    protected String situacao;
    @XmlElement(name = "UF")
    protected String uf;
    @XmlElement(name = "NumeroProcesso")
    protected String numeroProcesso;
    @XmlElement(name = "Observacao")
    protected String observacao;
    @XmlElement(required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar lstrDataInicioPrazo;

    /**
     * Obtém o valor da propriedade idPublicacao.
     * 
     */
    public int getIDPublicacao() {
        return idPublicacao;
    }

    /**
     * Define o valor da propriedade idPublicacao.
     * 
     */
    public void setIDPublicacao(int value) {
        this.idPublicacao = value;
    }

    /**
     * Obtém o valor da propriedade dataPublicacao.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDataPublicacao() {
        return dataPublicacao;
    }

    /**
     * Define o valor da propriedade dataPublicacao.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDataPublicacao(XMLGregorianCalendar value) {
        this.dataPublicacao = value;
    }

    /**
     * Obtém o valor da propriedade dataColagem.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDataColagem() {
        return dataColagem;
    }

    /**
     * Define o valor da propriedade dataColagem.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDataColagem(XMLGregorianCalendar value) {
        this.dataColagem = value;
    }

    /**
     * Obtém o valor da propriedade fonte.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFonte() {
        return fonte;
    }

    /**
     * Define o valor da propriedade fonte.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFonte(String value) {
        this.fonte = value;
    }

    /**
     * Obtém o valor da propriedade texto.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTexto() {
        return texto;
    }

    /**
     * Define o valor da propriedade texto.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTexto(String value) {
        this.texto = value;
    }

    /**
     * Obtém o valor da propriedade processoUnico.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProcessoUnico() {
        return processoUnico;
    }

    /**
     * Define o valor da propriedade processoUnico.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProcessoUnico(String value) {
        this.processoUnico = value;
    }

    /**
     * Obtém o valor da propriedade pegou.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPegou() {
        return pegou;
    }

    /**
     * Define o valor da propriedade pegou.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPegou(String value) {
        this.pegou = value;
    }

    /**
     * Obtém o valor da propriedade situacao.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSituacao() {
        return situacao;
    }

    /**
     * Define o valor da propriedade situacao.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSituacao(String value) {
        this.situacao = value;
    }

    /**
     * Obtém o valor da propriedade uf.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUF() {
        return uf;
    }

    /**
     * Define o valor da propriedade uf.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUF(String value) {
        this.uf = value;
    }

    /**
     * Obtém o valor da propriedade numeroProcesso.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNumeroProcesso() {
        return numeroProcesso;
    }

    /**
     * Define o valor da propriedade numeroProcesso.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNumeroProcesso(String value) {
        this.numeroProcesso = value;
    }

    /**
     * Obtém o valor da propriedade observacao.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getObservacao() {
        return observacao;
    }

    /**
     * Define o valor da propriedade observacao.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setObservacao(String value) {
        this.observacao = value;
    }

    /**
     * Obtém o valor da propriedade lstrDataInicioPrazo.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getLstrDataInicioPrazo() {
        return lstrDataInicioPrazo;
    }

    /**
     * Define o valor da propriedade lstrDataInicioPrazo.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setLstrDataInicioPrazo(XMLGregorianCalendar value) {
        this.lstrDataInicioPrazo = value;
    }

}
