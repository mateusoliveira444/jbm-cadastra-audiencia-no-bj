
package org.tempuri;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java de anonymous complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="lstrIdDiligencia" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="lstrCorrespondente" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="lstrTipoDiligencia" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="lstrStatusDiligencia" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="lstrDtSolicIni" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="lstrDtSolicFin" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="lstrDtConcIni" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="lstrDtConcFin" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="lstrUsuario" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="lstrSenha" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "lstrIdDiligencia",
    "lstrCorrespondente",
    "lstrTipoDiligencia",
    "lstrStatusDiligencia",
    "lstrDtSolicIni",
    "lstrDtSolicFin",
    "lstrDtConcIni",
    "lstrDtConcFin",
    "lstrUsuario",
    "lstrSenha"
})
@XmlRootElement(name = "RetornarDiligencias")
public class RetornarDiligencias {

    protected String lstrIdDiligencia;
    protected String lstrCorrespondente;
    protected String lstrTipoDiligencia;
    protected String lstrStatusDiligencia;
    protected String lstrDtSolicIni;
    protected String lstrDtSolicFin;
    protected String lstrDtConcIni;
    protected String lstrDtConcFin;
    protected String lstrUsuario;
    protected String lstrSenha;

    /**
     * Obtém o valor da propriedade lstrIdDiligencia.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLstrIdDiligencia() {
        return lstrIdDiligencia;
    }

    /**
     * Define o valor da propriedade lstrIdDiligencia.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLstrIdDiligencia(String value) {
        this.lstrIdDiligencia = value;
    }

    /**
     * Obtém o valor da propriedade lstrCorrespondente.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLstrCorrespondente() {
        return lstrCorrespondente;
    }

    /**
     * Define o valor da propriedade lstrCorrespondente.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLstrCorrespondente(String value) {
        this.lstrCorrespondente = value;
    }

    /**
     * Obtém o valor da propriedade lstrTipoDiligencia.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLstrTipoDiligencia() {
        return lstrTipoDiligencia;
    }

    /**
     * Define o valor da propriedade lstrTipoDiligencia.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLstrTipoDiligencia(String value) {
        this.lstrTipoDiligencia = value;
    }

    /**
     * Obtém o valor da propriedade lstrStatusDiligencia.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLstrStatusDiligencia() {
        return lstrStatusDiligencia;
    }

    /**
     * Define o valor da propriedade lstrStatusDiligencia.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLstrStatusDiligencia(String value) {
        this.lstrStatusDiligencia = value;
    }

    /**
     * Obtém o valor da propriedade lstrDtSolicIni.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLstrDtSolicIni() {
        return lstrDtSolicIni;
    }

    /**
     * Define o valor da propriedade lstrDtSolicIni.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLstrDtSolicIni(String value) {
        this.lstrDtSolicIni = value;
    }

    /**
     * Obtém o valor da propriedade lstrDtSolicFin.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLstrDtSolicFin() {
        return lstrDtSolicFin;
    }

    /**
     * Define o valor da propriedade lstrDtSolicFin.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLstrDtSolicFin(String value) {
        this.lstrDtSolicFin = value;
    }

    /**
     * Obtém o valor da propriedade lstrDtConcIni.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLstrDtConcIni() {
        return lstrDtConcIni;
    }

    /**
     * Define o valor da propriedade lstrDtConcIni.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLstrDtConcIni(String value) {
        this.lstrDtConcIni = value;
    }

    /**
     * Obtém o valor da propriedade lstrDtConcFin.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLstrDtConcFin() {
        return lstrDtConcFin;
    }

    /**
     * Define o valor da propriedade lstrDtConcFin.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLstrDtConcFin(String value) {
        this.lstrDtConcFin = value;
    }

    /**
     * Obtém o valor da propriedade lstrUsuario.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLstrUsuario() {
        return lstrUsuario;
    }

    /**
     * Define o valor da propriedade lstrUsuario.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLstrUsuario(String value) {
        this.lstrUsuario = value;
    }

    /**
     * Obtém o valor da propriedade lstrSenha.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLstrSenha() {
        return lstrSenha;
    }

    /**
     * Define o valor da propriedade lstrSenha.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLstrSenha(String value) {
        this.lstrSenha = value;
    }

}
