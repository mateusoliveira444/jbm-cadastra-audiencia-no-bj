
package org.tempuri;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java de ArrayOfOpcoesCampoLista complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="ArrayOfOpcoesCampoLista">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="OpcoesCampoLista" type="{http://tempuri.org/}OpcoesCampoLista" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ArrayOfOpcoesCampoLista", propOrder = {
    "opcoesCampoLista"
})
public class ArrayOfOpcoesCampoLista {

    @XmlElement(name = "OpcoesCampoLista", nillable = true)
    protected List<OpcoesCampoLista> opcoesCampoLista;

    /**
     * Gets the value of the opcoesCampoLista property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the opcoesCampoLista property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getOpcoesCampoLista().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link OpcoesCampoLista }
     * 
     * 
     */
    public List<OpcoesCampoLista> getOpcoesCampoLista() {
        if (opcoesCampoLista == null) {
            opcoesCampoLista = new ArrayList<OpcoesCampoLista>();
        }
        return this.opcoesCampoLista;
    }

}
