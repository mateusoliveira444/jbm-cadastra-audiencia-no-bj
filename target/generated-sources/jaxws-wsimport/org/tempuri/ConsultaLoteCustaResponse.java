
package org.tempuri;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java de anonymous complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ConsultaLoteCustaResult" type="{http://tempuri.org/}ArrayOfLote" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "consultaLoteCustaResult"
})
@XmlRootElement(name = "ConsultaLoteCustaResponse")
public class ConsultaLoteCustaResponse {

    @XmlElement(name = "ConsultaLoteCustaResult")
    protected ArrayOfLote consultaLoteCustaResult;

    /**
     * Obtém o valor da propriedade consultaLoteCustaResult.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfLote }
     *     
     */
    public ArrayOfLote getConsultaLoteCustaResult() {
        return consultaLoteCustaResult;
    }

    /**
     * Define o valor da propriedade consultaLoteCustaResult.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfLote }
     *     
     */
    public void setConsultaLoteCustaResult(ArrayOfLote value) {
        this.consultaLoteCustaResult = value;
    }

}
