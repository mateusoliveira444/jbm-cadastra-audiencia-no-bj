
package org.tempuri;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java de DivisaoCliente complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="DivisaoCliente">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="DescricaoDivisao" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="IdDivisaoCliente" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DivisaoCliente", propOrder = {
    "descricaoDivisao",
    "idDivisaoCliente"
})
public class DivisaoCliente {

    @XmlElement(name = "DescricaoDivisao")
    protected String descricaoDivisao;
    @XmlElement(name = "IdDivisaoCliente")
    protected int idDivisaoCliente;

    /**
     * Obtém o valor da propriedade descricaoDivisao.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescricaoDivisao() {
        return descricaoDivisao;
    }

    /**
     * Define o valor da propriedade descricaoDivisao.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescricaoDivisao(String value) {
        this.descricaoDivisao = value;
    }

    /**
     * Obtém o valor da propriedade idDivisaoCliente.
     * 
     */
    public int getIdDivisaoCliente() {
        return idDivisaoCliente;
    }

    /**
     * Define o valor da propriedade idDivisaoCliente.
     * 
     */
    public void setIdDivisaoCliente(int value) {
        this.idDivisaoCliente = value;
    }

}
