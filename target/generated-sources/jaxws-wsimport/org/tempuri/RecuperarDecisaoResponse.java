
package org.tempuri;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java de anonymous complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="RecuperarDecisaoResult" type="{http://tempuri.org/}ArrayOfDecisao" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "recuperarDecisaoResult"
})
@XmlRootElement(name = "RecuperarDecisaoResponse")
public class RecuperarDecisaoResponse {

    @XmlElement(name = "RecuperarDecisaoResult")
    protected ArrayOfDecisao recuperarDecisaoResult;

    /**
     * Obtém o valor da propriedade recuperarDecisaoResult.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfDecisao }
     *     
     */
    public ArrayOfDecisao getRecuperarDecisaoResult() {
        return recuperarDecisaoResult;
    }

    /**
     * Define o valor da propriedade recuperarDecisaoResult.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfDecisao }
     *     
     */
    public void setRecuperarDecisaoResult(ArrayOfDecisao value) {
        this.recuperarDecisaoResult = value;
    }

}
