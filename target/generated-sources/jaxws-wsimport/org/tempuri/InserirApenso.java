
package org.tempuri;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java de anonymous complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="lstrUsuario" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="lstrSenha" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="lstrProcessoUnicoPrincipal" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="lstrProcessoUnicoApenso" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "lstrUsuario",
    "lstrSenha",
    "lstrProcessoUnicoPrincipal",
    "lstrProcessoUnicoApenso"
})
@XmlRootElement(name = "InserirApenso")
public class InserirApenso {

    protected String lstrUsuario;
    protected String lstrSenha;
    protected String lstrProcessoUnicoPrincipal;
    protected String lstrProcessoUnicoApenso;

    /**
     * Obtém o valor da propriedade lstrUsuario.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLstrUsuario() {
        return lstrUsuario;
    }

    /**
     * Define o valor da propriedade lstrUsuario.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLstrUsuario(String value) {
        this.lstrUsuario = value;
    }

    /**
     * Obtém o valor da propriedade lstrSenha.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLstrSenha() {
        return lstrSenha;
    }

    /**
     * Define o valor da propriedade lstrSenha.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLstrSenha(String value) {
        this.lstrSenha = value;
    }

    /**
     * Obtém o valor da propriedade lstrProcessoUnicoPrincipal.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLstrProcessoUnicoPrincipal() {
        return lstrProcessoUnicoPrincipal;
    }

    /**
     * Define o valor da propriedade lstrProcessoUnicoPrincipal.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLstrProcessoUnicoPrincipal(String value) {
        this.lstrProcessoUnicoPrincipal = value;
    }

    /**
     * Obtém o valor da propriedade lstrProcessoUnicoApenso.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLstrProcessoUnicoApenso() {
        return lstrProcessoUnicoApenso;
    }

    /**
     * Define o valor da propriedade lstrProcessoUnicoApenso.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLstrProcessoUnicoApenso(String value) {
        this.lstrProcessoUnicoApenso = value;
    }

}
