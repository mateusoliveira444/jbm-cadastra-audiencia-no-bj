
package org.tempuri;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java de Atividade complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="Atividade">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="IDAtividade" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="AtividadeDesc" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Atividade", propOrder = {
    "idAtividade",
    "atividadeDesc"
})
public class Atividade {

    @XmlElement(name = "IDAtividade")
    protected int idAtividade;
    @XmlElement(name = "AtividadeDesc")
    protected String atividadeDesc;

    /**
     * Obtém o valor da propriedade idAtividade.
     * 
     */
    public int getIDAtividade() {
        return idAtividade;
    }

    /**
     * Define o valor da propriedade idAtividade.
     * 
     */
    public void setIDAtividade(int value) {
        this.idAtividade = value;
    }

    /**
     * Obtém o valor da propriedade atividadeDesc.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAtividadeDesc() {
        return atividadeDesc;
    }

    /**
     * Define o valor da propriedade atividadeDesc.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAtividadeDesc(String value) {
        this.atividadeDesc = value;
    }

}
