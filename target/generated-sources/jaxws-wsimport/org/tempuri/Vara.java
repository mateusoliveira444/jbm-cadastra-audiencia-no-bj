
package org.tempuri;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java de Vara complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="Vara">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="IDVara" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="VaraDesc" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="JuizadoEspecialCivel" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Vara", propOrder = {
    "idVara",
    "varaDesc",
    "juizadoEspecialCivel"
})
public class Vara {

    @XmlElement(name = "IDVara")
    protected int idVara;
    @XmlElement(name = "VaraDesc")
    protected String varaDesc;
    @XmlElement(name = "JuizadoEspecialCivel")
    protected String juizadoEspecialCivel;

    /**
     * Obtém o valor da propriedade idVara.
     * 
     */
    public int getIDVara() {
        return idVara;
    }

    /**
     * Define o valor da propriedade idVara.
     * 
     */
    public void setIDVara(int value) {
        this.idVara = value;
    }

    /**
     * Obtém o valor da propriedade varaDesc.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVaraDesc() {
        return varaDesc;
    }

    /**
     * Define o valor da propriedade varaDesc.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVaraDesc(String value) {
        this.varaDesc = value;
    }

    /**
     * Obtém o valor da propriedade juizadoEspecialCivel.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getJuizadoEspecialCivel() {
        return juizadoEspecialCivel;
    }

    /**
     * Define o valor da propriedade juizadoEspecialCivel.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setJuizadoEspecialCivel(String value) {
        this.juizadoEspecialCivel = value;
    }

}
