
package org.tempuri;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java de Agendamentos complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="Agendamentos">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="TipoAgendamento" type="{http://tempuri.org/}ArrayOfTipoAgendamentoMov" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Agendamentos", propOrder = {
    "tipoAgendamento"
})
public class Agendamentos {

    @XmlElement(name = "TipoAgendamento")
    protected ArrayOfTipoAgendamentoMov tipoAgendamento;

    /**
     * Obtém o valor da propriedade tipoAgendamento.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfTipoAgendamentoMov }
     *     
     */
    public ArrayOfTipoAgendamentoMov getTipoAgendamento() {
        return tipoAgendamento;
    }

    /**
     * Define o valor da propriedade tipoAgendamento.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfTipoAgendamentoMov }
     *     
     */
    public void setTipoAgendamento(ArrayOfTipoAgendamentoMov value) {
        this.tipoAgendamento = value;
    }

}
