
package org.tempuri;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java de anonymous complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="RecuperarRespostaSolicitacaoResult" type="{http://tempuri.org/}ArrayOfRespostaSolicitacao" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "recuperarRespostaSolicitacaoResult"
})
@XmlRootElement(name = "RecuperarRespostaSolicitacaoResponse")
public class RecuperarRespostaSolicitacaoResponse {

    @XmlElement(name = "RecuperarRespostaSolicitacaoResult")
    protected ArrayOfRespostaSolicitacao recuperarRespostaSolicitacaoResult;

    /**
     * Obtém o valor da propriedade recuperarRespostaSolicitacaoResult.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfRespostaSolicitacao }
     *     
     */
    public ArrayOfRespostaSolicitacao getRecuperarRespostaSolicitacaoResult() {
        return recuperarRespostaSolicitacaoResult;
    }

    /**
     * Define o valor da propriedade recuperarRespostaSolicitacaoResult.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfRespostaSolicitacao }
     *     
     */
    public void setRecuperarRespostaSolicitacaoResult(ArrayOfRespostaSolicitacao value) {
        this.recuperarRespostaSolicitacaoResult = value;
    }

}
