
package org.tempuri;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java de RetornoTipoFollowUP complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="RetornoTipoFollowUP">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="TipoFollowUP" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="IDTipoFollowUP" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "RetornoTipoFollowUP", propOrder = {
    "tipoFollowUP",
    "idTipoFollowUP"
})
public class RetornoTipoFollowUP {

    @XmlElement(name = "TipoFollowUP")
    protected String tipoFollowUP;
    @XmlElement(name = "IDTipoFollowUP")
    protected int idTipoFollowUP;

    /**
     * Obtém o valor da propriedade tipoFollowUP.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTipoFollowUP() {
        return tipoFollowUP;
    }

    /**
     * Define o valor da propriedade tipoFollowUP.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTipoFollowUP(String value) {
        this.tipoFollowUP = value;
    }

    /**
     * Obtém o valor da propriedade idTipoFollowUP.
     * 
     */
    public int getIDTipoFollowUP() {
        return idTipoFollowUP;
    }

    /**
     * Define o valor da propriedade idTipoFollowUP.
     * 
     */
    public void setIDTipoFollowUP(int value) {
        this.idTipoFollowUP = value;
    }

}
