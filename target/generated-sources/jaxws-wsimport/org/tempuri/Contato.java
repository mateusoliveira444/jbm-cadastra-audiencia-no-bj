
package org.tempuri;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java de Contato complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="Contato">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="TipoPessoa" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="NTERC" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="PessoaFisica" type="{http://tempuri.org/}PessoaFisica" minOccurs="0"/>
 *         &lt;element name="PessoaJuridica" type="{http://tempuri.org/}PessoaJuridica" minOccurs="0"/>
 *         &lt;element name="Atividades" type="{http://tempuri.org/}Atividades" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Contato", propOrder = {
    "tipoPessoa",
    "nterc",
    "pessoaFisica",
    "pessoaJuridica",
    "atividades"
})
public class Contato {

    @XmlElement(name = "TipoPessoa")
    protected String tipoPessoa;
    @XmlElement(name = "NTERC")
    protected int nterc;
    @XmlElement(name = "PessoaFisica")
    protected PessoaFisica pessoaFisica;
    @XmlElement(name = "PessoaJuridica")
    protected PessoaJuridica pessoaJuridica;
    @XmlElement(name = "Atividades")
    protected Atividades atividades;

    /**
     * Obtém o valor da propriedade tipoPessoa.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTipoPessoa() {
        return tipoPessoa;
    }

    /**
     * Define o valor da propriedade tipoPessoa.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTipoPessoa(String value) {
        this.tipoPessoa = value;
    }

    /**
     * Obtém o valor da propriedade nterc.
     * 
     */
    public int getNTERC() {
        return nterc;
    }

    /**
     * Define o valor da propriedade nterc.
     * 
     */
    public void setNTERC(int value) {
        this.nterc = value;
    }

    /**
     * Obtém o valor da propriedade pessoaFisica.
     * 
     * @return
     *     possible object is
     *     {@link PessoaFisica }
     *     
     */
    public PessoaFisica getPessoaFisica() {
        return pessoaFisica;
    }

    /**
     * Define o valor da propriedade pessoaFisica.
     * 
     * @param value
     *     allowed object is
     *     {@link PessoaFisica }
     *     
     */
    public void setPessoaFisica(PessoaFisica value) {
        this.pessoaFisica = value;
    }

    /**
     * Obtém o valor da propriedade pessoaJuridica.
     * 
     * @return
     *     possible object is
     *     {@link PessoaJuridica }
     *     
     */
    public PessoaJuridica getPessoaJuridica() {
        return pessoaJuridica;
    }

    /**
     * Define o valor da propriedade pessoaJuridica.
     * 
     * @param value
     *     allowed object is
     *     {@link PessoaJuridica }
     *     
     */
    public void setPessoaJuridica(PessoaJuridica value) {
        this.pessoaJuridica = value;
    }

    /**
     * Obtém o valor da propriedade atividades.
     * 
     * @return
     *     possible object is
     *     {@link Atividades }
     *     
     */
    public Atividades getAtividades() {
        return atividades;
    }

    /**
     * Define o valor da propriedade atividades.
     * 
     * @param value
     *     allowed object is
     *     {@link Atividades }
     *     
     */
    public void setAtividades(Atividades value) {
        this.atividades = value;
    }

}
