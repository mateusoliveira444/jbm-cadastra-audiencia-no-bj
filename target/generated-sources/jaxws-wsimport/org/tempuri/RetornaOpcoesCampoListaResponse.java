
package org.tempuri;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java de anonymous complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="RetornaOpcoesCampoListaResult" type="{http://tempuri.org/}ArrayOfOpcoesCampoLista" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "retornaOpcoesCampoListaResult"
})
@XmlRootElement(name = "RetornaOpcoesCampoListaResponse")
public class RetornaOpcoesCampoListaResponse {

    @XmlElement(name = "RetornaOpcoesCampoListaResult")
    protected ArrayOfOpcoesCampoLista retornaOpcoesCampoListaResult;

    /**
     * Obtém o valor da propriedade retornaOpcoesCampoListaResult.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfOpcoesCampoLista }
     *     
     */
    public ArrayOfOpcoesCampoLista getRetornaOpcoesCampoListaResult() {
        return retornaOpcoesCampoListaResult;
    }

    /**
     * Define o valor da propriedade retornaOpcoesCampoListaResult.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfOpcoesCampoLista }
     *     
     */
    public void setRetornaOpcoesCampoListaResult(ArrayOfOpcoesCampoLista value) {
        this.retornaOpcoesCampoListaResult = value;
    }

}
