
package org.tempuri;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java de TrocaSubTipoSolicitacao complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="TrocaSubTipoSolicitacao">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="TipoSolicitacao" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="SubTipoAtual" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DetalheSolicitacaoAtual" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="SubTipoNovo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DetalheSolicitacaoNovo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Justificativa" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TrocaSubTipoSolicitacao", propOrder = {
    "tipoSolicitacao",
    "subTipoAtual",
    "detalheSolicitacaoAtual",
    "subTipoNovo",
    "detalheSolicitacaoNovo",
    "justificativa"
})
public class TrocaSubTipoSolicitacao {

    @XmlElement(name = "TipoSolicitacao")
    protected String tipoSolicitacao;
    @XmlElement(name = "SubTipoAtual")
    protected String subTipoAtual;
    @XmlElement(name = "DetalheSolicitacaoAtual")
    protected String detalheSolicitacaoAtual;
    @XmlElement(name = "SubTipoNovo")
    protected String subTipoNovo;
    @XmlElement(name = "DetalheSolicitacaoNovo")
    protected String detalheSolicitacaoNovo;
    @XmlElement(name = "Justificativa")
    protected String justificativa;

    /**
     * Obtém o valor da propriedade tipoSolicitacao.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTipoSolicitacao() {
        return tipoSolicitacao;
    }

    /**
     * Define o valor da propriedade tipoSolicitacao.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTipoSolicitacao(String value) {
        this.tipoSolicitacao = value;
    }

    /**
     * Obtém o valor da propriedade subTipoAtual.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSubTipoAtual() {
        return subTipoAtual;
    }

    /**
     * Define o valor da propriedade subTipoAtual.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSubTipoAtual(String value) {
        this.subTipoAtual = value;
    }

    /**
     * Obtém o valor da propriedade detalheSolicitacaoAtual.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDetalheSolicitacaoAtual() {
        return detalheSolicitacaoAtual;
    }

    /**
     * Define o valor da propriedade detalheSolicitacaoAtual.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDetalheSolicitacaoAtual(String value) {
        this.detalheSolicitacaoAtual = value;
    }

    /**
     * Obtém o valor da propriedade subTipoNovo.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSubTipoNovo() {
        return subTipoNovo;
    }

    /**
     * Define o valor da propriedade subTipoNovo.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSubTipoNovo(String value) {
        this.subTipoNovo = value;
    }

    /**
     * Obtém o valor da propriedade detalheSolicitacaoNovo.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDetalheSolicitacaoNovo() {
        return detalheSolicitacaoNovo;
    }

    /**
     * Define o valor da propriedade detalheSolicitacaoNovo.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDetalheSolicitacaoNovo(String value) {
        this.detalheSolicitacaoNovo = value;
    }

    /**
     * Obtém o valor da propriedade justificativa.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getJustificativa() {
        return justificativa;
    }

    /**
     * Define o valor da propriedade justificativa.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setJustificativa(String value) {
        this.justificativa = value;
    }

}
