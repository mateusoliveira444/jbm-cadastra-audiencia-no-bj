
package org.tempuri;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Classe Java de Log complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="Log">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="TipoLog" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="IdOrigem" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="DataHora" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="Observacao" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Log", propOrder = {
    "tipoLog",
    "idOrigem",
    "dataHora",
    "observacao"
})
public class Log {

    @XmlElement(name = "TipoLog")
    protected String tipoLog;
    @XmlElement(name = "IdOrigem")
    protected int idOrigem;
    @XmlElement(name = "DataHora", required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar dataHora;
    @XmlElement(name = "Observacao")
    protected String observacao;

    /**
     * Obtém o valor da propriedade tipoLog.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTipoLog() {
        return tipoLog;
    }

    /**
     * Define o valor da propriedade tipoLog.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTipoLog(String value) {
        this.tipoLog = value;
    }

    /**
     * Obtém o valor da propriedade idOrigem.
     * 
     */
    public int getIdOrigem() {
        return idOrigem;
    }

    /**
     * Define o valor da propriedade idOrigem.
     * 
     */
    public void setIdOrigem(int value) {
        this.idOrigem = value;
    }

    /**
     * Obtém o valor da propriedade dataHora.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDataHora() {
        return dataHora;
    }

    /**
     * Define o valor da propriedade dataHora.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDataHora(XMLGregorianCalendar value) {
        this.dataHora = value;
    }

    /**
     * Obtém o valor da propriedade observacao.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getObservacao() {
        return observacao;
    }

    /**
     * Define o valor da propriedade observacao.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setObservacao(String value) {
        this.observacao = value;
    }

}
