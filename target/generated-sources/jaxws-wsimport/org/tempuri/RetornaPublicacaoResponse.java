
package org.tempuri;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java de anonymous complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="RetornaPublicacaoResult" type="{http://tempuri.org/}ArrayOfRetornoPublicacao" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "retornaPublicacaoResult"
})
@XmlRootElement(name = "RetornaPublicacaoResponse")
public class RetornaPublicacaoResponse {

    @XmlElement(name = "RetornaPublicacaoResult")
    protected ArrayOfRetornoPublicacao retornaPublicacaoResult;

    /**
     * Obtém o valor da propriedade retornaPublicacaoResult.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfRetornoPublicacao }
     *     
     */
    public ArrayOfRetornoPublicacao getRetornaPublicacaoResult() {
        return retornaPublicacaoResult;
    }

    /**
     * Define o valor da propriedade retornaPublicacaoResult.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfRetornoPublicacao }
     *     
     */
    public void setRetornaPublicacaoResult(ArrayOfRetornoPublicacao value) {
        this.retornaPublicacaoResult = value;
    }

}
