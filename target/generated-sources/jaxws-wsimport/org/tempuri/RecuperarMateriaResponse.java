
package org.tempuri;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java de anonymous complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="RecuperarMateriaResult" type="{http://tempuri.org/}ArrayOfMateria" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "recuperarMateriaResult"
})
@XmlRootElement(name = "RecuperarMateriaResponse")
public class RecuperarMateriaResponse {

    @XmlElement(name = "RecuperarMateriaResult")
    protected ArrayOfMateria recuperarMateriaResult;

    /**
     * Obtém o valor da propriedade recuperarMateriaResult.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfMateria }
     *     
     */
    public ArrayOfMateria getRecuperarMateriaResult() {
        return recuperarMateriaResult;
    }

    /**
     * Define o valor da propriedade recuperarMateriaResult.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfMateria }
     *     
     */
    public void setRecuperarMateriaResult(ArrayOfMateria value) {
        this.recuperarMateriaResult = value;
    }

}
