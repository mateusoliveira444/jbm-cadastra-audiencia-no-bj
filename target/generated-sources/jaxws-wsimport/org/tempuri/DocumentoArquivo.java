
package org.tempuri;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java de DocumentoArquivo complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="DocumentoArquivo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="IDArquivo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="NomeArquivo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DataAnexo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="TipoArquivo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="UrlDownload" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DocumentoArquivo", propOrder = {
    "idArquivo",
    "nomeArquivo",
    "dataAnexo",
    "tipoArquivo",
    "urlDownload"
})
public class DocumentoArquivo {

    @XmlElement(name = "IDArquivo")
    protected String idArquivo;
    @XmlElement(name = "NomeArquivo")
    protected String nomeArquivo;
    @XmlElement(name = "DataAnexo")
    protected String dataAnexo;
    @XmlElement(name = "TipoArquivo")
    protected String tipoArquivo;
    @XmlElement(name = "UrlDownload")
    protected String urlDownload;

    /**
     * Obtém o valor da propriedade idArquivo.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIDArquivo() {
        return idArquivo;
    }

    /**
     * Define o valor da propriedade idArquivo.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIDArquivo(String value) {
        this.idArquivo = value;
    }

    /**
     * Obtém o valor da propriedade nomeArquivo.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNomeArquivo() {
        return nomeArquivo;
    }

    /**
     * Define o valor da propriedade nomeArquivo.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNomeArquivo(String value) {
        this.nomeArquivo = value;
    }

    /**
     * Obtém o valor da propriedade dataAnexo.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDataAnexo() {
        return dataAnexo;
    }

    /**
     * Define o valor da propriedade dataAnexo.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDataAnexo(String value) {
        this.dataAnexo = value;
    }

    /**
     * Obtém o valor da propriedade tipoArquivo.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTipoArquivo() {
        return tipoArquivo;
    }

    /**
     * Define o valor da propriedade tipoArquivo.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTipoArquivo(String value) {
        this.tipoArquivo = value;
    }

    /**
     * Obtém o valor da propriedade urlDownload.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUrlDownload() {
        return urlDownload;
    }

    /**
     * Define o valor da propriedade urlDownload.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUrlDownload(String value) {
        this.urlDownload = value;
    }

}
