
package org.tempuri;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Classe Java de Solicitacao complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="Solicitacao">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="IDProcessoUnico" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="TextoSolicitacao" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="IDTipoSolicitacao" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="DataLimite" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="DataAudiencia" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="HoraAudienca" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Local" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Preposto" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="NomePreposto" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="IdComarca" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="NomeSolicitante" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="IDCorrespondente" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="UsuarioSolicitante" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="IdSistema" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="NumeroProcessoSolicitacao" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Jurisdicao" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Solicitacao", propOrder = {
    "idProcessoUnico",
    "textoSolicitacao",
    "idTipoSolicitacao",
    "dataLimite",
    "dataAudiencia",
    "horaAudienca",
    "local",
    "preposto",
    "nomePreposto",
    "idComarca",
    "nomeSolicitante",
    "idCorrespondente",
    "usuarioSolicitante",
    "idSistema",
    "numeroProcessoSolicitacao",
    "jurisdicao"
})
public class Solicitacao {

    @XmlElement(name = "IDProcessoUnico")
    protected String idProcessoUnico;
    @XmlElement(name = "TextoSolicitacao")
    protected String textoSolicitacao;
    @XmlElement(name = "IDTipoSolicitacao")
    protected int idTipoSolicitacao;
    @XmlElement(name = "DataLimite", required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar dataLimite;
    @XmlElement(name = "DataAudiencia", required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar dataAudiencia;
    @XmlElement(name = "HoraAudienca")
    protected String horaAudienca;
    @XmlElement(name = "Local")
    protected String local;
    @XmlElement(name = "Preposto")
    protected int preposto;
    @XmlElement(name = "NomePreposto")
    protected String nomePreposto;
    @XmlElement(name = "IdComarca")
    protected int idComarca;
    @XmlElement(name = "NomeSolicitante")
    protected String nomeSolicitante;
    @XmlElement(name = "IDCorrespondente", required = true, type = Integer.class, nillable = true)
    protected Integer idCorrespondente;
    @XmlElement(name = "UsuarioSolicitante")
    protected String usuarioSolicitante;
    @XmlElement(name = "IdSistema", required = true, type = Integer.class, nillable = true)
    protected Integer idSistema;
    @XmlElement(name = "NumeroProcessoSolicitacao")
    protected String numeroProcessoSolicitacao;
    @XmlElement(name = "Jurisdicao")
    protected String jurisdicao;

    /**
     * Obtém o valor da propriedade idProcessoUnico.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIDProcessoUnico() {
        return idProcessoUnico;
    }

    /**
     * Define o valor da propriedade idProcessoUnico.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIDProcessoUnico(String value) {
        this.idProcessoUnico = value;
    }

    /**
     * Obtém o valor da propriedade textoSolicitacao.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTextoSolicitacao() {
        return textoSolicitacao;
    }

    /**
     * Define o valor da propriedade textoSolicitacao.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTextoSolicitacao(String value) {
        this.textoSolicitacao = value;
    }

    /**
     * Obtém o valor da propriedade idTipoSolicitacao.
     * 
     */
    public int getIDTipoSolicitacao() {
        return idTipoSolicitacao;
    }

    /**
     * Define o valor da propriedade idTipoSolicitacao.
     * 
     */
    public void setIDTipoSolicitacao(int value) {
        this.idTipoSolicitacao = value;
    }

    /**
     * Obtém o valor da propriedade dataLimite.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDataLimite() {
        return dataLimite;
    }

    /**
     * Define o valor da propriedade dataLimite.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDataLimite(XMLGregorianCalendar value) {
        this.dataLimite = value;
    }

    /**
     * Obtém o valor da propriedade dataAudiencia.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDataAudiencia() {
        return dataAudiencia;
    }

    /**
     * Define o valor da propriedade dataAudiencia.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDataAudiencia(XMLGregorianCalendar value) {
        this.dataAudiencia = value;
    }

    /**
     * Obtém o valor da propriedade horaAudienca.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getHoraAudienca() {
        return horaAudienca;
    }

    /**
     * Define o valor da propriedade horaAudienca.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setHoraAudienca(String value) {
        this.horaAudienca = value;
    }

    /**
     * Obtém o valor da propriedade local.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLocal() {
        return local;
    }

    /**
     * Define o valor da propriedade local.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLocal(String value) {
        this.local = value;
    }

    /**
     * Obtém o valor da propriedade preposto.
     * 
     */
    public int getPreposto() {
        return preposto;
    }

    /**
     * Define o valor da propriedade preposto.
     * 
     */
    public void setPreposto(int value) {
        this.preposto = value;
    }

    /**
     * Obtém o valor da propriedade nomePreposto.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNomePreposto() {
        return nomePreposto;
    }

    /**
     * Define o valor da propriedade nomePreposto.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNomePreposto(String value) {
        this.nomePreposto = value;
    }

    /**
     * Obtém o valor da propriedade idComarca.
     * 
     */
    public int getIdComarca() {
        return idComarca;
    }

    /**
     * Define o valor da propriedade idComarca.
     * 
     */
    public void setIdComarca(int value) {
        this.idComarca = value;
    }

    /**
     * Obtém o valor da propriedade nomeSolicitante.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNomeSolicitante() {
        return nomeSolicitante;
    }

    /**
     * Define o valor da propriedade nomeSolicitante.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNomeSolicitante(String value) {
        this.nomeSolicitante = value;
    }

    /**
     * Obtém o valor da propriedade idCorrespondente.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getIDCorrespondente() {
        return idCorrespondente;
    }

    /**
     * Define o valor da propriedade idCorrespondente.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setIDCorrespondente(Integer value) {
        this.idCorrespondente = value;
    }

    /**
     * Obtém o valor da propriedade usuarioSolicitante.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUsuarioSolicitante() {
        return usuarioSolicitante;
    }

    /**
     * Define o valor da propriedade usuarioSolicitante.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUsuarioSolicitante(String value) {
        this.usuarioSolicitante = value;
    }

    /**
     * Obtém o valor da propriedade idSistema.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getIdSistema() {
        return idSistema;
    }

    /**
     * Define o valor da propriedade idSistema.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setIdSistema(Integer value) {
        this.idSistema = value;
    }

    /**
     * Obtém o valor da propriedade numeroProcessoSolicitacao.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNumeroProcessoSolicitacao() {
        return numeroProcessoSolicitacao;
    }

    /**
     * Define o valor da propriedade numeroProcessoSolicitacao.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNumeroProcessoSolicitacao(String value) {
        this.numeroProcessoSolicitacao = value;
    }

    /**
     * Obtém o valor da propriedade jurisdicao.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getJurisdicao() {
        return jurisdicao;
    }

    /**
     * Define o valor da propriedade jurisdicao.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setJurisdicao(String value) {
        this.jurisdicao = value;
    }

}
