
package org.tempuri;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Classe Java de Table complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="Table">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="NWQ_CCLIEN" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="NWQ_PROCO" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="NWQ_NUMPRO" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="NWQ_CAJURI" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="NWQ_DCLIEN" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="NWQ_NOMEPC" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="NWQ_DTPUBL" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="NWQ_OBSER" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="NWQ_TEXTO" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="NWQ_STATUS" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="NWQ_FONTE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="NWQ_CODIGO" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="NWQ_DADVOG" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="NWQ_DADVPC" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="NWQ_NOMEPI" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="NWQ_TIPO" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="NWQ_CCLIENKURIER" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Table", propOrder = {
    "nwqcclien",
    "nwqproco",
    "nwqnumpro",
    "nwqcajuri",
    "nwqdclien",
    "nwqnomepc",
    "nwqdtpubl",
    "nwqobser",
    "nwqtexto",
    "nwqstatus",
    "nwqfonte",
    "nwqcodigo",
    "nwqdadvog",
    "nwqdadvpc",
    "nwqnomepi",
    "nwqtipo",
    "nwqcclienkurier"
})
public class Table {

    @XmlElement(name = "NWQ_CCLIEN")
    protected String nwqcclien;
    @XmlElement(name = "NWQ_PROCO")
    protected String nwqproco;
    @XmlElement(name = "NWQ_NUMPRO")
    protected String nwqnumpro;
    @XmlElement(name = "NWQ_CAJURI")
    protected String nwqcajuri;
    @XmlElement(name = "NWQ_DCLIEN")
    protected String nwqdclien;
    @XmlElement(name = "NWQ_NOMEPC")
    protected String nwqnomepc;
    @XmlElement(name = "NWQ_DTPUBL", required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar nwqdtpubl;
    @XmlElement(name = "NWQ_OBSER")
    protected String nwqobser;
    @XmlElement(name = "NWQ_TEXTO")
    protected String nwqtexto;
    @XmlElement(name = "NWQ_STATUS")
    protected String nwqstatus;
    @XmlElement(name = "NWQ_FONTE")
    protected String nwqfonte;
    @XmlElement(name = "NWQ_CODIGO")
    protected int nwqcodigo;
    @XmlElement(name = "NWQ_DADVOG")
    protected String nwqdadvog;
    @XmlElement(name = "NWQ_DADVPC")
    protected String nwqdadvpc;
    @XmlElement(name = "NWQ_NOMEPI")
    protected String nwqnomepi;
    @XmlElement(name = "NWQ_TIPO")
    protected String nwqtipo;
    @XmlElement(name = "NWQ_CCLIENKURIER")
    protected int nwqcclienkurier;

    /**
     * Obtém o valor da propriedade nwqcclien.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNWQCCLIEN() {
        return nwqcclien;
    }

    /**
     * Define o valor da propriedade nwqcclien.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNWQCCLIEN(String value) {
        this.nwqcclien = value;
    }

    /**
     * Obtém o valor da propriedade nwqproco.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNWQPROCO() {
        return nwqproco;
    }

    /**
     * Define o valor da propriedade nwqproco.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNWQPROCO(String value) {
        this.nwqproco = value;
    }

    /**
     * Obtém o valor da propriedade nwqnumpro.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNWQNUMPRO() {
        return nwqnumpro;
    }

    /**
     * Define o valor da propriedade nwqnumpro.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNWQNUMPRO(String value) {
        this.nwqnumpro = value;
    }

    /**
     * Obtém o valor da propriedade nwqcajuri.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNWQCAJURI() {
        return nwqcajuri;
    }

    /**
     * Define o valor da propriedade nwqcajuri.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNWQCAJURI(String value) {
        this.nwqcajuri = value;
    }

    /**
     * Obtém o valor da propriedade nwqdclien.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNWQDCLIEN() {
        return nwqdclien;
    }

    /**
     * Define o valor da propriedade nwqdclien.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNWQDCLIEN(String value) {
        this.nwqdclien = value;
    }

    /**
     * Obtém o valor da propriedade nwqnomepc.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNWQNOMEPC() {
        return nwqnomepc;
    }

    /**
     * Define o valor da propriedade nwqnomepc.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNWQNOMEPC(String value) {
        this.nwqnomepc = value;
    }

    /**
     * Obtém o valor da propriedade nwqdtpubl.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getNWQDTPUBL() {
        return nwqdtpubl;
    }

    /**
     * Define o valor da propriedade nwqdtpubl.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setNWQDTPUBL(XMLGregorianCalendar value) {
        this.nwqdtpubl = value;
    }

    /**
     * Obtém o valor da propriedade nwqobser.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNWQOBSER() {
        return nwqobser;
    }

    /**
     * Define o valor da propriedade nwqobser.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNWQOBSER(String value) {
        this.nwqobser = value;
    }

    /**
     * Obtém o valor da propriedade nwqtexto.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNWQTEXTO() {
        return nwqtexto;
    }

    /**
     * Define o valor da propriedade nwqtexto.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNWQTEXTO(String value) {
        this.nwqtexto = value;
    }

    /**
     * Obtém o valor da propriedade nwqstatus.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNWQSTATUS() {
        return nwqstatus;
    }

    /**
     * Define o valor da propriedade nwqstatus.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNWQSTATUS(String value) {
        this.nwqstatus = value;
    }

    /**
     * Obtém o valor da propriedade nwqfonte.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNWQFONTE() {
        return nwqfonte;
    }

    /**
     * Define o valor da propriedade nwqfonte.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNWQFONTE(String value) {
        this.nwqfonte = value;
    }

    /**
     * Obtém o valor da propriedade nwqcodigo.
     * 
     */
    public int getNWQCODIGO() {
        return nwqcodigo;
    }

    /**
     * Define o valor da propriedade nwqcodigo.
     * 
     */
    public void setNWQCODIGO(int value) {
        this.nwqcodigo = value;
    }

    /**
     * Obtém o valor da propriedade nwqdadvog.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNWQDADVOG() {
        return nwqdadvog;
    }

    /**
     * Define o valor da propriedade nwqdadvog.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNWQDADVOG(String value) {
        this.nwqdadvog = value;
    }

    /**
     * Obtém o valor da propriedade nwqdadvpc.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNWQDADVPC() {
        return nwqdadvpc;
    }

    /**
     * Define o valor da propriedade nwqdadvpc.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNWQDADVPC(String value) {
        this.nwqdadvpc = value;
    }

    /**
     * Obtém o valor da propriedade nwqnomepi.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNWQNOMEPI() {
        return nwqnomepi;
    }

    /**
     * Define o valor da propriedade nwqnomepi.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNWQNOMEPI(String value) {
        this.nwqnomepi = value;
    }

    /**
     * Obtém o valor da propriedade nwqtipo.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNWQTIPO() {
        return nwqtipo;
    }

    /**
     * Define o valor da propriedade nwqtipo.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNWQTIPO(String value) {
        this.nwqtipo = value;
    }

    /**
     * Obtém o valor da propriedade nwqcclienkurier.
     * 
     */
    public int getNWQCCLIENKURIER() {
        return nwqcclienkurier;
    }

    /**
     * Define o valor da propriedade nwqcclienkurier.
     * 
     */
    public void setNWQCCLIENKURIER(int value) {
        this.nwqcclienkurier = value;
    }

}
