
package org.tempuri;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java de anonymous complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="FechamentoLoteCustasResult" type="{http://tempuri.org/}RetornoMetodos" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "fechamentoLoteCustasResult"
})
@XmlRootElement(name = "FechamentoLoteCustasResponse")
public class FechamentoLoteCustasResponse {

    @XmlElement(name = "FechamentoLoteCustasResult")
    protected RetornoMetodos fechamentoLoteCustasResult;

    /**
     * Obtém o valor da propriedade fechamentoLoteCustasResult.
     * 
     * @return
     *     possible object is
     *     {@link RetornoMetodos }
     *     
     */
    public RetornoMetodos getFechamentoLoteCustasResult() {
        return fechamentoLoteCustasResult;
    }

    /**
     * Define o valor da propriedade fechamentoLoteCustasResult.
     * 
     * @param value
     *     allowed object is
     *     {@link RetornoMetodos }
     *     
     */
    public void setFechamentoLoteCustasResult(RetornoMetodos value) {
        this.fechamentoLoteCustasResult = value;
    }

}
