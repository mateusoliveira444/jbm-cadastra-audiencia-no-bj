
package org.tempuri;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java de TipoVara complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="TipoVara">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="IDTipoVara" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="TipoVaraDesc" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TipoVara", propOrder = {
    "idTipoVara",
    "tipoVaraDesc"
})
public class TipoVara {

    @XmlElement(name = "IDTipoVara")
    protected int idTipoVara;
    @XmlElement(name = "TipoVaraDesc")
    protected String tipoVaraDesc;

    /**
     * Obtém o valor da propriedade idTipoVara.
     * 
     */
    public int getIDTipoVara() {
        return idTipoVara;
    }

    /**
     * Define o valor da propriedade idTipoVara.
     * 
     */
    public void setIDTipoVara(int value) {
        this.idTipoVara = value;
    }

    /**
     * Obtém o valor da propriedade tipoVaraDesc.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTipoVaraDesc() {
        return tipoVaraDesc;
    }

    /**
     * Define o valor da propriedade tipoVaraDesc.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTipoVaraDesc(String value) {
        this.tipoVaraDesc = value;
    }

}
