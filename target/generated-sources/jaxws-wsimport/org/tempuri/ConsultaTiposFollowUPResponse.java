
package org.tempuri;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java de anonymous complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ConsultaTiposFollowUPResult" type="{http://tempuri.org/}ArrayOfRetornoTipoFollowUP" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "consultaTiposFollowUPResult"
})
@XmlRootElement(name = "ConsultaTiposFollowUPResponse")
public class ConsultaTiposFollowUPResponse {

    @XmlElement(name = "ConsultaTiposFollowUPResult")
    protected ArrayOfRetornoTipoFollowUP consultaTiposFollowUPResult;

    /**
     * Obtém o valor da propriedade consultaTiposFollowUPResult.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfRetornoTipoFollowUP }
     *     
     */
    public ArrayOfRetornoTipoFollowUP getConsultaTiposFollowUPResult() {
        return consultaTiposFollowUPResult;
    }

    /**
     * Define o valor da propriedade consultaTiposFollowUPResult.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfRetornoTipoFollowUP }
     *     
     */
    public void setConsultaTiposFollowUPResult(ArrayOfRetornoTipoFollowUP value) {
        this.consultaTiposFollowUPResult = value;
    }

}
