
package org.tempuri;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java de RetornaAuditoriaResultado complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="RetornaAuditoriaResultado">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="TipoAgendamento" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DataPreenchimento" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Grupo" type="{http://tempuri.org/}Grupo" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "RetornaAuditoriaResultado", propOrder = {
    "tipoAgendamento",
    "dataPreenchimento",
    "grupo"
})
public class RetornaAuditoriaResultado {

    @XmlElement(name = "TipoAgendamento")
    protected String tipoAgendamento;
    @XmlElement(name = "DataPreenchimento")
    protected String dataPreenchimento;
    @XmlElement(name = "Grupo")
    protected List<Grupo> grupo;

    /**
     * Obtém o valor da propriedade tipoAgendamento.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTipoAgendamento() {
        return tipoAgendamento;
    }

    /**
     * Define o valor da propriedade tipoAgendamento.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTipoAgendamento(String value) {
        this.tipoAgendamento = value;
    }

    /**
     * Obtém o valor da propriedade dataPreenchimento.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDataPreenchimento() {
        return dataPreenchimento;
    }

    /**
     * Define o valor da propriedade dataPreenchimento.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDataPreenchimento(String value) {
        this.dataPreenchimento = value;
    }

    /**
     * Gets the value of the grupo property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the grupo property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getGrupo().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Grupo }
     * 
     * 
     */
    public List<Grupo> getGrupo() {
        if (grupo == null) {
            grupo = new ArrayList<Grupo>();
        }
        return this.grupo;
    }

}
