
package org.tempuri;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java de StatusProcesso complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="StatusProcesso">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="IDStatusProcesso" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="StatusProcessoDesc" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "StatusProcesso", propOrder = {
    "idStatusProcesso",
    "statusProcessoDesc"
})
public class StatusProcesso {

    @XmlElement(name = "IDStatusProcesso")
    protected int idStatusProcesso;
    @XmlElement(name = "StatusProcessoDesc")
    protected String statusProcessoDesc;

    /**
     * Obtém o valor da propriedade idStatusProcesso.
     * 
     */
    public int getIDStatusProcesso() {
        return idStatusProcesso;
    }

    /**
     * Define o valor da propriedade idStatusProcesso.
     * 
     */
    public void setIDStatusProcesso(int value) {
        this.idStatusProcesso = value;
    }

    /**
     * Obtém o valor da propriedade statusProcessoDesc.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStatusProcessoDesc() {
        return statusProcessoDesc;
    }

    /**
     * Define o valor da propriedade statusProcessoDesc.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStatusProcessoDesc(String value) {
        this.statusProcessoDesc = value;
    }

}
