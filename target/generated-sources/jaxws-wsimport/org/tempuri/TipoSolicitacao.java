
package org.tempuri;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java de TipoSolicitacao complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="TipoSolicitacao">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="IDTipoSolicitacao" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="Descricao" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="LocalObrigatorio" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="HoraLimiteObrigatoria" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TipoSolicitacao", propOrder = {
    "idTipoSolicitacao",
    "descricao",
    "localObrigatorio",
    "horaLimiteObrigatoria"
})
public class TipoSolicitacao {

    @XmlElement(name = "IDTipoSolicitacao")
    protected int idTipoSolicitacao;
    @XmlElement(name = "Descricao")
    protected String descricao;
    @XmlElement(name = "LocalObrigatorio")
    protected int localObrigatorio;
    @XmlElement(name = "HoraLimiteObrigatoria")
    protected int horaLimiteObrigatoria;

    /**
     * Obtém o valor da propriedade idTipoSolicitacao.
     * 
     */
    public int getIDTipoSolicitacao() {
        return idTipoSolicitacao;
    }

    /**
     * Define o valor da propriedade idTipoSolicitacao.
     * 
     */
    public void setIDTipoSolicitacao(int value) {
        this.idTipoSolicitacao = value;
    }

    /**
     * Obtém o valor da propriedade descricao.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescricao() {
        return descricao;
    }

    /**
     * Define o valor da propriedade descricao.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescricao(String value) {
        this.descricao = value;
    }

    /**
     * Obtém o valor da propriedade localObrigatorio.
     * 
     */
    public int getLocalObrigatorio() {
        return localObrigatorio;
    }

    /**
     * Define o valor da propriedade localObrigatorio.
     * 
     */
    public void setLocalObrigatorio(int value) {
        this.localObrigatorio = value;
    }

    /**
     * Obtém o valor da propriedade horaLimiteObrigatoria.
     * 
     */
    public int getHoraLimiteObrigatoria() {
        return horaLimiteObrigatoria;
    }

    /**
     * Define o valor da propriedade horaLimiteObrigatoria.
     * 
     */
    public void setHoraLimiteObrigatoria(int value) {
        this.horaLimiteObrigatoria = value;
    }

}
