
package org.tempuri;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Classe Java de Servico complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="Servico">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="IdServico" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="ServicoTipo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ServicoStatus" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Data" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="Celula" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="AdvogadoOperacional" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Responsavel" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Descricao" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Avulso" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="NumeroProcesso" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ParteContraria" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Regiao" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ControleCliente" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Ordem" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Comarca" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Uf" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ParteInteressada" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Valor" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="SuperEspecial" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="ProcessoTipoServico" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Area" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Projudi" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="DataEncerramento" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="Cliente" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Justica" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Foro" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Vara" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Orgao" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="IdOrdinal" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="Instancia" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ProcessoSistema" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Servico", propOrder = {
    "idServico",
    "servicoTipo",
    "servicoStatus",
    "data",
    "celula",
    "advogadoOperacional",
    "responsavel",
    "descricao",
    "avulso",
    "numeroProcesso",
    "parteContraria",
    "regiao",
    "controleCliente",
    "ordem",
    "comarca",
    "uf",
    "parteInteressada",
    "valor",
    "superEspecial",
    "processoTipoServico",
    "area",
    "projudi",
    "dataEncerramento",
    "cliente",
    "justica",
    "foro",
    "vara",
    "orgao",
    "idOrdinal",
    "instancia",
    "processoSistema"
})
public class Servico {

    @XmlElement(name = "IdServico")
    protected int idServico;
    @XmlElement(name = "ServicoTipo")
    protected String servicoTipo;
    @XmlElement(name = "ServicoStatus")
    protected String servicoStatus;
    @XmlElement(name = "Data", required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar data;
    @XmlElement(name = "Celula")
    protected String celula;
    @XmlElement(name = "AdvogadoOperacional")
    protected String advogadoOperacional;
    @XmlElement(name = "Responsavel")
    protected String responsavel;
    @XmlElement(name = "Descricao")
    protected String descricao;
    @XmlElement(name = "Avulso")
    protected boolean avulso;
    @XmlElement(name = "NumeroProcesso")
    protected String numeroProcesso;
    @XmlElement(name = "ParteContraria")
    protected String parteContraria;
    @XmlElement(name = "Regiao")
    protected String regiao;
    @XmlElement(name = "ControleCliente")
    protected String controleCliente;
    @XmlElement(name = "Ordem")
    protected String ordem;
    @XmlElement(name = "Comarca")
    protected String comarca;
    @XmlElement(name = "Uf")
    protected String uf;
    @XmlElement(name = "ParteInteressada")
    protected String parteInteressada;
    @XmlElement(name = "Valor")
    protected String valor;
    @XmlElement(name = "SuperEspecial", required = true, type = Boolean.class, nillable = true)
    protected Boolean superEspecial;
    @XmlElement(name = "ProcessoTipoServico")
    protected String processoTipoServico;
    @XmlElement(name = "Area")
    protected String area;
    @XmlElement(name = "Projudi", required = true, type = Boolean.class, nillable = true)
    protected Boolean projudi;
    @XmlElement(name = "DataEncerramento", required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar dataEncerramento;
    @XmlElement(name = "Cliente")
    protected String cliente;
    @XmlElement(name = "Justica")
    protected String justica;
    @XmlElement(name = "Foro")
    protected String foro;
    @XmlElement(name = "Vara")
    protected String vara;
    @XmlElement(name = "Orgao")
    protected String orgao;
    @XmlElement(name = "IdOrdinal")
    protected int idOrdinal;
    @XmlElement(name = "Instancia")
    protected String instancia;
    @XmlElement(name = "ProcessoSistema")
    protected String processoSistema;

    /**
     * Obtém o valor da propriedade idServico.
     * 
     */
    public int getIdServico() {
        return idServico;
    }

    /**
     * Define o valor da propriedade idServico.
     * 
     */
    public void setIdServico(int value) {
        this.idServico = value;
    }

    /**
     * Obtém o valor da propriedade servicoTipo.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getServicoTipo() {
        return servicoTipo;
    }

    /**
     * Define o valor da propriedade servicoTipo.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setServicoTipo(String value) {
        this.servicoTipo = value;
    }

    /**
     * Obtém o valor da propriedade servicoStatus.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getServicoStatus() {
        return servicoStatus;
    }

    /**
     * Define o valor da propriedade servicoStatus.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setServicoStatus(String value) {
        this.servicoStatus = value;
    }

    /**
     * Obtém o valor da propriedade data.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getData() {
        return data;
    }

    /**
     * Define o valor da propriedade data.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setData(XMLGregorianCalendar value) {
        this.data = value;
    }

    /**
     * Obtém o valor da propriedade celula.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCelula() {
        return celula;
    }

    /**
     * Define o valor da propriedade celula.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCelula(String value) {
        this.celula = value;
    }

    /**
     * Obtém o valor da propriedade advogadoOperacional.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAdvogadoOperacional() {
        return advogadoOperacional;
    }

    /**
     * Define o valor da propriedade advogadoOperacional.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAdvogadoOperacional(String value) {
        this.advogadoOperacional = value;
    }

    /**
     * Obtém o valor da propriedade responsavel.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getResponsavel() {
        return responsavel;
    }

    /**
     * Define o valor da propriedade responsavel.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setResponsavel(String value) {
        this.responsavel = value;
    }

    /**
     * Obtém o valor da propriedade descricao.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescricao() {
        return descricao;
    }

    /**
     * Define o valor da propriedade descricao.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescricao(String value) {
        this.descricao = value;
    }

    /**
     * Obtém o valor da propriedade avulso.
     * 
     */
    public boolean isAvulso() {
        return avulso;
    }

    /**
     * Define o valor da propriedade avulso.
     * 
     */
    public void setAvulso(boolean value) {
        this.avulso = value;
    }

    /**
     * Obtém o valor da propriedade numeroProcesso.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNumeroProcesso() {
        return numeroProcesso;
    }

    /**
     * Define o valor da propriedade numeroProcesso.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNumeroProcesso(String value) {
        this.numeroProcesso = value;
    }

    /**
     * Obtém o valor da propriedade parteContraria.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getParteContraria() {
        return parteContraria;
    }

    /**
     * Define o valor da propriedade parteContraria.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setParteContraria(String value) {
        this.parteContraria = value;
    }

    /**
     * Obtém o valor da propriedade regiao.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRegiao() {
        return regiao;
    }

    /**
     * Define o valor da propriedade regiao.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRegiao(String value) {
        this.regiao = value;
    }

    /**
     * Obtém o valor da propriedade controleCliente.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getControleCliente() {
        return controleCliente;
    }

    /**
     * Define o valor da propriedade controleCliente.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setControleCliente(String value) {
        this.controleCliente = value;
    }

    /**
     * Obtém o valor da propriedade ordem.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOrdem() {
        return ordem;
    }

    /**
     * Define o valor da propriedade ordem.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOrdem(String value) {
        this.ordem = value;
    }

    /**
     * Obtém o valor da propriedade comarca.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getComarca() {
        return comarca;
    }

    /**
     * Define o valor da propriedade comarca.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setComarca(String value) {
        this.comarca = value;
    }

    /**
     * Obtém o valor da propriedade uf.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUf() {
        return uf;
    }

    /**
     * Define o valor da propriedade uf.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUf(String value) {
        this.uf = value;
    }

    /**
     * Obtém o valor da propriedade parteInteressada.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getParteInteressada() {
        return parteInteressada;
    }

    /**
     * Define o valor da propriedade parteInteressada.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setParteInteressada(String value) {
        this.parteInteressada = value;
    }

    /**
     * Obtém o valor da propriedade valor.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getValor() {
        return valor;
    }

    /**
     * Define o valor da propriedade valor.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setValor(String value) {
        this.valor = value;
    }

    /**
     * Obtém o valor da propriedade superEspecial.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isSuperEspecial() {
        return superEspecial;
    }

    /**
     * Define o valor da propriedade superEspecial.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setSuperEspecial(Boolean value) {
        this.superEspecial = value;
    }

    /**
     * Obtém o valor da propriedade processoTipoServico.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProcessoTipoServico() {
        return processoTipoServico;
    }

    /**
     * Define o valor da propriedade processoTipoServico.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProcessoTipoServico(String value) {
        this.processoTipoServico = value;
    }

    /**
     * Obtém o valor da propriedade area.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getArea() {
        return area;
    }

    /**
     * Define o valor da propriedade area.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setArea(String value) {
        this.area = value;
    }

    /**
     * Obtém o valor da propriedade projudi.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isProjudi() {
        return projudi;
    }

    /**
     * Define o valor da propriedade projudi.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setProjudi(Boolean value) {
        this.projudi = value;
    }

    /**
     * Obtém o valor da propriedade dataEncerramento.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDataEncerramento() {
        return dataEncerramento;
    }

    /**
     * Define o valor da propriedade dataEncerramento.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDataEncerramento(XMLGregorianCalendar value) {
        this.dataEncerramento = value;
    }

    /**
     * Obtém o valor da propriedade cliente.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCliente() {
        return cliente;
    }

    /**
     * Define o valor da propriedade cliente.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCliente(String value) {
        this.cliente = value;
    }

    /**
     * Obtém o valor da propriedade justica.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getJustica() {
        return justica;
    }

    /**
     * Define o valor da propriedade justica.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setJustica(String value) {
        this.justica = value;
    }

    /**
     * Obtém o valor da propriedade foro.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getForo() {
        return foro;
    }

    /**
     * Define o valor da propriedade foro.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setForo(String value) {
        this.foro = value;
    }

    /**
     * Obtém o valor da propriedade vara.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVara() {
        return vara;
    }

    /**
     * Define o valor da propriedade vara.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVara(String value) {
        this.vara = value;
    }

    /**
     * Obtém o valor da propriedade orgao.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOrgao() {
        return orgao;
    }

    /**
     * Define o valor da propriedade orgao.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOrgao(String value) {
        this.orgao = value;
    }

    /**
     * Obtém o valor da propriedade idOrdinal.
     * 
     */
    public int getIdOrdinal() {
        return idOrdinal;
    }

    /**
     * Define o valor da propriedade idOrdinal.
     * 
     */
    public void setIdOrdinal(int value) {
        this.idOrdinal = value;
    }

    /**
     * Obtém o valor da propriedade instancia.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInstancia() {
        return instancia;
    }

    /**
     * Define o valor da propriedade instancia.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInstancia(String value) {
        this.instancia = value;
    }

    /**
     * Obtém o valor da propriedade processoSistema.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProcessoSistema() {
        return processoSistema;
    }

    /**
     * Define o valor da propriedade processoSistema.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProcessoSistema(String value) {
        this.processoSistema = value;
    }

}
