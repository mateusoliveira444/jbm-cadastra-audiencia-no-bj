
package org.tempuri;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java de ResultadoRetornoApenso complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="ResultadoRetornoApenso">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Retorno" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="Mensagem" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="TipoBusca" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="RetornoApenso" type="{http://tempuri.org/}RetornoApenso" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ResultadoRetornoApenso", propOrder = {
    "retorno",
    "mensagem",
    "tipoBusca",
    "retornoApenso"
})
public class ResultadoRetornoApenso {

    @XmlElement(name = "Retorno")
    protected boolean retorno;
    @XmlElement(name = "Mensagem")
    protected String mensagem;
    @XmlElement(name = "TipoBusca")
    protected String tipoBusca;
    @XmlElement(name = "RetornoApenso")
    protected List<RetornoApenso> retornoApenso;

    /**
     * Obtém o valor da propriedade retorno.
     * 
     */
    public boolean isRetorno() {
        return retorno;
    }

    /**
     * Define o valor da propriedade retorno.
     * 
     */
    public void setRetorno(boolean value) {
        this.retorno = value;
    }

    /**
     * Obtém o valor da propriedade mensagem.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMensagem() {
        return mensagem;
    }

    /**
     * Define o valor da propriedade mensagem.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMensagem(String value) {
        this.mensagem = value;
    }

    /**
     * Obtém o valor da propriedade tipoBusca.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTipoBusca() {
        return tipoBusca;
    }

    /**
     * Define o valor da propriedade tipoBusca.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTipoBusca(String value) {
        this.tipoBusca = value;
    }

    /**
     * Gets the value of the retornoApenso property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the retornoApenso property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRetornoApenso().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link RetornoApenso }
     * 
     * 
     */
    public List<RetornoApenso> getRetornoApenso() {
        if (retornoApenso == null) {
            retornoApenso = new ArrayList<RetornoApenso>();
        }
        return this.retornoApenso;
    }

}
