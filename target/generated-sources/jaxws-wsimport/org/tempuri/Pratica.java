
package org.tempuri;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java de Pratica complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="Pratica">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="DescricaoArea" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DescricaoPratica" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="IdPratica" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Pratica", propOrder = {
    "descricaoArea",
    "descricaoPratica",
    "idPratica"
})
public class Pratica {

    @XmlElement(name = "DescricaoArea")
    protected String descricaoArea;
    @XmlElement(name = "DescricaoPratica")
    protected String descricaoPratica;
    @XmlElement(name = "IdPratica")
    protected int idPratica;

    /**
     * Obtém o valor da propriedade descricaoArea.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescricaoArea() {
        return descricaoArea;
    }

    /**
     * Define o valor da propriedade descricaoArea.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescricaoArea(String value) {
        this.descricaoArea = value;
    }

    /**
     * Obtém o valor da propriedade descricaoPratica.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescricaoPratica() {
        return descricaoPratica;
    }

    /**
     * Define o valor da propriedade descricaoPratica.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescricaoPratica(String value) {
        this.descricaoPratica = value;
    }

    /**
     * Obtém o valor da propriedade idPratica.
     * 
     */
    public int getIdPratica() {
        return idPratica;
    }

    /**
     * Define o valor da propriedade idPratica.
     * 
     */
    public void setIdPratica(int value) {
        this.idPratica = value;
    }

}
