
package org.tempuri;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java de anonymous complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="lstrIDProcessoUnico" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="lstrMemo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="lstrTipoAgendamento" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="lstrDataAgendamento" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="lstrDataFinal" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="lstrResponsavel" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="lstrUsuario" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="lstrSenha" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "lstrIDProcessoUnico",
    "lstrMemo",
    "lstrTipoAgendamento",
    "lstrDataAgendamento",
    "lstrDataFinal",
    "lstrResponsavel",
    "lstrUsuario",
    "lstrSenha"
})
@XmlRootElement(name = "InsereAgendamentoProcesso")
public class InsereAgendamentoProcesso {

    protected String lstrIDProcessoUnico;
    protected String lstrMemo;
    protected String lstrTipoAgendamento;
    protected String lstrDataAgendamento;
    protected String lstrDataFinal;
    protected String lstrResponsavel;
    protected String lstrUsuario;
    protected String lstrSenha;

    /**
     * Obtém o valor da propriedade lstrIDProcessoUnico.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLstrIDProcessoUnico() {
        return lstrIDProcessoUnico;
    }

    /**
     * Define o valor da propriedade lstrIDProcessoUnico.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLstrIDProcessoUnico(String value) {
        this.lstrIDProcessoUnico = value;
    }

    /**
     * Obtém o valor da propriedade lstrMemo.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLstrMemo() {
        return lstrMemo;
    }

    /**
     * Define o valor da propriedade lstrMemo.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLstrMemo(String value) {
        this.lstrMemo = value;
    }

    /**
     * Obtém o valor da propriedade lstrTipoAgendamento.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLstrTipoAgendamento() {
        return lstrTipoAgendamento;
    }

    /**
     * Define o valor da propriedade lstrTipoAgendamento.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLstrTipoAgendamento(String value) {
        this.lstrTipoAgendamento = value;
    }

    /**
     * Obtém o valor da propriedade lstrDataAgendamento.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLstrDataAgendamento() {
        return lstrDataAgendamento;
    }

    /**
     * Define o valor da propriedade lstrDataAgendamento.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLstrDataAgendamento(String value) {
        this.lstrDataAgendamento = value;
    }

    /**
     * Obtém o valor da propriedade lstrDataFinal.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLstrDataFinal() {
        return lstrDataFinal;
    }

    /**
     * Define o valor da propriedade lstrDataFinal.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLstrDataFinal(String value) {
        this.lstrDataFinal = value;
    }

    /**
     * Obtém o valor da propriedade lstrResponsavel.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLstrResponsavel() {
        return lstrResponsavel;
    }

    /**
     * Define o valor da propriedade lstrResponsavel.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLstrResponsavel(String value) {
        this.lstrResponsavel = value;
    }

    /**
     * Obtém o valor da propriedade lstrUsuario.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLstrUsuario() {
        return lstrUsuario;
    }

    /**
     * Define o valor da propriedade lstrUsuario.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLstrUsuario(String value) {
        this.lstrUsuario = value;
    }

    /**
     * Obtém o valor da propriedade lstrSenha.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLstrSenha() {
        return lstrSenha;
    }

    /**
     * Define o valor da propriedade lstrSenha.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLstrSenha(String value) {
        this.lstrSenha = value;
    }

}
