
package org.tempuri;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java de Publicacoes complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="Publicacoes">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Pendentes" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="Processadas" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Publicacoes", propOrder = {
    "pendentes",
    "processadas"
})
public class Publicacoes {

    @XmlElement(name = "Pendentes")
    protected int pendentes;
    @XmlElement(name = "Processadas")
    protected int processadas;

    /**
     * Obtém o valor da propriedade pendentes.
     * 
     */
    public int getPendentes() {
        return pendentes;
    }

    /**
     * Define o valor da propriedade pendentes.
     * 
     */
    public void setPendentes(int value) {
        this.pendentes = value;
    }

    /**
     * Obtém o valor da propriedade processadas.
     * 
     */
    public int getProcessadas() {
        return processadas;
    }

    /**
     * Define o valor da propriedade processadas.
     * 
     */
    public void setProcessadas(int value) {
        this.processadas = value;
    }

}
