
package org.tempuri;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java de anonymous complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="RetornarAgendamentoPorTipoResult" type="{http://tempuri.org/}ArrayOfResultadoRetornaAgendamentoPorTipo" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "retornarAgendamentoPorTipoResult"
})
@XmlRootElement(name = "RetornarAgendamentoPorTipoResponse")
public class RetornarAgendamentoPorTipoResponse {

    @XmlElement(name = "RetornarAgendamentoPorTipoResult")
    protected ArrayOfResultadoRetornaAgendamentoPorTipo retornarAgendamentoPorTipoResult;

    /**
     * Obtém o valor da propriedade retornarAgendamentoPorTipoResult.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfResultadoRetornaAgendamentoPorTipo }
     *     
     */
    public ArrayOfResultadoRetornaAgendamentoPorTipo getRetornarAgendamentoPorTipoResult() {
        return retornarAgendamentoPorTipoResult;
    }

    /**
     * Define o valor da propriedade retornarAgendamentoPorTipoResult.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfResultadoRetornaAgendamentoPorTipo }
     *     
     */
    public void setRetornarAgendamentoPorTipoResult(ArrayOfResultadoRetornaAgendamentoPorTipo value) {
        this.retornarAgendamentoPorTipoResult = value;
    }

}
