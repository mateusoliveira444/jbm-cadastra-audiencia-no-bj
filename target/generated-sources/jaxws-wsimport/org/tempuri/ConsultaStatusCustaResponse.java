
package org.tempuri;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java de anonymous complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ConsultaStatusCustaResult" type="{http://tempuri.org/}ArrayOfStatusCusta" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "consultaStatusCustaResult"
})
@XmlRootElement(name = "ConsultaStatusCustaResponse")
public class ConsultaStatusCustaResponse {

    @XmlElement(name = "ConsultaStatusCustaResult")
    protected ArrayOfStatusCusta consultaStatusCustaResult;

    /**
     * Obtém o valor da propriedade consultaStatusCustaResult.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfStatusCusta }
     *     
     */
    public ArrayOfStatusCusta getConsultaStatusCustaResult() {
        return consultaStatusCustaResult;
    }

    /**
     * Define o valor da propriedade consultaStatusCustaResult.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfStatusCusta }
     *     
     */
    public void setConsultaStatusCustaResult(ArrayOfStatusCusta value) {
        this.consultaStatusCustaResult = value;
    }

}
