
package org.tempuri;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java de anonymous complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="lstrUsuario" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="lstrSenha" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="lstrIDProcesso" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="lstrIDProcessoUnico" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ldatDataCadastroIni" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ldatDataCadastroFim" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ldatDataEncerramentoIni" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ldatDataEncerramentoFim" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="lstrPasta" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="lstrControleCliente" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="lstrGrupoComplementar" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="lstrCampoComplementar" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="lstrDadoComplementar" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="lstrSistema" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="lstrNumeroProcesso" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="lstrDivisaoCliente" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "lstrUsuario",
    "lstrSenha",
    "lstrIDProcesso",
    "lstrIDProcessoUnico",
    "ldatDataCadastroIni",
    "ldatDataCadastroFim",
    "ldatDataEncerramentoIni",
    "ldatDataEncerramentoFim",
    "lstrPasta",
    "lstrControleCliente",
    "lstrGrupoComplementar",
    "lstrCampoComplementar",
    "lstrDadoComplementar",
    "lstrSistema",
    "lstrNumeroProcesso",
    "lstrDivisaoCliente"
})
@XmlRootElement(name = "ConsultaProcesso")
public class ConsultaProcesso {

    protected String lstrUsuario;
    protected String lstrSenha;
    protected String lstrIDProcesso;
    protected String lstrIDProcessoUnico;
    protected String ldatDataCadastroIni;
    protected String ldatDataCadastroFim;
    protected String ldatDataEncerramentoIni;
    protected String ldatDataEncerramentoFim;
    protected String lstrPasta;
    protected String lstrControleCliente;
    protected String lstrGrupoComplementar;
    protected String lstrCampoComplementar;
    protected String lstrDadoComplementar;
    protected String lstrSistema;
    protected String lstrNumeroProcesso;
    protected String lstrDivisaoCliente;

    /**
     * Obtém o valor da propriedade lstrUsuario.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLstrUsuario() {
        return lstrUsuario;
    }

    /**
     * Define o valor da propriedade lstrUsuario.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLstrUsuario(String value) {
        this.lstrUsuario = value;
    }

    /**
     * Obtém o valor da propriedade lstrSenha.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLstrSenha() {
        return lstrSenha;
    }

    /**
     * Define o valor da propriedade lstrSenha.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLstrSenha(String value) {
        this.lstrSenha = value;
    }

    /**
     * Obtém o valor da propriedade lstrIDProcesso.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLstrIDProcesso() {
        return lstrIDProcesso;
    }

    /**
     * Define o valor da propriedade lstrIDProcesso.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLstrIDProcesso(String value) {
        this.lstrIDProcesso = value;
    }

    /**
     * Obtém o valor da propriedade lstrIDProcessoUnico.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLstrIDProcessoUnico() {
        return lstrIDProcessoUnico;
    }

    /**
     * Define o valor da propriedade lstrIDProcessoUnico.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLstrIDProcessoUnico(String value) {
        this.lstrIDProcessoUnico = value;
    }

    /**
     * Obtém o valor da propriedade ldatDataCadastroIni.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLdatDataCadastroIni() {
        return ldatDataCadastroIni;
    }

    /**
     * Define o valor da propriedade ldatDataCadastroIni.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLdatDataCadastroIni(String value) {
        this.ldatDataCadastroIni = value;
    }

    /**
     * Obtém o valor da propriedade ldatDataCadastroFim.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLdatDataCadastroFim() {
        return ldatDataCadastroFim;
    }

    /**
     * Define o valor da propriedade ldatDataCadastroFim.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLdatDataCadastroFim(String value) {
        this.ldatDataCadastroFim = value;
    }

    /**
     * Obtém o valor da propriedade ldatDataEncerramentoIni.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLdatDataEncerramentoIni() {
        return ldatDataEncerramentoIni;
    }

    /**
     * Define o valor da propriedade ldatDataEncerramentoIni.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLdatDataEncerramentoIni(String value) {
        this.ldatDataEncerramentoIni = value;
    }

    /**
     * Obtém o valor da propriedade ldatDataEncerramentoFim.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLdatDataEncerramentoFim() {
        return ldatDataEncerramentoFim;
    }

    /**
     * Define o valor da propriedade ldatDataEncerramentoFim.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLdatDataEncerramentoFim(String value) {
        this.ldatDataEncerramentoFim = value;
    }

    /**
     * Obtém o valor da propriedade lstrPasta.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLstrPasta() {
        return lstrPasta;
    }

    /**
     * Define o valor da propriedade lstrPasta.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLstrPasta(String value) {
        this.lstrPasta = value;
    }

    /**
     * Obtém o valor da propriedade lstrControleCliente.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLstrControleCliente() {
        return lstrControleCliente;
    }

    /**
     * Define o valor da propriedade lstrControleCliente.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLstrControleCliente(String value) {
        this.lstrControleCliente = value;
    }

    /**
     * Obtém o valor da propriedade lstrGrupoComplementar.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLstrGrupoComplementar() {
        return lstrGrupoComplementar;
    }

    /**
     * Define o valor da propriedade lstrGrupoComplementar.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLstrGrupoComplementar(String value) {
        this.lstrGrupoComplementar = value;
    }

    /**
     * Obtém o valor da propriedade lstrCampoComplementar.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLstrCampoComplementar() {
        return lstrCampoComplementar;
    }

    /**
     * Define o valor da propriedade lstrCampoComplementar.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLstrCampoComplementar(String value) {
        this.lstrCampoComplementar = value;
    }

    /**
     * Obtém o valor da propriedade lstrDadoComplementar.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLstrDadoComplementar() {
        return lstrDadoComplementar;
    }

    /**
     * Define o valor da propriedade lstrDadoComplementar.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLstrDadoComplementar(String value) {
        this.lstrDadoComplementar = value;
    }

    /**
     * Obtém o valor da propriedade lstrSistema.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLstrSistema() {
        return lstrSistema;
    }

    /**
     * Define o valor da propriedade lstrSistema.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLstrSistema(String value) {
        this.lstrSistema = value;
    }

    /**
     * Obtém o valor da propriedade lstrNumeroProcesso.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLstrNumeroProcesso() {
        return lstrNumeroProcesso;
    }

    /**
     * Define o valor da propriedade lstrNumeroProcesso.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLstrNumeroProcesso(String value) {
        this.lstrNumeroProcesso = value;
    }

    /**
     * Obtém o valor da propriedade lstrDivisaoCliente.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLstrDivisaoCliente() {
        return lstrDivisaoCliente;
    }

    /**
     * Define o valor da propriedade lstrDivisaoCliente.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLstrDivisaoCliente(String value) {
        this.lstrDivisaoCliente = value;
    }

}
