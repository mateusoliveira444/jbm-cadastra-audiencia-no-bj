
package org.tempuri;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java de Materia complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="Materia">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="DescricaoMateria" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="IdMateria" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Materia", propOrder = {
    "descricaoMateria",
    "idMateria"
})
public class Materia {

    @XmlElement(name = "DescricaoMateria")
    protected String descricaoMateria;
    @XmlElement(name = "IdMateria")
    protected int idMateria;

    /**
     * Obtém o valor da propriedade descricaoMateria.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescricaoMateria() {
        return descricaoMateria;
    }

    /**
     * Define o valor da propriedade descricaoMateria.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescricaoMateria(String value) {
        this.descricaoMateria = value;
    }

    /**
     * Obtém o valor da propriedade idMateria.
     * 
     */
    public int getIdMateria() {
        return idMateria;
    }

    /**
     * Define o valor da propriedade idMateria.
     * 
     */
    public void setIdMateria(int value) {
        this.idMateria = value;
    }

}
