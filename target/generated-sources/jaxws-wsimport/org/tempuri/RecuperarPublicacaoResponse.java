
package org.tempuri;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java de anonymous complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="RecuperarPublicacaoResult" type="{http://tempuri.org/}ArrayOfNewDataSet" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "recuperarPublicacaoResult"
})
@XmlRootElement(name = "RecuperarPublicacaoResponse")
public class RecuperarPublicacaoResponse {

    @XmlElement(name = "RecuperarPublicacaoResult")
    protected ArrayOfNewDataSet recuperarPublicacaoResult;

    /**
     * Obtém o valor da propriedade recuperarPublicacaoResult.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfNewDataSet }
     *     
     */
    public ArrayOfNewDataSet getRecuperarPublicacaoResult() {
        return recuperarPublicacaoResult;
    }

    /**
     * Define o valor da propriedade recuperarPublicacaoResult.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfNewDataSet }
     *     
     */
    public void setRecuperarPublicacaoResult(ArrayOfNewDataSet value) {
        this.recuperarPublicacaoResult = value;
    }

}
