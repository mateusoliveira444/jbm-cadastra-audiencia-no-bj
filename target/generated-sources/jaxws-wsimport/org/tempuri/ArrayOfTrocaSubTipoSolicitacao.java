
package org.tempuri;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java de ArrayOfTrocaSubTipoSolicitacao complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="ArrayOfTrocaSubTipoSolicitacao">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="TrocaSubTipoSolicitacao" type="{http://tempuri.org/}TrocaSubTipoSolicitacao" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ArrayOfTrocaSubTipoSolicitacao", propOrder = {
    "trocaSubTipoSolicitacao"
})
public class ArrayOfTrocaSubTipoSolicitacao {

    @XmlElement(name = "TrocaSubTipoSolicitacao", nillable = true)
    protected List<TrocaSubTipoSolicitacao> trocaSubTipoSolicitacao;

    /**
     * Gets the value of the trocaSubTipoSolicitacao property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the trocaSubTipoSolicitacao property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getTrocaSubTipoSolicitacao().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link TrocaSubTipoSolicitacao }
     * 
     * 
     */
    public List<TrocaSubTipoSolicitacao> getTrocaSubTipoSolicitacao() {
        if (trocaSubTipoSolicitacao == null) {
            trocaSubTipoSolicitacao = new ArrayList<TrocaSubTipoSolicitacao>();
        }
        return this.trocaSubTipoSolicitacao;
    }

}
