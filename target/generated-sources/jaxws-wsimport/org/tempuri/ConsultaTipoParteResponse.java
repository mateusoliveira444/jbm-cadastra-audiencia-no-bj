
package org.tempuri;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java de anonymous complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ConsultaTipoParteResult" type="{http://tempuri.org/}ArrayOfTipoParte" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "consultaTipoParteResult"
})
@XmlRootElement(name = "ConsultaTipoParteResponse")
public class ConsultaTipoParteResponse {

    @XmlElement(name = "ConsultaTipoParteResult")
    protected ArrayOfTipoParte consultaTipoParteResult;

    /**
     * Obtém o valor da propriedade consultaTipoParteResult.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfTipoParte }
     *     
     */
    public ArrayOfTipoParte getConsultaTipoParteResult() {
        return consultaTipoParteResult;
    }

    /**
     * Define o valor da propriedade consultaTipoParteResult.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfTipoParte }
     *     
     */
    public void setConsultaTipoParteResult(ArrayOfTipoParte value) {
        this.consultaTipoParteResult = value;
    }

}
