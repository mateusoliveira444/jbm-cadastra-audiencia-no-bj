
package org.tempuri;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java de anonymous complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ConsultaOperacaoFinanceiraResult" type="{http://tempuri.org/}ArrayOfOperacaoFinanceira" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "consultaOperacaoFinanceiraResult"
})
@XmlRootElement(name = "ConsultaOperacaoFinanceiraResponse")
public class ConsultaOperacaoFinanceiraResponse {

    @XmlElement(name = "ConsultaOperacaoFinanceiraResult")
    protected ArrayOfOperacaoFinanceira consultaOperacaoFinanceiraResult;

    /**
     * Obtém o valor da propriedade consultaOperacaoFinanceiraResult.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfOperacaoFinanceira }
     *     
     */
    public ArrayOfOperacaoFinanceira getConsultaOperacaoFinanceiraResult() {
        return consultaOperacaoFinanceiraResult;
    }

    /**
     * Define o valor da propriedade consultaOperacaoFinanceiraResult.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfOperacaoFinanceira }
     *     
     */
    public void setConsultaOperacaoFinanceiraResult(ArrayOfOperacaoFinanceira value) {
        this.consultaOperacaoFinanceiraResult = value;
    }

}
