
package org.tempuri;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java de StatusCusta complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="StatusCusta">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Descricao" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="IdStatusCusta" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="OrdemExibicao" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "StatusCusta", propOrder = {
    "descricao",
    "idStatusCusta",
    "ordemExibicao"
})
public class StatusCusta {

    @XmlElement(name = "Descricao")
    protected String descricao;
    @XmlElement(name = "IdStatusCusta")
    protected int idStatusCusta;
    @XmlElement(name = "OrdemExibicao")
    protected int ordemExibicao;

    /**
     * Obtém o valor da propriedade descricao.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescricao() {
        return descricao;
    }

    /**
     * Define o valor da propriedade descricao.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescricao(String value) {
        this.descricao = value;
    }

    /**
     * Obtém o valor da propriedade idStatusCusta.
     * 
     */
    public int getIdStatusCusta() {
        return idStatusCusta;
    }

    /**
     * Define o valor da propriedade idStatusCusta.
     * 
     */
    public void setIdStatusCusta(int value) {
        this.idStatusCusta = value;
    }

    /**
     * Obtém o valor da propriedade ordemExibicao.
     * 
     */
    public int getOrdemExibicao() {
        return ordemExibicao;
    }

    /**
     * Define o valor da propriedade ordemExibicao.
     * 
     */
    public void setOrdemExibicao(int value) {
        this.ordemExibicao = value;
    }

}
