
package org.tempuri;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java de ArrayOfTipoAgendamentoMov complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="ArrayOfTipoAgendamentoMov">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Agendamentos" type="{http://tempuri.org/}TipoAgendamentoMov" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ArrayOfTipoAgendamentoMov", propOrder = {
    "agendamentos"
})
public class ArrayOfTipoAgendamentoMov {

    @XmlElement(name = "Agendamentos", nillable = true)
    protected List<TipoAgendamentoMov> agendamentos;

    /**
     * Gets the value of the agendamentos property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the agendamentos property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAgendamentos().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link TipoAgendamentoMov }
     * 
     * 
     */
    public List<TipoAgendamentoMov> getAgendamentos() {
        if (agendamentos == null) {
            agendamentos = new ArrayList<TipoAgendamentoMov>();
        }
        return this.agendamentos;
    }

}
