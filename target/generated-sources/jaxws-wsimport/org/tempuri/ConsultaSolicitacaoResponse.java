
package org.tempuri;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java de anonymous complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ConsultaSolicitacaoResult" type="{http://tempuri.org/}SolicitacaoRetorno" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "consultaSolicitacaoResult"
})
@XmlRootElement(name = "ConsultaSolicitacaoResponse")
public class ConsultaSolicitacaoResponse {

    @XmlElement(name = "ConsultaSolicitacaoResult")
    protected SolicitacaoRetorno consultaSolicitacaoResult;

    /**
     * Obtém o valor da propriedade consultaSolicitacaoResult.
     * 
     * @return
     *     possible object is
     *     {@link SolicitacaoRetorno }
     *     
     */
    public SolicitacaoRetorno getConsultaSolicitacaoResult() {
        return consultaSolicitacaoResult;
    }

    /**
     * Define o valor da propriedade consultaSolicitacaoResult.
     * 
     * @param value
     *     allowed object is
     *     {@link SolicitacaoRetorno }
     *     
     */
    public void setConsultaSolicitacaoResult(SolicitacaoRetorno value) {
        this.consultaSolicitacaoResult = value;
    }

}
