
package org.tempuri;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Classe Java de RetornoFollowUP complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="RetornoFollowUP">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="IDFollowUP" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="DataFollowUP" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="Usuario" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="TipoFollowUP" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="TextoFollowUP" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Origem" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "RetornoFollowUP", propOrder = {
    "idFollowUP",
    "dataFollowUP",
    "usuario",
    "tipoFollowUP",
    "textoFollowUP",
    "origem"
})
public class RetornoFollowUP {

    @XmlElement(name = "IDFollowUP")
    protected int idFollowUP;
    @XmlElement(name = "DataFollowUP", required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar dataFollowUP;
    @XmlElement(name = "Usuario")
    protected String usuario;
    @XmlElement(name = "TipoFollowUP")
    protected String tipoFollowUP;
    @XmlElement(name = "TextoFollowUP")
    protected String textoFollowUP;
    @XmlElement(name = "Origem")
    protected String origem;

    /**
     * Obtém o valor da propriedade idFollowUP.
     * 
     */
    public int getIDFollowUP() {
        return idFollowUP;
    }

    /**
     * Define o valor da propriedade idFollowUP.
     * 
     */
    public void setIDFollowUP(int value) {
        this.idFollowUP = value;
    }

    /**
     * Obtém o valor da propriedade dataFollowUP.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDataFollowUP() {
        return dataFollowUP;
    }

    /**
     * Define o valor da propriedade dataFollowUP.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDataFollowUP(XMLGregorianCalendar value) {
        this.dataFollowUP = value;
    }

    /**
     * Obtém o valor da propriedade usuario.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUsuario() {
        return usuario;
    }

    /**
     * Define o valor da propriedade usuario.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUsuario(String value) {
        this.usuario = value;
    }

    /**
     * Obtém o valor da propriedade tipoFollowUP.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTipoFollowUP() {
        return tipoFollowUP;
    }

    /**
     * Define o valor da propriedade tipoFollowUP.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTipoFollowUP(String value) {
        this.tipoFollowUP = value;
    }

    /**
     * Obtém o valor da propriedade textoFollowUP.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTextoFollowUP() {
        return textoFollowUP;
    }

    /**
     * Define o valor da propriedade textoFollowUP.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTextoFollowUP(String value) {
        this.textoFollowUP = value;
    }

    /**
     * Obtém o valor da propriedade origem.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOrigem() {
        return origem;
    }

    /**
     * Define o valor da propriedade origem.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOrigem(String value) {
        this.origem = value;
    }

}
