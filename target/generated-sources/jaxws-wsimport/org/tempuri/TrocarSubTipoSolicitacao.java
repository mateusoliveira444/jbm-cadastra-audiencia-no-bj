
package org.tempuri;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java de anonymous complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="lintIDSolicitacao" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="lintIDRespostaAlteracao" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="lstrObservacao" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="lstrUsuario" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="lstrSenha" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "lintIDSolicitacao",
    "lintIDRespostaAlteracao",
    "lstrObservacao",
    "lstrUsuario",
    "lstrSenha"
})
@XmlRootElement(name = "TrocarSubTipoSolicitacao")
public class TrocarSubTipoSolicitacao {

    protected int lintIDSolicitacao;
    protected int lintIDRespostaAlteracao;
    protected String lstrObservacao;
    protected String lstrUsuario;
    protected String lstrSenha;

    /**
     * Obtém o valor da propriedade lintIDSolicitacao.
     * 
     */
    public int getLintIDSolicitacao() {
        return lintIDSolicitacao;
    }

    /**
     * Define o valor da propriedade lintIDSolicitacao.
     * 
     */
    public void setLintIDSolicitacao(int value) {
        this.lintIDSolicitacao = value;
    }

    /**
     * Obtém o valor da propriedade lintIDRespostaAlteracao.
     * 
     */
    public int getLintIDRespostaAlteracao() {
        return lintIDRespostaAlteracao;
    }

    /**
     * Define o valor da propriedade lintIDRespostaAlteracao.
     * 
     */
    public void setLintIDRespostaAlteracao(int value) {
        this.lintIDRespostaAlteracao = value;
    }

    /**
     * Obtém o valor da propriedade lstrObservacao.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLstrObservacao() {
        return lstrObservacao;
    }

    /**
     * Define o valor da propriedade lstrObservacao.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLstrObservacao(String value) {
        this.lstrObservacao = value;
    }

    /**
     * Obtém o valor da propriedade lstrUsuario.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLstrUsuario() {
        return lstrUsuario;
    }

    /**
     * Define o valor da propriedade lstrUsuario.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLstrUsuario(String value) {
        this.lstrUsuario = value;
    }

    /**
     * Obtém o valor da propriedade lstrSenha.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLstrSenha() {
        return lstrSenha;
    }

    /**
     * Define o valor da propriedade lstrSenha.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLstrSenha(String value) {
        this.lstrSenha = value;
    }

}
