
package org.tempuri;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java de anonymous complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ConsultaParteProcessoResult" type="{http://tempuri.org/}ArrayOfParteProcesso" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "consultaParteProcessoResult"
})
@XmlRootElement(name = "ConsultaParteProcessoResponse")
public class ConsultaParteProcessoResponse {

    @XmlElement(name = "ConsultaParteProcessoResult")
    protected ArrayOfParteProcesso consultaParteProcessoResult;

    /**
     * Obtém o valor da propriedade consultaParteProcessoResult.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfParteProcesso }
     *     
     */
    public ArrayOfParteProcesso getConsultaParteProcessoResult() {
        return consultaParteProcessoResult;
    }

    /**
     * Define o valor da propriedade consultaParteProcessoResult.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfParteProcesso }
     *     
     */
    public void setConsultaParteProcessoResult(ArrayOfParteProcesso value) {
        this.consultaParteProcessoResult = value;
    }

}
