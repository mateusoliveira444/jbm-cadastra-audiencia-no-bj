
package org.tempuri;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java de Justica complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="Justica">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="IDJustica" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="JusticaDesc" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Justica", propOrder = {
    "idJustica",
    "justicaDesc"
})
public class Justica {

    @XmlElement(name = "IDJustica")
    protected int idJustica;
    @XmlElement(name = "JusticaDesc")
    protected String justicaDesc;

    /**
     * Obtém o valor da propriedade idJustica.
     * 
     */
    public int getIDJustica() {
        return idJustica;
    }

    /**
     * Define o valor da propriedade idJustica.
     * 
     */
    public void setIDJustica(int value) {
        this.idJustica = value;
    }

    /**
     * Obtém o valor da propriedade justicaDesc.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getJusticaDesc() {
        return justicaDesc;
    }

    /**
     * Define o valor da propriedade justicaDesc.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setJusticaDesc(String value) {
        this.justicaDesc = value;
    }

}
