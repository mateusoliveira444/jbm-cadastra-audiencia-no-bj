
package org.tempuri;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java de anonymous complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="RecuperarForoResult" type="{http://tempuri.org/}ArrayOfForo" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "recuperarForoResult"
})
@XmlRootElement(name = "RecuperarForoResponse")
public class RecuperarForoResponse {

    @XmlElement(name = "RecuperarForoResult")
    protected ArrayOfForo recuperarForoResult;

    /**
     * Obtém o valor da propriedade recuperarForoResult.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfForo }
     *     
     */
    public ArrayOfForo getRecuperarForoResult() {
        return recuperarForoResult;
    }

    /**
     * Define o valor da propriedade recuperarForoResult.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfForo }
     *     
     */
    public void setRecuperarForoResult(ArrayOfForo value) {
        this.recuperarForoResult = value;
    }

}
