
package org.tempuri;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java de anonymous complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="RetornaAgendamentoProcessoResult" type="{http://tempuri.org/}ArrayOfRetornoAgendamento" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "retornaAgendamentoProcessoResult"
})
@XmlRootElement(name = "RetornaAgendamentoProcessoResponse")
public class RetornaAgendamentoProcessoResponse {

    @XmlElement(name = "RetornaAgendamentoProcessoResult")
    protected ArrayOfRetornoAgendamento retornaAgendamentoProcessoResult;

    /**
     * Obtém o valor da propriedade retornaAgendamentoProcessoResult.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfRetornoAgendamento }
     *     
     */
    public ArrayOfRetornoAgendamento getRetornaAgendamentoProcessoResult() {
        return retornaAgendamentoProcessoResult;
    }

    /**
     * Define o valor da propriedade retornaAgendamentoProcessoResult.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfRetornoAgendamento }
     *     
     */
    public void setRetornaAgendamentoProcessoResult(ArrayOfRetornoAgendamento value) {
        this.retornaAgendamentoProcessoResult = value;
    }

}
