
package org.tempuri;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java de Foro complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="Foro">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="IDForo" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="ForoDesc" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="IDComarca" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="ComarcaDesc" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="UF" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Foro", propOrder = {
    "idForo",
    "foroDesc",
    "idComarca",
    "comarcaDesc",
    "uf"
})
public class Foro {

    @XmlElement(name = "IDForo")
    protected int idForo;
    @XmlElement(name = "ForoDesc")
    protected String foroDesc;
    @XmlElement(name = "IDComarca")
    protected int idComarca;
    @XmlElement(name = "ComarcaDesc")
    protected String comarcaDesc;
    @XmlElement(name = "UF")
    protected String uf;

    /**
     * Obtém o valor da propriedade idForo.
     * 
     */
    public int getIDForo() {
        return idForo;
    }

    /**
     * Define o valor da propriedade idForo.
     * 
     */
    public void setIDForo(int value) {
        this.idForo = value;
    }

    /**
     * Obtém o valor da propriedade foroDesc.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getForoDesc() {
        return foroDesc;
    }

    /**
     * Define o valor da propriedade foroDesc.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setForoDesc(String value) {
        this.foroDesc = value;
    }

    /**
     * Obtém o valor da propriedade idComarca.
     * 
     */
    public int getIDComarca() {
        return idComarca;
    }

    /**
     * Define o valor da propriedade idComarca.
     * 
     */
    public void setIDComarca(int value) {
        this.idComarca = value;
    }

    /**
     * Obtém o valor da propriedade comarcaDesc.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getComarcaDesc() {
        return comarcaDesc;
    }

    /**
     * Define o valor da propriedade comarcaDesc.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setComarcaDesc(String value) {
        this.comarcaDesc = value;
    }

    /**
     * Obtém o valor da propriedade uf.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUF() {
        return uf;
    }

    /**
     * Define o valor da propriedade uf.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUF(String value) {
        this.uf = value;
    }

}
