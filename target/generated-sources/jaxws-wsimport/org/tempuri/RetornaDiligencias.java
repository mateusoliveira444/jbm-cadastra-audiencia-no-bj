
package org.tempuri;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java de RetornaDiligencias complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="RetornaDiligencias">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="mintIdSolicitacao" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="mstrCorrespondente" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="mstrTipoDiligencia" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="mstrStatusDiligencia" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="mstrDtLimite" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="mstrDtSolicitacao" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="mstrObservacao" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="mstrSubTipoDiligencia" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="mstrSituacao" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="mstrDetalheSubTipo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="mstrProcessoUnico" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="mstrNumProcesso" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="mstrNumDesdobramento" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="mstrParteContraria" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="mstrParteInteressada" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="mstrControleCliente" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="mstrCelula" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="mstrDtConclusao" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="mstrInstancia" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="marrArquivos" type="{http://tempuri.org/}ArrayOfArquivo" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "RetornaDiligencias", propOrder = {
    "mintIdSolicitacao",
    "mstrCorrespondente",
    "mstrTipoDiligencia",
    "mstrStatusDiligencia",
    "mstrDtLimite",
    "mstrDtSolicitacao",
    "mstrObservacao",
    "mstrSubTipoDiligencia",
    "mstrSituacao",
    "mstrDetalheSubTipo",
    "mstrProcessoUnico",
    "mstrNumProcesso",
    "mstrNumDesdobramento",
    "mstrParteContraria",
    "mstrParteInteressada",
    "mstrControleCliente",
    "mstrCelula",
    "mstrDtConclusao",
    "mstrInstancia",
    "marrArquivos"
})
public class RetornaDiligencias {

    protected int mintIdSolicitacao;
    protected String mstrCorrespondente;
    protected String mstrTipoDiligencia;
    protected String mstrStatusDiligencia;
    protected String mstrDtLimite;
    protected String mstrDtSolicitacao;
    protected String mstrObservacao;
    protected String mstrSubTipoDiligencia;
    protected String mstrSituacao;
    protected String mstrDetalheSubTipo;
    protected String mstrProcessoUnico;
    protected String mstrNumProcesso;
    protected String mstrNumDesdobramento;
    protected String mstrParteContraria;
    protected String mstrParteInteressada;
    protected String mstrControleCliente;
    protected String mstrCelula;
    protected String mstrDtConclusao;
    protected String mstrInstancia;
    protected ArrayOfArquivo marrArquivos;

    /**
     * Obtém o valor da propriedade mintIdSolicitacao.
     * 
     */
    public int getMintIdSolicitacao() {
        return mintIdSolicitacao;
    }

    /**
     * Define o valor da propriedade mintIdSolicitacao.
     * 
     */
    public void setMintIdSolicitacao(int value) {
        this.mintIdSolicitacao = value;
    }

    /**
     * Obtém o valor da propriedade mstrCorrespondente.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMstrCorrespondente() {
        return mstrCorrespondente;
    }

    /**
     * Define o valor da propriedade mstrCorrespondente.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMstrCorrespondente(String value) {
        this.mstrCorrespondente = value;
    }

    /**
     * Obtém o valor da propriedade mstrTipoDiligencia.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMstrTipoDiligencia() {
        return mstrTipoDiligencia;
    }

    /**
     * Define o valor da propriedade mstrTipoDiligencia.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMstrTipoDiligencia(String value) {
        this.mstrTipoDiligencia = value;
    }

    /**
     * Obtém o valor da propriedade mstrStatusDiligencia.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMstrStatusDiligencia() {
        return mstrStatusDiligencia;
    }

    /**
     * Define o valor da propriedade mstrStatusDiligencia.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMstrStatusDiligencia(String value) {
        this.mstrStatusDiligencia = value;
    }

    /**
     * Obtém o valor da propriedade mstrDtLimite.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMstrDtLimite() {
        return mstrDtLimite;
    }

    /**
     * Define o valor da propriedade mstrDtLimite.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMstrDtLimite(String value) {
        this.mstrDtLimite = value;
    }

    /**
     * Obtém o valor da propriedade mstrDtSolicitacao.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMstrDtSolicitacao() {
        return mstrDtSolicitacao;
    }

    /**
     * Define o valor da propriedade mstrDtSolicitacao.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMstrDtSolicitacao(String value) {
        this.mstrDtSolicitacao = value;
    }

    /**
     * Obtém o valor da propriedade mstrObservacao.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMstrObservacao() {
        return mstrObservacao;
    }

    /**
     * Define o valor da propriedade mstrObservacao.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMstrObservacao(String value) {
        this.mstrObservacao = value;
    }

    /**
     * Obtém o valor da propriedade mstrSubTipoDiligencia.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMstrSubTipoDiligencia() {
        return mstrSubTipoDiligencia;
    }

    /**
     * Define o valor da propriedade mstrSubTipoDiligencia.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMstrSubTipoDiligencia(String value) {
        this.mstrSubTipoDiligencia = value;
    }

    /**
     * Obtém o valor da propriedade mstrSituacao.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMstrSituacao() {
        return mstrSituacao;
    }

    /**
     * Define o valor da propriedade mstrSituacao.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMstrSituacao(String value) {
        this.mstrSituacao = value;
    }

    /**
     * Obtém o valor da propriedade mstrDetalheSubTipo.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMstrDetalheSubTipo() {
        return mstrDetalheSubTipo;
    }

    /**
     * Define o valor da propriedade mstrDetalheSubTipo.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMstrDetalheSubTipo(String value) {
        this.mstrDetalheSubTipo = value;
    }

    /**
     * Obtém o valor da propriedade mstrProcessoUnico.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMstrProcessoUnico() {
        return mstrProcessoUnico;
    }

    /**
     * Define o valor da propriedade mstrProcessoUnico.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMstrProcessoUnico(String value) {
        this.mstrProcessoUnico = value;
    }

    /**
     * Obtém o valor da propriedade mstrNumProcesso.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMstrNumProcesso() {
        return mstrNumProcesso;
    }

    /**
     * Define o valor da propriedade mstrNumProcesso.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMstrNumProcesso(String value) {
        this.mstrNumProcesso = value;
    }

    /**
     * Obtém o valor da propriedade mstrNumDesdobramento.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMstrNumDesdobramento() {
        return mstrNumDesdobramento;
    }

    /**
     * Define o valor da propriedade mstrNumDesdobramento.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMstrNumDesdobramento(String value) {
        this.mstrNumDesdobramento = value;
    }

    /**
     * Obtém o valor da propriedade mstrParteContraria.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMstrParteContraria() {
        return mstrParteContraria;
    }

    /**
     * Define o valor da propriedade mstrParteContraria.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMstrParteContraria(String value) {
        this.mstrParteContraria = value;
    }

    /**
     * Obtém o valor da propriedade mstrParteInteressada.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMstrParteInteressada() {
        return mstrParteInteressada;
    }

    /**
     * Define o valor da propriedade mstrParteInteressada.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMstrParteInteressada(String value) {
        this.mstrParteInteressada = value;
    }

    /**
     * Obtém o valor da propriedade mstrControleCliente.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMstrControleCliente() {
        return mstrControleCliente;
    }

    /**
     * Define o valor da propriedade mstrControleCliente.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMstrControleCliente(String value) {
        this.mstrControleCliente = value;
    }

    /**
     * Obtém o valor da propriedade mstrCelula.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMstrCelula() {
        return mstrCelula;
    }

    /**
     * Define o valor da propriedade mstrCelula.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMstrCelula(String value) {
        this.mstrCelula = value;
    }

    /**
     * Obtém o valor da propriedade mstrDtConclusao.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMstrDtConclusao() {
        return mstrDtConclusao;
    }

    /**
     * Define o valor da propriedade mstrDtConclusao.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMstrDtConclusao(String value) {
        this.mstrDtConclusao = value;
    }

    /**
     * Obtém o valor da propriedade mstrInstancia.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMstrInstancia() {
        return mstrInstancia;
    }

    /**
     * Define o valor da propriedade mstrInstancia.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMstrInstancia(String value) {
        this.mstrInstancia = value;
    }

    /**
     * Obtém o valor da propriedade marrArquivos.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfArquivo }
     *     
     */
    public ArrayOfArquivo getMarrArquivos() {
        return marrArquivos;
    }

    /**
     * Define o valor da propriedade marrArquivos.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfArquivo }
     *     
     */
    public void setMarrArquivos(ArrayOfArquivo value) {
        this.marrArquivos = value;
    }

}
