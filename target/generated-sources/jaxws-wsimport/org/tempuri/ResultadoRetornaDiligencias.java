
package org.tempuri;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java de ResultadoRetornaDiligencias complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="ResultadoRetornaDiligencias">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Retorno" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="RetornaDiligencias" type="{http://tempuri.org/}RetornaDiligencias" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ResultadoRetornaDiligencias", propOrder = {
    "retorno",
    "retornaDiligencias"
})
public class ResultadoRetornaDiligencias {

    @XmlElement(name = "Retorno")
    protected String retorno;
    @XmlElement(name = "RetornaDiligencias")
    protected List<RetornaDiligencias> retornaDiligencias;

    /**
     * Obtém o valor da propriedade retorno.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRetorno() {
        return retorno;
    }

    /**
     * Define o valor da propriedade retorno.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRetorno(String value) {
        this.retorno = value;
    }

    /**
     * Gets the value of the retornaDiligencias property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the retornaDiligencias property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRetornaDiligencias().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link RetornaDiligencias }
     * 
     * 
     */
    public List<RetornaDiligencias> getRetornaDiligencias() {
        if (retornaDiligencias == null) {
            retornaDiligencias = new ArrayList<RetornaDiligencias>();
        }
        return this.retornaDiligencias;
    }

}
