
package org.tempuri;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java de anonymous complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ResultadoTamanhoArquivo" type="{http://tempuri.org/}ResultadoTamanhoArquivo"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "resultadoTamanhoArquivo"
})
@XmlRootElement(name = "ConsultaTamanhoArquivoResponse")
public class ConsultaTamanhoArquivoResponse {

    @XmlElement(name = "ResultadoTamanhoArquivo", required = true, nillable = true)
    protected ResultadoTamanhoArquivo resultadoTamanhoArquivo;

    /**
     * Obtém o valor da propriedade resultadoTamanhoArquivo.
     * 
     * @return
     *     possible object is
     *     {@link ResultadoTamanhoArquivo }
     *     
     */
    public ResultadoTamanhoArquivo getResultadoTamanhoArquivo() {
        return resultadoTamanhoArquivo;
    }

    /**
     * Define o valor da propriedade resultadoTamanhoArquivo.
     * 
     * @param value
     *     allowed object is
     *     {@link ResultadoTamanhoArquivo }
     *     
     */
    public void setResultadoTamanhoArquivo(ResultadoTamanhoArquivo value) {
        this.resultadoTamanhoArquivo = value;
    }

}
