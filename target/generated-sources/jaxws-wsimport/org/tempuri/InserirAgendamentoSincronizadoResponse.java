
package org.tempuri;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java de anonymous complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="InserirAgendamentoSincronizadoResult" type="{http://tempuri.org/}RetornoMetodos" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "inserirAgendamentoSincronizadoResult"
})
@XmlRootElement(name = "InserirAgendamentoSincronizadoResponse")
public class InserirAgendamentoSincronizadoResponse {

    @XmlElement(name = "InserirAgendamentoSincronizadoResult")
    protected RetornoMetodos inserirAgendamentoSincronizadoResult;

    /**
     * Obtém o valor da propriedade inserirAgendamentoSincronizadoResult.
     * 
     * @return
     *     possible object is
     *     {@link RetornoMetodos }
     *     
     */
    public RetornoMetodos getInserirAgendamentoSincronizadoResult() {
        return inserirAgendamentoSincronizadoResult;
    }

    /**
     * Define o valor da propriedade inserirAgendamentoSincronizadoResult.
     * 
     * @param value
     *     allowed object is
     *     {@link RetornoMetodos }
     *     
     */
    public void setInserirAgendamentoSincronizadoResult(RetornoMetodos value) {
        this.inserirAgendamentoSincronizadoResult = value;
    }

}
