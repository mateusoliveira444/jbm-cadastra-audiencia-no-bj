
package org.tempuri;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java de anonymous complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ValidarAlteracaoSubTipoSolicitacaoResult" type="{http://tempuri.org/}ArrayOfTrocaSubTipoSolicitacao" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "validarAlteracaoSubTipoSolicitacaoResult"
})
@XmlRootElement(name = "ValidarAlteracaoSubTipoSolicitacaoResponse")
public class ValidarAlteracaoSubTipoSolicitacaoResponse {

    @XmlElement(name = "ValidarAlteracaoSubTipoSolicitacaoResult")
    protected ArrayOfTrocaSubTipoSolicitacao validarAlteracaoSubTipoSolicitacaoResult;

    /**
     * Obtém o valor da propriedade validarAlteracaoSubTipoSolicitacaoResult.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfTrocaSubTipoSolicitacao }
     *     
     */
    public ArrayOfTrocaSubTipoSolicitacao getValidarAlteracaoSubTipoSolicitacaoResult() {
        return validarAlteracaoSubTipoSolicitacaoResult;
    }

    /**
     * Define o valor da propriedade validarAlteracaoSubTipoSolicitacaoResult.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfTrocaSubTipoSolicitacao }
     *     
     */
    public void setValidarAlteracaoSubTipoSolicitacaoResult(ArrayOfTrocaSubTipoSolicitacao value) {
        this.validarAlteracaoSubTipoSolicitacaoResult = value;
    }

}
