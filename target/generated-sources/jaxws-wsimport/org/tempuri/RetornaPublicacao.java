
package org.tempuri;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java de anonymous complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="lstrUsuario" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="lstrSenha" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="lstrDataColagemIni" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="lstrDataColagemFim" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="lstrProcessoUnico" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="lstrSituacao" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="lstrPegou" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="lstrDataInicioPrazo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "lstrUsuario",
    "lstrSenha",
    "lstrDataColagemIni",
    "lstrDataColagemFim",
    "lstrProcessoUnico",
    "lstrSituacao",
    "lstrPegou",
    "lstrDataInicioPrazo"
})
@XmlRootElement(name = "RetornaPublicacao")
public class RetornaPublicacao {

    protected String lstrUsuario;
    protected String lstrSenha;
    protected String lstrDataColagemIni;
    protected String lstrDataColagemFim;
    protected String lstrProcessoUnico;
    protected String lstrSituacao;
    protected String lstrPegou;
    protected String lstrDataInicioPrazo;

    /**
     * Obtém o valor da propriedade lstrUsuario.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLstrUsuario() {
        return lstrUsuario;
    }

    /**
     * Define o valor da propriedade lstrUsuario.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLstrUsuario(String value) {
        this.lstrUsuario = value;
    }

    /**
     * Obtém o valor da propriedade lstrSenha.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLstrSenha() {
        return lstrSenha;
    }

    /**
     * Define o valor da propriedade lstrSenha.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLstrSenha(String value) {
        this.lstrSenha = value;
    }

    /**
     * Obtém o valor da propriedade lstrDataColagemIni.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLstrDataColagemIni() {
        return lstrDataColagemIni;
    }

    /**
     * Define o valor da propriedade lstrDataColagemIni.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLstrDataColagemIni(String value) {
        this.lstrDataColagemIni = value;
    }

    /**
     * Obtém o valor da propriedade lstrDataColagemFim.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLstrDataColagemFim() {
        return lstrDataColagemFim;
    }

    /**
     * Define o valor da propriedade lstrDataColagemFim.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLstrDataColagemFim(String value) {
        this.lstrDataColagemFim = value;
    }

    /**
     * Obtém o valor da propriedade lstrProcessoUnico.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLstrProcessoUnico() {
        return lstrProcessoUnico;
    }

    /**
     * Define o valor da propriedade lstrProcessoUnico.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLstrProcessoUnico(String value) {
        this.lstrProcessoUnico = value;
    }

    /**
     * Obtém o valor da propriedade lstrSituacao.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLstrSituacao() {
        return lstrSituacao;
    }

    /**
     * Define o valor da propriedade lstrSituacao.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLstrSituacao(String value) {
        this.lstrSituacao = value;
    }

    /**
     * Obtém o valor da propriedade lstrPegou.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLstrPegou() {
        return lstrPegou;
    }

    /**
     * Define o valor da propriedade lstrPegou.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLstrPegou(String value) {
        this.lstrPegou = value;
    }

    /**
     * Obtém o valor da propriedade lstrDataInicioPrazo.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLstrDataInicioPrazo() {
        return lstrDataInicioPrazo;
    }

    /**
     * Define o valor da propriedade lstrDataInicioPrazo.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLstrDataInicioPrazo(String value) {
        this.lstrDataInicioPrazo = value;
    }

}
