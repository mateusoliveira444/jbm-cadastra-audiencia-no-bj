
package org.tempuri;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java de anonymous complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="RecuperarTipoVaraResult" type="{http://tempuri.org/}ArrayOfTipoVara" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "recuperarTipoVaraResult"
})
@XmlRootElement(name = "RecuperarTipoVaraResponse")
public class RecuperarTipoVaraResponse {

    @XmlElement(name = "RecuperarTipoVaraResult")
    protected ArrayOfTipoVara recuperarTipoVaraResult;

    /**
     * Obtém o valor da propriedade recuperarTipoVaraResult.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfTipoVara }
     *     
     */
    public ArrayOfTipoVara getRecuperarTipoVaraResult() {
        return recuperarTipoVaraResult;
    }

    /**
     * Define o valor da propriedade recuperarTipoVaraResult.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfTipoVara }
     *     
     */
    public void setRecuperarTipoVaraResult(ArrayOfTipoVara value) {
        this.recuperarTipoVaraResult = value;
    }

}
