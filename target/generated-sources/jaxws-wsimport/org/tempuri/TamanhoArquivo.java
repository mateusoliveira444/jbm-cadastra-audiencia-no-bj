
package org.tempuri;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java de TamanhoArquivo complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="TamanhoArquivo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="IDGED_PROFILETIPO" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="DescricaoProfileTipo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Extensao" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Tamanho" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TamanhoArquivo", propOrder = {
    "idgedprofiletipo",
    "descricaoProfileTipo",
    "extensao",
    "tamanho"
})
public class TamanhoArquivo {

    @XmlElement(name = "IDGED_PROFILETIPO")
    protected int idgedprofiletipo;
    @XmlElement(name = "DescricaoProfileTipo")
    protected String descricaoProfileTipo;
    @XmlElement(name = "Extensao")
    protected String extensao;
    @XmlElement(name = "Tamanho")
    protected int tamanho;

    /**
     * Obtém o valor da propriedade idgedprofiletipo.
     * 
     */
    public int getIDGEDPROFILETIPO() {
        return idgedprofiletipo;
    }

    /**
     * Define o valor da propriedade idgedprofiletipo.
     * 
     */
    public void setIDGEDPROFILETIPO(int value) {
        this.idgedprofiletipo = value;
    }

    /**
     * Obtém o valor da propriedade descricaoProfileTipo.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescricaoProfileTipo() {
        return descricaoProfileTipo;
    }

    /**
     * Define o valor da propriedade descricaoProfileTipo.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescricaoProfileTipo(String value) {
        this.descricaoProfileTipo = value;
    }

    /**
     * Obtém o valor da propriedade extensao.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getExtensao() {
        return extensao;
    }

    /**
     * Define o valor da propriedade extensao.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setExtensao(String value) {
        this.extensao = value;
    }

    /**
     * Obtém o valor da propriedade tamanho.
     * 
     */
    public int getTamanho() {
        return tamanho;
    }

    /**
     * Define o valor da propriedade tamanho.
     * 
     */
    public void setTamanho(int value) {
        this.tamanho = value;
    }

}
