
package org.tempuri;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java de anonymous complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="RetornaCorrespondenteResult" type="{http://tempuri.org/}ArrayOfRetornoCorrespondente" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "retornaCorrespondenteResult"
})
@XmlRootElement(name = "RetornaCorrespondenteResponse")
public class RetornaCorrespondenteResponse {

    @XmlElement(name = "RetornaCorrespondenteResult")
    protected ArrayOfRetornoCorrespondente retornaCorrespondenteResult;

    /**
     * Obtém o valor da propriedade retornaCorrespondenteResult.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfRetornoCorrespondente }
     *     
     */
    public ArrayOfRetornoCorrespondente getRetornaCorrespondenteResult() {
        return retornaCorrespondenteResult;
    }

    /**
     * Define o valor da propriedade retornaCorrespondenteResult.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfRetornoCorrespondente }
     *     
     */
    public void setRetornaCorrespondenteResult(ArrayOfRetornoCorrespondente value) {
        this.retornaCorrespondenteResult = value;
    }

}
