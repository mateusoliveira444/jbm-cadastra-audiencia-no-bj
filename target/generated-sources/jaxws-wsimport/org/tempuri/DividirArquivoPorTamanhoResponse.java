
package org.tempuri;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java de anonymous complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="DividirArquivoPorTamanhoResult" type="{http://tempuri.org/}RetornoMetodos" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "dividirArquivoPorTamanhoResult"
})
@XmlRootElement(name = "DividirArquivoPorTamanhoResponse")
public class DividirArquivoPorTamanhoResponse {

    @XmlElement(name = "DividirArquivoPorTamanhoResult")
    protected RetornoMetodos dividirArquivoPorTamanhoResult;

    /**
     * Obtém o valor da propriedade dividirArquivoPorTamanhoResult.
     * 
     * @return
     *     possible object is
     *     {@link RetornoMetodos }
     *     
     */
    public RetornoMetodos getDividirArquivoPorTamanhoResult() {
        return dividirArquivoPorTamanhoResult;
    }

    /**
     * Define o valor da propriedade dividirArquivoPorTamanhoResult.
     * 
     * @param value
     *     allowed object is
     *     {@link RetornoMetodos }
     *     
     */
    public void setDividirArquivoPorTamanhoResult(RetornoMetodos value) {
        this.dividirArquivoPorTamanhoResult = value;
    }

}
