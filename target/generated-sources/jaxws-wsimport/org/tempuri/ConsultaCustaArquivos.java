
package org.tempuri;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java de anonymous complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="lstrUsuario" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="lstrSenha" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="lstrStatusCusta" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="lstrIdCusta" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="lstrIdSolicitacao" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="lstrDataInicioSolicitacao" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="lstrDataFimSolicitacao" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="lstrStatusSolicitacao" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="lstrCorrespondente" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="lstrCustaCancelada" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="lstrSolicitacaoEmCumprimento" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="lstrDataLimiteInicio" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="lstrDataLimiteFim" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "lstrUsuario",
    "lstrSenha",
    "lstrStatusCusta",
    "lstrIdCusta",
    "lstrIdSolicitacao",
    "lstrDataInicioSolicitacao",
    "lstrDataFimSolicitacao",
    "lstrStatusSolicitacao",
    "lstrCorrespondente",
    "lstrCustaCancelada",
    "lstrSolicitacaoEmCumprimento",
    "lstrDataLimiteInicio",
    "lstrDataLimiteFim"
})
@XmlRootElement(name = "ConsultaCustaArquivos")
public class ConsultaCustaArquivos {

    protected String lstrUsuario;
    protected String lstrSenha;
    protected String lstrStatusCusta;
    protected String lstrIdCusta;
    protected String lstrIdSolicitacao;
    protected String lstrDataInicioSolicitacao;
    protected String lstrDataFimSolicitacao;
    protected String lstrStatusSolicitacao;
    protected String lstrCorrespondente;
    protected String lstrCustaCancelada;
    protected String lstrSolicitacaoEmCumprimento;
    protected String lstrDataLimiteInicio;
    protected String lstrDataLimiteFim;

    /**
     * Obtém o valor da propriedade lstrUsuario.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLstrUsuario() {
        return lstrUsuario;
    }

    /**
     * Define o valor da propriedade lstrUsuario.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLstrUsuario(String value) {
        this.lstrUsuario = value;
    }

    /**
     * Obtém o valor da propriedade lstrSenha.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLstrSenha() {
        return lstrSenha;
    }

    /**
     * Define o valor da propriedade lstrSenha.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLstrSenha(String value) {
        this.lstrSenha = value;
    }

    /**
     * Obtém o valor da propriedade lstrStatusCusta.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLstrStatusCusta() {
        return lstrStatusCusta;
    }

    /**
     * Define o valor da propriedade lstrStatusCusta.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLstrStatusCusta(String value) {
        this.lstrStatusCusta = value;
    }

    /**
     * Obtém o valor da propriedade lstrIdCusta.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLstrIdCusta() {
        return lstrIdCusta;
    }

    /**
     * Define o valor da propriedade lstrIdCusta.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLstrIdCusta(String value) {
        this.lstrIdCusta = value;
    }

    /**
     * Obtém o valor da propriedade lstrIdSolicitacao.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLstrIdSolicitacao() {
        return lstrIdSolicitacao;
    }

    /**
     * Define o valor da propriedade lstrIdSolicitacao.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLstrIdSolicitacao(String value) {
        this.lstrIdSolicitacao = value;
    }

    /**
     * Obtém o valor da propriedade lstrDataInicioSolicitacao.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLstrDataInicioSolicitacao() {
        return lstrDataInicioSolicitacao;
    }

    /**
     * Define o valor da propriedade lstrDataInicioSolicitacao.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLstrDataInicioSolicitacao(String value) {
        this.lstrDataInicioSolicitacao = value;
    }

    /**
     * Obtém o valor da propriedade lstrDataFimSolicitacao.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLstrDataFimSolicitacao() {
        return lstrDataFimSolicitacao;
    }

    /**
     * Define o valor da propriedade lstrDataFimSolicitacao.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLstrDataFimSolicitacao(String value) {
        this.lstrDataFimSolicitacao = value;
    }

    /**
     * Obtém o valor da propriedade lstrStatusSolicitacao.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLstrStatusSolicitacao() {
        return lstrStatusSolicitacao;
    }

    /**
     * Define o valor da propriedade lstrStatusSolicitacao.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLstrStatusSolicitacao(String value) {
        this.lstrStatusSolicitacao = value;
    }

    /**
     * Obtém o valor da propriedade lstrCorrespondente.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLstrCorrespondente() {
        return lstrCorrespondente;
    }

    /**
     * Define o valor da propriedade lstrCorrespondente.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLstrCorrespondente(String value) {
        this.lstrCorrespondente = value;
    }

    /**
     * Obtém o valor da propriedade lstrCustaCancelada.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLstrCustaCancelada() {
        return lstrCustaCancelada;
    }

    /**
     * Define o valor da propriedade lstrCustaCancelada.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLstrCustaCancelada(String value) {
        this.lstrCustaCancelada = value;
    }

    /**
     * Obtém o valor da propriedade lstrSolicitacaoEmCumprimento.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLstrSolicitacaoEmCumprimento() {
        return lstrSolicitacaoEmCumprimento;
    }

    /**
     * Define o valor da propriedade lstrSolicitacaoEmCumprimento.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLstrSolicitacaoEmCumprimento(String value) {
        this.lstrSolicitacaoEmCumprimento = value;
    }

    /**
     * Obtém o valor da propriedade lstrDataLimiteInicio.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLstrDataLimiteInicio() {
        return lstrDataLimiteInicio;
    }

    /**
     * Define o valor da propriedade lstrDataLimiteInicio.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLstrDataLimiteInicio(String value) {
        this.lstrDataLimiteInicio = value;
    }

    /**
     * Obtém o valor da propriedade lstrDataLimiteFim.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLstrDataLimiteFim() {
        return lstrDataLimiteFim;
    }

    /**
     * Define o valor da propriedade lstrDataLimiteFim.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLstrDataLimiteFim(String value) {
        this.lstrDataLimiteFim = value;
    }

}
