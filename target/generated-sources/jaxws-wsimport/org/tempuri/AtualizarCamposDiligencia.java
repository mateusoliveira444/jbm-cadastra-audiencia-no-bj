
package org.tempuri;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java de anonymous complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="lstrUsuario" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="lstrSenha" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="lintIDDiligencia" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="lstrCampo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="lstrValor" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "lstrUsuario",
    "lstrSenha",
    "lintIDDiligencia",
    "lstrCampo",
    "lstrValor"
})
@XmlRootElement(name = "AtualizarCamposDiligencia")
public class AtualizarCamposDiligencia {

    protected String lstrUsuario;
    protected String lstrSenha;
    protected int lintIDDiligencia;
    protected String lstrCampo;
    protected String lstrValor;

    /**
     * Obtém o valor da propriedade lstrUsuario.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLstrUsuario() {
        return lstrUsuario;
    }

    /**
     * Define o valor da propriedade lstrUsuario.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLstrUsuario(String value) {
        this.lstrUsuario = value;
    }

    /**
     * Obtém o valor da propriedade lstrSenha.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLstrSenha() {
        return lstrSenha;
    }

    /**
     * Define o valor da propriedade lstrSenha.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLstrSenha(String value) {
        this.lstrSenha = value;
    }

    /**
     * Obtém o valor da propriedade lintIDDiligencia.
     * 
     */
    public int getLintIDDiligencia() {
        return lintIDDiligencia;
    }

    /**
     * Define o valor da propriedade lintIDDiligencia.
     * 
     */
    public void setLintIDDiligencia(int value) {
        this.lintIDDiligencia = value;
    }

    /**
     * Obtém o valor da propriedade lstrCampo.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLstrCampo() {
        return lstrCampo;
    }

    /**
     * Define o valor da propriedade lstrCampo.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLstrCampo(String value) {
        this.lstrCampo = value;
    }

    /**
     * Obtém o valor da propriedade lstrValor.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLstrValor() {
        return lstrValor;
    }

    /**
     * Define o valor da propriedade lstrValor.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLstrValor(String value) {
        this.lstrValor = value;
    }

}
