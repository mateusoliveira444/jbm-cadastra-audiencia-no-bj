
package org.tempuri;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java de anonymous complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="RecuperarInstanciaResult" type="{http://tempuri.org/}ArrayOfInstancia" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "recuperarInstanciaResult"
})
@XmlRootElement(name = "RecuperarInstanciaResponse")
public class RecuperarInstanciaResponse {

    @XmlElement(name = "RecuperarInstanciaResult")
    protected ArrayOfInstancia recuperarInstanciaResult;

    /**
     * Obtém o valor da propriedade recuperarInstanciaResult.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfInstancia }
     *     
     */
    public ArrayOfInstancia getRecuperarInstanciaResult() {
        return recuperarInstanciaResult;
    }

    /**
     * Define o valor da propriedade recuperarInstanciaResult.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfInstancia }
     *     
     */
    public void setRecuperarInstanciaResult(ArrayOfInstancia value) {
        this.recuperarInstanciaResult = value;
    }

}
