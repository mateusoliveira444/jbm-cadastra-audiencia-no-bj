
package org.tempuri;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java de anonymous complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="lintIdSolicitacao" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="lintIdTipoCusta" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="lintIdOperacaoFinanceira" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="lintIdCelulaCnpj" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="ldblValorCusta" type="{http://www.w3.org/2001/XMLSchema}double"/>
 *         &lt;element name="lstrDataPagamento" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="lstrObservacao" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="lblnInternetBanking" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="lstrUsuario" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="lstrSenha" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "lintIdSolicitacao",
    "lintIdTipoCusta",
    "lintIdOperacaoFinanceira",
    "lintIdCelulaCnpj",
    "ldblValorCusta",
    "lstrDataPagamento",
    "lstrObservacao",
    "lblnInternetBanking",
    "lstrUsuario",
    "lstrSenha"
})
@XmlRootElement(name = "InserirCusta")
public class InserirCusta {

    protected int lintIdSolicitacao;
    protected int lintIdTipoCusta;
    protected int lintIdOperacaoFinanceira;
    protected int lintIdCelulaCnpj;
    protected double ldblValorCusta;
    protected String lstrDataPagamento;
    protected String lstrObservacao;
    protected boolean lblnInternetBanking;
    protected String lstrUsuario;
    protected String lstrSenha;

    /**
     * Obtém o valor da propriedade lintIdSolicitacao.
     * 
     */
    public int getLintIdSolicitacao() {
        return lintIdSolicitacao;
    }

    /**
     * Define o valor da propriedade lintIdSolicitacao.
     * 
     */
    public void setLintIdSolicitacao(int value) {
        this.lintIdSolicitacao = value;
    }

    /**
     * Obtém o valor da propriedade lintIdTipoCusta.
     * 
     */
    public int getLintIdTipoCusta() {
        return lintIdTipoCusta;
    }

    /**
     * Define o valor da propriedade lintIdTipoCusta.
     * 
     */
    public void setLintIdTipoCusta(int value) {
        this.lintIdTipoCusta = value;
    }

    /**
     * Obtém o valor da propriedade lintIdOperacaoFinanceira.
     * 
     */
    public int getLintIdOperacaoFinanceira() {
        return lintIdOperacaoFinanceira;
    }

    /**
     * Define o valor da propriedade lintIdOperacaoFinanceira.
     * 
     */
    public void setLintIdOperacaoFinanceira(int value) {
        this.lintIdOperacaoFinanceira = value;
    }

    /**
     * Obtém o valor da propriedade lintIdCelulaCnpj.
     * 
     */
    public int getLintIdCelulaCnpj() {
        return lintIdCelulaCnpj;
    }

    /**
     * Define o valor da propriedade lintIdCelulaCnpj.
     * 
     */
    public void setLintIdCelulaCnpj(int value) {
        this.lintIdCelulaCnpj = value;
    }

    /**
     * Obtém o valor da propriedade ldblValorCusta.
     * 
     */
    public double getLdblValorCusta() {
        return ldblValorCusta;
    }

    /**
     * Define o valor da propriedade ldblValorCusta.
     * 
     */
    public void setLdblValorCusta(double value) {
        this.ldblValorCusta = value;
    }

    /**
     * Obtém o valor da propriedade lstrDataPagamento.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLstrDataPagamento() {
        return lstrDataPagamento;
    }

    /**
     * Define o valor da propriedade lstrDataPagamento.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLstrDataPagamento(String value) {
        this.lstrDataPagamento = value;
    }

    /**
     * Obtém o valor da propriedade lstrObservacao.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLstrObservacao() {
        return lstrObservacao;
    }

    /**
     * Define o valor da propriedade lstrObservacao.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLstrObservacao(String value) {
        this.lstrObservacao = value;
    }

    /**
     * Obtém o valor da propriedade lblnInternetBanking.
     * 
     */
    public boolean isLblnInternetBanking() {
        return lblnInternetBanking;
    }

    /**
     * Define o valor da propriedade lblnInternetBanking.
     * 
     */
    public void setLblnInternetBanking(boolean value) {
        this.lblnInternetBanking = value;
    }

    /**
     * Obtém o valor da propriedade lstrUsuario.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLstrUsuario() {
        return lstrUsuario;
    }

    /**
     * Define o valor da propriedade lstrUsuario.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLstrUsuario(String value) {
        this.lstrUsuario = value;
    }

    /**
     * Obtém o valor da propriedade lstrSenha.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLstrSenha() {
        return lstrSenha;
    }

    /**
     * Define o valor da propriedade lstrSenha.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLstrSenha(String value) {
        this.lstrSenha = value;
    }

}
