
package org.tempuri;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java de anonymous complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="lstrUsuario" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="lstrSenha" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="lintIDSubTipoDiligencia" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="lstrProcessoUnico" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="lintIDComarca" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="lintIDSistemaVirtual" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "lstrUsuario",
    "lstrSenha",
    "lintIDSubTipoDiligencia",
    "lstrProcessoUnico",
    "lintIDComarca",
    "lintIDSistemaVirtual"
})
@XmlRootElement(name = "RetornaCorrespondente")
public class RetornaCorrespondente {

    protected String lstrUsuario;
    protected String lstrSenha;
    protected int lintIDSubTipoDiligencia;
    protected String lstrProcessoUnico;
    protected int lintIDComarca;
    protected int lintIDSistemaVirtual;

    /**
     * Obtém o valor da propriedade lstrUsuario.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLstrUsuario() {
        return lstrUsuario;
    }

    /**
     * Define o valor da propriedade lstrUsuario.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLstrUsuario(String value) {
        this.lstrUsuario = value;
    }

    /**
     * Obtém o valor da propriedade lstrSenha.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLstrSenha() {
        return lstrSenha;
    }

    /**
     * Define o valor da propriedade lstrSenha.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLstrSenha(String value) {
        this.lstrSenha = value;
    }

    /**
     * Obtém o valor da propriedade lintIDSubTipoDiligencia.
     * 
     */
    public int getLintIDSubTipoDiligencia() {
        return lintIDSubTipoDiligencia;
    }

    /**
     * Define o valor da propriedade lintIDSubTipoDiligencia.
     * 
     */
    public void setLintIDSubTipoDiligencia(int value) {
        this.lintIDSubTipoDiligencia = value;
    }

    /**
     * Obtém o valor da propriedade lstrProcessoUnico.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLstrProcessoUnico() {
        return lstrProcessoUnico;
    }

    /**
     * Define o valor da propriedade lstrProcessoUnico.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLstrProcessoUnico(String value) {
        this.lstrProcessoUnico = value;
    }

    /**
     * Obtém o valor da propriedade lintIDComarca.
     * 
     */
    public int getLintIDComarca() {
        return lintIDComarca;
    }

    /**
     * Define o valor da propriedade lintIDComarca.
     * 
     */
    public void setLintIDComarca(int value) {
        this.lintIDComarca = value;
    }

    /**
     * Obtém o valor da propriedade lintIDSistemaVirtual.
     * 
     */
    public int getLintIDSistemaVirtual() {
        return lintIDSistemaVirtual;
    }

    /**
     * Define o valor da propriedade lintIDSistemaVirtual.
     * 
     */
    public void setLintIDSistemaVirtual(int value) {
        this.lintIDSistemaVirtual = value;
    }

}
