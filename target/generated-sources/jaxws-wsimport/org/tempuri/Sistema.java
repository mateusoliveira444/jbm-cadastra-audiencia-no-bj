
package org.tempuri;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java de Sistema complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="Sistema">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="IDSistema" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="SistemaDescricao" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="UF" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="IdEstado" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Sistema", propOrder = {
    "idSistema",
    "sistemaDescricao",
    "uf",
    "idEstado"
})
public class Sistema {

    @XmlElement(name = "IDSistema")
    protected int idSistema;
    @XmlElement(name = "SistemaDescricao")
    protected String sistemaDescricao;
    @XmlElement(name = "UF")
    protected String uf;
    @XmlElement(name = "IdEstado")
    protected int idEstado;

    /**
     * Obtém o valor da propriedade idSistema.
     * 
     */
    public int getIDSistema() {
        return idSistema;
    }

    /**
     * Define o valor da propriedade idSistema.
     * 
     */
    public void setIDSistema(int value) {
        this.idSistema = value;
    }

    /**
     * Obtém o valor da propriedade sistemaDescricao.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSistemaDescricao() {
        return sistemaDescricao;
    }

    /**
     * Define o valor da propriedade sistemaDescricao.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSistemaDescricao(String value) {
        this.sistemaDescricao = value;
    }

    /**
     * Obtém o valor da propriedade uf.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUF() {
        return uf;
    }

    /**
     * Define o valor da propriedade uf.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUF(String value) {
        this.uf = value;
    }

    /**
     * Obtém o valor da propriedade idEstado.
     * 
     */
    public int getIdEstado() {
        return idEstado;
    }

    /**
     * Define o valor da propriedade idEstado.
     * 
     */
    public void setIdEstado(int value) {
        this.idEstado = value;
    }

}
