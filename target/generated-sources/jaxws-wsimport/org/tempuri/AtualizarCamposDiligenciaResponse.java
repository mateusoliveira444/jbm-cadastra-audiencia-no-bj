
package org.tempuri;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java de anonymous complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="AtualizarCamposDiligenciaResult" type="{http://tempuri.org/}RetornoMetodos" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "atualizarCamposDiligenciaResult"
})
@XmlRootElement(name = "AtualizarCamposDiligenciaResponse")
public class AtualizarCamposDiligenciaResponse {

    @XmlElement(name = "AtualizarCamposDiligenciaResult")
    protected RetornoMetodos atualizarCamposDiligenciaResult;

    /**
     * Obtém o valor da propriedade atualizarCamposDiligenciaResult.
     * 
     * @return
     *     possible object is
     *     {@link RetornoMetodos }
     *     
     */
    public RetornoMetodos getAtualizarCamposDiligenciaResult() {
        return atualizarCamposDiligenciaResult;
    }

    /**
     * Define o valor da propriedade atualizarCamposDiligenciaResult.
     * 
     * @param value
     *     allowed object is
     *     {@link RetornoMetodos }
     *     
     */
    public void setAtualizarCamposDiligenciaResult(RetornoMetodos value) {
        this.atualizarCamposDiligenciaResult = value;
    }

}
