
package org.tempuri;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java de anonymous complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="lintIDCusta" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="lstrLinhaDigitavel" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ldblValorRobo" type="{http://www.w3.org/2001/XMLSchema}double"/>
 *         &lt;element name="lstrUsuario" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="lstrSenha" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "lintIDCusta",
    "lstrLinhaDigitavel",
    "ldblValorRobo",
    "lstrUsuario",
    "lstrSenha"
})
@XmlRootElement(name = "AtualizaCustaRobo")
public class AtualizaCustaRobo {

    protected int lintIDCusta;
    protected String lstrLinhaDigitavel;
    protected double ldblValorRobo;
    protected String lstrUsuario;
    protected String lstrSenha;

    /**
     * Obtém o valor da propriedade lintIDCusta.
     * 
     */
    public int getLintIDCusta() {
        return lintIDCusta;
    }

    /**
     * Define o valor da propriedade lintIDCusta.
     * 
     */
    public void setLintIDCusta(int value) {
        this.lintIDCusta = value;
    }

    /**
     * Obtém o valor da propriedade lstrLinhaDigitavel.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLstrLinhaDigitavel() {
        return lstrLinhaDigitavel;
    }

    /**
     * Define o valor da propriedade lstrLinhaDigitavel.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLstrLinhaDigitavel(String value) {
        this.lstrLinhaDigitavel = value;
    }

    /**
     * Obtém o valor da propriedade ldblValorRobo.
     * 
     */
    public double getLdblValorRobo() {
        return ldblValorRobo;
    }

    /**
     * Define o valor da propriedade ldblValorRobo.
     * 
     */
    public void setLdblValorRobo(double value) {
        this.ldblValorRobo = value;
    }

    /**
     * Obtém o valor da propriedade lstrUsuario.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLstrUsuario() {
        return lstrUsuario;
    }

    /**
     * Define o valor da propriedade lstrUsuario.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLstrUsuario(String value) {
        this.lstrUsuario = value;
    }

    /**
     * Obtém o valor da propriedade lstrSenha.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLstrSenha() {
        return lstrSenha;
    }

    /**
     * Define o valor da propriedade lstrSenha.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLstrSenha(String value) {
        this.lstrSenha = value;
    }

}
