
package org.tempuri;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Classe Java de RetornoDesdobramento complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="RetornoDesdobramento">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="IDDesdobramento" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="IDProcesso" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="IDProcessoUnico" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="NumeroDesdobramento" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DataDistribuicao" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="ExecucaoProvisoria" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Justica" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Instancia" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Foro" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="StatusProcesso" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Reparticao" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="UF" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Pasta" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DataEncerramento" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="Observacao" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Ordinal" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="DataCadastro" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="NumeroAntigo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "RetornoDesdobramento", propOrder = {
    "idDesdobramento",
    "idProcesso",
    "idProcessoUnico",
    "numeroDesdobramento",
    "dataDistribuicao",
    "execucaoProvisoria",
    "justica",
    "instancia",
    "foro",
    "statusProcesso",
    "reparticao",
    "uf",
    "pasta",
    "dataEncerramento",
    "observacao",
    "ordinal",
    "dataCadastro",
    "numeroAntigo"
})
public class RetornoDesdobramento {

    @XmlElement(name = "IDDesdobramento")
    protected int idDesdobramento;
    @XmlElement(name = "IDProcesso")
    protected int idProcesso;
    @XmlElement(name = "IDProcessoUnico")
    protected String idProcessoUnico;
    @XmlElement(name = "NumeroDesdobramento")
    protected String numeroDesdobramento;
    @XmlElement(name = "DataDistribuicao", required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar dataDistribuicao;
    @XmlElement(name = "ExecucaoProvisoria")
    protected String execucaoProvisoria;
    @XmlElement(name = "Justica")
    protected String justica;
    @XmlElement(name = "Instancia")
    protected String instancia;
    @XmlElement(name = "Foro")
    protected String foro;
    @XmlElement(name = "StatusProcesso")
    protected String statusProcesso;
    @XmlElement(name = "Reparticao")
    protected String reparticao;
    @XmlElement(name = "UF")
    protected String uf;
    @XmlElement(name = "Pasta")
    protected String pasta;
    @XmlElement(name = "DataEncerramento", required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar dataEncerramento;
    @XmlElement(name = "Observacao")
    protected String observacao;
    @XmlElement(name = "Ordinal")
    protected int ordinal;
    @XmlElement(name = "DataCadastro", required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar dataCadastro;
    @XmlElement(name = "NumeroAntigo")
    protected String numeroAntigo;

    /**
     * Obtém o valor da propriedade idDesdobramento.
     * 
     */
    public int getIDDesdobramento() {
        return idDesdobramento;
    }

    /**
     * Define o valor da propriedade idDesdobramento.
     * 
     */
    public void setIDDesdobramento(int value) {
        this.idDesdobramento = value;
    }

    /**
     * Obtém o valor da propriedade idProcesso.
     * 
     */
    public int getIDProcesso() {
        return idProcesso;
    }

    /**
     * Define o valor da propriedade idProcesso.
     * 
     */
    public void setIDProcesso(int value) {
        this.idProcesso = value;
    }

    /**
     * Obtém o valor da propriedade idProcessoUnico.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIDProcessoUnico() {
        return idProcessoUnico;
    }

    /**
     * Define o valor da propriedade idProcessoUnico.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIDProcessoUnico(String value) {
        this.idProcessoUnico = value;
    }

    /**
     * Obtém o valor da propriedade numeroDesdobramento.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNumeroDesdobramento() {
        return numeroDesdobramento;
    }

    /**
     * Define o valor da propriedade numeroDesdobramento.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNumeroDesdobramento(String value) {
        this.numeroDesdobramento = value;
    }

    /**
     * Obtém o valor da propriedade dataDistribuicao.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDataDistribuicao() {
        return dataDistribuicao;
    }

    /**
     * Define o valor da propriedade dataDistribuicao.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDataDistribuicao(XMLGregorianCalendar value) {
        this.dataDistribuicao = value;
    }

    /**
     * Obtém o valor da propriedade execucaoProvisoria.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getExecucaoProvisoria() {
        return execucaoProvisoria;
    }

    /**
     * Define o valor da propriedade execucaoProvisoria.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setExecucaoProvisoria(String value) {
        this.execucaoProvisoria = value;
    }

    /**
     * Obtém o valor da propriedade justica.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getJustica() {
        return justica;
    }

    /**
     * Define o valor da propriedade justica.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setJustica(String value) {
        this.justica = value;
    }

    /**
     * Obtém o valor da propriedade instancia.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInstancia() {
        return instancia;
    }

    /**
     * Define o valor da propriedade instancia.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInstancia(String value) {
        this.instancia = value;
    }

    /**
     * Obtém o valor da propriedade foro.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getForo() {
        return foro;
    }

    /**
     * Define o valor da propriedade foro.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setForo(String value) {
        this.foro = value;
    }

    /**
     * Obtém o valor da propriedade statusProcesso.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStatusProcesso() {
        return statusProcesso;
    }

    /**
     * Define o valor da propriedade statusProcesso.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStatusProcesso(String value) {
        this.statusProcesso = value;
    }

    /**
     * Obtém o valor da propriedade reparticao.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReparticao() {
        return reparticao;
    }

    /**
     * Define o valor da propriedade reparticao.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReparticao(String value) {
        this.reparticao = value;
    }

    /**
     * Obtém o valor da propriedade uf.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUF() {
        return uf;
    }

    /**
     * Define o valor da propriedade uf.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUF(String value) {
        this.uf = value;
    }

    /**
     * Obtém o valor da propriedade pasta.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPasta() {
        return pasta;
    }

    /**
     * Define o valor da propriedade pasta.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPasta(String value) {
        this.pasta = value;
    }

    /**
     * Obtém o valor da propriedade dataEncerramento.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDataEncerramento() {
        return dataEncerramento;
    }

    /**
     * Define o valor da propriedade dataEncerramento.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDataEncerramento(XMLGregorianCalendar value) {
        this.dataEncerramento = value;
    }

    /**
     * Obtém o valor da propriedade observacao.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getObservacao() {
        return observacao;
    }

    /**
     * Define o valor da propriedade observacao.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setObservacao(String value) {
        this.observacao = value;
    }

    /**
     * Obtém o valor da propriedade ordinal.
     * 
     */
    public int getOrdinal() {
        return ordinal;
    }

    /**
     * Define o valor da propriedade ordinal.
     * 
     */
    public void setOrdinal(int value) {
        this.ordinal = value;
    }

    /**
     * Obtém o valor da propriedade dataCadastro.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDataCadastro() {
        return dataCadastro;
    }

    /**
     * Define o valor da propriedade dataCadastro.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDataCadastro(XMLGregorianCalendar value) {
        this.dataCadastro = value;
    }

    /**
     * Obtém o valor da propriedade numeroAntigo.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNumeroAntigo() {
        return numeroAntigo;
    }

    /**
     * Define o valor da propriedade numeroAntigo.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNumeroAntigo(String value) {
        this.numeroAntigo = value;
    }

}
