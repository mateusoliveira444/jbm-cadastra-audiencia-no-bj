
package org.tempuri;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java de RetornoCorrespondente complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="RetornoCorrespondente">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="IDCorrespondente" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="NomeCorrespondente" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "RetornoCorrespondente", propOrder = {
    "idCorrespondente",
    "nomeCorrespondente"
})
public class RetornoCorrespondente {

    @XmlElement(name = "IDCorrespondente")
    protected int idCorrespondente;
    @XmlElement(name = "NomeCorrespondente")
    protected String nomeCorrespondente;

    /**
     * Obtém o valor da propriedade idCorrespondente.
     * 
     */
    public int getIDCorrespondente() {
        return idCorrespondente;
    }

    /**
     * Define o valor da propriedade idCorrespondente.
     * 
     */
    public void setIDCorrespondente(int value) {
        this.idCorrespondente = value;
    }

    /**
     * Obtém o valor da propriedade nomeCorrespondente.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNomeCorrespondente() {
        return nomeCorrespondente;
    }

    /**
     * Define o valor da propriedade nomeCorrespondente.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNomeCorrespondente(String value) {
        this.nomeCorrespondente = value;
    }

}
