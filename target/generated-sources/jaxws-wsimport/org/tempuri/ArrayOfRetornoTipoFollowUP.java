
package org.tempuri;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java de ArrayOfRetornoTipoFollowUP complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="ArrayOfRetornoTipoFollowUP">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="RetornoTipoFollowUP" type="{http://tempuri.org/}RetornoTipoFollowUP" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ArrayOfRetornoTipoFollowUP", propOrder = {
    "retornoTipoFollowUP"
})
public class ArrayOfRetornoTipoFollowUP {

    @XmlElement(name = "RetornoTipoFollowUP", nillable = true)
    protected List<RetornoTipoFollowUP> retornoTipoFollowUP;

    /**
     * Gets the value of the retornoTipoFollowUP property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the retornoTipoFollowUP property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRetornoTipoFollowUP().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link RetornoTipoFollowUP }
     * 
     * 
     */
    public List<RetornoTipoFollowUP> getRetornoTipoFollowUP() {
        if (retornoTipoFollowUP == null) {
            retornoTipoFollowUP = new ArrayList<RetornoTipoFollowUP>();
        }
        return this.retornoTipoFollowUP;
    }

}
