
package org.tempuri;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java de anonymous complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="lstrUsuario" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="lstrSenha" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="lstrIdCustas" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="lstrStatusCusta" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="lstrDataPagamento" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="lstrObservacaoLote" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "lstrUsuario",
    "lstrSenha",
    "lstrIdCustas",
    "lstrStatusCusta",
    "lstrDataPagamento",
    "lstrObservacaoLote"
})
@XmlRootElement(name = "FechamentoLoteCustas")
public class FechamentoLoteCustas {

    protected String lstrUsuario;
    protected String lstrSenha;
    protected String lstrIdCustas;
    protected String lstrStatusCusta;
    protected String lstrDataPagamento;
    protected String lstrObservacaoLote;

    /**
     * Obtém o valor da propriedade lstrUsuario.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLstrUsuario() {
        return lstrUsuario;
    }

    /**
     * Define o valor da propriedade lstrUsuario.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLstrUsuario(String value) {
        this.lstrUsuario = value;
    }

    /**
     * Obtém o valor da propriedade lstrSenha.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLstrSenha() {
        return lstrSenha;
    }

    /**
     * Define o valor da propriedade lstrSenha.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLstrSenha(String value) {
        this.lstrSenha = value;
    }

    /**
     * Obtém o valor da propriedade lstrIdCustas.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLstrIdCustas() {
        return lstrIdCustas;
    }

    /**
     * Define o valor da propriedade lstrIdCustas.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLstrIdCustas(String value) {
        this.lstrIdCustas = value;
    }

    /**
     * Obtém o valor da propriedade lstrStatusCusta.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLstrStatusCusta() {
        return lstrStatusCusta;
    }

    /**
     * Define o valor da propriedade lstrStatusCusta.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLstrStatusCusta(String value) {
        this.lstrStatusCusta = value;
    }

    /**
     * Obtém o valor da propriedade lstrDataPagamento.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLstrDataPagamento() {
        return lstrDataPagamento;
    }

    /**
     * Define o valor da propriedade lstrDataPagamento.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLstrDataPagamento(String value) {
        this.lstrDataPagamento = value;
    }

    /**
     * Obtém o valor da propriedade lstrObservacaoLote.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLstrObservacaoLote() {
        return lstrObservacaoLote;
    }

    /**
     * Define o valor da propriedade lstrObservacaoLote.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLstrObservacaoLote(String value) {
        this.lstrObservacaoLote = value;
    }

}
