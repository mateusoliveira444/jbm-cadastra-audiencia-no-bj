
package org.tempuri;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java de anonymous complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="lintIdCusta" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="lintIdLote" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="lstrUsuario" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="lstrSenha" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "lintIdCusta",
    "lintIdLote",
    "lstrUsuario",
    "lstrSenha"
})
@XmlRootElement(name = "ConsultaLoteCusta")
public class ConsultaLoteCusta {

    protected int lintIdCusta;
    protected int lintIdLote;
    protected String lstrUsuario;
    protected String lstrSenha;

    /**
     * Obtém o valor da propriedade lintIdCusta.
     * 
     */
    public int getLintIdCusta() {
        return lintIdCusta;
    }

    /**
     * Define o valor da propriedade lintIdCusta.
     * 
     */
    public void setLintIdCusta(int value) {
        this.lintIdCusta = value;
    }

    /**
     * Obtém o valor da propriedade lintIdLote.
     * 
     */
    public int getLintIdLote() {
        return lintIdLote;
    }

    /**
     * Define o valor da propriedade lintIdLote.
     * 
     */
    public void setLintIdLote(int value) {
        this.lintIdLote = value;
    }

    /**
     * Obtém o valor da propriedade lstrUsuario.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLstrUsuario() {
        return lstrUsuario;
    }

    /**
     * Define o valor da propriedade lstrUsuario.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLstrUsuario(String value) {
        this.lstrUsuario = value;
    }

    /**
     * Obtém o valor da propriedade lstrSenha.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLstrSenha() {
        return lstrSenha;
    }

    /**
     * Define o valor da propriedade lstrSenha.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLstrSenha(String value) {
        this.lstrSenha = value;
    }

}
