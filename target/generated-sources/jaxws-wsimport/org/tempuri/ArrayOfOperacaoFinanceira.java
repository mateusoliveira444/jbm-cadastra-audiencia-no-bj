
package org.tempuri;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java de ArrayOfOperacaoFinanceira complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="ArrayOfOperacaoFinanceira">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="OperacaoFinanceira" type="{http://tempuri.org/}OperacaoFinanceira" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ArrayOfOperacaoFinanceira", propOrder = {
    "operacaoFinanceira"
})
public class ArrayOfOperacaoFinanceira {

    @XmlElement(name = "OperacaoFinanceira", nillable = true)
    protected List<OperacaoFinanceira> operacaoFinanceira;

    /**
     * Gets the value of the operacaoFinanceira property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the operacaoFinanceira property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getOperacaoFinanceira().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link OperacaoFinanceira }
     * 
     * 
     */
    public List<OperacaoFinanceira> getOperacaoFinanceira() {
        if (operacaoFinanceira == null) {
            operacaoFinanceira = new ArrayList<OperacaoFinanceira>();
        }
        return this.operacaoFinanceira;
    }

}
