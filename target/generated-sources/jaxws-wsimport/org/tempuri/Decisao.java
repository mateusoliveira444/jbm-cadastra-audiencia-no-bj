
package org.tempuri;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java de Decisao complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="Decisao">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="IDDecisao" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="DecisaoDesc" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="IDInstancia" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="InstanciaDesc" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Decisao", propOrder = {
    "idDecisao",
    "decisaoDesc",
    "idInstancia",
    "instanciaDesc"
})
public class Decisao {

    @XmlElement(name = "IDDecisao")
    protected int idDecisao;
    @XmlElement(name = "DecisaoDesc")
    protected String decisaoDesc;
    @XmlElement(name = "IDInstancia")
    protected int idInstancia;
    @XmlElement(name = "InstanciaDesc")
    protected String instanciaDesc;

    /**
     * Obtém o valor da propriedade idDecisao.
     * 
     */
    public int getIDDecisao() {
        return idDecisao;
    }

    /**
     * Define o valor da propriedade idDecisao.
     * 
     */
    public void setIDDecisao(int value) {
        this.idDecisao = value;
    }

    /**
     * Obtém o valor da propriedade decisaoDesc.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDecisaoDesc() {
        return decisaoDesc;
    }

    /**
     * Define o valor da propriedade decisaoDesc.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDecisaoDesc(String value) {
        this.decisaoDesc = value;
    }

    /**
     * Obtém o valor da propriedade idInstancia.
     * 
     */
    public int getIDInstancia() {
        return idInstancia;
    }

    /**
     * Define o valor da propriedade idInstancia.
     * 
     */
    public void setIDInstancia(int value) {
        this.idInstancia = value;
    }

    /**
     * Obtém o valor da propriedade instanciaDesc.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInstanciaDesc() {
        return instanciaDesc;
    }

    /**
     * Define o valor da propriedade instanciaDesc.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInstanciaDesc(String value) {
        this.instanciaDesc = value;
    }

}
