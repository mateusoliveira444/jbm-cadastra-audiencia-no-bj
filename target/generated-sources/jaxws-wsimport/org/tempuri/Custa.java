
package org.tempuri;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Classe Java de Custa complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="Custa">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="IdCusta" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="IdTipoCusta" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="IdOpercaoFinanceira" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="IdCelulaCnpj" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="ValorCusta" type="{http://www.w3.org/2001/XMLSchema}double"/>
 *         &lt;element name="DataPagamento" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="Observacao" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="InternetBanking" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="IdStatusCusta" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="IdSolicitacao" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="Usuario" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="StatusCusta" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CelulaCnpj" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="OperacaoFinanceira" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="TipoCusta" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Cancelada" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Custa", propOrder = {
    "idCusta",
    "idTipoCusta",
    "idOpercaoFinanceira",
    "idCelulaCnpj",
    "valorCusta",
    "dataPagamento",
    "observacao",
    "internetBanking",
    "idStatusCusta",
    "idSolicitacao",
    "usuario",
    "statusCusta",
    "celulaCnpj",
    "operacaoFinanceira",
    "tipoCusta",
    "cancelada"
})
public class Custa {

    @XmlElement(name = "IdCusta")
    protected int idCusta;
    @XmlElement(name = "IdTipoCusta")
    protected int idTipoCusta;
    @XmlElement(name = "IdOpercaoFinanceira")
    protected int idOpercaoFinanceira;
    @XmlElement(name = "IdCelulaCnpj")
    protected int idCelulaCnpj;
    @XmlElement(name = "ValorCusta")
    protected double valorCusta;
    @XmlElement(name = "DataPagamento", required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar dataPagamento;
    @XmlElement(name = "Observacao")
    protected String observacao;
    @XmlElement(name = "InternetBanking")
    protected boolean internetBanking;
    @XmlElement(name = "IdStatusCusta")
    protected int idStatusCusta;
    @XmlElement(name = "IdSolicitacao")
    protected int idSolicitacao;
    @XmlElement(name = "Usuario")
    protected String usuario;
    @XmlElement(name = "StatusCusta")
    protected String statusCusta;
    @XmlElement(name = "CelulaCnpj")
    protected String celulaCnpj;
    @XmlElement(name = "OperacaoFinanceira")
    protected String operacaoFinanceira;
    @XmlElement(name = "TipoCusta")
    protected String tipoCusta;
    @XmlElement(name = "Cancelada")
    protected boolean cancelada;

    /**
     * Obtém o valor da propriedade idCusta.
     * 
     */
    public int getIdCusta() {
        return idCusta;
    }

    /**
     * Define o valor da propriedade idCusta.
     * 
     */
    public void setIdCusta(int value) {
        this.idCusta = value;
    }

    /**
     * Obtém o valor da propriedade idTipoCusta.
     * 
     */
    public int getIdTipoCusta() {
        return idTipoCusta;
    }

    /**
     * Define o valor da propriedade idTipoCusta.
     * 
     */
    public void setIdTipoCusta(int value) {
        this.idTipoCusta = value;
    }

    /**
     * Obtém o valor da propriedade idOpercaoFinanceira.
     * 
     */
    public int getIdOpercaoFinanceira() {
        return idOpercaoFinanceira;
    }

    /**
     * Define o valor da propriedade idOpercaoFinanceira.
     * 
     */
    public void setIdOpercaoFinanceira(int value) {
        this.idOpercaoFinanceira = value;
    }

    /**
     * Obtém o valor da propriedade idCelulaCnpj.
     * 
     */
    public int getIdCelulaCnpj() {
        return idCelulaCnpj;
    }

    /**
     * Define o valor da propriedade idCelulaCnpj.
     * 
     */
    public void setIdCelulaCnpj(int value) {
        this.idCelulaCnpj = value;
    }

    /**
     * Obtém o valor da propriedade valorCusta.
     * 
     */
    public double getValorCusta() {
        return valorCusta;
    }

    /**
     * Define o valor da propriedade valorCusta.
     * 
     */
    public void setValorCusta(double value) {
        this.valorCusta = value;
    }

    /**
     * Obtém o valor da propriedade dataPagamento.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDataPagamento() {
        return dataPagamento;
    }

    /**
     * Define o valor da propriedade dataPagamento.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDataPagamento(XMLGregorianCalendar value) {
        this.dataPagamento = value;
    }

    /**
     * Obtém o valor da propriedade observacao.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getObservacao() {
        return observacao;
    }

    /**
     * Define o valor da propriedade observacao.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setObservacao(String value) {
        this.observacao = value;
    }

    /**
     * Obtém o valor da propriedade internetBanking.
     * 
     */
    public boolean isInternetBanking() {
        return internetBanking;
    }

    /**
     * Define o valor da propriedade internetBanking.
     * 
     */
    public void setInternetBanking(boolean value) {
        this.internetBanking = value;
    }

    /**
     * Obtém o valor da propriedade idStatusCusta.
     * 
     */
    public int getIdStatusCusta() {
        return idStatusCusta;
    }

    /**
     * Define o valor da propriedade idStatusCusta.
     * 
     */
    public void setIdStatusCusta(int value) {
        this.idStatusCusta = value;
    }

    /**
     * Obtém o valor da propriedade idSolicitacao.
     * 
     */
    public int getIdSolicitacao() {
        return idSolicitacao;
    }

    /**
     * Define o valor da propriedade idSolicitacao.
     * 
     */
    public void setIdSolicitacao(int value) {
        this.idSolicitacao = value;
    }

    /**
     * Obtém o valor da propriedade usuario.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUsuario() {
        return usuario;
    }

    /**
     * Define o valor da propriedade usuario.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUsuario(String value) {
        this.usuario = value;
    }

    /**
     * Obtém o valor da propriedade statusCusta.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStatusCusta() {
        return statusCusta;
    }

    /**
     * Define o valor da propriedade statusCusta.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStatusCusta(String value) {
        this.statusCusta = value;
    }

    /**
     * Obtém o valor da propriedade celulaCnpj.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCelulaCnpj() {
        return celulaCnpj;
    }

    /**
     * Define o valor da propriedade celulaCnpj.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCelulaCnpj(String value) {
        this.celulaCnpj = value;
    }

    /**
     * Obtém o valor da propriedade operacaoFinanceira.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOperacaoFinanceira() {
        return operacaoFinanceira;
    }

    /**
     * Define o valor da propriedade operacaoFinanceira.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOperacaoFinanceira(String value) {
        this.operacaoFinanceira = value;
    }

    /**
     * Obtém o valor da propriedade tipoCusta.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTipoCusta() {
        return tipoCusta;
    }

    /**
     * Define o valor da propriedade tipoCusta.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTipoCusta(String value) {
        this.tipoCusta = value;
    }

    /**
     * Obtém o valor da propriedade cancelada.
     * 
     */
    public boolean isCancelada() {
        return cancelada;
    }

    /**
     * Define o valor da propriedade cancelada.
     * 
     */
    public void setCancelada(boolean value) {
        this.cancelada = value;
    }

}
