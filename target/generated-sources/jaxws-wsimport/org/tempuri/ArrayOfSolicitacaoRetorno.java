
package org.tempuri;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java de ArrayOfSolicitacaoRetorno complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="ArrayOfSolicitacaoRetorno">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="SolicitacaoRetorno" type="{http://tempuri.org/}SolicitacaoRetorno" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ArrayOfSolicitacaoRetorno", propOrder = {
    "solicitacaoRetorno"
})
public class ArrayOfSolicitacaoRetorno {

    @XmlElement(name = "SolicitacaoRetorno", nillable = true)
    protected List<SolicitacaoRetorno> solicitacaoRetorno;

    /**
     * Gets the value of the solicitacaoRetorno property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the solicitacaoRetorno property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSolicitacaoRetorno().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SolicitacaoRetorno }
     * 
     * 
     */
    public List<SolicitacaoRetorno> getSolicitacaoRetorno() {
        if (solicitacaoRetorno == null) {
            solicitacaoRetorno = new ArrayList<SolicitacaoRetorno>();
        }
        return this.solicitacaoRetorno;
    }

}
