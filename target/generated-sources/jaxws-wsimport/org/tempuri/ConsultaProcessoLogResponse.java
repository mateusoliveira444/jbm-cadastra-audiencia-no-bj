
package org.tempuri;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java de anonymous complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ConsultaProcessoLogResult" type="{http://tempuri.org/}ArrayOfRetornoProcesso" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "consultaProcessoLogResult"
})
@XmlRootElement(name = "ConsultaProcessoLogResponse")
public class ConsultaProcessoLogResponse {

    @XmlElement(name = "ConsultaProcessoLogResult")
    protected ArrayOfRetornoProcesso consultaProcessoLogResult;

    /**
     * Obtém o valor da propriedade consultaProcessoLogResult.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfRetornoProcesso }
     *     
     */
    public ArrayOfRetornoProcesso getConsultaProcessoLogResult() {
        return consultaProcessoLogResult;
    }

    /**
     * Define o valor da propriedade consultaProcessoLogResult.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfRetornoProcesso }
     *     
     */
    public void setConsultaProcessoLogResult(ArrayOfRetornoProcesso value) {
        this.consultaProcessoLogResult = value;
    }

}
