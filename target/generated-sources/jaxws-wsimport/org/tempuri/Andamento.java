
package org.tempuri;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Classe Java de Andamento complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="Andamento">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ProcessoUnico" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="IDAndamento" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="DataHora" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="DataCadastro" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="AndamentoTipo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ProvAdministrativa" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="Memo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Fase" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Advogado" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ControleCliente" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Andamento", propOrder = {
    "processoUnico",
    "idAndamento",
    "dataHora",
    "dataCadastro",
    "andamentoTipo",
    "provAdministrativa",
    "memo",
    "fase",
    "advogado",
    "controleCliente"
})
public class Andamento {

    @XmlElement(name = "ProcessoUnico")
    protected String processoUnico;
    @XmlElement(name = "IDAndamento")
    protected int idAndamento;
    @XmlElement(name = "DataHora", required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar dataHora;
    @XmlElement(name = "DataCadastro", required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar dataCadastro;
    @XmlElement(name = "AndamentoTipo")
    protected String andamentoTipo;
    @XmlElement(name = "ProvAdministrativa")
    protected int provAdministrativa;
    @XmlElement(name = "Memo")
    protected String memo;
    @XmlElement(name = "Fase")
    protected String fase;
    @XmlElement(name = "Advogado")
    protected String advogado;
    @XmlElement(name = "ControleCliente")
    protected String controleCliente;

    /**
     * Obtém o valor da propriedade processoUnico.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProcessoUnico() {
        return processoUnico;
    }

    /**
     * Define o valor da propriedade processoUnico.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProcessoUnico(String value) {
        this.processoUnico = value;
    }

    /**
     * Obtém o valor da propriedade idAndamento.
     * 
     */
    public int getIDAndamento() {
        return idAndamento;
    }

    /**
     * Define o valor da propriedade idAndamento.
     * 
     */
    public void setIDAndamento(int value) {
        this.idAndamento = value;
    }

    /**
     * Obtém o valor da propriedade dataHora.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDataHora() {
        return dataHora;
    }

    /**
     * Define o valor da propriedade dataHora.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDataHora(XMLGregorianCalendar value) {
        this.dataHora = value;
    }

    /**
     * Obtém o valor da propriedade dataCadastro.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDataCadastro() {
        return dataCadastro;
    }

    /**
     * Define o valor da propriedade dataCadastro.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDataCadastro(XMLGregorianCalendar value) {
        this.dataCadastro = value;
    }

    /**
     * Obtém o valor da propriedade andamentoTipo.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAndamentoTipo() {
        return andamentoTipo;
    }

    /**
     * Define o valor da propriedade andamentoTipo.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAndamentoTipo(String value) {
        this.andamentoTipo = value;
    }

    /**
     * Obtém o valor da propriedade provAdministrativa.
     * 
     */
    public int getProvAdministrativa() {
        return provAdministrativa;
    }

    /**
     * Define o valor da propriedade provAdministrativa.
     * 
     */
    public void setProvAdministrativa(int value) {
        this.provAdministrativa = value;
    }

    /**
     * Obtém o valor da propriedade memo.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMemo() {
        return memo;
    }

    /**
     * Define o valor da propriedade memo.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMemo(String value) {
        this.memo = value;
    }

    /**
     * Obtém o valor da propriedade fase.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFase() {
        return fase;
    }

    /**
     * Define o valor da propriedade fase.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFase(String value) {
        this.fase = value;
    }

    /**
     * Obtém o valor da propriedade advogado.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAdvogado() {
        return advogado;
    }

    /**
     * Define o valor da propriedade advogado.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAdvogado(String value) {
        this.advogado = value;
    }

    /**
     * Obtém o valor da propriedade controleCliente.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getControleCliente() {
        return controleCliente;
    }

    /**
     * Define o valor da propriedade controleCliente.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setControleCliente(String value) {
        this.controleCliente = value;
    }

}
