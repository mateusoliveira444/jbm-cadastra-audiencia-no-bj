
package org.tempuri;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java de anonymous complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ConsultaTipoAgendamentoResult" type="{http://tempuri.org/}ArrayOfTipoAgendamento" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "consultaTipoAgendamentoResult"
})
@XmlRootElement(name = "ConsultaTipoAgendamentoResponse")
public class ConsultaTipoAgendamentoResponse {

    @XmlElement(name = "ConsultaTipoAgendamentoResult")
    protected ArrayOfTipoAgendamento consultaTipoAgendamentoResult;

    /**
     * Obtém o valor da propriedade consultaTipoAgendamentoResult.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfTipoAgendamento }
     *     
     */
    public ArrayOfTipoAgendamento getConsultaTipoAgendamentoResult() {
        return consultaTipoAgendamentoResult;
    }

    /**
     * Define o valor da propriedade consultaTipoAgendamentoResult.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfTipoAgendamento }
     *     
     */
    public void setConsultaTipoAgendamentoResult(ArrayOfTipoAgendamento value) {
        this.consultaTipoAgendamentoResult = value;
    }

}
