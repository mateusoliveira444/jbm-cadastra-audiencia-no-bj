
package org.tempuri;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java de ArrayOfCustaArquivo complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="ArrayOfCustaArquivo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="CustaArquivo" type="{http://tempuri.org/}CustaArquivo" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ArrayOfCustaArquivo", propOrder = {
    "custaArquivo"
})
public class ArrayOfCustaArquivo {

    @XmlElement(name = "CustaArquivo", nillable = true)
    protected List<CustaArquivo> custaArquivo;

    /**
     * Gets the value of the custaArquivo property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the custaArquivo property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getCustaArquivo().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link CustaArquivo }
     * 
     * 
     */
    public List<CustaArquivo> getCustaArquivo() {
        if (custaArquivo == null) {
            custaArquivo = new ArrayList<CustaArquivo>();
        }
        return this.custaArquivo;
    }

}
