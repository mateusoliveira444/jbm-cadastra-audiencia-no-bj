
package org.tempuri;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java de Andamentos complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="Andamentos">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="TipoAndamentos" type="{http://tempuri.org/}ArrayOfTipoAndamentoMov" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Andamentos", propOrder = {
    "tipoAndamentos"
})
public class Andamentos {

    @XmlElement(name = "TipoAndamentos")
    protected ArrayOfTipoAndamentoMov tipoAndamentos;

    /**
     * Obtém o valor da propriedade tipoAndamentos.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfTipoAndamentoMov }
     *     
     */
    public ArrayOfTipoAndamentoMov getTipoAndamentos() {
        return tipoAndamentos;
    }

    /**
     * Define o valor da propriedade tipoAndamentos.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfTipoAndamentoMov }
     *     
     */
    public void setTipoAndamentos(ArrayOfTipoAndamentoMov value) {
        this.tipoAndamentos = value;
    }

}
