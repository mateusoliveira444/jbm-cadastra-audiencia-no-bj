
package org.tempuri;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Classe Java de StepSolicitacao complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="StepSolicitacao">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="IDStep" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="Descricao" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Sequencia" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="Situacao" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DataRealizado" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="RealizadoPor" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Observacao" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "StepSolicitacao", propOrder = {
    "idStep",
    "descricao",
    "sequencia",
    "situacao",
    "dataRealizado",
    "realizadoPor",
    "observacao"
})
public class StepSolicitacao {

    @XmlElement(name = "IDStep")
    protected int idStep;
    @XmlElement(name = "Descricao")
    protected String descricao;
    @XmlElement(name = "Sequencia")
    protected int sequencia;
    @XmlElement(name = "Situacao")
    protected String situacao;
    @XmlElement(name = "DataRealizado", required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar dataRealizado;
    @XmlElement(name = "RealizadoPor")
    protected String realizadoPor;
    @XmlElement(name = "Observacao")
    protected String observacao;

    /**
     * Obtém o valor da propriedade idStep.
     * 
     */
    public int getIDStep() {
        return idStep;
    }

    /**
     * Define o valor da propriedade idStep.
     * 
     */
    public void setIDStep(int value) {
        this.idStep = value;
    }

    /**
     * Obtém o valor da propriedade descricao.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescricao() {
        return descricao;
    }

    /**
     * Define o valor da propriedade descricao.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescricao(String value) {
        this.descricao = value;
    }

    /**
     * Obtém o valor da propriedade sequencia.
     * 
     */
    public int getSequencia() {
        return sequencia;
    }

    /**
     * Define o valor da propriedade sequencia.
     * 
     */
    public void setSequencia(int value) {
        this.sequencia = value;
    }

    /**
     * Obtém o valor da propriedade situacao.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSituacao() {
        return situacao;
    }

    /**
     * Define o valor da propriedade situacao.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSituacao(String value) {
        this.situacao = value;
    }

    /**
     * Obtém o valor da propriedade dataRealizado.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDataRealizado() {
        return dataRealizado;
    }

    /**
     * Define o valor da propriedade dataRealizado.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDataRealizado(XMLGregorianCalendar value) {
        this.dataRealizado = value;
    }

    /**
     * Obtém o valor da propriedade realizadoPor.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRealizadoPor() {
        return realizadoPor;
    }

    /**
     * Define o valor da propriedade realizadoPor.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRealizadoPor(String value) {
        this.realizadoPor = value;
    }

    /**
     * Obtém o valor da propriedade observacao.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getObservacao() {
        return observacao;
    }

    /**
     * Define o valor da propriedade observacao.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setObservacao(String value) {
        this.observacao = value;
    }

}
