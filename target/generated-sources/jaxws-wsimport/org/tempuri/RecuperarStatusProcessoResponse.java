
package org.tempuri;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java de anonymous complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="RecuperarStatusProcessoResult" type="{http://tempuri.org/}ArrayOfStatusProcesso" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "recuperarStatusProcessoResult"
})
@XmlRootElement(name = "RecuperarStatusProcessoResponse")
public class RecuperarStatusProcessoResponse {

    @XmlElement(name = "RecuperarStatusProcessoResult")
    protected ArrayOfStatusProcesso recuperarStatusProcessoResult;

    /**
     * Obtém o valor da propriedade recuperarStatusProcessoResult.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfStatusProcesso }
     *     
     */
    public ArrayOfStatusProcesso getRecuperarStatusProcessoResult() {
        return recuperarStatusProcessoResult;
    }

    /**
     * Define o valor da propriedade recuperarStatusProcessoResult.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfStatusProcesso }
     *     
     */
    public void setRecuperarStatusProcessoResult(ArrayOfStatusProcesso value) {
        this.recuperarStatusProcessoResult = value;
    }

}
