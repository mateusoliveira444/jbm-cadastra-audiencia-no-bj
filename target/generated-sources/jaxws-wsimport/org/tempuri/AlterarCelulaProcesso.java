
package org.tempuri;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java de anonymous complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="lstrUsuarioAtual" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="lstrSenhaAtual" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="lstrUsuarioNovo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="lstrSenhaNovo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="lstrProcessoUnico" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="lstrOperacional" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "lstrUsuarioAtual",
    "lstrSenhaAtual",
    "lstrUsuarioNovo",
    "lstrSenhaNovo",
    "lstrProcessoUnico",
    "lstrOperacional"
})
@XmlRootElement(name = "AlterarCelulaProcesso")
public class AlterarCelulaProcesso {

    protected String lstrUsuarioAtual;
    protected String lstrSenhaAtual;
    protected String lstrUsuarioNovo;
    protected String lstrSenhaNovo;
    protected String lstrProcessoUnico;
    protected String lstrOperacional;

    /**
     * Obtém o valor da propriedade lstrUsuarioAtual.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLstrUsuarioAtual() {
        return lstrUsuarioAtual;
    }

    /**
     * Define o valor da propriedade lstrUsuarioAtual.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLstrUsuarioAtual(String value) {
        this.lstrUsuarioAtual = value;
    }

    /**
     * Obtém o valor da propriedade lstrSenhaAtual.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLstrSenhaAtual() {
        return lstrSenhaAtual;
    }

    /**
     * Define o valor da propriedade lstrSenhaAtual.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLstrSenhaAtual(String value) {
        this.lstrSenhaAtual = value;
    }

    /**
     * Obtém o valor da propriedade lstrUsuarioNovo.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLstrUsuarioNovo() {
        return lstrUsuarioNovo;
    }

    /**
     * Define o valor da propriedade lstrUsuarioNovo.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLstrUsuarioNovo(String value) {
        this.lstrUsuarioNovo = value;
    }

    /**
     * Obtém o valor da propriedade lstrSenhaNovo.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLstrSenhaNovo() {
        return lstrSenhaNovo;
    }

    /**
     * Define o valor da propriedade lstrSenhaNovo.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLstrSenhaNovo(String value) {
        this.lstrSenhaNovo = value;
    }

    /**
     * Obtém o valor da propriedade lstrProcessoUnico.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLstrProcessoUnico() {
        return lstrProcessoUnico;
    }

    /**
     * Define o valor da propriedade lstrProcessoUnico.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLstrProcessoUnico(String value) {
        this.lstrProcessoUnico = value;
    }

    /**
     * Obtém o valor da propriedade lstrOperacional.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLstrOperacional() {
        return lstrOperacional;
    }

    /**
     * Define o valor da propriedade lstrOperacional.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLstrOperacional(String value) {
        this.lstrOperacional = value;
    }

}
