
package org.tempuri;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Classe Java de Arquivo complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="Arquivo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="IDProcessoUnico" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="IDSolicitacao" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="DataSolicitacao" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="IDArquivo" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="DataAnexo" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="TipoProfile" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="NomeArquivo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Url" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ArquivoFilialFinch" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="IDSistemaOrigem" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Arquivo", propOrder = {
    "idProcessoUnico",
    "idSolicitacao",
    "dataSolicitacao",
    "idArquivo",
    "dataAnexo",
    "tipoProfile",
    "nomeArquivo",
    "url",
    "arquivoFilialFinch",
    "idSistemaOrigem"
})
public class Arquivo {

    @XmlElement(name = "IDProcessoUnico")
    protected String idProcessoUnico;
    @XmlElement(name = "IDSolicitacao")
    protected int idSolicitacao;
    @XmlElement(name = "DataSolicitacao", required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar dataSolicitacao;
    @XmlElement(name = "IDArquivo")
    protected int idArquivo;
    @XmlElement(name = "DataAnexo", required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar dataAnexo;
    @XmlElement(name = "TipoProfile")
    protected String tipoProfile;
    @XmlElement(name = "NomeArquivo")
    protected String nomeArquivo;
    @XmlElement(name = "Url")
    protected String url;
    @XmlElement(name = "ArquivoFilialFinch")
    protected String arquivoFilialFinch;
    @XmlElement(name = "IDSistemaOrigem")
    protected int idSistemaOrigem;

    /**
     * Obtém o valor da propriedade idProcessoUnico.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIDProcessoUnico() {
        return idProcessoUnico;
    }

    /**
     * Define o valor da propriedade idProcessoUnico.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIDProcessoUnico(String value) {
        this.idProcessoUnico = value;
    }

    /**
     * Obtém o valor da propriedade idSolicitacao.
     * 
     */
    public int getIDSolicitacao() {
        return idSolicitacao;
    }

    /**
     * Define o valor da propriedade idSolicitacao.
     * 
     */
    public void setIDSolicitacao(int value) {
        this.idSolicitacao = value;
    }

    /**
     * Obtém o valor da propriedade dataSolicitacao.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDataSolicitacao() {
        return dataSolicitacao;
    }

    /**
     * Define o valor da propriedade dataSolicitacao.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDataSolicitacao(XMLGregorianCalendar value) {
        this.dataSolicitacao = value;
    }

    /**
     * Obtém o valor da propriedade idArquivo.
     * 
     */
    public int getIDArquivo() {
        return idArquivo;
    }

    /**
     * Define o valor da propriedade idArquivo.
     * 
     */
    public void setIDArquivo(int value) {
        this.idArquivo = value;
    }

    /**
     * Obtém o valor da propriedade dataAnexo.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDataAnexo() {
        return dataAnexo;
    }

    /**
     * Define o valor da propriedade dataAnexo.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDataAnexo(XMLGregorianCalendar value) {
        this.dataAnexo = value;
    }

    /**
     * Obtém o valor da propriedade tipoProfile.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTipoProfile() {
        return tipoProfile;
    }

    /**
     * Define o valor da propriedade tipoProfile.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTipoProfile(String value) {
        this.tipoProfile = value;
    }

    /**
     * Obtém o valor da propriedade nomeArquivo.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNomeArquivo() {
        return nomeArquivo;
    }

    /**
     * Define o valor da propriedade nomeArquivo.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNomeArquivo(String value) {
        this.nomeArquivo = value;
    }

    /**
     * Obtém o valor da propriedade url.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUrl() {
        return url;
    }

    /**
     * Define o valor da propriedade url.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUrl(String value) {
        this.url = value;
    }

    /**
     * Obtém o valor da propriedade arquivoFilialFinch.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getArquivoFilialFinch() {
        return arquivoFilialFinch;
    }

    /**
     * Define o valor da propriedade arquivoFilialFinch.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setArquivoFilialFinch(String value) {
        this.arquivoFilialFinch = value;
    }

    /**
     * Obtém o valor da propriedade idSistemaOrigem.
     * 
     */
    public int getIDSistemaOrigem() {
        return idSistemaOrigem;
    }

    /**
     * Define o valor da propriedade idSistemaOrigem.
     * 
     */
    public void setIDSistemaOrigem(int value) {
        this.idSistemaOrigem = value;
    }

}
