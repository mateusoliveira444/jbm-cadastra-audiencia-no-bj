
package org.tempuri;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java de anonymous complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="lstRemetenteEmail" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="lstDestinatarioEmail" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="lstrTituloEmail" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="lstrCorpoEmail" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="lstrValidaEnvio" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="lstrUsuario" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="lstrSenha" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "lstRemetenteEmail",
    "lstDestinatarioEmail",
    "lstrTituloEmail",
    "lstrCorpoEmail",
    "lstrValidaEnvio",
    "lstrUsuario",
    "lstrSenha"
})
@XmlRootElement(name = "EnviaEmail")
public class EnviaEmail {

    protected String lstRemetenteEmail;
    protected String lstDestinatarioEmail;
    protected String lstrTituloEmail;
    protected String lstrCorpoEmail;
    protected String lstrValidaEnvio;
    protected String lstrUsuario;
    protected String lstrSenha;

    /**
     * Obtém o valor da propriedade lstRemetenteEmail.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLstRemetenteEmail() {
        return lstRemetenteEmail;
    }

    /**
     * Define o valor da propriedade lstRemetenteEmail.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLstRemetenteEmail(String value) {
        this.lstRemetenteEmail = value;
    }

    /**
     * Obtém o valor da propriedade lstDestinatarioEmail.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLstDestinatarioEmail() {
        return lstDestinatarioEmail;
    }

    /**
     * Define o valor da propriedade lstDestinatarioEmail.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLstDestinatarioEmail(String value) {
        this.lstDestinatarioEmail = value;
    }

    /**
     * Obtém o valor da propriedade lstrTituloEmail.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLstrTituloEmail() {
        return lstrTituloEmail;
    }

    /**
     * Define o valor da propriedade lstrTituloEmail.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLstrTituloEmail(String value) {
        this.lstrTituloEmail = value;
    }

    /**
     * Obtém o valor da propriedade lstrCorpoEmail.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLstrCorpoEmail() {
        return lstrCorpoEmail;
    }

    /**
     * Define o valor da propriedade lstrCorpoEmail.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLstrCorpoEmail(String value) {
        this.lstrCorpoEmail = value;
    }

    /**
     * Obtém o valor da propriedade lstrValidaEnvio.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLstrValidaEnvio() {
        return lstrValidaEnvio;
    }

    /**
     * Define o valor da propriedade lstrValidaEnvio.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLstrValidaEnvio(String value) {
        this.lstrValidaEnvio = value;
    }

    /**
     * Obtém o valor da propriedade lstrUsuario.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLstrUsuario() {
        return lstrUsuario;
    }

    /**
     * Define o valor da propriedade lstrUsuario.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLstrUsuario(String value) {
        this.lstrUsuario = value;
    }

    /**
     * Obtém o valor da propriedade lstrSenha.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLstrSenha() {
        return lstrSenha;
    }

    /**
     * Define o valor da propriedade lstrSenha.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLstrSenha(String value) {
        this.lstrSenha = value;
    }

}
