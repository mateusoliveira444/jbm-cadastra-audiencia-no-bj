
package org.tempuri;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java de RetornoMetodos complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="RetornoMetodos">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Retorno" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="Mensagem" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="IDProcessoUnico" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="IDSolicitacao" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="IDAndamento" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="IdCusta" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="IdGarantia" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="ControleCliente" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="IdContato" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="IdLoteCusta" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="IdServico" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="IdAgendamento" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "RetornoMetodos", propOrder = {
    "retorno",
    "mensagem",
    "idProcessoUnico",
    "idSolicitacao",
    "idAndamento",
    "idCusta",
    "idGarantia",
    "controleCliente",
    "idContato",
    "idLoteCusta",
    "idServico",
    "idAgendamento"
})
public class RetornoMetodos {

    @XmlElement(name = "Retorno")
    protected boolean retorno;
    @XmlElement(name = "Mensagem")
    protected String mensagem;
    @XmlElement(name = "IDProcessoUnico")
    protected String idProcessoUnico;
    @XmlElement(name = "IDSolicitacao")
    protected int idSolicitacao;
    @XmlElement(name = "IDAndamento")
    protected int idAndamento;
    @XmlElement(name = "IdCusta")
    protected int idCusta;
    @XmlElement(name = "IdGarantia")
    protected int idGarantia;
    @XmlElement(name = "ControleCliente")
    protected String controleCliente;
    @XmlElement(name = "IdContato")
    protected int idContato;
    @XmlElement(name = "IdLoteCusta")
    protected int idLoteCusta;
    @XmlElement(name = "IdServico")
    protected int idServico;
    @XmlElement(name = "IdAgendamento")
    protected int idAgendamento;

    /**
     * Obtém o valor da propriedade retorno.
     * 
     */
    public boolean isRetorno() {
        return retorno;
    }

    /**
     * Define o valor da propriedade retorno.
     * 
     */
    public void setRetorno(boolean value) {
        this.retorno = value;
    }

    /**
     * Obtém o valor da propriedade mensagem.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMensagem() {
        return mensagem;
    }

    /**
     * Define o valor da propriedade mensagem.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMensagem(String value) {
        this.mensagem = value;
    }

    /**
     * Obtém o valor da propriedade idProcessoUnico.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIDProcessoUnico() {
        return idProcessoUnico;
    }

    /**
     * Define o valor da propriedade idProcessoUnico.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIDProcessoUnico(String value) {
        this.idProcessoUnico = value;
    }

    /**
     * Obtém o valor da propriedade idSolicitacao.
     * 
     */
    public int getIDSolicitacao() {
        return idSolicitacao;
    }

    /**
     * Define o valor da propriedade idSolicitacao.
     * 
     */
    public void setIDSolicitacao(int value) {
        this.idSolicitacao = value;
    }

    /**
     * Obtém o valor da propriedade idAndamento.
     * 
     */
    public int getIDAndamento() {
        return idAndamento;
    }

    /**
     * Define o valor da propriedade idAndamento.
     * 
     */
    public void setIDAndamento(int value) {
        this.idAndamento = value;
    }

    /**
     * Obtém o valor da propriedade idCusta.
     * 
     */
    public int getIdCusta() {
        return idCusta;
    }

    /**
     * Define o valor da propriedade idCusta.
     * 
     */
    public void setIdCusta(int value) {
        this.idCusta = value;
    }

    /**
     * Obtém o valor da propriedade idGarantia.
     * 
     */
    public int getIdGarantia() {
        return idGarantia;
    }

    /**
     * Define o valor da propriedade idGarantia.
     * 
     */
    public void setIdGarantia(int value) {
        this.idGarantia = value;
    }

    /**
     * Obtém o valor da propriedade controleCliente.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getControleCliente() {
        return controleCliente;
    }

    /**
     * Define o valor da propriedade controleCliente.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setControleCliente(String value) {
        this.controleCliente = value;
    }

    /**
     * Obtém o valor da propriedade idContato.
     * 
     */
    public int getIdContato() {
        return idContato;
    }

    /**
     * Define o valor da propriedade idContato.
     * 
     */
    public void setIdContato(int value) {
        this.idContato = value;
    }

    /**
     * Obtém o valor da propriedade idLoteCusta.
     * 
     */
    public int getIdLoteCusta() {
        return idLoteCusta;
    }

    /**
     * Define o valor da propriedade idLoteCusta.
     * 
     */
    public void setIdLoteCusta(int value) {
        this.idLoteCusta = value;
    }

    /**
     * Obtém o valor da propriedade idServico.
     * 
     */
    public int getIdServico() {
        return idServico;
    }

    /**
     * Define o valor da propriedade idServico.
     * 
     */
    public void setIdServico(int value) {
        this.idServico = value;
    }

    /**
     * Obtém o valor da propriedade idAgendamento.
     * 
     */
    public int getIdAgendamento() {
        return idAgendamento;
    }

    /**
     * Define o valor da propriedade idAgendamento.
     * 
     */
    public void setIdAgendamento(int value) {
        this.idAgendamento = value;
    }

}
