
package org.tempuri;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java de Parcela complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="Parcela">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="IdParcela" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="ValorParcela" type="{http://www.w3.org/2001/XMLSchema}float"/>
 *         &lt;element name="DataVencimento" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DataPagamento" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Pago" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Parcela", propOrder = {
    "idParcela",
    "valorParcela",
    "dataVencimento",
    "dataPagamento",
    "pago"
})
public class Parcela {

    @XmlElement(name = "IdParcela")
    protected int idParcela;
    @XmlElement(name = "ValorParcela")
    protected float valorParcela;
    @XmlElement(name = "DataVencimento")
    protected String dataVencimento;
    @XmlElement(name = "DataPagamento")
    protected String dataPagamento;
    @XmlElement(name = "Pago")
    protected boolean pago;

    /**
     * Obtém o valor da propriedade idParcela.
     * 
     */
    public int getIdParcela() {
        return idParcela;
    }

    /**
     * Define o valor da propriedade idParcela.
     * 
     */
    public void setIdParcela(int value) {
        this.idParcela = value;
    }

    /**
     * Obtém o valor da propriedade valorParcela.
     * 
     */
    public float getValorParcela() {
        return valorParcela;
    }

    /**
     * Define o valor da propriedade valorParcela.
     * 
     */
    public void setValorParcela(float value) {
        this.valorParcela = value;
    }

    /**
     * Obtém o valor da propriedade dataVencimento.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDataVencimento() {
        return dataVencimento;
    }

    /**
     * Define o valor da propriedade dataVencimento.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDataVencimento(String value) {
        this.dataVencimento = value;
    }

    /**
     * Obtém o valor da propriedade dataPagamento.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDataPagamento() {
        return dataPagamento;
    }

    /**
     * Define o valor da propriedade dataPagamento.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDataPagamento(String value) {
        this.dataPagamento = value;
    }

    /**
     * Obtém o valor da propriedade pago.
     * 
     */
    public boolean isPago() {
        return pago;
    }

    /**
     * Define o valor da propriedade pago.
     * 
     */
    public void setPago(boolean value) {
        this.pago = value;
    }

}
