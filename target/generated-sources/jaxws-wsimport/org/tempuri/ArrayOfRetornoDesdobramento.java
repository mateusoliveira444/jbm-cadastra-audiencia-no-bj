
package org.tempuri;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java de ArrayOfRetornoDesdobramento complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="ArrayOfRetornoDesdobramento">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="RetornoDesdobramento" type="{http://tempuri.org/}RetornoDesdobramento" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ArrayOfRetornoDesdobramento", propOrder = {
    "retornoDesdobramento"
})
public class ArrayOfRetornoDesdobramento {

    @XmlElement(name = "RetornoDesdobramento", nillable = true)
    protected List<RetornoDesdobramento> retornoDesdobramento;

    /**
     * Gets the value of the retornoDesdobramento property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the retornoDesdobramento property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRetornoDesdobramento().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link RetornoDesdobramento }
     * 
     * 
     */
    public List<RetornoDesdobramento> getRetornoDesdobramento() {
        if (retornoDesdobramento == null) {
            retornoDesdobramento = new ArrayList<RetornoDesdobramento>();
        }
        return this.retornoDesdobramento;
    }

}
