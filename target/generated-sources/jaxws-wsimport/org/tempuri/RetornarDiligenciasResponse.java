
package org.tempuri;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java de anonymous complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="RetornarDiligenciasResult" type="{http://tempuri.org/}ArrayOfResultadoRetornaDiligencias" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "retornarDiligenciasResult"
})
@XmlRootElement(name = "RetornarDiligenciasResponse")
public class RetornarDiligenciasResponse {

    @XmlElement(name = "RetornarDiligenciasResult")
    protected ArrayOfResultadoRetornaDiligencias retornarDiligenciasResult;

    /**
     * Obtém o valor da propriedade retornarDiligenciasResult.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfResultadoRetornaDiligencias }
     *     
     */
    public ArrayOfResultadoRetornaDiligencias getRetornarDiligenciasResult() {
        return retornarDiligenciasResult;
    }

    /**
     * Define o valor da propriedade retornarDiligenciasResult.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfResultadoRetornaDiligencias }
     *     
     */
    public void setRetornarDiligenciasResult(ArrayOfResultadoRetornaDiligencias value) {
        this.retornarDiligenciasResult = value;
    }

}
