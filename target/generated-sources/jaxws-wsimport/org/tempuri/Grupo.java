
package org.tempuri;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java de Grupo complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="Grupo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="NomeGrupo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Campos" type="{http://tempuri.org/}Campos" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="Arquivos" type="{http://tempuri.org/}Arquivos" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Grupo", propOrder = {
    "nomeGrupo",
    "campos",
    "arquivos"
})
public class Grupo {

    @XmlElement(name = "NomeGrupo")
    protected String nomeGrupo;
    @XmlElement(name = "Campos")
    protected List<Campos> campos;
    @XmlElement(name = "Arquivos")
    protected List<Arquivos> arquivos;

    /**
     * Obtém o valor da propriedade nomeGrupo.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNomeGrupo() {
        return nomeGrupo;
    }

    /**
     * Define o valor da propriedade nomeGrupo.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNomeGrupo(String value) {
        this.nomeGrupo = value;
    }

    /**
     * Gets the value of the campos property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the campos property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getCampos().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Campos }
     * 
     * 
     */
    public List<Campos> getCampos() {
        if (campos == null) {
            campos = new ArrayList<Campos>();
        }
        return this.campos;
    }

    /**
     * Gets the value of the arquivos property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the arquivos property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getArquivos().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Arquivos }
     * 
     * 
     */
    public List<Arquivos> getArquivos() {
        if (arquivos == null) {
            arquivos = new ArrayList<Arquivos>();
        }
        return this.arquivos;
    }

}
