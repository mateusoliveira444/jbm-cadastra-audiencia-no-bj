
package org.tempuri;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java de anonymous complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ConsultaAndamentoIDProcessoUnicoResult" type="{http://tempuri.org/}ArrayOfAndamento" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "consultaAndamentoIDProcessoUnicoResult"
})
@XmlRootElement(name = "ConsultaAndamentoIDProcessoUnicoResponse")
public class ConsultaAndamentoIDProcessoUnicoResponse {

    @XmlElement(name = "ConsultaAndamentoIDProcessoUnicoResult")
    protected ArrayOfAndamento consultaAndamentoIDProcessoUnicoResult;

    /**
     * Obtém o valor da propriedade consultaAndamentoIDProcessoUnicoResult.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfAndamento }
     *     
     */
    public ArrayOfAndamento getConsultaAndamentoIDProcessoUnicoResult() {
        return consultaAndamentoIDProcessoUnicoResult;
    }

    /**
     * Define o valor da propriedade consultaAndamentoIDProcessoUnicoResult.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfAndamento }
     *     
     */
    public void setConsultaAndamentoIDProcessoUnicoResult(ArrayOfAndamento value) {
        this.consultaAndamentoIDProcessoUnicoResult = value;
    }

}
