
package org.tempuri;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java de anonymous complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="RetornarContagemDiariaResult" type="{http://tempuri.org/}ArrayOfMovimento" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "retornarContagemDiariaResult"
})
@XmlRootElement(name = "RetornarContagemDiariaResponse")
public class RetornarContagemDiariaResponse {

    @XmlElement(name = "RetornarContagemDiariaResult")
    protected ArrayOfMovimento retornarContagemDiariaResult;

    /**
     * Obtém o valor da propriedade retornarContagemDiariaResult.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfMovimento }
     *     
     */
    public ArrayOfMovimento getRetornarContagemDiariaResult() {
        return retornarContagemDiariaResult;
    }

    /**
     * Define o valor da propriedade retornarContagemDiariaResult.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfMovimento }
     *     
     */
    public void setRetornarContagemDiariaResult(ArrayOfMovimento value) {
        this.retornarContagemDiariaResult = value;
    }

}
