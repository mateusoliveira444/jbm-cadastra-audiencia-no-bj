
package org.tempuri;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java de anonymous complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ConsultaLogCustaResult" type="{http://tempuri.org/}ArrayOfLog" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "consultaLogCustaResult"
})
@XmlRootElement(name = "ConsultaLogCustaResponse")
public class ConsultaLogCustaResponse {

    @XmlElement(name = "ConsultaLogCustaResult")
    protected ArrayOfLog consultaLogCustaResult;

    /**
     * Obtém o valor da propriedade consultaLogCustaResult.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfLog }
     *     
     */
    public ArrayOfLog getConsultaLogCustaResult() {
        return consultaLogCustaResult;
    }

    /**
     * Define o valor da propriedade consultaLogCustaResult.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfLog }
     *     
     */
    public void setConsultaLogCustaResult(ArrayOfLog value) {
        this.consultaLogCustaResult = value;
    }

}
