
package org.tempuri;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java de ArrayOfCelulaCnpj complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="ArrayOfCelulaCnpj">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="CelulaCnpj" type="{http://tempuri.org/}CelulaCnpj" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ArrayOfCelulaCnpj", propOrder = {
    "celulaCnpj"
})
public class ArrayOfCelulaCnpj {

    @XmlElement(name = "CelulaCnpj", nillable = true)
    protected List<CelulaCnpj> celulaCnpj;

    /**
     * Gets the value of the celulaCnpj property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the celulaCnpj property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getCelulaCnpj().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link CelulaCnpj }
     * 
     * 
     */
    public List<CelulaCnpj> getCelulaCnpj() {
        if (celulaCnpj == null) {
            celulaCnpj = new ArrayList<CelulaCnpj>();
        }
        return this.celulaCnpj;
    }

}
