
package org.tempuri;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java de anonymous complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="RecuperarTipoSolicitacaoResult" type="{http://tempuri.org/}ArrayOfTipoSolicitacao" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "recuperarTipoSolicitacaoResult"
})
@XmlRootElement(name = "RecuperarTipoSolicitacaoResponse")
public class RecuperarTipoSolicitacaoResponse {

    @XmlElement(name = "RecuperarTipoSolicitacaoResult")
    protected ArrayOfTipoSolicitacao recuperarTipoSolicitacaoResult;

    /**
     * Obtém o valor da propriedade recuperarTipoSolicitacaoResult.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfTipoSolicitacao }
     *     
     */
    public ArrayOfTipoSolicitacao getRecuperarTipoSolicitacaoResult() {
        return recuperarTipoSolicitacaoResult;
    }

    /**
     * Define o valor da propriedade recuperarTipoSolicitacaoResult.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfTipoSolicitacao }
     *     
     */
    public void setRecuperarTipoSolicitacaoResult(ArrayOfTipoSolicitacao value) {
        this.recuperarTipoSolicitacaoResult = value;
    }

}
