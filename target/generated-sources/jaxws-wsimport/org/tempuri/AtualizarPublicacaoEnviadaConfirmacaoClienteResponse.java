
package org.tempuri;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java de anonymous complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="AtualizarPublicacaoEnviadaConfirmacao_ClienteResult" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "atualizarPublicacaoEnviadaConfirmacaoClienteResult"
})
@XmlRootElement(name = "AtualizarPublicacaoEnviadaConfirmacao_ClienteResponse")
public class AtualizarPublicacaoEnviadaConfirmacaoClienteResponse {

    @XmlElement(name = "AtualizarPublicacaoEnviadaConfirmacao_ClienteResult")
    protected String atualizarPublicacaoEnviadaConfirmacaoClienteResult;

    /**
     * Obtém o valor da propriedade atualizarPublicacaoEnviadaConfirmacaoClienteResult.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAtualizarPublicacaoEnviadaConfirmacaoClienteResult() {
        return atualizarPublicacaoEnviadaConfirmacaoClienteResult;
    }

    /**
     * Define o valor da propriedade atualizarPublicacaoEnviadaConfirmacaoClienteResult.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAtualizarPublicacaoEnviadaConfirmacaoClienteResult(String value) {
        this.atualizarPublicacaoEnviadaConfirmacaoClienteResult = value;
    }

}
