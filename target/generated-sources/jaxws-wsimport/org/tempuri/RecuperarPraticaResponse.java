
package org.tempuri;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java de anonymous complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="RecuperarPraticaResult" type="{http://tempuri.org/}ArrayOfPratica" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "recuperarPraticaResult"
})
@XmlRootElement(name = "RecuperarPraticaResponse")
public class RecuperarPraticaResponse {

    @XmlElement(name = "RecuperarPraticaResult")
    protected ArrayOfPratica recuperarPraticaResult;

    /**
     * Obtém o valor da propriedade recuperarPraticaResult.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfPratica }
     *     
     */
    public ArrayOfPratica getRecuperarPraticaResult() {
        return recuperarPraticaResult;
    }

    /**
     * Define o valor da propriedade recuperarPraticaResult.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfPratica }
     *     
     */
    public void setRecuperarPraticaResult(ArrayOfPratica value) {
        this.recuperarPraticaResult = value;
    }

}
