
package org.tempuri;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java de anonymous complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ConsultaSolicitacaoProcessoResult" type="{http://tempuri.org/}ArrayOfSolicitacaoRetorno" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "consultaSolicitacaoProcessoResult"
})
@XmlRootElement(name = "ConsultaSolicitacaoProcessoResponse")
public class ConsultaSolicitacaoProcessoResponse {

    @XmlElement(name = "ConsultaSolicitacaoProcessoResult")
    protected ArrayOfSolicitacaoRetorno consultaSolicitacaoProcessoResult;

    /**
     * Obtém o valor da propriedade consultaSolicitacaoProcessoResult.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfSolicitacaoRetorno }
     *     
     */
    public ArrayOfSolicitacaoRetorno getConsultaSolicitacaoProcessoResult() {
        return consultaSolicitacaoProcessoResult;
    }

    /**
     * Define o valor da propriedade consultaSolicitacaoProcessoResult.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfSolicitacaoRetorno }
     *     
     */
    public void setConsultaSolicitacaoProcessoResult(ArrayOfSolicitacaoRetorno value) {
        this.consultaSolicitacaoProcessoResult = value;
    }

}
