
package org.tempuri;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java de ArrayOfResultadoRetornoAgendamento complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="ArrayOfResultadoRetornoAgendamento">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ResultadoRetornoAgendamento" type="{http://tempuri.org/}ResultadoRetornoAgendamento" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ArrayOfResultadoRetornoAgendamento", propOrder = {
    "resultadoRetornoAgendamento"
})
public class ArrayOfResultadoRetornoAgendamento {

    @XmlElement(name = "ResultadoRetornoAgendamento", nillable = true)
    protected List<ResultadoRetornoAgendamento> resultadoRetornoAgendamento;

    /**
     * Gets the value of the resultadoRetornoAgendamento property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the resultadoRetornoAgendamento property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getResultadoRetornoAgendamento().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ResultadoRetornoAgendamento }
     * 
     * 
     */
    public List<ResultadoRetornoAgendamento> getResultadoRetornoAgendamento() {
        if (resultadoRetornoAgendamento == null) {
            resultadoRetornoAgendamento = new ArrayList<ResultadoRetornoAgendamento>();
        }
        return this.resultadoRetornoAgendamento;
    }

}
