
package org.tempuri;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java de anonymous complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ConsultaDadoComplementarDisponivelResult" type="{http://tempuri.org/}ArrayOfRetornoDadoComplementar" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "consultaDadoComplementarDisponivelResult"
})
@XmlRootElement(name = "ConsultaDadoComplementarDisponivelResponse")
public class ConsultaDadoComplementarDisponivelResponse {

    @XmlElement(name = "ConsultaDadoComplementarDisponivelResult")
    protected ArrayOfRetornoDadoComplementar consultaDadoComplementarDisponivelResult;

    /**
     * Obtém o valor da propriedade consultaDadoComplementarDisponivelResult.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfRetornoDadoComplementar }
     *     
     */
    public ArrayOfRetornoDadoComplementar getConsultaDadoComplementarDisponivelResult() {
        return consultaDadoComplementarDisponivelResult;
    }

    /**
     * Define o valor da propriedade consultaDadoComplementarDisponivelResult.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfRetornoDadoComplementar }
     *     
     */
    public void setConsultaDadoComplementarDisponivelResult(ArrayOfRetornoDadoComplementar value) {
        this.consultaDadoComplementarDisponivelResult = value;
    }

}
