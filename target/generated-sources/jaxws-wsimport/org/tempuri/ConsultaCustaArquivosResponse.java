
package org.tempuri;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java de anonymous complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ConsultaCustaArquivosResult" type="{http://tempuri.org/}ArrayOfCustaArquivo" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "consultaCustaArquivosResult"
})
@XmlRootElement(name = "ConsultaCustaArquivosResponse")
public class ConsultaCustaArquivosResponse {

    @XmlElement(name = "ConsultaCustaArquivosResult")
    protected ArrayOfCustaArquivo consultaCustaArquivosResult;

    /**
     * Obtém o valor da propriedade consultaCustaArquivosResult.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfCustaArquivo }
     *     
     */
    public ArrayOfCustaArquivo getConsultaCustaArquivosResult() {
        return consultaCustaArquivosResult;
    }

    /**
     * Define o valor da propriedade consultaCustaArquivosResult.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfCustaArquivo }
     *     
     */
    public void setConsultaCustaArquivosResult(ArrayOfCustaArquivo value) {
        this.consultaCustaArquivosResult = value;
    }

}
