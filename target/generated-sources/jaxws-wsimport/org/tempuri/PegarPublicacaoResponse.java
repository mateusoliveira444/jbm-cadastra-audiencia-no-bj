
package org.tempuri;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java de anonymous complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="PegarPublicacaoResult" type="{http://tempuri.org/}RetornoMetodos" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "pegarPublicacaoResult"
})
@XmlRootElement(name = "PegarPublicacaoResponse")
public class PegarPublicacaoResponse {

    @XmlElement(name = "PegarPublicacaoResult")
    protected RetornoMetodos pegarPublicacaoResult;

    /**
     * Obtém o valor da propriedade pegarPublicacaoResult.
     * 
     * @return
     *     possible object is
     *     {@link RetornoMetodos }
     *     
     */
    public RetornoMetodos getPegarPublicacaoResult() {
        return pegarPublicacaoResult;
    }

    /**
     * Define o valor da propriedade pegarPublicacaoResult.
     * 
     * @param value
     *     allowed object is
     *     {@link RetornoMetodos }
     *     
     */
    public void setPegarPublicacaoResult(RetornoMetodos value) {
        this.pegarPublicacaoResult = value;
    }

}
