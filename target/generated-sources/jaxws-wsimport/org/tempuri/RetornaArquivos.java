
package org.tempuri;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java de anonymous complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="lstrIDProcessoUnico" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="lstrIDSolicitacao" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="lstrUsuario" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="lstrSenha" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="lstrArquivosAdicionais" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "lstrIDProcessoUnico",
    "lstrIDSolicitacao",
    "lstrUsuario",
    "lstrSenha",
    "lstrArquivosAdicionais"
})
@XmlRootElement(name = "RetornaArquivos")
public class RetornaArquivos {

    protected String lstrIDProcessoUnico;
    protected String lstrIDSolicitacao;
    protected String lstrUsuario;
    protected String lstrSenha;
    protected String lstrArquivosAdicionais;

    /**
     * Obtém o valor da propriedade lstrIDProcessoUnico.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLstrIDProcessoUnico() {
        return lstrIDProcessoUnico;
    }

    /**
     * Define o valor da propriedade lstrIDProcessoUnico.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLstrIDProcessoUnico(String value) {
        this.lstrIDProcessoUnico = value;
    }

    /**
     * Obtém o valor da propriedade lstrIDSolicitacao.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLstrIDSolicitacao() {
        return lstrIDSolicitacao;
    }

    /**
     * Define o valor da propriedade lstrIDSolicitacao.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLstrIDSolicitacao(String value) {
        this.lstrIDSolicitacao = value;
    }

    /**
     * Obtém o valor da propriedade lstrUsuario.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLstrUsuario() {
        return lstrUsuario;
    }

    /**
     * Define o valor da propriedade lstrUsuario.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLstrUsuario(String value) {
        this.lstrUsuario = value;
    }

    /**
     * Obtém o valor da propriedade lstrSenha.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLstrSenha() {
        return lstrSenha;
    }

    /**
     * Define o valor da propriedade lstrSenha.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLstrSenha(String value) {
        this.lstrSenha = value;
    }

    /**
     * Obtém o valor da propriedade lstrArquivosAdicionais.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLstrArquivosAdicionais() {
        return lstrArquivosAdicionais;
    }

    /**
     * Define o valor da propriedade lstrArquivosAdicionais.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLstrArquivosAdicionais(String value) {
        this.lstrArquivosAdicionais = value;
    }

}
