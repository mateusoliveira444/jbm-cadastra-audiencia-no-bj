package dao;

import bean.AcessoYank;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import java.util.concurrent.TimeUnit;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import org.json.JSONArray;
import org.json.JSONObject;

public class AcessoYankDao {

    private int idRobo = 0;
    private String sistema;
    private List<AcessoYank> acessoSistemas;
    private AcessoYank acessoYank;

    public AcessoYankDao(int idRobo, String sistema) {
        this.idRobo = idRobo;
        this.sistema = sistema;
        acessoSistemas = new ArrayList<>();
    }

    //PEGA URL, USUÁRIO E SENHA PARA O SISTEMA
    public List<AcessoYank> dadosAcesso() throws IOException {

        try {

            OkHttpClient.Builder builder = new OkHttpClient.Builder();
            builder.connectTimeout(5, TimeUnit.MINUTES)
                    .writeTimeout(5, TimeUnit.MINUTES)
                    .readTimeout(5, TimeUnit.MINUTES);
            OkHttpClient client = new OkHttpClient();
            client = builder.build();
            MediaType mediaType = MediaType.parse("text/plain");
            RequestBody body = RequestBody.create(mediaType, "SELECT robo_usuario, robo_senha, robo_sistema FROM robo_x_usuario WHERE robo_id = " + idRobo + " AND robo_sistema like '" + sistema + "%'");
            Request request = new Request.Builder()
                    .url("http://workflow.yanksolutions.com.br/ws/api/v1/yanksolutions/post/select")
                    .post(body)
                    .addHeader("username", "yank")
                    .addHeader("password", "Y@nk123")
                    .addHeader("content-type", "text/plain")
                    .addHeader("cache-control", "no-cache")
                    .build();

            Response response = null;

            response = client.newCall(request).execute();
            String json = response.body().string();
            JSONArray jsonList = null;
            try {
                jsonList = new JSONArray(json);
            } catch (Exception e) {
                JSONObject jErro = new JSONObject(json);
                System.out.println("Erro");
                System.out.println(jErro.get("mensagem").toString());
            }

            for (int i = 0; i < jsonList.length(); i++) {
                acessoYank = new AcessoYank();
                JSONObject jObject = jsonList.getJSONObject(i);
                acessoYank.setUsuario(jObject.get("robo_usuario").toString());
                acessoYank.setSenha(jObject.get("robo_senha").toString());
                acessoSistemas.add(acessoYank);
            }
        } catch (Exception e) {
            System.out.println(e);
        }

        return acessoSistemas;
    }
    

    public AcessoYank acessoSite() throws IOException {

        try {

            OkHttpClient.Builder builder = new OkHttpClient.Builder();
            builder.connectTimeout(5, TimeUnit.MINUTES)
                    .writeTimeout(5, TimeUnit.MINUTES)
                    .readTimeout(5, TimeUnit.MINUTES);
            OkHttpClient client = new OkHttpClient();
            client = builder.build();
            MediaType mediaType = MediaType.parse("text/plain");
            RequestBody body = RequestBody.create(mediaType, "SELECT robo_usuario, robo_senha, robo_sistema FROM robo_x_usuario WHERE robo_id = " + idRobo + " AND robo_sistema like '" + sistema + "%'");
            Request request = new Request.Builder()
                    .url("http://workflow.yanksolutions.com.br/ws/api/v1/yanksolutions/post/select")
                    .post(body)
                    .addHeader("username", "yank")
                    .addHeader("password", "Y@nk123")
                    .addHeader("content-type", "text/plain")
                    .addHeader("cache-control", "no-cache")
                    .build();

            Response response = null;

            response = client.newCall(request).execute();
            String json = response.body().string();
            JSONArray jsonList = null;
            try {
                jsonList = new JSONArray(json);
            } catch (Exception e) {
                JSONObject jErro = new JSONObject(json);
                System.out.println("Erro");
                System.out.println(jErro.get("mensagem").toString());
            }

            for (int i = 0; i < jsonList.length(); i++) {
                acessoYank = new AcessoYank();
                JSONObject jObject = jsonList.getJSONObject(i);
                acessoYank.setUsuario(jObject.get("robo_usuario").toString());
                acessoYank.setSenha(jObject.get("robo_senha").toString());
                acessoSistemas.add(acessoYank);
            }
        } catch (Exception e) {
            System.out.println(e);
        }

        return acessoYank;
    }

}
