package dao;

import bean.AudienciaBean;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.concurrent.TimeUnit;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import utils.AudienciaBj;

/**
 *
 * @author Mateus Oliveira
 */
public class BJdao {

    private static OkHttpClient client;
    private String tabela;
    private static Calendar dataAtual = new GregorianCalendar();
    private List<AudienciaBj> audiencia = new ArrayList<>();

    public static String dataHoraAtual() {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        dataAtual = new GregorianCalendar();
        Date agora = dataAtual.getTime();
        return sdf.format(agora);
    }

    public BJdao() {

        client = conexaoWebService();
        this.tabela = "yank_00002168";

    }

    private OkHttpClient conexaoWebService() {

        OkHttpClient.Builder builder = new OkHttpClient.Builder();
        builder.connectTimeout(5, TimeUnit.MINUTES)
                .writeTimeout(5, TimeUnit.MINUTES)
                .readTimeout(5, TimeUnit.MINUTES);

        OkHttpClient client = new OkHttpClient();
        client = builder
                .build();
        return client;
    }

    public void realizaUpdate(AudienciaBean cliente) throws JSONException {

        JSONObject jo = new JSONObject();
        jo.put("NUMEROCLIENTE", cliente.getNumeroCliente());
        jo.put("IDPROCESSO", cliente.getIdProcesso());
        jo.put("IDAGENDAMENTO", cliente.getIdAgendamento());
        jo.put("HORAINICIO", cliente.getHora());
        jo.put("DATAINICIO", cliente.getData());
        jo.put("GLOSA", cliente.getGlosa());
        jo.put("STATUS", cliente.getStatus());
        jo.put("TIPOAGENDAMENTO", cliente.getTipoAgendamento());
        jo.put("AUDIENCIANOBJ", cliente.getAudienciaNoBj());
        jo.put("INSTANCIA", 1);
        jo.put("STATUS_EXECUCAO", cliente.getStatus_exec());

        MediaType mediaType = MediaType.parse("application/json");
        RequestBody body = RequestBody.create(mediaType, jo.toString());
        Request request = new Request.Builder()
                .url("http://workflow.yanksolutions.com.br/ws/api/v1/yanksolutions/post/update/" + tabela + "/" + cliente.getId())
                .post(body)
                .addHeader("username", "yank")
                .addHeader("password", "Y@nk123")
                .addHeader("content-type", "application/json")
                .addHeader("cache-control", "no-cache")
                .build();

        Response response = null;
        String json = null;
        while (true) {
            try {
                response = client.newCall(request).execute();
                json = response.body().string();
                break;
            } catch (Exception e) {
                System.out.println("STATUS: ERRO AO REALIZAR UPDATE, VERIFICAR");
                e.printStackTrace();
            }
        }

        response.close();

        try {
            JSONObject jObjRetornoWS = new JSONObject(json);
            System.out.println(jObjRetornoWS.get("mensagem").toString());
        } catch (Exception e) {
            System.out.println("ERRO AO CAPTURAR RETORNO DO WS AO REALIZAR UPDATE, VERIFICAR");
            e.printStackTrace();
        }

    }

    //APÓS REALIZAR OS INSERTS NECESSÁRIOS, EXECUTA ESSE SELECT PARA SABER
    //QUAL FOI O ID GERADO PRO REGISTRO
    public void capturaUltimoId(AudienciaBean cliente) {

        try {

            MediaType mediaType = MediaType.parse("text/plain");

            String query = "SELECT max(id) FROM " + tabela;
            RequestBody body = RequestBody.create(mediaType, query);
            Request request = new Request.Builder()
                    .url("http://workflow.yanksolutions.com.br/ws/api/v1/yanksolutions/post/select")
                    .post(body)
                    .addHeader("username", "yank")
                    .addHeader("password", "Y@nk123")
                    .addHeader("content-type", "text/plain")
                    .addHeader("cache-control", "no-cache")
                    .build();

            Response response = null;
            while (true) {
                try {
                    response = client.newCall(request).execute();
                    break;
                } catch (Exception e) {
                    System.out.println(e);
                }
            }

            String json = response.body().string();
            response.close();

            JSONArray jArray = new JSONArray(json);

            JSONObject jObjRetorno = jArray.getJSONObject(0);
     //       System.out.println("ID CAPTURADO: " + jObjRetorno.get("max(id)").toString());

//            cliente.setId(Integer.parseInt(jObjRetorno.get("max(id)").toString()));
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("ERRO AO CAPTURAR ID GERADO");
        }

    }

    public void realizaInsert(AudienciaBean cliente) throws JSONException {

        JSONObject jo = null;

        jo = new JSONObject();

        JSONArray ja = new JSONArray();

        jo.put("NUMEROCLIENTE", cliente.getNumeroCliente());
        jo.put("IDPROCESSO", cliente.getIdProcesso());
        jo.put("IDAGENDAMENTO", cliente.getIdAgendamento());
        jo.put("HORAINICIO", cliente.getHora());
        jo.put("DATAINICIO", cliente.getData());
        jo.put("GLOSA", cliente.getGlosa());
        jo.put("STATUS", cliente.getStatus());
        jo.put("TIPOAGENDAMENTO", cliente.getTipoAgendamento());
        jo.put("AUDIENCIANOBJ", cliente.getAudienciaNoBj());
        jo.put("TIME_EXEC", "NOW()");
        jo.put("TIME_IMPORT", dataHoraAtual());
        jo.put("STATUS_EXECUCAO", "PENDENTE");
        jo.put("INSTANCIA", 1);
        jo.put("USERNAME", cliente.getUsername());
        jo.put("PASSWORD", cliente.getPassword());
        jo.put("DATACRIACAO", cliente.getDataCriacao());
        ja.put(jo);

        MediaType mediaType = MediaType.parse("application/json");
        RequestBody body = RequestBody.create(mediaType, ja.toString());
        Request request = new Request.Builder()
                .url("http://workflow.yanksolutions.com.br/ws/api/v1/yanksolutions/post/inserts/" + tabela)
                .post(body)
                .addHeader("username", "yank")
                .addHeader("password", "Y@nk123")
                .addHeader("content-type", "application/json")
                .addHeader("cache-control", "no-cache")
                .build();

        Response response = null;
        String json = null;
        while (true) {
            try {
                response = client.newCall(request).execute();
                json = response.body().string();
                break;
            } catch (Exception e) {
                System.out.println("ERRO AO CONECTAR COM BD YANK: " + e);
                e.printStackTrace();
            }
        }

        response.close();

        JSONObject jObjRetornoWS = null;
        try {
            jObjRetornoWS = new JSONObject(json);
            jObjRetornoWS.get("mensagem").toString();

        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("ERRO AO CAPTURAR RETORNO DO WS AO INSERT");
        }

        capturaUltimoId(cliente);

        if (jObjRetornoWS.getString("sucesso").equals("true")) {
            System.out.println("STATUS: DADOS INSERIDOS COM SUCESSO!");
        } else {
            System.out.println("RETORNO WS:" + jObjRetornoWS.get("mensagem").toString());
        }
    }

    public List<AudienciaBj> realizaSelect() {
        List<AudienciaBj> audiencias = new ArrayList<AudienciaBj>();
        AudienciaBj audiencia = null;
        try {

            MediaType mediaType = MediaType.parse("text/plain");

            String query = "SELECT NUMEROCLIENTE, IDPROCESSO, IDAGENDAMENTO, GLOSA, DATAINICIO, HORAINICIO, TIPOAGENDAMENTO, AUDIENCIANOBJ, PASSWORD, USERNAME, DATACRIACAO, id FROM " + tabela + " WHERE status = '0'";
            RequestBody body = RequestBody.create(mediaType, query);
            Request request = new Request.Builder()
                    .url("http://workflow.yanksolutions.com.br/ws/api/v1/yanksolutions/post/select")
                    .post(body)
                    .addHeader("username", "yank")
                    .addHeader("password", "Y@nk123")
                    .addHeader("content-type", "text/plain")
                    .addHeader("cache-control", "no-cache")
                    .build();

            Response response = null;

            response = client.newCall(request).execute();
            String json = response.body().string();
            System.out.println(json);
            response.close();

            JSONArray jsonList = null;
            try {
                jsonList = new JSONArray(json);
            } catch (Exception e) {
                JSONObject jErro = new JSONObject(json);
                System.out.println("Erro");
                System.out.println(jErro.get("mensagem").toString());
            }

            for (int i = 0; i < jsonList.length(); i++) {
                JSONObject jObject = jsonList.getJSONObject(i);
                audiencia = new AudienciaBj();
                audiencia.setHorario((jObject.get("HORAINICIO").toString()));
                audiencia.setData((jObject.get("DATAINICIO").toString()));
                audiencia.setIdAgendamento((jObject.get("IDAGENDAMENTO").toString()));
                audiencia.setGlosa((jObject.get("GLOSA").toString()));
                audiencia.setNumeroCliente((jObject.get("NUMEROCLIENTE").toString()));
                audiencia.setIdProcesso((jObject.get("IDPROCESSO").toString()));
                audiencia.setTipoAgendamento((jObject.get("TIPOAGENDAMENTO").toString()));
                audiencia.setAudienciaNoBj((jObject.get("AUDIENCIANOBJ").toString()));
                audiencia.setId((jObject.get("id").toString()));
                audiencia.setPassword((jObject.get("PASSWORD").toString()));
                audiencia.setUsername((jObject.get("USERNAME").toString()));
                audiencia.setDataCriacao((jObject.get("DATACRIACAO").toString()));
                audiencias.add(audiencia);
            }

        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("Erro ao capturar informações do caso");
        }
        return audiencias;
    }

    public boolean verificaExistenciaAgendamento(String IdAgendamento) {

        try {

            MediaType mediaType = MediaType.parse("text/plain");

            String query = "SELECT * FROM " + tabela + " WHERE IDAGENDAMENTO = " + IdAgendamento;
            RequestBody body = RequestBody.create(mediaType, query);
            Request request = new Request.Builder()
                    .url("http://workflow.yanksolutions.com.br/ws/api/v1/yanksolutions/post/select")
                    .post(body)
                    .addHeader("username", "yank")
                    .addHeader("password", "Y@nk123")
                    .addHeader("content-type", "text/plain")
                    .addHeader("cache-control", "no-cache")
                    .build();

            Response response = null;

            response = client.newCall(request).execute();
            String json = response.body().string();
            if (json.equals("[]\n")) {
                response.close();
                return false;
            }
            response.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return true;

    }
}
