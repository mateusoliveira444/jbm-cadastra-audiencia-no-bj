/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utils;

/**
 *
 * @author Mateus Oliveira
 */
public class AudienciaBj {

    private String data;
    private String horario;
    private String numeroCliente;
    private String idProcesso;
    private String idAgendamento;
    private String audienciaNoBj;
    private String tipoAgendamento;
    private String Glosa;
    private String id;
    private String dataCriacao;
    private String username;
    private String password;
    
    /**
     * @return the data
     */
    public String getData() {
        return data;
    }

    /**
     * @param data the data to set
     */
    public void setData(String data) {
        this.data = data;
    }

    /**
     * @return the horario
     */
    public String getHorario() {
        return horario;
    }

    /**
     * @param horario the horario to set
     */
    public void setHorario(String horario) {
        this.horario = horario;
    }

    /**
     * @return the numeroCliente
     */
    public String getNumeroCliente() {
        return numeroCliente;
    }

    /**
     * @param numeroCliente the numeroCliente to set
     */
    public void setNumeroCliente(String numeroCliente) {
        this.numeroCliente = numeroCliente;
    }

    /**
     * @return the idProcesso
     */
    public String getIdProcesso() {
        return idProcesso;
    }

    /**
     * @param idProcesso the idProcesso to set
     */
    public void setIdProcesso(String idProcesso) {
        this.idProcesso = idProcesso;
    }

    /**
     * @return the idAgendamento
     */
    public String getIdAgendamento() {
        return idAgendamento;
    }

    /**
     * @param idAgendamento the idAgendamento to set
     */
    public void setIdAgendamento(String idAgendamento) {
        this.idAgendamento = idAgendamento;
    }

    /**
     * @return the audienciaNoBj
     */
    public String getAudienciaNoBj() {
        return audienciaNoBj;
    }

    /**
     * @param audienciaNoBj the audienciaNoBj to set
     */
    public void setAudienciaNoBj(String audienciaNoBj) {
        this.audienciaNoBj = audienciaNoBj;
    }

    /**
     * @return the tipoAgendamento
     */
    public String getTipoAgendamento() {
        return tipoAgendamento;
    }

    /**
     * @param tipoAgendamento the tipoAgendamento to set
     */
    public void setTipoAgendamento(String tipoAgendamento) {
        this.tipoAgendamento = tipoAgendamento;
    }

    /**
     * @return the Glosa
     */
    public String getGlosa() {
        return Glosa;
    }

    /**
     * @param Glosa the Glosa to set
     */
    public void setGlosa(String Glosa) {
        this.Glosa = Glosa;
    }

    /**
     * @return the id
     */
    public String getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * @return the dataCriacao
     */
    public String getDataCriacao() {
        return dataCriacao;
    }

    /**
     * @param dataCriacao the dataCriacao to set
     */
    public void setDataCriacao(String dataCriacao) {
        this.dataCriacao = dataCriacao;
    }

    /**
     * @return the username
     */
    public String getUsername() {
        return username;
    }

    /**
     * @param username the username to set
     */
    public void setUsername(String username) {
        this.username = username;
    }

    /**
     * @return the paswword
     */
    public String getPaswword() {
        return password;
    }

    /**
     * @param paswword the paswword to set
     */
    public void setPassword(String paswword) {
        this.password = paswword;
    }
    
   

}
