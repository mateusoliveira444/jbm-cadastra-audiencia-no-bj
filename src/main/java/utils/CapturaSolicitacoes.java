/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utils;

import bean.AcessoYank;
import bean.AudienciaBean;
import dao.AcessoYankDao;
import dao.BJdao;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import javax.xml.datatype.DatatypeConfigurationException;
import org.tempuri.ArrayOfResultadoRetornaAgendamentoPorTipo;
import org.tempuri.ResultadoRetornaAgendamentoPorTipo;
import org.tempuri.RetornaAgendamentoPorTipo;
import org.tempuri.RetornoMetodos;
import org.tempuri.WSIntegracao;
import org.tempuri.WSIntegracaoSoap;

/**
 *
 * @author Mateus Oliveira
 */
public class CapturaSolicitacoes {

    private ArrayOfResultadoRetornaAgendamentoPorTipo solicitacoes;
    private GetDate data = new GetDate();
    private String dataInicioCriacao;
    private String dataFimCriacao;
    private AcessoYankDao acessoYankDao;
    private AudienciaBean audienciaBean;
    private BJdao audienciaDao;
    private String[] tipoAgendamento = {
        "AUDIÊNCIA UNA",
        "AUDIÊNCIA PROCON",
        "AUDIÊNCIA PRELIMINAR (ART. 334)",
        "AUDIÊNCIA PERICIAL (ART. 464, §3º)",
        "AUDIÊNCIA JUSTIFICAÇÃO PRÉVIA (ART. 300, §2º)",
        "AUDIÊNCIA DE SANEAMENTO (ART. 357, §3º)",
        "AUDIÊNCIA DE OITIVA DE TESTEMUNHA",
        "AUDIÊNCIA DE INSTRUÇÃO E JULGAMENTO",
        "AUDIÊNCIA DE CONCILIAÇÃO",
        "AUDIÊNCIA AVULSA",
        "AUDIENCIA - TRIBUNAL"
    };
    private String statusAgendamento = "";
    private String dataInicioAgendamento = "";
    private String dataFimAgendamento = "";
    private String statusSincronizado = "";
    private String statusConclusaoAgendamento = "";
    private String motivoConclusaoAgendamento = "";
    private String dataInicioConclusao = "";
    private String dataFimConclusao = "";
    private AudienciaBj audienciaBj;
    private List<AudienciaBj> audienciasBj;
    private List<AcessoYank> acessoSistemas;
    public CapturaSolicitacoes() throws DatatypeConfigurationException, IOException {
        acessoYankDao = new AcessoYankDao(141, "GRACCO");
        audienciaBean = new AudienciaBean();
        audienciaDao = new BJdao();
        solicitacoes = null;
        dataInicioCriacao = data.formatDayBefore();
//        dataFimCriacao = data.getUltimoDiaUtil();
//        dataInicioCriacao = data.getCurrentDay();
        dataFimCriacao = data.getCurrentDay();
        audienciasBj = new ArrayList<>();
        acessoSistemas = acessoYankDao.dadosAcesso();

        System.out.println("- - - - C A P T U R A N D O  D A D O S - - - -");
        for (AcessoYank sistema : acessoSistemas) {
            for (int contaSolicitacoes = 0; contaSolicitacoes <= 10; contaSolicitacoes++) {
                solicitacoes = retornarAgendamentoPorTipo(sistema.getUsuario(), sistema.getSenha(), tipoAgendamento[contaSolicitacoes], statusAgendamento, dataInicioAgendamento,
                        dataFimAgendamento, dataInicioConclusao, dataFimConclusao, statusSincronizado, statusConclusaoAgendamento,
                        motivoConclusaoAgendamento, dataInicioCriacao, dataFimCriacao);

                List<ResultadoRetornaAgendamentoPorTipo> audiencias = solicitacoes.getResultadoRetornaAgendamentoPorTipo();

                for (ResultadoRetornaAgendamentoPorTipo audiencia : audiencias) {

                    List<RetornaAgendamentoPorTipo> retornarAgendamentoPorTipo = audiencia.getRetornarAgendamentoPorTipo();

                    for (RetornaAgendamentoPorTipo camposAgendamentoPorTipo : retornarAgendamentoPorTipo) {
                        audienciaBj = new AudienciaBj();
                        audienciaBj.setNumeroCliente(camposAgendamentoPorTipo.getNumeroControleCliente());
                        audienciaBj.setData(camposAgendamentoPorTipo.getDataAgendamento());
                        audienciaBj.setHorario(camposAgendamentoPorTipo.getHoraAgendamento());
                        audienciaBj.setIdProcesso(camposAgendamentoPorTipo.getIdProcessoUnico());
                        audienciaBj.setIdAgendamento(camposAgendamentoPorTipo.getIdAgendamento());
                        audienciaBj.setTipoAgendamento(camposAgendamentoPorTipo.getTipoAgendamento());
                        System.out.println("");
                        System.out.println("AUDIENCIA CRIADA NO GRACCO EM :" + camposAgendamentoPorTipo.getDataCriacao());
                        System.out.println("DATA DE AGENDAMENTO" + camposAgendamentoPorTipo.getDataAgendamento());
                        audienciaBj.setDataCriacao(camposAgendamentoPorTipo.getDataCriacao());
                        switch (contaSolicitacoes) {

                            case 0:
                                audienciaBj.setAudienciaNoBj("SUMÁRIA - ACIJ");
                                break;

                            case 1:
                                audienciaBj.setAudienciaNoBj("CONCILIAÇÃO - AC");
                                break;

                            case 2:
                                audienciaBj.setAudienciaNoBj("MEDIAÇÃO");
                                break;

                            case 3:
                                audienciaBj.setAudienciaNoBj("CONCILIAÇÃO - AC");
                                break;

                            case 4:
                                audienciaBj.setAudienciaNoBj("CONCILIAÇÃO - AC");
                                break;

                            case 5:
                                audienciaBj.setAudienciaNoBj("CONCILIAÇÃO - AC");
                                break;

                            case 6:
                                audienciaBj.setAudienciaNoBj("OITIVA DE TESTEMUNHA - AIJ");
                                break;

                            case 7:
                                audienciaBj.setAudienciaNoBj("INSTRUÇÃO E JULGAMENTO - AIJ");
                                break;

                            case 8:
                                audienciaBj.setAudienciaNoBj("CONCILIAÇÃO - AC");
                                break;

                            case 9:
                                audienciaBj.setAudienciaNoBj("CONCILIAÇÃO - AC");
                                break;

                            case 10:
                                audienciaBj.setAudienciaNoBj("CONCILIAÇÃO - AC");
                                break;

                        }
                        try {
                            audienciaBean.setData(data.formataData(audienciaBj.getData()));
                            audienciaBean.setHora((audienciaBj.getHorario().substring(0, 5)));
                            audienciaBean.setNumeroCliente(audienciaBj.getNumeroCliente());
                            audienciaBean.setIdAgendamento(audienciaBj.getIdAgendamento());
                            audienciaBean.setIdProcesso(audienciaBj.getIdProcesso());
                            audienciaBean.setGlosa("0");
                            audienciaBean.setAudienciaNoBj(audienciaBj.getAudienciaNoBj());
                            audienciaBean.setTipoAgendamento(audienciaBj.getTipoAgendamento());
                            audienciaBean.setDataCriacao(audienciaBj.getDataCriacao());
                            audienciaBean.setUsername(sistema.getUsuario());
                            audienciaBean.setPassword(sistema.getSenha());
                            System.out.println("Adicionando audiência para cliente de n° : " + audienciaBean.getNumeroCliente());
                            System.out.println("Verificando se audiência ja existe no Banco...");
                            if (!audienciaDao.verificaExistenciaAgendamento(audienciaBean.getIdAgendamento())) {
                                audienciaDao.realizaInsert(audienciaBean);
                            } else {
                                System.out.println("Audiência ja existe no Banco");
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
            }

        }
    }
//    

    public static RetornoMetodos insereAgendamentoProcesso(String lstrIDProcessoUnico, String lstrMemo, String lstrTipoAgendamento,
            String lstrDataAgendamento, String lstrDataFinal, String lstrResponsavel, String lstrUsuario, String lstrSenha) {
        WSIntegracao service = new org.tempuri.WSIntegracao();
        WSIntegracaoSoap port = service.getWSIntegracaoSoap();
        return port.insereAgendamentoProcesso(lstrIDProcessoUnico, lstrMemo, lstrTipoAgendamento, lstrDataAgendamento, lstrDataFinal, lstrResponsavel, lstrUsuario, lstrSenha);
        
    }

    private static ArrayOfResultadoRetornaAgendamentoPorTipo retornarAgendamentoPorTipo(String lstrUsuario,
            String lstrSenha, String lstrTipoAgendamento, String lstrStatusAgendamento,
            String lstrDataInicioAgendamento, String lstrDataFimAgendamento,
            String lstrDataInicioConclusao, String lstrDataFimConclusao,
            String lstrStatusSincronizado, String lstrStatusConclusaoAgendamento,
            String lstrMotivoConclusaoAgendamento, String lstrDataInicioCriacao, String lstrDataFimCriacao) {

        WSIntegracao service = new org.tempuri.WSIntegracao();
        WSIntegracaoSoap port = service.getWSIntegracaoSoap();
        return port.retornarAgendamentoPorTipo(lstrUsuario, lstrSenha, lstrTipoAgendamento,
                lstrStatusAgendamento, lstrDataInicioAgendamento, lstrDataFimAgendamento,
                lstrDataInicioConclusao, lstrDataFimConclusao, lstrStatusSincronizado,
                lstrStatusConclusaoAgendamento, lstrMotivoConclusaoAgendamento, lstrDataInicioCriacao, lstrDataFimCriacao);

    }

    public List<AudienciaBj> getAudienciasBj() {
        return this.audienciasBj;

    }
}
