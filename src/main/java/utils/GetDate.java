/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utils;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

/**
 *
 * @author Mateus Oliveira
 */
public class GetDate {

    public GetDate() {

    }

    public String formatYear() {
        DateFormat dateFormatYear = new SimpleDateFormat("yyyy");
        Date date = new Date();
        String dateYear = dateFormatYear.format(date);
        return dateYear;
    }

    public String formatMonth() {
        DateFormat dateFormatMonth = new SimpleDateFormat("MM");
        Date date = new Date();
        String dateMonth = dateFormatMonth.format(date);
        return dateMonth;
    }

    public String formatDay() {
        DateFormat dateFormatDay = new SimpleDateFormat("dd");
        Date date = new Date();
        String dateDay = dateFormatDay.format(date);
        return dateDay;
    }

    public String formatDayBefore() {
        DateFormat dateFormatDay = new SimpleDateFormat("yyyy-MM-dd");
        String yesterday = dateFormatDay.format(yesterday());
        return yesterday;
    }

    public Date yesterday() {
        final Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DATE, -1);
        return cal.getTime();
    }

    public String getUltimoDiaUtil() {
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Date date = new Date();
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);

        int dayOfWeek;
        do {
            cal.add(Calendar.DAY_OF_MONTH, -1);
            dayOfWeek = cal.get(Calendar.DAY_OF_WEEK);
        } while (dayOfWeek == Calendar.SATURDAY || dayOfWeek == Calendar.SUNDAY);

        return dateFormat.format(cal.getTime());

    }

    public String getCurrentDay() {
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Date date = new Date();
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);

        return dateFormat.format(cal.getTime());

    }

    public String formataData(String dataGracco) {
        dataGracco = dataGracco.replace("-", "/");
        String[] formata = dataGracco.split("/");
        dataGracco = formata[2] + "/" + formata[1] + "/" + formata[0];
        return dataGracco;
    }

}
