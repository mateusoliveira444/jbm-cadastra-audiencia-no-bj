/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bean;

/**
 *
 * @author Mateus Oliveira
 */
public class AudienciaBean {

    private String numeroCliente;
    private String idProcesso;
    private String idAgendamento;
    private String audienciaNoBj;
    private String tipoAgendamento;
    private String id;
    private String status_exec;
    private String glosa;
    private String hora;
    private String data;
    private String dataCriacao;
    private String username;
    private String password;
    
    
    private int status;

    /**
     * @return the numeroCliente
     */
    public String getNumeroCliente() {
        return numeroCliente;
    }

    /**
     * @param numeroCliente the numeroCliente to set
     */
    public void setNumeroCliente(String numeroCliente) {
        this.numeroCliente = numeroCliente;
    }

    /**
     * @return the idProcesso
     */
    public String getIdProcesso() {
        return idProcesso;
    }

    /**
     * @param idProcesso the idProcesso to set
     */
    public void setIdProcesso(String idProcesso) {
        this.idProcesso = idProcesso;
    }

    /**
     * @return the idAgendamento
     */
    public String getIdAgendamento() {
        return idAgendamento;
    }

    /**
     * @param idAgendamento the idAgendamento to set
     */
    public void setIdAgendamento(String idAgendamento) {
        this.idAgendamento = idAgendamento;
    }

    /**
     * @return the audienciaNoBj
     */
    public String getAudienciaNoBj() {
        return audienciaNoBj;
    }

    /**
     * @param audienciaNoBj the audienciaNoBj to set
     */
    public void setAudienciaNoBj(String audienciaNoBj) {
        this.audienciaNoBj = audienciaNoBj;
    }

    /**
     * @return the tipoAgendamento
     */
    public String getTipoAgendamento() {
        return tipoAgendamento;
    }

    /**
     * @param tipoAgendamento the tipoAgendamento to set
     */
    public void setTipoAgendamento(String tipoAgendamento) {
        this.tipoAgendamento = tipoAgendamento;
    }

    /**
     * @return the id
     */
    public String getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * @return the status_exec
     */
    public String getStatus_exec() {
        return status_exec;
    }

    /**
     * @param status_exec the status_exec to set
     */
    public void setStatus_exec(String status_exec) {
        this.status_exec = status_exec;
    }

    /**
     * @return the status
     */
    public int getStatus() {
        return status;
    }

    /**
     * @param status the status to set
     */
    public void setStatus(int status) {
        this.status = status;
    }

    /**
     * @return the glosa
     */
    public String getGlosa() {
        return glosa;
    }

    /**
     * @param glosa the glosa to set
     */
    public void setGlosa(String glosa) {
        this.glosa = glosa;
    }

    /**
     * @return the hora
     */
    public String getHora() {
        return hora;
    }

    /**
     * @param hora the hora to set
     */
    public void setHora(String hora) {
        this.hora = hora;
    }

    /**
     * @return the data
     */
    public String getData() {
        return data;
    }

    /**
     * @param data the data to set
     */
    public void setData(String data) {
        this.data = data;
    }

    /**
     * @return the dataCriacao
     */
    public String getDataCriacao() {
        return dataCriacao;
    }

    /**
     * @param dataCriacao the dataCriacao to set
     */
    public void setDataCriacao(String dataCriacao) {
        this.dataCriacao = dataCriacao;
    }

    /**
     * @return the username
     */
    public String getUsername() {
        return username;
    }

    /**
     * @param username the username to set
     */
    public void setUsername(String username) {
        this.username = username;
    }

    /**
     * @return the password
     */
    public String getPassword() {
        return password;
    }

    /**
     * @param password the password to set
     */
    public void setPassword(String password) {
        this.password = password;
    }
}
