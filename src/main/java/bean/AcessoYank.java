
package bean;

public class AcessoYank {
    private String url;
    private String dadoExtra;
    private String usuario;
    private String senha;
    
    public AcessoYank(){
        
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getDadoExtra() {
        return dadoExtra;
    }

    public void setDadoExtra(String dadoExtra) {
        this.dadoExtra = dadoExtra;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getSenha() {
        return senha;
    }

    public void setSenha(String senha) {
        this.senha = senha;
    }

}
