/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package flow;

import bean.AcessoYank;
import bean.AudienciaBean;
import dao.AcessoYankDao;
import dao.BJdao;
import java.io.IOException;
import java.util.List;
import javax.xml.datatype.DatatypeConfigurationException;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import utils.AudienciaBj;
import utils.CapturaSolicitacoes;
import utils.GetDate;

/**
 *
 * @author Mateus Oliveira
 */
public class Fluxo {

    private WebDriver driver;
    private final List<AudienciaBj> audiencias;
    private Alert alerta;
    private String dataGracco;
    private String horaGracco;
    private int flagProxCliente;
    private GetDate date;
    private String dataGlosa;
    private final AudienciaBean audienciaBean;
    private final BJdao audienciaDao;
    private int flagAudienciaJaCadastrada;
    private final AcessoYank acessoYank;
    private AcessoYankDao acessoYankDao;
    private String alertText;

    public Fluxo() throws DatatypeConfigurationException, IOException {
        date = new GetDate(); //Método que captura datas
        dataGlosa = date.getCurrentDay(); //pega dia atual
        audienciaBean = new AudienciaBean(); //instancia 
        audienciaDao = new BJdao();
        audiencias = audienciaDao.realizaSelect();
        acessoYankDao = new AcessoYankDao(141, "SITE-ITAU-BJ");
        acessoYank = acessoYankDao.acessoSite();
    }

    public void fluxo() throws InterruptedException {

        DesiredCapabilities capabilities;
        capabilities = DesiredCapabilities.firefox();
        capabilities.setCapability("marionette", false);
        this.driver = new FirefoxDriver(capabilities);
        driver.get("https://ww39.itau.com.br/j146/PJURIDICO/"); // começa fluxo
        driver.findElement(By.id("username")).sendKeys(acessoYank.getUsuario());
        driver.findElement(By.id("password")).sendKeys(acessoYank.getSenha());
        driver.findElement(By.className("BotOk")).submit();

        for (AudienciaBj audiencia : audiencias) {

            audienciaBean.setId(audiencia.getId());
            System.out.println("- - - - - S T A T U S  C L I E N T E - - - - ");
            System.out.println("DATA    " + audiencia.getData());
            System.out.println("HORA    " + audiencia.getHorario());
            System.out.println("ID AGENDAMENTO    " + audiencia.getIdAgendamento());
            System.out.println("ID PROCESSO    " + audiencia.getIdProcesso());
            System.out.println("NUMERO CLIENTE     " + audiencia.getNumeroCliente());
            System.out.println("TIPO AGENDAMENTO   " + audiencia.getTipoAgendamento());
            System.out.println("AGENDAMENTO NO BJ   " + audiencia.getAudienciaNoBj());
            flagAudienciaJaCadastrada = 0;
            flagProxCliente = 0;
            System.out.println("- - - - C O M E Ç A  F L U X O - - - -");
            driver.findElement(By.id("pasta")).clear();
            driver.findElement(By.id("pasta")).sendKeys(audiencia.getNumeroCliente());
            driver.findElement(By.xpath("//*[contains(@src, 'Imagens/lupa.gif')]")).click();

            try {
                WebDriverWait wait = new WebDriverWait(driver, 2);
                wait.until(ExpectedConditions.alertIsPresent());
                alerta = driver.switchTo().alert();
                alertText = alerta.getText();
                if (alertText.equals("Pasta não encontrada ou usuário não possui acesso.")) {
                    alerta.accept();
                    CapturaSolicitacoes.insereAgendamentoProcesso(audiencia.getIdProcesso(), "Pasta não encontrada", "MENSAGEM DE GLOSA - CADASTRO DE AUDIÊNCIA",
                            dataGlosa, dataGlosa, "ESTEIRA - TUTELA", audiencia.getUsername(), audiencia.getPaswword());
                    audienciaBean.setGlosa("1");
                    audienciaBean.setStatus_exec("Agendamento de Glosa criado.");
                    audienciaBean.setStatus(1);
                    audienciaDao.realizaUpdate(audienciaBean);
                    flagProxCliente = 1;
                }
            } catch (Exception e) {
                System.out.println("NENHUM ALERT EXIBIDO AO PROCURAR PELA PASTA");
            }

            if (flagProxCliente == 1) {
                continue;
            }

            driver.switchTo().frame("framePrincipal");
            driver.findElement(By.xpath("//*[contains   (text(), 'Audiências')]")).click();
            Thread.sleep(2000);
            driver.switchTo().defaultContent();

            driver.switchTo().frame("framePrincipal");

            List<WebElement> bodys = driver.findElements(By.xpath("//*[@id=\"divDetalhe\"]/table/tbody"));
            if (bodys.size() >= 1) {
                WebElement body = driver.findElement(By.xpath("//*[@id=\"divDetalhe\"]/table/tbody"));
                List<WebElement> trs = body.findElements(By.tagName("tr"));
                System.out.println("- - - - V E R I F I C A  A U D I Ê N C I A - - - -");
                horaGracco = audiencia.getHorario();
                dataGracco = audiencia.getData();
                String concatenaDataeHoraGracco = dataGracco + " " + horaGracco;
                System.out.println("Data e hora Gracco: " + concatenaDataeHoraGracco);
                for (WebElement tr : trs) { //Captura data e horas das audiencias cadastradas no bj
                    Thread.sleep(1000);
                    List<WebElement> tds = tr.findElements(By.tagName("td"));
                    tds.get(2); //Coluna da data

                    System.out.println("Data e hora Bj: " + tds.get(2).getText());
                    if (tds.get(2).getText().equals(concatenaDataeHoraGracco)) {
                        flagAudienciaJaCadastrada = 1;
                        System.out.println("AUDIÊNCIA JA CADASTRADA, PRÓXIMO CLIENTE...");
                        break;
                    }
                }
                if (flagAudienciaJaCadastrada == 1) {
                    driver.switchTo().defaultContent();
                    audienciaBean.setStatus(1);
                    audienciaBean.setStatus_exec("Audiência já cadastrada");
                    audienciaDao.realizaUpdate(audienciaBean);
                    continue;
                }
            }
            System.out.println("- - - - C A D A S T R O  D A  A U D I Ê N C I A - - - - ");

            System.out.println("TIPO AGENDAMENTO   " + audiencia.getTipoAgendamento());
            System.out.println("AGENDAMENTO NO BJ   " + audiencia.getAudienciaNoBj());
            driver.navigate().to("https://ww39.itau.com.br/pjuridico/Modulos/Audiencia/Detalhe.aspx?acao=I&pasta=" + audiencia.getNumeroCliente());
            driver.findElement(By.id("ctl00_ctl00_cphc_corpo_txtData")).sendKeys(dataGracco);
            driver.findElement(By.id("ctl00_ctl00_cphc_corpo_txtHora")).sendKeys((horaGracco));
            Select dropdown = new Select(driver.findElement(By.id("ctl00_ctl00_cphc_corpo_ddlTipoAudiencia"))); //menu dropdown            
            dropdown.selectByVisibleText(audiencia.getAudienciaNoBj());
            driver.findElement(By.id("ctl00_ctl00_cphc_corpo_btnSalvar")).click();
            int flagUpdate = 0;
            try {
                alerta = driver.switchTo().alert();
                alertText = alerta.getText();
                if (alertText.equals("A data e hora informados estão no passado, deseja salvar os dados da audiência?")) {
                    alerta.dismiss();
                    audienciaBean.setStatus_exec("Audiência não cadastrada, pois esta já aconteceu");
                    System.out.println("AUDIÊNCIA NÃO CADASTRADA POIS JÁ ACONTECEU, PRÓXIMO CLIENTE...");
                    flagUpdate = 1;
                } else if (alertText.equals("Audiência cadastrada com sucesso.")) {
                    alerta.accept();
                    audienciaBean.setStatus_exec("Audiência cadastrada com sucesso");
                    System.out.println("AUDIÊNCIA CADASTRADA COM SUCESSO, PRÓXIMO CLIENTE...");
                    flagUpdate = 1;
                }
            } catch (Exception e) {
            }
            if (flagUpdate == 1) {
                audienciaBean.setStatus(1);
                audienciaDao.realizaUpdate(audienciaBean);
            }
            driver.navigate().to("https://ww39.itau.com.br/j146/PJURIDICO/");
            driver.switchTo().defaultContent();

        }
        driver.quit();
    }

}
