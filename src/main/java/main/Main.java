/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package main;

import flow.Fluxo;
import java.io.IOException;
import javax.xml.datatype.DatatypeConfigurationException;
import org.sikuli.script.FindFailed;
import utils.CapturaSolicitacoes;

/**
 *
 * @author Mateus Oliveira
 */
public class Main {

    public static void main(String[] args) throws IOException, InterruptedException, FindFailed, DatatypeConfigurationException {
        new CapturaSolicitacoes();
        Fluxo fluxo = new Fluxo();
        fluxo.fluxo();
    }

}
